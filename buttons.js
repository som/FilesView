/*
 * Copyright 2018 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2018 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 6 */

const Atk = imports.gi.Atk;
const Clutter = imports.gi.Clutter;
const Gio = imports.gi.Gio;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const St = imports.gi.St;

const Config = imports.misc.config;
const Dash = imports.ui.dash;
const ExtensionUtils = imports.misc.extensionUtils;
const IconGrid = imports.ui.iconGrid;
const Main = imports.ui.main;

const Me = ExtensionUtils.getCurrentExtension();
const Mang = Me.imports.mang;

const _ = imports.gettext.domain(Me.metadata['gettext-domain']).gettext;

const baseIconSizes = [ 16, 22, 24, 32, 48, 64 ];
const DASH_ANIMATION_TIME = 0.2; // old Dash.DASH_ANIMATION_TIME (GS < 3.34)
const GS_VERSION = Config.PACKAGE_VERSION;
const MainOverviewControls = GS_VERSION < '3.36.0' ? Main.overview._controls : Main.overview._overview._controls;
const UUID = Me.uuid.replace(/@/gi, '_at_').replace(/[^a-z0-9+_-]/gi, '_');

var PanelButton = new Lang.Class({
    Name: UUID + '-PanelIcon',
    
    // callback: on button clicked (toggleFilesPage)
    // actor: with which to synchronize 'checked' (filesPage)
    
    _init: function(callback, actor) {
        this.settings = Me.settings;
        
        if (!this.settings.get_boolean('panel-custom-button') || this.settings.get_string('panel-button-appearance') == 'icon')
            this.content = new St.Icon({ gicon: this._getGicon(),
                                         style_class: 'system-status-icon' });
        else
            this.content = new St.Label({ text: _("Files"),
                                          y_expand: true,
                                          y_align: Clutter.ActorAlign.CENTER });
        
        this.button = new St.Button({ name: 'toggle-files-view',
                                      style_class: 'panel-button',
                                      reactive: true,
                                      can_focus: true,
                                      track_hover: true,
                                      accessible_name: _("Show Files"),
                                      accessible_role: Atk.Role.TOGGLE_BUTTON,
                                      toggle_mode: actor ? true : false,
                                      child: this.content });
        
        this.button.connect('style-changed', Lang.bind(this, this._onStyleChanged));
        
        this.container = new St.Bin({ child: this.button });
        if (GS_VERSION < '3.35')
            this.container.x_fill = this.container.y_fill = true;
        
        let side = this.settings.get_string('panel-button');
        this.box = (side == 'left') ? Main.panel._leftBox :
                   (side == 'center') ? Main.panel._centerBox :
                   Main.panel._rightBox;
        
        this.box.insert_child_at_index(this.container, this.settings.get_int('panel-button-position'));
        
        if (callback)
            this.button.connect('clicked', callback);
        
        if (actor) {
            this.actorMappedHandler = actor.connect('notify::mapped', () => this.button.set_checked(actor.mapped));
            this.button.connect('destroy', () => actor.disconnect(this.actorMappedHandler));
        }
    },
    
    _onStyleChanged: function(actor) {
        let themeNode = actor.get_theme_node();

        let natHPadding = themeNode.get_length('-natural-hpadding');
        this.button.set_style('padding-left:' + natHPadding + 'px;padding-right:' + natHPadding + 'px');
    },
    
    disable: function() {
        this.box.remove_child(this.container);
        this.container.destroy();
    },
    
    updateButtonPosition: function() {
        this.box.set_child_at_index(this.container, this.settings.get_int('panel-button-position'));
    },
    
    _getGicon: function() {
        if (this.settings.get_boolean('panel-custom-button'))
            return new Gio.ThemedIcon({ name: this.settings.get_string('panel-icon-name') });
        else
            return new Gio.FileIcon({ file: Me.dir.get_child('data').get_child('icons').get_child('files-view.png') });
    },
    
    updateIcon: function() {
        if (this.content.gicon)
            this.content.set_gicon(this._getGicon());
    }
});

const getDash = function() {
    return GS_VERSION < '3.36.0' ? Main.overview._dash : Main.overview.dash;
}

const getActor = function(object) {
    return object instanceof Clutter.Actor ? object : object.actor;
};

// used by extension.js
var getShowFilesButton = function() {
    if (getDash().showFilesButton && getActor(getDash()).visible)
        return getDash().showFilesButton;
    else if (MainOverviewControls.dash.showFilesButton && getActor(MainOverviewControls.dash).visible)
        return MainOverviewControls.dash.showFilesButton;
    else
        return null;
};

var DashButton = new Lang.Class({
    Name: UUID + '-DashIcon',
    
    // callback: on button clicked (toggleFilesPage)
    // actor: with which to synchronize 'checked' (filesPage)
    
    _init: function(position, callback, actor) {
        let dash = getDash();
        // Dash-to-Panel appropriates Main.overview._dash but original dash is still available in MainOverviewControls.dash
        if (dash.constructor.name.toLowerCase().indexOf('dashtopanel') != -1)
            dash = MainOverviewControls.dash;
        this.dash = dash;
        
        this.dashActor = dash instanceof Clutter.Actor ? dash : dash.actor; // dashActor == dash for Dash-to-Dock since v67+
        this.dashIsDock = (dash.constructor.name.toLowerCase().indexOf('dashtodock') != -1);
        let dashIsVertical = dash._isHorizontal ? false : true; // _isHorizontal is specific to Dash-to-Dock
        if (!this.dashIsDock) {
            this._adjustIconSizeOld = _adjustIconSize;
            dash._adjustIconSize = _adjustIconSize;
        }
        this.showFilesIcon = new DashShowFilesIcon(callback, actor);
        
        //tooltip
        dash._hookUpLabel(this.showFilesIcon);
        
        this.showFilesIcon.childScale = 1;
        this.showFilesIcon.childOpacity = 255;
        this.showFilesIcon.show(false);
        this.showFilesIcon.icon.setIconSize(dash.iconSize);
        
        dash._showFilesIcon = this.showFilesIcon;
        dash.showFilesButton = this.showFilesIcon.toggleButton; // needed to animate filesPage
        dash._container.remove_actor(dash._showAppsIcon);
        this.boxLayout = new St.BoxLayout({ vertical: dashIsVertical, clip_to_allocation: true });
        
        this.boxLayout.add_actor(dash._showAppsIcon);
        // 'above' in the list of children of boxLayout is graphically 'below' (in vertical layout)
        if (position == 'above') {
            this.boxLayout.insert_child_below(this.showFilesIcon, dash._showAppsIcon);
        } else {
            this.boxLayout.insert_child_above(this.showFilesIcon, dash._showAppsIcon);
        }
        
        dash._container.add_actor(this.boxLayout);
        
        if (dashIsVertical) {
            this.showFilesIcon.set_x_align(Clutter.ActorAlign.CENTER);
            this.boxLayout.set_style('padding-top:6px;spacing:0px');
        } else {
            this.showFilesIcon.set_y_align(Clutter.ActorAlign.CENTER);
            this.boxLayout.set_style('padding-left:6px;spacing:0px');
        }
        
        if (this.dashIsDock && GS_VERSION < '3.36.0') {
            if (dashIsVertical)
                this.dashActor.set_width(this.boxLayout.get_width());
            else
                this.dashActor.set_height(this.boxLayout.get_height());
        }
        
        this.iconSizeChangedHandler = dash.connect('icon-size-changed', () => {
            this.showFilesIcon.icon.setIconSize(dash.iconSize);
            if (this.dashIsDock)
                this.iconSizeTimeout = Mainloop.timeout_add(400, () => {
                    if (dashIsVertical)
                        this.dashActor.set_width(this.boxLayout.get_width());
                    else
                        this.dashActor.set_height(this.boxLayout.get_height());
                    Mainloop.source_remove(this.iconSizeTimeout);
                    this.iconSizeTimeout = null;
                });
        });
    },
    
    changeButtonPosition: function(position) {
        if (!this.showFilesIcon.get_parent() || this.dash._showAppsIcon.get_parent() != this.showFilesIcon.get_parent())
            return;
        
        // 'above' in the list of children of boxLayout is graphically 'below' (in vertical layout)
        if (position == 'above') {
            this.boxLayout.set_child_below_sibling(this.showFilesIcon, this.dash._showAppsIcon);
        } else {
            this.boxLayout.set_child_above_sibling(this.showFilesIcon, this.dash._showAppsIcon);
        }
            
    },
    
    updateIcon: function() {
        this.showFilesIcon.updateIcon();
    },
    
    disable: function() {
        let dash = this.dash;
        if (this.iconSizeChangedHandler)
            dash.disconnect(this.iconSizeChangedHandler);
        if (this.iconSizeTimeout)
            Mainloop.source_remove(this.iconSizeTimeout);
        if (this._adjustIconSizeOld)
            dash._adjustIconSize = this._adjustIconSizeOld;
        this.boxLayout.remove_actor(dash._showAppsIcon);
        dash._container.remove_actor(this.boxLayout);
        dash._container.add_actor(dash._showAppsIcon);
        this.dashActor.set_width(-1);
        this.boxLayout.destroy();
        delete dash._showFilesIcon;
        delete dash.showFilesButton;
    }
});

const DashShowFilesIcon = new Mang.Class({
    Name: UUID + '-ShowFilesIcon',
    Extends: Dash.DashItemContainer,
    
    _init: function(callback, actor) {
        this.callParent('_init');
        this.settings = Me.settings;
        
        this.toggleButton = new St.Button({ style_class: 'show-apps',
                                            track_hover: true,
                                            can_focus: true,
                                            toggle_mode: actor ? true : false });
        this._iconActor = null;
        this.icon = new IconGrid.BaseIcon(_("Show Files"),
                                           { setSizeManually: true,
                                             showLabel: false,
                                             createIcon: Lang.bind(this, this._createIcon) });
        
        // GS 3.32- : this.icon.actor, GS 3.32+ this.icon
        this.toggleButton.add_actor(GS_VERSION < '3.32.0' ? this.icon.actor : this.icon);
        this.toggleButton._delegate = this;
        
        this.setChild(this.toggleButton);
        this.setLabelText(_("Show Files"));
        
        if (callback)
            this.toggleButton.connect('clicked', callback);
        
        if (actor) {
            this.actorMappedHandler = actor.connect('notify::mapped', () => this.toggleButton.set_checked(actor.mapped));
            this.connect('destroy', () => actor.disconnect(this.actorMappedHandler));
        }
    },
    
    _getGicon: function() {
        if (this.settings.get_boolean('dash-custom-button'))
            return new Gio.ThemedIcon({ name: this.settings.get_string('dash-icon-name') });
        else
            return new Gio.FileIcon({ file: Me.dir.get_child('data').get_child('icons').get_child('files-view.png') });
    },

    _createIcon: function(size) {
        this._iconActor = new St.Icon({ gicon: this._getGicon(),
                                        icon_size: size*0.80, // reduce size to visually equalize 'show-apps' icon size
                                        style_class: 'show-apps-icon',
                                        track_hover: true });
        return this._iconActor;
    },
    
    updateIcon: function() {
        this._iconActor.set_gicon(this._getGicon());
    },
    
    setDragApp: function(app) {
        // nothing;
    },

    handleDragOver: function(source, actor, x, y, time) {
        return 0; // 0 is DND.DragMotionResult.NO_DROP
    },

    acceptDrop: function(source, actor, x, y, time) {
        return false;
    }
});

// just add _showFilesIcon to iconChildren
const _adjustIconSize = function() {
    // For the icon size, we only consider children which are "proper"
    // icons (i.e. ignoring drag placeholders) and which are not
    // animating out (which means they will be destroyed at the end of
    // the animation)
    let iconChildren = this._box.get_children().filter(actor => {
        return actor.child &&
               actor.child._delegate &&
               actor.child._delegate.icon &&
               !actor.animatingOut;
    });

    iconChildren.push(this._showAppsIcon);
    if (this._showFilesIcon)
        iconChildren.push(this._showFilesIcon);

    if (this._maxHeight == -1)
        return;

    let themeNode = this._container.get_theme_node();
    let maxAllocation = new Clutter.ActorBox({ x1: 0, y1: 0,
                                               x2: 42 /* whatever */,
                                               y2: this._maxHeight });
    let maxContent = themeNode.get_content_box(maxAllocation);
    let availHeight = maxContent.y2 - maxContent.y1;
    let spacing = themeNode.get_length('spacing');

    let firstButton = iconChildren[0].child;
    let firstIcon = firstButton._delegate.icon;

    let minHeight, natHeight;
    let scaleFactor = St.ThemeContext.get_for_stage(global.stage).scale_factor;

    // Enforce the current icon size during the size request
    firstIcon.icon.ensure_style();
    let [currentWidth, currentHeight] = firstIcon.icon.get_size();
    firstIcon.icon.set_size(this.iconSize * scaleFactor, this.iconSize * scaleFactor);
    [minHeight, natHeight] = firstButton.get_preferred_height(-1);
    firstIcon.icon.set_size(currentWidth, currentHeight);

    // Subtract icon padding and box spacing from the available height
    availHeight -= iconChildren.length * (natHeight - this.iconSize * scaleFactor) +
                   (iconChildren.length - 1) * spacing;

    let availSize = availHeight / iconChildren.length;

    let iconSizes = baseIconSizes.map(s => s * scaleFactor);

    let newIconSize = baseIconSizes[0];
    for (let i = 0; i < iconSizes.length; i++) {
        if (iconSizes[i] < availSize)
            newIconSize = baseIconSizes[i];
    }

    if (newIconSize == this.iconSize)
        return;

    let oldIconSize = this.iconSize;
    this.iconSize = newIconSize;
    this.emit('icon-size-changed');

    let scale = oldIconSize / newIconSize;
    for (let i = 0; i < iconChildren.length; i++) {
        if (!iconChildren[i] || !iconChildren[i].child)
            continue;
        
        let icon = iconChildren[i].child._delegate.icon;

        // Set the new size immediately, to keep the icons' sizes
        // in sync with this.iconSize
        icon.setIconSize(this.iconSize);

        // Don't animate the icon size change when the overview
        // is transitioning, not visible or when initially filling
        // the dash
        if (!Main.overview.visible || Main.overview.animationInProgress ||
            !this._shownInitially)
            continue;

        let [targetWidth, targetHeight] = icon.icon.get_size();

        // Scale the icon's texture to the previous size and
        // tween to the new size
        icon.icon.set_size(icon.icon.width * scale,
                           icon.icon.height * scale);

        Mang.addTween(icon.icon,
                      { width: targetWidth,
                        height: targetHeight,
                        time: DASH_ANIMATION_TIME,
                        transition: 'easeOutQuad' });
    }
};

const OperationContainer = new Lang.Class({
    Name: UUID + '-OperationContainer',
    Extends: St.Widget,

    _init: function() {
        this.parent();
        
        let containerPrototype = Dash.DashItemContainer.prototype;
        Object.defineProperties(this, {
            'showLabel': Object.getOwnPropertyDescriptor(containerPrototype, "showLabel"),
            'setLabelText': Object.getOwnPropertyDescriptor(containerPrototype, "setLabelText"),
            'hideLabel': Object.getOwnPropertyDescriptor(containerPrototype, "hideLabel"),
            'setChild': Object.getOwnPropertyDescriptor(containerPrototype, "setChild")
        });

        this._labelText = "";
        this.label = new St.Label({ style_class: 'dash-label'});
        this.label.hide();
        Main.layoutManager.addChrome(this.label);

        this.child = null;
        this._childScale = 1.0;
        this._childOpacity = 255;
        this.animatingOut = false;

        this.connect('destroy', () => {
            Main.layoutManager.removeChrome(this.label);
            this.label.destroy();
        });
        
        this._showLabelTimeoutId = 0;
        this._resetHoverTimeoutId = 0;
        this._labelShowing = false;

        let dashPrototype = Dash.Dash.prototype;
        Object.defineProperties(this, {
            '_hookUpLabel': Object.getOwnPropertyDescriptor(dashPrototype, "_hookUpLabel"),
            '_syncLabel': Object.getOwnPropertyDescriptor(dashPrototype, "_syncLabel")
        });
    },
    
    // from GS 3.32
    set childScale(scale) {
        this._childScale = scale;

        this.set_scale(scale, scale);
        this.queue_relayout();
    },

    get childScale() {
        return this._childScale;
    },

    set childOpacity(opacity) {
        this._childOpacity = opacity;

        this.set_opacity(opacity);
        this.queue_redraw();
    },

    get childOpacity() {
        return this._childOpacity;
    },
    
});

const OperationButton = new Lang.Class({
    Name: UUID + '-OperationButton',
    Extends: OperationContainer,

    _init: function(iconName) {
        this.parent();
        
        this.icon = new St.Icon({ icon_name: iconName });
        
        this.button = new St.Button({ can_focus: true,
                                      x_expand: true,
                                      child: this.icon });
        this.button._delegate = this;
        this.setChild(this.button);
        
        this._hookUpLabel(this);
    }
});

var UndoButton = new Lang.Class({
    Name: UUID + '-UndoButton',
    Extends: OperationButton,

    _init: function(manager) {
        this.parent('edit-undo-symbolic');
        
        this.settings = Me.settings;
        
        this.manager = manager;
        this.setLabelText(this.manager.undoText);
        this.button.visible = this.manager.hasUndo;
        
        this.button.connect('clicked', () => {
            if (this.settings.get_boolean('file-manager'))
                this.manager.onUndoClicked();
        });
        
        this.managerWorkingHandler = this.manager.connect('working-changed', () => {
            if (this.manager.isWorking) {
                this.button.set_opacity(120);
                this.button.set_reactive(false);
                this.setLabelText("");
                this.label.set_text(this._labelText);
            } else {
                this.button.set_opacity(255);
                this.button.set_reactive(true);
                this.button.visible = this.manager.hasUndo;
                this.icon.set_icon_name(this.manager.undoIconName);
                this.setLabelText(this.manager.undoText);
                this.label.set_text(this._labelText);
            }
        });
        
        this.connect('destroy', () => {
            this.manager.disconnect(this.managerWorkingHandler);
        });
    }
});

var CancelButton = new Lang.Class({
    Name: UUID + '-CancelButton',
    Extends: OperationButton,

    _init: function(manager) {
        this.parent('process-stop-symbolic');
        
        this.settings = Me.settings;
        
        this.manager = manager;
        this.setLabelText(manager.cancelText);
        this.button.visible = false;
        
        this.button.connect('clicked', () => {
            if (this.settings.get_boolean('file-manager'))
                this.manager.onCancelClicked();
        });
        
        this.managerWorkingHandler = this.manager.connect('working-changed', () => {
            this.button.visible = this.manager.isWorking;
            if (!this.manager.isWorking)
                this._labelText = manager.cancelText;
        });
        
        this.operationsProgressedHandler = this.manager.connect('operations-progressed', (manager, text) => {
            this._labelText = text;
            this.label.set_text(this._labelText);
            this.label.get_clutter_text().set_use_markup(true);
        });
        
        this.connect('destroy', () => {
            this.manager.disconnect(this.managerWorkingHandler);
            this.manager.disconnect(this.operationsProgressedHandler);
        });
    }
});



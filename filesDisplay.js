/*
 * Copyright 2018 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2018 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 6 */

/*
 * This is based on GNOME Shell ui.appDisplay.js/part1
 * https://gitlab.gnome.org/GNOME/gnome-shell/blob/master/js/ui/appDisplay.js
 *
 * This whole file is mainly about the display
 * Each view has a separate manager (Gtk.RecentManager, FolderManger and FavoritesManager)
 */

const Clutter = imports.gi.Clutter;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Meta = imports.gi.Meta;
const Signals = imports.signals;
const St = imports.gi.St;

const Config = imports.misc.config;
const DND = imports.ui.dnd;
const ExtensionUtils = imports.misc.extensionUtils;
const Main = imports.ui.main;
const Params = imports.misc.params;

const Me = ExtensionUtils.getCurrentExtension();
const Buttons = Me.imports.buttons;
const Mang = Me.imports.mang;
const Misc = Me.imports.misc;
const FileIcon = Me.imports.fileIcon;
const FolderManager = Me.imports.folderManager;
const RecentManager = Me.imports.recentManager;

const _ = imports.gettext.domain(Me.metadata['gettext-domain']).gettext;
const NautilusGettext = imports.gettext.domain('nautilus');
const _N = NautilusGettext.gettext;
const _Nn = NautilusGettext.ngettext;

const MAX_FOLDER_VIEW_NAME_LENGTH = 18;
const MAX_FILES_WARNING = 500;
const MAX_FILES = 1500;
const MAX_PAGES = 5;
const GS_VERSION = Config.PACKAGE_VERSION;
const UUID = Me.uuid.replace(/@/gi, '_at_').replace(/[^a-z0-9+_-]/gi, '_');

const AppDisplay = GS_VERSION < '3.37.0' ? imports.ui.appDisplay : Me.imports.ui.appDisplay336;
const IconGrid = GS_VERSION < '3.37.0' ? imports.ui.iconGrid : Me.imports.ui.iconGrid336;

// old values from GS 3.32 (s -> ms in 3.34)
const VIEWS_SWITCH_TIME = 0.4; // old AppDisplay.VIEWS_SWITCH_TIME
const VIEWS_SWITCH_ANIMATION_DELAY = 0.1; // old AppDisplay.VIEWS_SWITCH_ANIMATION_DELAY
const PAGE_SWITCH_TIME = 0.3; // old AppDisplay.PAGE_SWITCH_TIME
const ICONGRID_ANIMATION_TIME_IN = 0.350; // old IconGrid.ANIMATION_TIME_IN

const OVERSHOOT_THRESHOLD = 20;
const OVERSHOOT_TIMEOUT = 1000; // ms

const clamp = function(value, min, max) {
    return Math.max(min, Math.min(max, value));
};

const fmt = function(string) {
    return GLib.str_to_ascii(string, null).toLowerCase();
};

const getDisplayName = function(file) {
    try {
        let info = file.query_info('standard::display-name', 0, null);
        return info.get_display_name();
    } catch(e) {
        if (e instanceof Gio.IOErrorEnum)
            return file.get_basename();
        else
            return null;
    }
};

// forked from BaseAppView
const BaseView = new Lang.Class({
    Name: UUID + '-BaseView',
    Abstract: true,

    _init: function(params, gridParams) {
        this.settings = Me.settings;
        gridParams = Params.parse(gridParams, { xAlign: St.Align.MIDDLE,
                                                columnLimit: this.settings.get_int('max-columns'),
                                                minRows: this.settings.get_int('min-rows'),
                                                minColumns: this.settings.get_int('min-rows'),
                                                fillParent: false,
                                                padWithSpacing: true });
        params = Params.parse(params, { usePagination: false });

        if(params.usePagination)
            this._grid = new IconGrid.PaginatedIconGrid(gridParams);
        else
            this._grid = new IconGrid.IconGrid(gridParams);
        
        // GS 3.32- : this._grid.actor, GS 3.32+ this._grid
        this._gridActor = GS_VERSION < '3.32.0' ? this._grid.actor : this._grid;
        this._gridGrid = GS_VERSION < '3.32.0' ? this._grid._grid : this._grid;
        
        if (GS_VERSION < '3.31.0') {
            this.childFocusedId = this._grid.connect('key-focus-in', (grid, actor) => {
                this._childFocused(actor);
            });
        } else {
            this.childFocusedId = this._grid.connect('child-focused', (grid, actor) => {
                this._childFocused(actor);
            });
        }
        
        // Standard hack for ClutterBinLayout
        this._gridActor.x_expand = true;

        this._items = {};
        this._allItems = [];
    },
    
    // _keyFocusIn in GS 3.30-
    _childFocused: function(actor) {
        // Nothing by default
    },

    removeAll: function() {
        this._grid.destroyAll();
        this._items = {};
        this._allItems = [];
    },

    _redisplay: function() {
        this.removeAll();
        this._loadFiles();
    },

    addItem: function(icon) {
        let id = icon.id;
        if (this._items[id] !== undefined)
            return;

        this._allItems.push(icon);
        this._items[id] = icon;
    },
    
    // GS 3.34-: 'insert_child_at_index(item.actor, index)', 'add_actor(item.actor)'
    // GS 3.36+: 'insert_child_at_index(item, index)', 'add_actor(item)'
    _addItemToGrid: function(item, index) {
        if (!(item.icon instanceof IconGrid.BaseIcon))
            throw new Error('Only items with a BaseIcon icon property can be added to IconGrid');

        this._grid._items.push(item);
        if (index !== undefined)
            this._gridGrid.insert_child_at_index(item.actor, index);
        else
            this._gridGrid.add_actor(item.actor);
    },
    
    // same as _addItemToGrid
    _removeItemFromGrid: function(item) {
        this._gridGrid.remove_child(item.actor);
    },

    _doSpringAnimation: function(animationDirection) {
        this._gridActor.opacity = 255;
        this._grid.animateSpring(animationDirection,
                                 Main.overview.viewSelector.showFilesButton);
    },
    
    animate: function(animationDirection, onComplete) {
        if (onComplete) {
            let animationDoneId = this._grid.connect('animation-done', () => {
                this._grid.disconnect(animationDoneId);
                onComplete();
            });
        }

        if (animationDirection == IconGrid.AnimationDirection.IN) {
            let id = this._gridActor.connect('paint', () => {
                this._gridActor.disconnect(id);
                this._doSpringAnimation(animationDirection);
            });
        } else {
            this._doSpringAnimation(animationDirection);
        }
    },

    animateSwitch: function(animationDirection) {
        Mang.removeTweens(this.actor);
        Mang.removeTweens(this._gridActor);

        let params = { time: VIEWS_SWITCH_TIME,
                       transition: 'easeOutQuad' };
        if (animationDirection == IconGrid.AnimationDirection.IN) {
            this.actor.show();
            params.opacity = 255;
            params.delay = VIEWS_SWITCH_ANIMATION_DELAY;
        } else {
            params.opacity = 0;
            params.delay = 0;
            params.onComplete = () => { this.actor.hide(); };
        }

        Mang.addTween(this._gridActor, params);
    }
});
Signals.addSignalMethods(BaseView.prototype);

// forked from AllView
const AllView = new Lang.Class({
    Name: UUID + '-AllView',
    Abstract: true,
    Extends: BaseView,

    _init: function() {
        this.parent({ usePagination: true }, null);
        this._scrollView = new St.ScrollView({ style_class: 'all-apps',
                                               x_expand: true,
                                               y_expand: true,
                                               reactive: true,
                                               y_align: St.Align.START });
        if (GS_VERSION < '3.35') {
            this._scrollView.x_fill = true;
            this._scrollView.y_fill = false;
        }
        
        this.actor = new St.Widget({ layout_manager: new Clutter.BinLayout(),
                                     x_expand:true, y_expand:true });
        this.actor.add_actor(this._scrollView);

        this._scrollView.set_policy(Gtk.PolicyType.NEVER,
                                    Gtk.PolicyType.EXTERNAL);
        this._adjustment = this._scrollView.vscroll.adjustment;
        
        if (GS_VERSION >= '3.32.0') {
            this._pageIndicators = new imports.ui.pageIndicators.AnimatedPageIndicators();
            this._pageIndicatorsActor = this._pageIndicators;
        } else {
            this._pageIndicators = new AppDisplay.PageIndicators();
            this._pageIndicatorsActor = this._pageIndicators.actor;
        }
        this._pageIndicators.connect('page-activated',
            (indicators, pageIndex) => {
                this.goToPage(pageIndex);
            });
        this._pageIndicatorsActor.connect('scroll-event', Lang.bind(this, this._onScroll));
        this.actor.add_actor(this._pageIndicatorsActor);

        this._stack = new St.Widget({ layout_manager: new Clutter.BinLayout() });
        let box = new St.BoxLayout({ vertical: true });

        this._grid.currentPage = 0;
        this._stack.add_actor(this._gridActor);
        
        // remove eventBlocker because it prevent drag&drop targeting actors in the view
        // (it is used with folderPopup in appDisplay.js)
        //this._eventBlocker = new St.Widget({ x_expand: true, y_expand: true });
        //this._stack.add_actor(this._eventBlocker);

        box.add_actor(this._stack);
        this._scrollView.add_actor(box);

        this._scrollView.connect('scroll-event', Lang.bind(this, this._onScroll));

        let panAction = new Clutter.PanAction({ interpolate: false });
        panAction.connect('pan', Lang.bind(this, this._onPan));
        panAction.connect('gesture-cancel', Lang.bind(this, this._onPanEnd));
        panAction.connect('gesture-end', Lang.bind(this, this._onPanEnd));
        this._panAction = panAction;
        this._scrollView.add_action(panAction);
        this._panning = false;
        this._clickAction = new Clutter.ClickAction();
        this._clickAction.connect('clicked', () => {
            if (!this._currentPopup)
                return;

            let [x, y] = this._clickAction.get_coords();
            let actor = global.stage.get_actor_at_pos(Clutter.PickMode.ALL, x, y);
            if (!this._currentPopup.actor.contains(actor))
                this._currentPopup.popdown();
        });
        //this._eventBlocker.add_action(this._clickAction);

        this._displayingPopup = false;

        this._availWidth = 0;
        this._availHeight = 0;

        if (GS_VERSION < '3.36.0') {
            this._grid.connect('space-opened', () => {
                let fadeEffect = this._scrollView.get_effect('fade');
                if (fadeEffect)
                    fadeEffect.enabled = false;

                this.emit('space-ready');
            });
            this._grid.connect('space-closed', () => {
                this._displayingPopup = false;
            });
        }

        this.actor.connect('notify::mapped', () => {
            if (this.actor.mapped) {
                this._keyPressEventId =
                    global.stage.connect('key-press-event',
                                         Lang.bind(this, this._onKeyPressEvent));
            } else {
                if (this._keyPressEventId)
                    global.stage.disconnect(this._keyPressEventId);
                this._keyPressEventId = 0;
            }
        });
        
        this.itemDragBeginHandler = Main.overview.connect('files-view-item-drag-begin', Lang.bind(this, this._onDragBegin));
        this.itemDragEndHandler = Main.overview.connect('files-view-item-drag-end', Lang.bind(this, this._onDragEnd));
        this.itemDragCancelledHandler = Main.overview.connect('files-view-item-drag-cancelled', Lang.bind(this, this._onDragCancelled));
    },
    
    disable: function() {
        if (this._keyPressEventId)
            global.stage.disconnect(this._keyPressEventId);
        if (this.scrollTimeout)
            Mainloop.source_remove(this.scrollTimeout);
        if (this._lastOvershootTimeoutId)
            GLib.source_remove(this._lastOvershootTimeoutId);
        
        Main.overview.disconnect(this.itemDragBeginHandler);
        Main.overview.disconnect(this.itemDragEndHandler);
        Main.overview.disconnect(this.itemDragCancelledHandler);
                        
        this.removeAll();
        this._grid.disconnect(this.childFocusedId);
        this.actor.destroy();
        
        // necessary to redisplay (need view.actor null in FilesDisplay._onAllocatedSizeChanged)
        this.actor = null;
    },
    
    // Overriden from BaseAppView
    animate: function(animationDirection, onComplete) {
        this._scrollView.reactive = false;
        let completionFunc = () => {
            this._scrollView.reactive = true;
            if (onComplete)
                onComplete();
        };

        if (animationDirection == IconGrid.AnimationDirection.OUT &&
            this._displayingPopup && this._currentPopup) {
            this._currentPopup.popdown();
            let spaceClosedId = this._grid.connect('space-closed', () => {
                this._grid.disconnect(spaceClosedId);
                // Given that we can't call this.parent() inside the
                // signal handler, call again animate which will
                // call the parent given that popup is already
                // closed.
                this.animate(animationDirection, completionFunc);
            });
        } else {
            this.parent(animationDirection, completionFunc);
            if (animationDirection == IconGrid.AnimationDirection.OUT)
                this._pageIndicators.animateIndicators(animationDirection);
        }
    },

    animateSwitch: function(animationDirection) {
        this.parent(animationDirection);

        if (this._currentPopup && this._displayingPopup &&
            animationDirection == IconGrid.AnimationDirection.OUT)
            Mang.addTween(this._currentPopup.actor,
                          { time: VIEWS_SWITCH_TIME,
                            transition: 'easeOutQuad',
                            opacity: 0,
                            onComplete: function() {
                                this.opacity = 255;
                            } });

        if (animationDirection == IconGrid.AnimationDirection.OUT)
            this._pageIndicators.animateIndicators(animationDirection);
    },
    
    simpleSwitchIn: function() {
        this.actor.show();
        this._gridActor.opacity = 255;
    },
    
    simpleSwitchOut: function() {
        this._pageIndicators.animateIndicators(IconGrid.AnimationDirection.OUT);
        Mang.removeTweens(this.actor);
        Mang.removeTweens(this._gridActor);
        this.actor.hide();
    },

    getCurrentPageY: function() {
        return this._grid.getPageY(this._grid.currentPage);
    },

    goToPage: function(pageNumber) {
        pageNumber = clamp(pageNumber, 0, this._grid.nPages() - 1);

        if (this._grid.currentPage == pageNumber && this._displayingPopup && this._currentPopup)
            return;
        if (this._displayingPopup && this._currentPopup)
            this._currentPopup.popdown();

        let velocity;
        if (!this._panning)
            velocity = 0;
        else
            velocity = Math.abs(this._panAction.get_velocity(0)[2]);
        // Tween the change between pages.
        // If velocity is not specified (i.e. scrolling with mouse wheel),
        // use the same speed regardless of original position
        // if velocity is specified, it's in pixels per milliseconds
        let diffToPage = this._diffToPage(pageNumber);
        let childBox = this._scrollView.get_allocation_box();
        let totalHeight = childBox.y2 - childBox.y1;
        let time;
        // Only take the velocity into account on page changes, otherwise
        // return smoothly to the current page using the default velocity
        if (this._grid.currentPage != pageNumber) {
            let minVelocity = totalHeight / (PAGE_SWITCH_TIME * 1000);
            velocity = Math.max(minVelocity, velocity);
            time = (diffToPage / velocity) / 1000;
        } else {
            time = PAGE_SWITCH_TIME * diffToPage / totalHeight;
        }
        // When changing more than one page, make sure to not take
        // longer than PAGE_SWITCH_TIME
        time = Math.min(time, PAGE_SWITCH_TIME);

        this._grid.currentPage = pageNumber;
        Mang.addTween(this._adjustment,
                      { value: this._grid.getPageY(this._grid.currentPage),
                        time: time,
                        transition: 'easeOutQuad' });
        if (GS_VERSION < '3.36.0')
            this._pageIndicators.setCurrentPage(pageNumber);
        else
            this._pageIndicators.setCurrentPosition(pageNumber);
    },

    _diffToPage: function(pageNumber) {
        let currentScrollPosition = this._adjustment.value;
        return Math.abs(currentScrollPosition - this._grid.getPageY(pageNumber));
    },

    _onScroll: function(actor, event) {
        if (this._displayingPopup || !this._scrollView.reactive)
            return Clutter.EVENT_STOP;

        let direction = event.get_scroll_direction();
        
        // reduce scroll speed for touchpad
        if (event.get_source_device().get_device_type() == Clutter.InputDeviceType.TOUCHPAD_DEVICE) {
            if (direction != Clutter.ScrollDirection.UP && direction != Clutter.ScrollDirection.DOWN)
                return Clutter.EVENT_STOP;
            
            if (this.scrollCount === undefined)
                this.scrollCount = 0;
            this.scrollCount++ ;
            
            if (this.scrollTimeout)
                Mainloop.source_remove(this.scrollTimeout);
            
            this.scrollTimeout = Mainloop.timeout_add(50, () => {
                this.scrollCount = 0;
                Mainloop.source_remove(this.scrollTimeout);
                this.scrollTimeout = null;
            });
            
            if (this.scrollCount % 2 == 0)
                return Clutter.EVENT_STOP;
        }

        if (direction == Clutter.ScrollDirection.UP)
            this.goToPage(this._grid.currentPage - 1);
        else if (direction == Clutter.ScrollDirection.DOWN)
            this.goToPage(this._grid.currentPage + 1);

        return Clutter.EVENT_STOP;
    },

    _onPan: function(action) {
        if (this._displayingPopup)
            return false;
        this._panning = true;
        this._clickAction.release();
        let [dist, dx, dy] = action.get_motion_delta(0);
        let adjustment = this._adjustment;
        adjustment.value -= (dy / this._scrollView.height) * adjustment.page_size;
        return false;
    },

    _onPanEnd: function(action) {
         if (this._displayingPopup)
            return;

        let pageHeight = this._grid.getPageHeight();

        // Calculate the scroll value we'd be at, which is our current
        // scroll plus any velocity the user had when they released
        // their finger.

        let velocity = -action.get_velocity(0)[2];
        let endPanValue = this._adjustment.value + velocity;

        let closestPage = Math.round(endPanValue / pageHeight);
        this.goToPage(closestPage);

        this._panning = false;
    },

    _onKeyPressEvent: function(actor, event) {
        if (this._displayingPopup)
            return Clutter.EVENT_STOP;

        if (event.get_key_symbol() == Clutter.KEY_Page_Up) {
            this.goToPage(this._grid.currentPage - 1);
            return Clutter.EVENT_STOP;
        } else if (event.get_key_symbol() == Clutter.KEY_Page_Down) {
            this.goToPage(this._grid.currentPage + 1);
            return Clutter.EVENT_STOP;
        }

        return Clutter.EVENT_PROPAGATE;
    },

    _childFocused: function(icon) {
        let itemPage = this._grid.getItemPage(icon);
        this.goToPage(itemPage);
    },

    // Called before allocation to calculate dynamic spacing
    adaptToSize: function(width, height) {
        let box = new Clutter.ActorBox();
        box.x1 = 0;
        box.x2 = width;
        box.y1 = 0;
        box.y2 = height;
        box = this.actor.get_theme_node().get_content_box(box);
        box = this._scrollView.get_theme_node().get_content_box(box);
        box = this._gridActor.get_theme_node().get_content_box(box);
        let availWidth = box.x2 - box.x1;
        let availHeight = box.y2 - box.y1;
        let oldNPages = this._grid.nPages();

        this._grid.adaptToSize(availWidth, availHeight);

        let fadeOffset = Math.min(this._grid.topPadding,
                                  this._grid.bottomPadding);
        this._scrollView.update_fade_effect(fadeOffset, 0);
        if (fadeOffset > 0)
            this._scrollView.get_effect('fade').fade_edges = true;

        if (this._availWidth != availWidth || this._availHeight != availHeight || oldNPages != this._grid.nPages()) {
            // no reset because of 'infinite scrolling' (_updateDisplayedFiles())
            this._adjustment.value = this._adjustment.value ? this._adjustment.value : 0;
            this._grid.currentPage = this._grid.currentPage ? this._grid.currentPage : 0;
            Meta.later_add(Meta.LaterType.BEFORE_REDRAW, () => {
                this._pageIndicators.setNPages(this._grid.nPages());
                if (GS_VERSION < '3.36.0')
                    this._pageIndicators.setCurrentPage(this._pageIndicators._currentPage ? this._pageIndicators._currentPage : 0);
                else
                    this._pageIndicators.setCurrentPosition(this._pageIndicators._currentPosition ? this._pageIndicators._currentPosition : 0);
            });
        }

        this._availWidth = availWidth;
        this._availHeight = availHeight;
    },
    
    updateLayout: function() {
        this._grid._colLimit = this.settings.get_int('max-columns');
        this._grid._minRows = this.settings.get_int('min-rows');
        this._grid._minColumns = this.settings.get_int('min-rows');
        this._gridGrid.queue_relayout();
    },
    
    _resetOvershoot: function() {
        if (this._lastOvershootTimeoutId)
            GLib.source_remove(this._lastOvershootTimeoutId);
        this._lastOvershootTimeoutId = 0;
        this._lastOvershootY = -1;
    },

    _handleDragOvershoot: function(dragEvent) {
        let [gridX, gridY] = this.actor.get_transformed_position();
        let [gridWidth, gridHeight] = this.actor.get_transformed_size();
        let gridBottom = gridY + gridHeight;

        // Already animating
        if (GS_VERSION > '3.33.0' && this._adjustment.get_transition('value') !== null)
            return;

        // Within the grid boundaries
        if (dragEvent.y > gridY && dragEvent.y < gridBottom) {
            // Check whether we moved out the area of the last switch
            if (Math.abs(this._lastOvershootY - dragEvent.y) > OVERSHOOT_THRESHOLD)
                this._resetOvershoot();

            return;
        }

        // Still in the area of the previous page switch
        if (this._lastOvershootY >= 0)
            return;

        let currentY = this._adjustment.value;
        let maxY = this._adjustment.upper - this._adjustment.page_size;

        if (dragEvent.y <= gridY && currentY > 0)
            this.goToPage(this._grid.currentPage - 1);
        else if (dragEvent.y >= gridBottom && currentY < maxY)
            this.goToPage(this._grid.currentPage + 1);
        else
            return; // don't go beyond first/last page

        this._lastOvershootY = dragEvent.y;

        if (this._lastOvershootTimeoutId > 0)
            GLib.source_remove(this._lastOvershootTimeoutId);

        this._lastOvershootTimeoutId =
            GLib.timeout_add(GLib.PRIORITY_DEFAULT, OVERSHOOT_TIMEOUT, () => {
                this._resetOvershoot();
                this._handleDragOvershoot(dragEvent);
                return GLib.SOURCE_REMOVE;
            });
        GLib.Source.set_name_by_id(this._lastOvershootTimeoutId,
            '[gnome-shell] this._lastOvershootTimeoutId');
    },
    
    _onDragBegin: function() {
        this._resetOvershoot();
        this._dragCancelled = false;
        this._dragMonitor = {
            dragMotion: Lang.bind(this, this._onDragMotion)
        };
        DND.addDragMonitor(this._dragMonitor);
    },
    
    _onDragCancelled: function(emitter, source) {
        this._dragCancelled = true;
        this._endDrag();
    },

    _onDragEnd: function(emitter, source) {
        if (this._dragCancelled)
            return;
        this._endDrag();
    },

    _endDrag: function() {
        if (this._dragMonitor) {
            DND.removeDragMonitor(this._dragMonitor);
            this._dragMonitor = null;
        }

        this._resetOvershoot();
    },

    _onDragMotion: function(dragEvent) {
        if (!(dragEvent.source instanceof FileIcon.FileIcon))
            return DND.DragMotionResult.CONTINUE;

        // Handle the drag overshoot. When dragging to above the
        // icon grid, move to the page above; when dragging below,
        // move to the page below.
        if (this._gridActor.contains(dragEvent.source.actor))
            this._handleDragOvershoot(dragEvent);

        return DND.DragMotionResult.CONTINUE;
    }
});
Signals.addSignalMethods(AllView.prototype);

const FilesView = new Lang.Class({
    Name: UUID + '-FilesView',
    Abstract: true,
    Extends: AllView,

    _init: function(manager) {
        this.parent();
        
        this.manager = manager;
        this.offset = 0; // offset represents the 'go-parent' button
        this.uris = [];
        this.searchText = '';
        
        this.searchTextChangedHandler = Main.overview.viewSelector.filesSearchEntry.connect('text-changed', () => {
            if (this.actor.mapped) {
                this._updateSearchText();
                if (!this.mappingHandler)
                    this._updateItemsVisibility();
            }
        });
        
        this.filesChangedHandler = this.manager.connect('changed', () => {
            if (this.mappingHandler)
                return;
            if (this.actor.mapped) {
                this._onFilesChanged();
                this._updateItemsVisibility();
            } else {
                this.mappingHandler = this.actor.connect('notify::mapped', () => {
                    if (this.actor.mapped) {
                        if (this.mappingHandler) {
                            this.actor.disconnect(this.mappingHandler);
                            this.mappingHandler = null;
                        }
                        this._onFilesChanged();
                        this._updateItemsVisibility();
                    }
                });
            }
        });
        
        // load files on first display
        this.mappingHandler = this.actor.connect('notify::mapped', () => {
            if (this.actor.mapped) {
                if (this.mappingHandler) {
                    this.actor.disconnect(this.mappingHandler);
                    this.mappingHandler = null;
                }
                this.redisplay();
            }
        });
    },
    
    disable: function() {
        if (this.filesChangedHandler) {
            this.manager.disconnect(this.filesChangedHandler);
            this.filesChangedHandler = null;
        }
        if (this.searchTextChangedHandler) {
            Main.overview.viewSelector.filesSearchEntry.disconnect(this.searchTextChangedHandler);
            this.searchTextChangedHandler = null;
        }
        
        this.parent();
    },
    
    _removeUri: function(uri) {
        let uriIndex = this.uris.indexOf(uri);
        this.uris.splice(uriIndex, 1);
        let fileItem = this._grid.getItemAtIndex(uriIndex + this.offset)._delegate;
        this._removeItemFromGrid(fileItem);
        fileItem.actor.destroy();
        let itemIndex = this._grid._items.indexOf(fileItem);
        if (itemIndex != -1)
            this._grid._items.splice(itemIndex, 1);
    },
    
    _addUri: function(uri, index) {
        let file = Gio.File.new_for_uri(uri);
        if (!file.query_exists(null)) {
            if (this.manager.removeUri)
                this.manager.removeUri(uri);
            return;
        }
        
        let fileItem = new FileIcon.FileIcon(file, this);
        if (!fileItem.hasError) {
            if (index !== undefined)
                this.uris.splice(index, 0, uri);
            else
                this.uris.push(uri);
            this._addItemToGrid(fileItem, index !== undefined ? index + this.offset : index);
        } else {
            if (this.manager.removeUri)
                this.manager.removeUri(uri);
        }
    },
    
    _moveUri: function(uri, newIndex) {
        let uriIndex = this.uris.indexOf(uri);
        this.uris.splice(uriIndex, 1);
        this.uris.splice(newIndex, 0, uri);
        
        let fileItem = this._grid.getItemAtIndex(uriIndex + this.offset)._delegate;
        
        this._gridGrid.set_child_at_index(fileItem.actor, newIndex + this.offset);
    },
    
    _updateSearchText: function() {
        let text = Main.overview.viewSelector.filesSearchEntry.get_text();
        
        if (text.indexOf('~/') == 0 || text.indexOf('/') == 0 || text.indexOf('./') == 0 || text.indexOf('../') == 0 || GLib.uri_parse_scheme(text))
            text = null;
        
        // text === null excludes the case text == ''
        this.actor.opacity = text === null ? 120 : 255;
        this.searchText = text === null ? '' : fmt(text);
    },
    
    _updateItemsVisibility: function() {
        this._grid._items.forEach(item => item.actor.visible = fmt(item.labelText).indexOf(this.searchText) != -1);
        if (GS_VERSION >= '3.36.0')
            Mainloop.idle_add(() => this._gridGrid.queue_relayout());
    }
});

const RecentView = new Lang.Class({
    Name: UUID + '-RecentView',
    Extends: FilesView,

    _init: function(recentManager) {
        this.parent(recentManager);
    },
    
    redisplay: function() {
        this.removeAll();
        this.uris = [];
        this.manager.get_items().forEach(item => this._addUri(item.get_uri()));
    },
    
    _onFilesChanged: function() {
        let newUris = this.manager.get_items().map(item => item.get_uri());
        
        this.uris.filter(uri => newUris.indexOf(uri) == -1)
                 .forEach(uri => this._removeUri(uri));
        
        newUris.forEach((uri, index) => {
            if (index < this.uris.length && this.uris[index] == uri)
                return;
            
            if (this.uris.indexOf(uri) == -1) {
                this._addUri(uri, index);
                return;
            }
            
            this._moveUri(uri, index);
        });
    }
});

const FavoritesView = new Lang.Class({
    Name: UUID + '-FavoritesView',
    Extends: FilesView,

    _init: function() {
        this.parent(Main.overview.favoritesManager);
    },
    
    redisplay: function() {
        this.removeAll();
        
        if (this.manager.hasTracker) {
            let goTagsItem = new FileIcon.TagIcon(null, false, true);
            this._addItemToGrid(goTagsItem);
            this.offset = 1;
        }
        
        this.uris = [];
        this.manager.favoriteUris = [];
        this.manager.updateFavorites();
    },
    
    _onFilesChanged: function() {
        let newUris = this.manager.favoriteUris;
        for (let i = 0; i < newUris.length; i++) {
            if (i < this.uris.length && this.uris[i] == newUris[i])
                continue;
            
            if (this.uris.indexOf(newUris[i]) == -1) {
                this._addUri(newUris[i], i);
                continue;
            }
            
            this._moveUri(newUris[i], i);
        }
        
        this.uris.filter(uri => newUris.indexOf(uri) == -1)
                 .forEach(uri => this._removeUri(uri));
    }
});

const AllTagsView = new Lang.Class({
    Name: UUID + '-AllTagsView',
    Extends: FilesView,

    _init: function() {
        this.parent(Main.overview.favoritesManager.allTagsManager);
    },
    
    redisplay: function() {
        this.removeAll();
        
        let goParentItem = new FileIcon.TagIcon(null, false, false, true);
        this._addItemToGrid(goParentItem);
        this.offset = 1;
        
        this.tags = [];
        this.manager.tags = [];
        this.manager.updateTags();
    },
    
    _onFilesChanged: function() {
        let newTags = this.manager.tags;
        for (let i = 0; i < newTags.length; i++) {
            if (i < this.tags.length && this.tags[i] == newTags[i])
                continue;
            
            if (this.tags.indexOf(newTags[i]) == -1) {
                this._addTag(newTags[i], i);
                continue;
            }
            
            this._moveTag(newTags[i], i);
        }
        
        this.tags.filter(tag => newTags.indexOf(tag) == -1)
                 .forEach(tag => this._removeTag(tag));
    },
    
    _removeTag: function(tag) {
        let tagIndex = this.tags.indexOf(tag);
        this.tags.splice(tagIndex, 1);
        let tagItem = this._grid.getItemAtIndex(tagIndex + this.offset)._delegate;
        this._removeItemFromGrid(tagItem);
        tagItem.actor.destroy();
        let itemIndex = this._grid._items.indexOf(tagItem);
        if (itemIndex != -1)
            this._grid._items.splice(itemIndex, 1);
    },
    
    _addTag: function(tag, index) {
        let tagItem = new FileIcon.TagIcon(tag, false, false, false);
        if (index !== undefined)
            this.tags.splice(index, 0, tag);
        else
            this.tags.push(tag);
        this._addItemToGrid(tagItem, index !== undefined ? index + this.offset : index);
    },
    
    _moveTag: function(tag, newIndex) {
        let tagIndex = this.tags.indexOf(tag);
        this.tags.splice(tagIndex, 1);
        this.tags.splice(newIndex, 0, tag);
        
        let tagItem = this._grid.getItemAtIndex(tagIndex + this.offset)._delegate;
        
        this._gridGrid.set_child_at_index(tagItem.actor, newIndex + this.offset);
    }
});

const TagView = new Lang.Class({
    Name: UUID + '-TagView',
    Extends: FilesView,

    _init: function(tag) {
        this.parent(Main.overview.favoritesManager.allTagsManager.getNewTagManager(tag));
        this.tag = tag;
    },
    
    redisplay: function() {
        this.removeAll();
        
        let goParentItem = new FileIcon.TagIcon(this.tag, true, false, false);
        this._addItemToGrid(goParentItem);
        this.offset = 1;
        
        this.uris = [];
        this.manager.uris = [];
        this.manager.updateUris();
    },
    
    _onFilesChanged: function() {
        let newUris = this.manager.uris;
        for (let i = 0; i < newUris.length; i++) {
            if (i < this.uris.length && this.uris[i] == newUris[i])
                continue;
            
            if (this.uris.indexOf(newUris[i]) == -1) {
                this._addUri(newUris[i], i);
                continue;
            }
            
            this._moveUri(newUris[i], i);
        }
        
        this.uris.filter(uri => newUris.indexOf(uri) == -1)
                 .forEach(uri => this._removeUri(uri));
    }
});

const FolderView = new Lang.Class({
    Name: UUID + '-FolderView',
    Extends: FilesView,

    _init: function(directory) {
        // duplicate GFile to avoid file property conflicts (file.info, file.isGoParent ...)
        directory = directory.dup();
        this.parent(new FolderManager.FolderManager(directory));
        
        this.directory = directory;
        this.files = [];
        
        this.fileRemovedHandler = this.manager.connect('file-removed', (obj, file) => {
            if (!this.mappingHandler) {
                this._removeFile(file);
            }
        });
        this.fileAddedHandler = this.manager.connect('file-added', (obj, file, index) => {
            if (!this.mappingHandler && file.query_exists(null)) {
                this._addFile(file, index);
                this._updateItemsVisibility();
            }
        });
        this.directoryDeletedHandler = this.manager.connect('directory-deleted', Lang.bind(this, this._onDirectoryDeleted));
    },
    
    disable: function() {
        this.manager.disconnect(this.fileRemovedHandler);
        this.manager.disconnect(this.fileAddedHandler);
        this.manager.disconnect(this.directoryDeletedHandler);
        this.manager.disable();
        
        this.parent();
    },
    
    generateThumbnails: function(large) {
        this.manager.generateThumbnails(large);
    },
    
    sortBy: function(criterion) {
        this.manager.sortBy(criterion);
    },
    
    reverseSorting: function() {
        this.manager.reverseSorting();
    },
    
    eraseSortMetadata: function() {
        this.manager.eraseSortMetadata();
    },
    
    redisplay: function() {
        this.manager.updateFolder();
    },
    
    resort: function() {
        this.manager.resortFiles();
    },
    
    _onFilesChanged: function() {
        this.removeAll();
        this.files = [];
        
        let filesCount = this.manager.allFiles.length;
        if (filesCount > MAX_FILES_WARNING && Main.overview.visible && Main.overview.viewSelector._activePage == Main.overview.viewSelector._filesPage) {
            new Misc.ConfirmDialog({
                iconName: 'dialog-warning-symbolic',
                title: _N("Opening “%s”.").format(getDisplayName(this.directory)),
                body: _Nn("Opening %d item.","Opening %d items.", filesCount).format(filesCount),
                cancelText: _N("Cancel"),
                confirmText: _N("_Display").replace(" (_D)", "").replace("(_D)", "").replace("_", ""),
                confirmAction: () => {
                 // reset scroll and pageIndicators in case of resorting
                 this._adjustment.value = 0;
                 this._grid.currentPage = 0;
                 if (GS_VERSION < '3.36.0')
                    this._pageIndicators.setCurrentPage(0);
                else
                    this._pageIndicators.setCurrentPosition(0);
                 
                 this._displayFiles();
                },
                cancelAction: () =>  Main.overview.viewSelector.filesDisplay.cancelDirectoryOpening()
            }).open();
        } else {
            this._displayFiles();
        }
    },
    
    _displayFiles: function() {
        this._updatelastDisplayedIndex();
        this.manager.allFiles.forEach(this._addFile, this);
    },
    
    _removeFile: function(file) {
        let fileIndex = this.files.map(a => a.get_uri()).indexOf(file.get_uri());
        if (fileIndex == -1)
            return;
        
        this.files.splice(fileIndex, 1);
        
        let fileActor = this._grid.getItemAtIndex(fileIndex);
        if (!fileActor)
            return;
        
        let fileItem = fileActor._delegate;
        this._removeItemFromGrid(fileItem);
        fileItem.actor.destroy();
        let itemIndex = this._grid._items.indexOf(fileItem);
        if (itemIndex != -1)
            this._grid._items.splice(itemIndex, 1);
    },
    
    _addFile: function(file, index) {
        if (this.files.length > MAX_FILES && (index === undefined || index >= this.files.length))
            return;
        
        if (index !== undefined) {
            this.files.splice(index, 0, file);
        } else {
            this.files.push(file);
            index = this.files.length - 1;
        }
        
        if (index > this.lastDisplayedIndex)
            return;
        
        let fileItem = new FileIcon.FileIcon(file, this);
        
        if (!fileItem.hasError)
            this._addItemToGrid(fileItem, index);
        else
            this.files.splice(index, 1);
    },
    
    getIsMonitored: function() {
        return this.manager.directoryMonitor ? true : false;
    },
    
    // view's directory is deleted, moved or unmounted
    _onDirectoryDeleted: function() {
        Main.overview.viewSelector.filesDisplay.onViewDirectoryDeleted(this);
    },
    
    goToPage: function(pageNumber) {
        this.parent(pageNumber);
        
        if (pageNumber >= this._grid.nPages() - 1) {
            Mainloop.idle_add(() => {
                this._updatelastDisplayedIndex();
                this._updateDisplayedFiles();
                return GLib.SOURCE_REMOVE;
            });
        }
    },
    
    _updatelastDisplayedIndex: function() {
        let childrenPerPage = clamp(this._grid._colLimit * this._grid._minRows, 12, 48);
        this.lastDisplayedIndex = Math.max(MAX_PAGES, this._grid.currentPage + 2) * childrenPerPage - 1;
    },
    
    _updateDisplayedFiles: function() {
        let errorFiles = [];
        let displayedFiles = this._grid._items.map(item => item.file);
        
        this.files.forEach((file, index) => {
            let lastIndex, condition;
            
            if (this.searchText === '') {
                lastIndex = this.lastDisplayedIndex;
                condition = index <= lastIndex && displayedFiles.indexOf(file) == -1;
            } else {
                lastIndex = this.lastDisplayedIndex + (this._grid._items.length - this._grid.visibleItemsCount());
                condition = index <= lastIndex && displayedFiles.indexOf(file) == -1 &&
                            fmt(file.get_basename()).indexOf(this.searchText) != -1;
            }
        
            if (condition) {
                let fileItem = new FileIcon.FileIcon(file, this);
        
                if (!fileItem.hasError)
                    this._addItemToGrid(fileItem, index);
                else
                    errorFiles.push(file);
            }
        });
        
        errorFiles.forEach(file => {
            let fileIndex = this.files.indexOf(file);
            if (fileIndex != -1)
                this.files.splice(fileIndex, 1);
        });
    },
    
    _updateSearchText: function() {
        let text = Main.overview.viewSelector.filesSearchEntry.get_text();
        
        if (text.indexOf('~/') == 0)
            text = GLib.get_home_dir() + text.slice(1);
        else if (text.indexOf('/') != 0 && !GLib.uri_parse_scheme(text))
            text = GLib.build_filenamev([this.directory.get_uri(), text]);
            
        if (text.indexOf('/') == 0 || GLib.uri_parse_scheme(text)) {
            let file;
            if (GLib.uri_parse_scheme(text))
                file = Gio.File.new_for_uri(text);
            else
                file = Gio.File.new_for_path(text);
                
            if (this.directory.equal(file))
                text = '';
            else
                text = this.directory.get_relative_path(file); // return null if file is not a descendant
        }               
        
        // text === null excludes the case text == ''
        this.actor.opacity = text === null ? 120 : 255;
        this.searchText = text === null ? '' : fmt(text);
    },
    
    _updateItemsVisibility: function() {
        let text = this.searchText.split('/')[0];
        this._grid._items.forEach(item => item.actor.visible = fmt(item.labelText).indexOf(this.searchText) != -1 ||
                                                               fmt(item.labelText) == text );
        
        if (this.searchText !== '' && this._grid._items.length != this.files.length) {
            this._updatelastDisplayedIndex();
            this._updateDisplayedFiles();
        }
        
        if (GS_VERSION >= '3.36.0')
            Mainloop.idle_add(() => this._gridGrid.queue_relayout());
    }
});


const Views = {
    RECENT: 0,
    FOLDER: 1,
    TAGS: 2
};

// map 'start-view' setting
const PrefsViews = {
    'recent': 0,
    'folder': 1,
    'favorites': 2, // For compatibility
    'tags': 2
};

//It's a fork of AppDisplay
var FilesDisplay = new Lang.Class({
    Name: UUID + '-FilesDisplay',

    _init: function() {
        this.settings = Me.settings;
        
        this.startPath = this.settings.get_string('start-path');
        if ((this.startPath === '') || !Gio.File.new_for_path(this.startPath).query_exists(null))
            this.startPath = GLib.get_home_dir();
        this.startDirectory = Gio.File.new_for_path(this.startPath);
        
        this.directory = this.startDirectory;
        this.oldFolderView = null;
        this.redisplayTimeout = null;
        this._views = [];
        let view, button;
        let folderLabel = getDisplayName(this.directory);
        if (folderLabel.length > MAX_FOLDER_VIEW_NAME_LENGTH)
            folderLabel = folderLabel.slice(0,MAX_FOLDER_VIEW_NAME_LENGTH) + "…";
        
        // Only the activeView is loaded when user query it (in _showView)
        //because of loading time and fluidity
        let styleClass = (GS_VERSION < '3.37' ? 'app-view-control' : 'files-view-app-view-control-336') + ' files-view-control button';
        
        view = null;
        button = new St.Button({ label: folderLabel,
                                 style_class: styleClass,
                                 can_focus: true,
                                 x_expand: true });
        this._views[Views.FOLDER] = { 'view': view, 'control': button };
        
        view = null;
        button = new St.Button({ label: _("Recent"),
                                 style_class: styleClass,
                                 can_focus: true,
                                 x_expand: true });
        this._views[Views.RECENT] = { 'view': view, 'control': button };
        
        view = null;
        button = new St.Button({ label: Main.overview.favoritesManager.hasTracker ? _("Tags") : _("Favorites"),
                                 style_class: styleClass,
                                 can_focus: true,
                                 x_expand: true });
        this._views[Views.TAGS] = { 'view': view, 'control': button };

        this.actor = new St.BoxLayout ({ style_class: 'app-display',
                                         x_expand: true, y_expand: true,
                                         vertical: true });
        this._viewStackLayout = new AppDisplay.ViewStackLayout();
        this._viewStack = new St.Widget({ x_expand: true, y_expand: true,
                                          layout_manager: this._viewStackLayout });
        this._viewStackLayout.connect('allocated-size-changed', Lang.bind(this, this._onAllocatedSizeChanged));
        this.actor.add_actor(this._viewStack);
        let layout = new AppDisplay.ControlsBoxLayout({ homogeneous: true });
        this._controls = new St.Widget({ style_class: GS_VERSION < '3.37' ? 'app-view-controls' : 'files-view-app-view-controls-336',
                                         layout_manager: layout });
        this._controls.connect('notify::mapped', () => {
            // controls are faded either with their parent or
            // explicitly in animate(); we can't know how they'll be
            // shown next, so make sure to restore their opacity
            // when they are hidden
            if (this._controls.mapped)
                return;

            Mang.removeTweens(this._controls);
            this._controls.opacity = 255;
        });

        layout.hookup_style(this._controls);
        this._controlsBox = new St.BoxLayout({ vertical: false, x_align: Clutter.ActorAlign.CENTER });
        this._controlsBox.add_actor(this._controls);
        this.actor.add_actor(new St.Bin({ child: this._controlsBox }));

        for (let i = 0; i < this._views.length; i++) {
            //"this._viewStack.add_actor(this._views[i].view.actor);"
            //moves to _showView method
            this._controls.add_actor(this._views[i].control);

            let viewIndex = i;
            this._views[i].control.connect('clicked', actor => {
                    this._showView(viewIndex);
            });
        }
        
        this._addOperationBox();
        
        this.recentManager = new RecentManager.RecentManager();
        
        if (this.settings.get_string('start-view') != 'previous') {
            this._showView(PrefsViews[this.settings.get_string('start-view')]);
        } else {
            this._showView(Views.FOLDER);
        }
        
        this.timeout = Mainloop.timeout_add(60000, () => {
            Mainloop.source_remove(this.timeout);
            this.timeout = null;
        });
    },
    
    _addOperationBox: function() {
        // empty button to keep centering
        // use 'app-view-controls' to get the same padding-bottom as other controls
        let styleClass = (GS_VERSION < '3.37' ? 'app-view-controls' : 'files-view-app-view-controls-336') + ' files-view-operation-box';
        
        let emptyBox = new St.BoxLayout({ style_class: styleClass, y_align: Clutter.ActorAlign.CENTER });
        this._controlsBox.insert_child_at_index(emptyBox, 0);
        
        let operationBox = new St.BoxLayout({ style_class: styleClass, y_align: Clutter.ActorAlign.CENTER });
        this._controlsBox.add_child(operationBox);
        
        operationBox.connect('notify::width', () => {
            emptyBox.width = operationBox.width;
        });
        
        let undoButton = new Buttons.UndoButton(Main.overview.fileManager);
        operationBox.add_child(new St.Bin({ child: undoButton }));
        let cancelButton = new Buttons.CancelButton(Main.overview.fileManager);
        operationBox.add_child(new St.Bin({ child: cancelButton }));
    },
    
    disable: function() {
        if (this.fileManagerWorkingHandler && Main.overview.fileManager) {
            Main.overview.fileManager.disconnect(this.fileManagerWorkingHandler);
            this.fileManagerWorkingHandler = null;
        }
        
        ['tagView', 'allTagsView', 'topFavoritesView'].forEach(prop => this._removeTagsView(prop));
        
        this.recentManager.disable();
        
        for (let i = 0; i < this._views.length; i++) {
            if (this._views[i].view)
                this._views[i].view.disable();
        }
        if (this.oldFolderView)
            this.oldFolderView.disable();
            
        this.actor.destroy();
        if (this.timeout)
            Mainloop.source_remove(this.timeout);
    },
    
    animate: function(animationDirection, onComplete) {
        let currentView = this._views.filter(v => v.control.has_style_pseudo_class('checked')).pop().view;

        // Animate controls opacity using iconGrid animation time, since
        // it will be the time the AllView or FrequentView takes to show
        // it entirely.
        let finalOpacity;
        if (animationDirection == IconGrid.AnimationDirection.IN) {
            this._controls.opacity = 0;
            finalOpacity = 255;
        } else {
            finalOpacity = 0;
        }

        Mang.addTween(this._controls,
                      { time: ICONGRID_ANIMATION_TIME_IN,
                        transition: 'easeInOutQuad',
                        opacity: finalOpacity });

        currentView.animate(animationDirection, onComplete);
    },
    
    resetSearch: function() {
        Main.overview.viewSelector.filesSearchEntry.reset();
    },
    
    sizeRedisplay: function() {
        this.iconSize = null;
        this.redisplay();
    },
    
    updateViewLayout: function() {
        [this._views[0], this._views[1], { view: this.oldFolderView }, { view: this.tagView }, { view: this.allTagsView }, { view: this.favoritesView }]
            .filter(_view => _view.view)
            .forEach(_view => _view.view.updateLayout());
    },
    
    redisplay: function() {
        if (!this.redisplayTimeout) {
            // delete current view
            this.getCurrentView().simpleSwitchOut();
            this._viewStack.remove_actor(this.getCurrentView().actor);
            this.getCurrentView().disable();
            
            // delete other views
            this.removeOldFolderView();
            
            ['tagView', 'allTagsView', 'favoritesView'].forEach(prop => {
                if (this[prop] && this[prop] !== this.getCurrentView())
                    this._removeTagsView(prop);
            });
                
            this.tagView = this.allTagsView = this.favoritesView = null;
            
            for (let i = 0; i < this._views.length; i++) {
                if (!this._views[i].view || this._views[i].view == this.getCurrentView())
                    continue;
                if (this._views[i].view.actor)
                    this._viewStack.remove_actor(this._views[i].view.actor);
                this._views[i].view.disable();
                this._views[i].view = null;
            }
        
            //add timeout because it's too fast for viewStack
            let timeout = Mainloop.timeout_add(50, () => {
                Mainloop.source_remove(timeout);
                this._createView(this.currentIndex);
                this.getCurrentView().simpleSwitchIn();
                return GLib.SOURCE_REMOVE;
            });
            //add redisplayTimeout in case of many redisplay queries in a short time
            this.redisplayTimeout = Mainloop.timeout_add(500, () => {
                Mainloop.source_remove(this.redisplayTimeout);
                this.redisplayTimeout = null;
                return GLib.SOURCE_REMOVE;
            });
        } else {
            Mainloop.source_remove(this.redisplayTimeout);
            this.redisplayTimeout = Mainloop.timeout_add(500, () => {
                Mainloop.source_remove(this.redisplayTimeout);
                this.redisplayTimeout = null;
                this.redisplay();
                return GLib.SOURCE_REMOVE;
            });
        }
    },
    
    switchLeft: function() {
        let activeIndex = (this.currentIndex == 0) ? 2 : this.currentIndex - 1;
        this._showView(activeIndex);
    },
    
    switchRight: function() {
        let activeIndex = (this.currentIndex == 2) ? 0 : this.currentIndex + 1;
        this._showView(activeIndex);
    },
    
    enterCurrentLocationInSearch: function() {
        if (this.getCurrentView().directory)
            Main.overview.viewSelector.filesSearchEntry.setLocation(this.getCurrentView().directory);
    },
    
    openHomeDirectory: function() {
        this.openNewDirectory(Gio.File.new_for_path(GLib.get_home_dir())); 
    },
    
    openPreviousDirectory: function() {
        if (this.oldFolderView)
            this.openNewDirectory(this.oldFolderView.directory);
    },
    
    // used in FolderView._onFilesChanged on opening cancelled
    cancelDirectoryOpening: function() {
        if (this.oldFolderView)
            this.openNewDirectory(this.oldFolderView.directory);
        else
            this.openHomeDirectory();
        this.removeOldFolderView();
    },
    
    openTrash: function() {
        let trashDirectory = Gio.File.new_for_uri('trash:///');
        this.openNewDirectory(trashDirectory);
    },
    
    openParentDirectory: function() {
        if (this.desktopMountDirectory && this.directory.equal(this.desktopMountDirectory))
            this.openNewDirectory(Gio.File.new_for_path(GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DESKTOP)));
        else if (this.directory.has_parent(null) && this.directory.get_parent().query_exists(null))
            this.openNewDirectory(this.directory.get_parent());
        else if (this.desktopMountDirectory) {
            let basename = this.directory.get_basename();
            let parentDirectoryPath = this.directory.get_path().slice(0, - basename.length);
            this.openNewDirectory(Gio.File.new_for_path(parentDirectoryPath));
        } else if (this.directory.get_uri() != 'file:///') {
            this.openNewDirectory(Gio.File.new_for_path(GLib.get_home_dir()));
        }
    },
    
    openNewDesktopMountDirectory: function(newDirectory) {
        this.openNewDirectory(newDirectory);
        this.desktopMountDirectory = newDirectory;
    },
    
    openNewDirectory: function(newDirectory) {
        //newDirectory is a GFile object
        //we keep in memory the previous folder view (when we browse we often go back)
        
        this.resetSearch();
        
        if (!newDirectory.query_exists(null)) {
            // "Could not display “%s”." changes to "Could Not Display “%s” in Nautilus 3.32
            let text = (_N("Could not display “%s”.") != "Could not display “%s”.") ? _N("Could not display “%s”.") : _N("Could Not Display “%s”");
            Main.notifyError(text.format(getDisplayName(newDirectory)));
        } else {
            this.recentManager.addPending(!newDirectory.equal(this.startDirectory) ? newDirectory.get_uri() : null);
        }
        
        // check newDirectory, if not: take parent
        while (!newDirectory.query_exists(null)) {
            if (newDirectory.has_parent(null))
                newDirectory = newDirectory.get_parent();
            else
                newDirectory = Gio.File.new_for_path(GLib.get_home_dir());
        }
        
        if (this.desktopMountDirectory && !newDirectory.equal(this.desktopMountDirectory) && !newDirectory.has_prefix(this.desktopMountDirectory))
            this.desktopMountDirectory = null;
        // after a time, this previous folder view is removed in order to stay in sync with the filesystem
        if (!this.timeout && this.oldFolderView) {
            this.removeOldFolderView();
        } else if (this.timeout) {
            Mainloop.source_remove(this.timeout);
        }
        this.timeout = Mainloop.timeout_add(30000, () => {
            Mainloop.source_remove(this.timeout);
            this.timeout = null;
        });
                          
        //start hiding the current view before the new view is loaded
        // index 1 : folder view
        if (this.currentIndex == 1 && !this.settings.get_boolean('fade-in-browsing'))
            this.getCurrentView().simpleSwitchOut();
        else
            this.getCurrentView().animateSwitch(IconGrid.AnimationDirection.OUT);
        this._views[this.currentIndex].control.remove_style_pseudo_class('checked');
        
        if (this.oldFolderView && (newDirectory.equal(this.oldFolderView.directory))) {
            //switch new and previous view
            let temp = this._views[1].view;
            this._views[1].view = this.oldFolderView;
            this.oldFolderView = temp;
        } else {
            //remove previous view and build the new        
            this.removeOldFolderView();
            this.oldFolderView = this._views[1].view;
            this._views[1].view = new FolderView(newDirectory);
            this._viewStack.add_actor(this._views[1].view.actor);
        }
        
        // don't want to keep oldFolderView if it's a non monitored directory (network mounts...)
        if (this.oldFolderView && !this.oldFolderView.getIsMonitored())
            this.removeOldFolderView();
        
        let label = getDisplayName(newDirectory);
        if (label.length > MAX_FOLDER_VIEW_NAME_LENGTH)
            label = label.slice(0,MAX_FOLDER_VIEW_NAME_LENGTH) + "…";
        this._views[1].control.set_label(label);
        this._views[1].control.add_style_pseudo_class('checked');
        if (this.currentIndex == 1 && !this.settings.get_boolean('fade-in-browsing'))
            this.getCurrentView().simpleSwitchIn();
        else
            this._views[1].view.animateSwitch(IconGrid.AnimationDirection.IN);
        
        this.currentIndex = 1;
        this.directory = newDirectory;
        
        if (this._views[1].view.hasListingError)
            this.cancelDirectoryOpening();
    },
    
    removeOldFolderView: function() {
        if (this.oldFolderView) {
            if (this.oldFolderView.actor)
                this._viewStack.remove_actor(this.oldFolderView.actor);
            this.oldFolderView.disable();
            this.oldFolderView = null;
        }
    },
    
    onViewDirectoryDeleted: function(view) {
        // FIXME: if the current index is on favorites or recent when handler receive signal,
        // there is a painful and unintended switch to folder view (highly abstract case).
        // it will be necessary to create a new method in filesDisplay to open a new directory
        // without switching. Here is small trick : a second switch in the other direction
        if (this._views[Views.FOLDER].view == view) {
            let viewIndex = this.currentIndex;
            this.openParentDirectory();
            if (viewIndex != Views.FOLDER)
                this._showView(viewIndex);
        }
        // if the changes concern the oldFolderView(so not displayed), don't open parent, just delete it
        else
            this.removeOldFolderView();
    },
    
    openAllTagsView: function() {
        if (!this.allTagsView) {
            this.allTagsView = new AllTagsView();
            this._viewStack.add_actor(this.allTagsView.actor);
        }
        
        this._openTagsView(this.allTagsView, _("Tags"));
        this._removeTagsView('tagView');
    },
    
    openFavoritesView: function() {
        if (!this.favoritesView) {
            this.favoritesView = new FavoritesView();
            this._viewStack.add_actor(this.favoritesView.actor);
        }
        this._openTagsView(this.favoritesView, _("Favorites"));
    },
    
    openNewTagView: function(tag) {
        this._removeTagsView('tagView');
        this.tagView = new TagView(tag);
        this._viewStack.add_actor(this.tagView.actor);
        this._openTagsView(this.tagView, tag);
    },
    
    _removeTagsView: function(viewProp) {
        if (this[viewProp]) {
            if (this[viewProp].actor)
                this._viewStack.remove_actor(this[viewProp].actor);
            this[viewProp].disable();
            if (this._views[2].view === this[viewProp])
                this._views[2].view = null;
            this[viewProp] = null;
        }
    },
    
    _openTagsView: function(view, label) {
        this.resetSearch();
        
        if (this.currentIndex == 2 && !this.settings.get_boolean('fade-in-browsing'))
            this.getCurrentView().simpleSwitchOut();
        else
            this.getCurrentView().animateSwitch(IconGrid.AnimationDirection.OUT);
        this._views[this.currentIndex].control.remove_style_pseudo_class('checked');
        
        this._views[2].view = view;
        
        if (label.length > MAX_FOLDER_VIEW_NAME_LENGTH)
            label = label.slice(0,MAX_FOLDER_VIEW_NAME_LENGTH) + "…";
        this._views[2].control.set_label(label);
        this._views[2].control.add_style_pseudo_class('checked');
        if (this.currentIndex == 2 && !this.settings.get_boolean('fade-in-browsing'))
            this.getCurrentView().simpleSwitchIn();
        else
            this._views[2].view.animateSwitch(IconGrid.AnimationDirection.IN);
        
        this.currentIndex = 2;
    },
    
    _createView: function(activeIndex) {
        if ((activeIndex == 0)) {
            this._views[0].view = new RecentView(this.recentManager);
            this._viewStack.add_actor(this._views[0].view.actor);
        } else if ((activeIndex == 1)) {
            this._views[1].view = new FolderView(this.directory);
            this._viewStack.add_actor(this._views[1].view.actor);
        } else if ((activeIndex == 2)) {
            if (Main.overview.favoritesManager.hasTracker) {
                this._views[2].view = new AllTagsView();
                this.allTagsView = this._views[2].view;
            } else {
                this._views[2].view = new FavoritesView();
                this.favoritesView = this._views[2].view;
            }
            this._viewStack.add_actor(this._views[2].view.actor);
        }
    },

    _showView: function(activeIndex) {
        this.resetSearch();
        
        if (!this._views[activeIndex].view)
            this._createView(activeIndex);
            
        for (let i = 0; i < this._views.length; i++) {
            if (i == activeIndex) {
                this._views[i].control.add_style_pseudo_class('checked');
                this.currentIndex = i;
            } else {
                this._views[i].control.remove_style_pseudo_class('checked');
            }

            let animationDirection = i == activeIndex ? IconGrid.AnimationDirection.IN :
                                                        IconGrid.AnimationDirection.OUT;
            if (this._views[i].view)
                this._views[i].view.animateSwitch(animationDirection);
        }
    },
    
    switchDefaultView: function() {
        let defaultView = this.settings.get_string('start-view');
        let defaultPath = this.settings.get_string('start-path');
        if ((defaultView == 'folder') && (this.directory.get_path() != defaultPath)) {
            if ((defaultPath.indexOf('~') !== -1) || !Gio.File.new_for_path(defaultPath).query_exists(null))
                defaultPath = GLib.get_home_dir();
            this.openNewDirectory(Gio.File.new_for_path(defaultPath));
        }
        else if ((defaultView != "previous") && (PrefsViews[defaultView] != this.currentIndex))
            this._showView(PrefsViews[defaultView]);
    },

    _onAllocatedSizeChanged: function(actor, width, height) {
        let box = new Clutter.ActorBox();
        box.x1 = box.y1 =0;
        box.x2 = width;
        box.y2 = height;
        box = this._viewStack.get_theme_node().get_content_box(box);
        let availWidth = box.x2 - box.x1;
        let availHeight = box.y2 - box.y1;
        for (let i = 0; i < this._views.length; i++)
            if (this._views[i].view && this._views[i].view._grid && this._views[i].view.actor)
                this._views[i].view.adaptToSize(availWidth, availHeight);
    },
    
    sortBy: function(criterion) {
        if (this.getFolderView() && this.getFolderView() == this.getCurrentView())
            this.getFolderView().sortBy(criterion);
    },
    
    reverseSorting: function() {
        if (this.getFolderView() && this.getFolderView() == this.getCurrentView())
            this.getFolderView().reverseSorting();
    },
    
    eraseSortMetadata: function() {
        if (this.getFolderView() && this.getFolderView() == this.getCurrentView())
            this.getFolderView().eraseSortMetadata();
    },
    
    getFolderView: function() {
        return this._views[Views.FOLDER].view;
    },
    
    getCurrentView: function() {
        return this._views[this.currentIndex].view;
    }
});



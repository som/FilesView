/*
 * Copyright 2018 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2018 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 6 */

const Clutter = imports.gi.Clutter;
const Lang = imports.lang;
const Meta = imports.gi.Meta;
const Signals = imports.signals;

const Config = imports.misc.config;
const ExtensionUtils = imports.misc.extensionUtils;
const Main = imports.ui.main;
const Layout = imports.ui.layout;

const Me = ExtensionUtils.getCurrentExtension();
const GS_VERSION = Config.PACKAGE_VERSION;
const UUID = Me.uuid.replace(/@/gi, '_at_').replace(/[^a-z0-9+_-]/gi, '_');

const CORNER = {
    'top-left': { x: 0, y: 0, angle: 0, vx1: 0, vx2: 0, vy1: 0, vy2: 1, vdir: 'POSITIVE_X', hx1: 0, hx2: 1, hy1: 0, hy2: 0, hdir: 'POSITIVE_Y' },
    'top-right': { x: 1, y: 0, angle: 90, vx1: 0, vx2: 0, vy1: 0, vy2: 1, vdir: 'NEGATIVE_X', hx1: -1, hx2: 0, hy1: 0, hy2: 0, hdir: 'POSITIVE_Y' },
    'bottom-left': { x: 0, y: 1, angle: 270, vx1: 0, vx2: 0, vy1: -1, vy2: 0, vdir: 'POSITIVE_X', hx1: 0, hx2: 1, hy1: 0, hy2: 0, hdir: 'NEGATIVE_Y' },
    'bottom-right': { x: 1, y: 1, angle: 180, vx1: 0, vx2: 0, vy1: -1, vy2: 0, vdir: 'NEGATIVE_X', hx1: -1, hx2: 0, hy1: 0, hy2: 0, hdir: 'NEGATIVE_Y' }
};

const HOT_CORNER_PRESSURE_THRESHOLD = 90; // pixels (100 in gnome-shell)

var HotCorner = new Lang.Class({
    Name: UUID + '-HotCorner',
    
    _init: function(position, sensitivity) {
        if (!CORNER[position])
            return;
        
        let monitor = Main.layoutManager.primaryMonitor;
        let cornerX = monitor.x + CORNER[position].x * monitor.width;
        let cornerY = monitor.y + CORNER[position].y * monitor.height;
        let corner = new Layout.HotCorner(Main.layoutManager, monitor, cornerX, cornerY);
        this.corner = corner;
        
        if (GS_VERSION < '3.33.0') {
            corner._ripple1.rotation_angle_z = CORNER[position].angle;
            corner._ripple2.rotation_angle_z = CORNER[position].angle;
            corner._ripple3.rotation_angle_z = CORNER[position].angle;
            
            corner._ripple1.set_text_direction(Clutter.TextDirection.LTR);
            corner._ripple2.set_text_direction(Clutter.TextDirection.LTR);
            corner._ripple3.set_text_direction(Clutter.TextDirection.LTR);
        } else {
            corner._ripples._ripple1.rotation_angle_z = CORNER[position].angle;
            corner._ripples._ripple2.rotation_angle_z = CORNER[position].angle;
            corner._ripples._ripple3.rotation_angle_z = CORNER[position].angle;
            
            corner._ripples._ripple1.set_text_direction(Clutter.TextDirection.LTR);
            corner._ripples._ripple2.set_text_direction(Clutter.TextDirection.LTR);
            corner._ripples._ripple3.set_text_direction(Clutter.TextDirection.LTR);
        }
        
        corner._pressureBarrier._trigger = _trigger;
        corner.setBarrierSize = setBarrierSize;
        
        this.setSensitivity(sensitivity);
        
        let size = Main.layoutManager.panelBox.height;
        corner.setBarrierSize(size, position);
        
        corner._pressureBarrier.connect('trigger-bis', () => {
            if (monitor.inFullscreen)
                return;
            if (GS_VERSION < '3.33.0')
                corner._rippleAnimation();
            else
                corner._ripples.playAnimation(corner._x, corner._y);
            this.emit('hot-corner-event');
        });
    },
    
    disable: function() {
        if (!this.corner)
            return;
        
        // since 3.34, ripples are destroyed in Layout.HotCorner.destroy
        if (GS_VERSION < '3.33.0') {
            let [ripple1, ripple2, ripple3] = [this.corner._ripple1, this.corner._ripple2, this.corner._ripple3];
            
            Main.layoutManager.uiGroup.remove_actor(ripple1);
            Main.layoutManager.uiGroup.remove_actor(ripple2);
            Main.layoutManager.uiGroup.remove_actor(ripple3);
            
            ripple1.destroy();
            ripple2.destroy();
            ripple3.destroy();
        }
        
        this.corner.destroy();
    },
    
    setSensitivity: function(sensitivity) {
        if (sensitivity !== undefined) {
            sensitivity = Math.min(Math.max(sensitivity, 0), 1);
            let threshold = 200 - sensitivity*200;
            this.corner._pressureBarrier._threshold = threshold;
        } else {
            this.corner._pressureBarrier._threshold = HOT_CORNER_PRESSURE_THRESHOLD;
        }
    },
});
Signals.addSignalMethods(HotCorner.prototype);

const _trigger = function() {
    this._isTriggered = true;
    this.emit('trigger-bis');
    this._reset();
};

const setBarrierSize = function(size, position) {
    if (this._verticalBarrier) {
        this._pressureBarrier.removeBarrier(this._verticalBarrier);
        this._verticalBarrier.destroy();
        this._verticalBarrier = null;
    }

    if (this._horizontalBarrier) {
        this._pressureBarrier.removeBarrier(this._horizontalBarrier);
        this._horizontalBarrier.destroy();
        this._horizontalBarrier = null;
    }

    let c = CORNER[position];
    
    if (size > 0) {
        this._verticalBarrier = new Meta.Barrier({ display: global.display,
                                                   x1: this._x + c.vx1*size, x2: this._x + c.vx2*size, y1: this._y + c.vy1*size, y2: this._y + c.vy2*size,
                                                   directions: Meta.BarrierDirection[c.vdir] });
        this._horizontalBarrier = new Meta.Barrier({ display: global.display,
                                                     x1: this._x + c.hx1*size, x2: this._x + c.hx2*size, y1: this._y + c.hy1*size, y2: this._y + c.hy2*size,
                                                     directions: Meta.BarrierDirection[c.hdir] });

        this._pressureBarrier.addBarrier(this._verticalBarrier);
        this._pressureBarrier.addBarrier(this._horizontalBarrier);
    }
};


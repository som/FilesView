/*
 * Copyright 2019 charlesg99 <https://github.com/charlesg99>
 * Copyright 2019 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2019 Charles Gagnon
 * SPDX-FileCopyrightText: 2019 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 6 */

const Clutter = imports.gi.Clutter;
const GObject = imports.gi.GObject;
const St = imports.gi.St;

const Config = imports.misc.config;

const GS_VERSION = Config.PACKAGE_VERSION;
const Tweener = GS_VERSION < '3.33.0' ? imports.ui.tweener : null;

/*
 * Copied from 'defineClass' function in Dash to panel extension
 * author: charlesg99 (https://github.com/charlesg99)
 * https://github.com/home-sweet-gnome/dash-to-panel/blob/master/utils.js
 * https://github.com/home-sweet-gnome/dash-to-panel/commit/8ae941ba8bf08e2ffd57eff405a18b6cbf319bf9
 *
 * Signals added to GObject classes
 */

var Class = function(classDef) {
    let parentProto = classDef.Extends ? classDef.Extends.prototype : null;
    
    if (GS_VERSION < '3.31.9') {
        if (parentProto && (classDef.Extends.name || classDef.Extends.toString()).indexOf('FilesView') < 0) {
            classDef.callParent = function() {
                let args = Array.prototype.slice.call(arguments);
                let func = args.shift();

                classDef.Extends.prototype[func].apply(this, args);
            };
        }

        return new imports.lang.Class(classDef);
    }

    let isGObject = parentProto instanceof GObject.Object;
    let needsSuper = parentProto && !isGObject;
    let getParentArgs = function(args) {
        let parentArgs = [];

        (classDef.ParentConstrParams || parentArgs).forEach(p => {
            if (p.constructor === Array) {
                let param = args[p[0]];
                
                parentArgs.push(p[1] ? param[p[1]] : param);
            } else {
                parentArgs.push(p);
            }
        });

        return parentArgs;
    };
    
    let C = eval(
        '(class C ' + (needsSuper ? 'extends Object' : '') + ' { ' +
        '     constructor(...args) { ' +
                  (needsSuper ? 'super(...getParentArgs(args));' : '') +
                  (needsSuper || !parentProto ? 'this._init(...args);' : '') +
        '     }' +
        '     callParent(...args) { ' +
        '         let func = args.shift(); ' +
        '         if (!(func === \'_init\' && needsSuper))' +
        '             super[func](...args); ' +
        '     }' +    
        '})'
    );

    if (parentProto) {
        Object.setPrototypeOf(C.prototype, parentProto);
        Object.setPrototypeOf(C, classDef.Extends);
    } 
    
    Object.defineProperty(C, 'name', { value: classDef.Name });
    Object.keys(classDef)
          .filter(k => classDef.hasOwnProperty(k) && classDef[k] instanceof Function)
          .forEach(k => C.prototype[k] = classDef[k]);

    if (isGObject) {
        if (classDef.Signals)
            C = GObject.registerClass({ Signals: classDef.Signals }, C);
        else
            C = GObject.registerClass(C);
    }
    
    return C;
};

/* Wrapper for Tweener and Clutter.Actor.ease
 * 'ease' is itself a javascript wrapper and not a Clutter method.
 * Adjustements have specific 'ease' method.
 *
 * See gnome-shell misc/environment.js for informations about ease implementation.
 */

const MODES = { 'customMode': 'CUSTOM_MODE',
                'linear': 'LINEAR',
                'easeInQuad': 'EASE_IN_QUAD',
                'easeOutQuad': 'EASE_OUT_QUAD',
                'easeInOutQuad': 'EASE_IN_OUT_QUAD',
                'easeInCubic': 'EASE_IN_CUBIC',
                'easeOutCubic': 'EASE_OUT_CUBIC',
                'easeInOutCubic': 'EASE_IN_OUT_CUBIC',
                'easeInQuart': 'EASE_IN_QUART',
                'easeOutQuart': 'EASE_OUT_QUART',
                'easeInOutQuart': 'EASE_IN_OUT_QUART',
                'easeInQuint': 'EASE_IN_QUINT',
                'easeOutQuint': 'EASE_OUT_QUINT',
                'easeInOutQuint': 'EASE_IN_OUT_QUINT',
                'easeInSine': 'EASE_IN_SINE',
                'easeOutSine': 'EASE_OUT_SINE',
                'easeInOutSine': 'EASE_IN_OUT_SINE',
                'easeInExpo': 'EASE_IN_EXPO',
                'easeOuExpo': 'EASE_OUT_EXPO',
                'easeInOutExpo': 'EASE_IN_OUT_EXPO',
                'easeInCirc': 'EASE_IN_CIRC',
                'easeOutCirc': 'EASE_OUT_CIRC',
                'easeInOutCirc': 'EASE_IN_OUT_CIRC',
                'easeInElastic': 'EASE_IN_ELASTIC',
                'easeOutElastic': 'EASE_OUT_ELASTIC',
                'easeInOutElastic': 'EASE_IN_OUT_ELASTIC',
                'easeInBack': 'EASE_IN_BACK',
                'easeOutBack': 'EASE_OUT_BACK',
                'easeInOutBack': 'EASE_IN_OUT_BACK',
                'easeInBounce': 'EASE_IN_BOUNCE',
                'easeOutBounce': 'EASE_OUT_BOUNCE',
                'easeInOutBounce': 'EASE_IN_OUT_BOUNCE',
                'steps': 'STEPS',
                'stepStart': 'STEP_START',
                'stepEnd': 'STEP_END',
                'cubicBezier': 'CUBIC_BEZIER',
                'ease': 'EASE',
                'easeIn': 'EASE_IN',
                'easeOut': 'EASE_OUT',
                'easeInOut': 'EASE_IN_OUT',
                'animationLast': 'ANIMATION_LAST' };

function removeTweens(actor) {
    if (Tweener) {
        Tweener.removeTweens(actor);
    } else {
        // Adjustments are not Clutter actors
        // and so don't have a remove_all_transitions method.
        if (actor instanceof St.Adjustment)
            return;
        
        actor.remove_all_transitions();
    }
}

function addTween(actor, params) {
    if (Tweener) {
        Tweener.addTween(actor, params);
        return;
    }
    
    if (params.time) {
        params.duration = 1000 * params.time;
        delete params.time;
    }
    
    if (params.delay)
        params.delay = 1000 * params.delay;
    
    if (params.transition && MODES[params.transition])
        params.mode = Clutter.AnimationMode[MODES[params.transition]];
    else
        params.mode = Clutter.AnimationMode.LINEAR;
    delete params.transition;
    
    let value;
    if (params.hasOwnProperty('value')) {
        value = params.value;
        delete params.value;
    }
    
    if (value !== undefined)
        actor.ease(value, params);
    else
        actor.ease(params);
}



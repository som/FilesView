/*
 * Copyright 2018 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2018 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 6 */

const Clutter = imports.gi.Clutter;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const GObject = imports.gi.GObject;
const Lang = imports.lang;
const Pango = imports.gi.Pango;
const Shell = imports.gi.Shell;
const St = imports.gi.St;

const Animation = imports.ui.animation;
const CheckBox = imports.ui.checkBox;
const Config = imports.misc.config;
var Dialog;
try { Dialog = imports.ui.dialog; } catch(e) {} // GS 3.26+
const ExtensionUtils = imports.misc.extensionUtils;
const Main = imports.ui.main;
const ModalDialog = imports.ui.modalDialog;
const Params = imports.misc.params;
const ShellEntry = imports.ui.shellEntry;
const ShellMountOperation = imports.ui.shellMountOperation;
const Util = imports.misc.util;

const Me = ExtensionUtils.getCurrentExtension();
const Mang = Me.imports.mang;

const _ = imports.gettext.domain(Me.metadata['gettext-domain']).gettext;
const _GS = imports.gettext.domain('gnome-shell').gettext;
const _GSE = imports.gettext.domain('gnome-shell-extensions').gettext;
const _GTK = imports.gettext.domain('gtk30').gettext;
const _N = imports.gettext.domain('nautilus').gettext;

const GS_VERSION = Config.PACKAGE_VERSION;
const UUID = Me.uuid.replace(/@/gi, '_at_').replace(/[^a-z0-9+_-]/gi, '_');

// used by search.js
function getChildrenNames(directory) {
    let childrenNames = [];
    
    let enumerator;
    try {
        enumerator = directory.enumerate_children('standard::name,standard::type', Gio.FileQueryInfoFlags.NONE, null);
    } catch(e) {
        return [];
    }
    
    let i = 0;
    let fileInfo = enumerator.next_file(null);
    while (fileInfo && i < 500) {
        childrenNames.push(fileInfo.get_name() + (fileInfo.get_file_type() == Gio.FileType.DIRECTORY ? "/" : ""));
        fileInfo = enumerator.next_file(null);
        i++;
    }
    enumerator.close(null);
    
    childrenNames.sort((a, b) => {
        if ((a[0] === ".") && (b[0] !== "."))
            return 1;
        else if ((a[0] !== ".") && (b[0] === "."))
            return -1;
        else
            return a.localeCompare(b);
    });
    
    return childrenNames;
}

function hasChildren(directory) {
    let enumerator;
    try {
        enumerator = directory.enumerate_children('', Gio.FileQueryInfoFlags.NONE, null);
    } catch(e) {
        return false;
    }
    
    let fileInfo = enumerator.next_file(null);
    
    return fileInfo ? true : false;
}

function openDirectory(file) {
    Main.overview.viewSelector.filesDisplay.openNewDirectory(file);
}

function launchFile(file, info) {
    if (!info)
        info = file.query_info('standard::', Gio.FileQueryInfoFlags.NONE, null);
    
    try {
        Gio.AppInfo.launch_default_for_uri(file.get_uri(), global.create_app_launch_context(0, -1));
        Main.overview.hide();
    } catch(e) {
        if (info.get_is_symlink()) {
            let target = info.get_symlink_target();
            if (!target || !GLib.file_test(target, GLib.FileTest.EXISTS)) {
                Main.notify(_N("The link “%s” is broken.").format(info.get_display_name()),
                            target ? _N("This link cannot be used because its target “%s” doesn’t exist.").format(target) :
                                     _N("This link cannot be used because it has no target."));
                return;
            }
        }
        Main.notifyError(_GS("Failed to launch “%s”").format(info.get_display_name()), e.message);
    }
}

function openFile(file) {
    let info = file.query_info('standard::', Gio.FileQueryInfoFlags.NONE, null);
    let type = info.get_file_type();
    
    if (type == Gio.FileType.DIRECTORY) {
        openDirectory(file);
        return;
    }
    
    if (type == Gio.FileType.REGULAR) {
        launchFile(file, info);
        return;
    }
    
    if (type == Gio.FileType.SYMBOLIC_LINK) {
        let target = info.get_symlink_target();
        let targetFile = target ? Gio.File.new_for_path(target) : null;
        
        if (!targetFile || !targetFile.query_exists(null)) {
            Main.notify(_N("The link “%s” is broken.").format(info.get_display_name()),
                        target ? _N("This link cannot be used because its target “%s” doesn’t exist.").format(target) :
                                 _N("This link cannot be used because it has no target."));
            return;
        }
        
        openFile(targetFile);
        return;
    }
    
    // smb shortcuts in network:///
    if (type == Gio.FileType.SHORTCUT) {
        let targetUri = info.get_attribute_string('standard::target-uri');
        if (!targetUri)
            return;
        
        let targetFile = Gio.File.new_for_uri(targetUri);
        try {
            openFile(targetFile);
        } catch(e) {
            if (e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.NOT_MOUNTED))
                mountEnclosingVolume(targetFile, () => {
                    if (targetFile.query_exists(null))
                        openFile(targetFile);
                });
            else
                Main.notifyError(_GS("Error"), e.message);
        }
        return;
    }
    
    // the file is a children of 'network:///' or 'computer:///'
    if (type == Gio.FileType.MOUNTABLE) {
        let targetUri = info.get_attribute_string('standard::target-uri');
        let targetFile = targetUri ? Gio.File.new_for_uri(targetUri) : null;
        
        if (targetFile && targetFile.query_exists(null)) {
            openFile(targetFile);
        } else {
            mountMountable(file, () => {
                let newInfo = file.query_info('standard::target-uri', Gio.FileQueryInfoFlags.NONE, null);
                targetUri = newInfo.get_attribute_string('standard::target-uri');
                if (targetUri) {
                    targetFile = Gio.File.new_for_uri(targetUri);
                    if (targetFile.query_exists(null)) {
                        openFile(targetFile);
                    } else {
                        targetFile = Gio.File.new_for_uri(targetUri.toLowerCase());
                        if (targetFile.query_exists(null))
                            openFile(targetFile);
                    }
                }
            });
        }
        return;
    }
}

function mountEnclosingVolume(file, callback) {
    let source = { get_icon: () => Gio.ThemedIcon.new_from_names(['dialog-password-symbolic']) };
    let op = new MountOperation(source);
    
    file.mount_enclosing_volume(0, op.mountOp, null, (object, result) => {
        try {
            op.close();
            object.mount_enclosing_volume_finish(result);
        } catch(e) {
            if (e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.FAILED_HANDLED))
                // e.g. user canceled the password dialog
                return;
            Main.notifyError(_GSE("Failed to mount volume for “%s”").format(file.get_uri()), e.message);
            return;
        }
        
        if (callback)
            callback();
        
    });
}

function mountMountable(file, callback) {
    let source = { get_icon: () => Gio.ThemedIcon.new_from_names(['dialog-password-symbolic']) };
    let op = new MountOperation(source);
    
    file.mount_mountable(0, op.mountOp, null, (object, result) => {
        try {
            op.close();
            object.mount_mountable_finish(result);
        } catch(e) {
            if (e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.FAILED_HANDLED))
                // e.g. user canceled the password dialog
                return;
            if (!e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.ALREADY_MOUNTED)) {
                Main.notifyError(_GSE("Failed to mount volume for “%s”").format(file.get_uri()), e.message);
                return;
            }
        }
        
        if (callback)
            callback();
    });
}

function mountVolume(volume, callback) {
    let source = { get_icon: () => volume.get_symbolic_icon() };
    let op = new MountOperation(source);
    
    volume.mount(0, op.mountOp, null, (object, result) => {
        try {
            op.close();
            object.mount_finish(result);
        } catch(e) {
            if (e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.FAILED_HANDLED))
                // e.g. user canceled the password dialog
                return;
            Main.notifyError(_GSE("Failed to mount volume for “%s”").format(volume.get_name()), e.message);
            return;
        }
        
        if (callback)
            callback();
    });
}

function stopDrive(drive) {
    let source = { get_icon: () => drive.get_symbolic_icon() };
    
    let op = new ShellMountOperation.ShellMountOperation(source);
    drive.stop(0, op.mountOp, null, (object, result) => {
        try {
            op.close();
            object.stop_finish(result);
        } catch(e) {
            if (e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.FAILED_HANDLED))
                // e.g. user canceled the password dialog
                return;
            Main.notifyError(_("Failed to stop drive “%s”").format(drive.get_name()), e.message);
            return;
        }
    });
}

var MountOperation = new Mang.Class({
    Name: UUID + '-ShellMountOperation',
    Extends: ShellMountOperation.ShellMountOperation,
    ParentConstrParams: [[0], [1]],
    
    _init: function(source, params) {
        this.callParent('_init', source, params);
        this._gicon = source.get_icon();
    },

    _onAskPassword: function(op, message, defaultUser, defaultDomain, flags) {
        if (this._existingDialog) {
            this._dialog = this._existingDialog;
            this._dialog.reaskPassword();
        } else {
            this._dialog = new MountPasswordDialog(message, this._gicon, flags);
        }

        this._dialogId = this._dialog.connect('response',
            (object, choice, password, anonymous, remember, hiddenVolume, systemVolume, pim) => {
                
                if (choice == -1) {
                    this.mountOp.reply(Gio.MountOperationResult.ABORTED);
                } else {
                    if (anonymous) {
                        this.mountOp.set_anonymous(true);
                    } else if (password) {
                        if (remember)
                            this.mountOp.set_password_save(Gio.PasswordSave.PERMANENTLY);
                        else
                            this.mountOp.set_password_save(Gio.PasswordSave.NEVER);
                        this.mountOp.set_password(password);
                    }
                    
                    if (this.mountOp.set_is_tcrypt_hidden_volume)
                        this.mountOp.set_is_tcrypt_hidden_volume(hiddenVolume);
                    if (this.mountOp.set_is_tcrypt_system_volume)
                        this.mountOp.set_is_tcrypt_system_volume(systemVolume);
                    if (this.mountOp.set_pim)
                        this.mountOp.set_pim(pim);
                    
                    this.mountOp.reply(Gio.MountOperationResult.HANDLED);
                }
            });
        this._dialog.open();
    }
});

const REMEMBER_MOUNT_PASSWORD_KEY = 'remember-mount-password';

// Based on ShellMountPasswordDialog (shellMountOperation.js).
// Fix https://gitlab.gnome.org/GNOME/gnome-shell/issues/860
// and add "Anonymous" option.
const MountPasswordDialog334 = new Mang.Class({
    Name: UUID + '-MountPasswordDialog334',
    Extends: ModalDialog.ModalDialog,
    ParentConstrParams: [{ styleClass: 'prompt-dialog', shouldFadeIn: true, shouldFadeOut: true }],
    Signals: { 'response': { param_types: [GObject.TYPE_INT,
                                           GObject.TYPE_STRING,
                                           GObject.TYPE_BOOLEAN,
                                           GObject.TYPE_BOOLEAN,
                                           GObject.TYPE_BOOLEAN,
                                           GObject.TYPE_BOOLEAN,
                                           GObject.TYPE_UINT] } },
    
    _init: function(message, icon, flags) {
        let strings = message.split('\n');
        let title = strings.shift() || null;
        let body = strings.shift() || null;
        this.callParent('_init', { styleClass: 'prompt-dialog', shouldFadeIn: true, shouldFadeOut: true });

        let content = new MessageDialogContent({ icon: icon, title: title, body: body });
        this.contentLayout.add_child(content);
        
        if (flags & Gio.AskPasswordFlags.ANONYMOUS_SUPPORTED) {
            this._anonymousChoice = new CheckBox.CheckBox();
            this._anonymousChoice.getLabelActor().text = _GTK("_Anonymous").replace(" (_D)", "").replace("_", "");
            this._anonymousChoice.actor.checked = false;
            content.messageBox.add_child(this._anonymousChoice.actor);
        } else {
            this._anonymousChoice = null;
        }

        this._passwordBox = new St.BoxLayout({ vertical: false, style_class: 'prompt-dialog-password-box' });
        content.messageBox.add_child(this._passwordBox);

        this._passwordLabel = new St.Label(({ style_class: 'prompt-dialog-password-label',
                                              text: _GS("Password"),
                                              y_align: St.Align.MIDDLE }));
        this._passwordBox.add_child(this._passwordLabel);

        this._passwordEntry = new St.Entry({ style_class: 'prompt-dialog-password-entry',
                                             text: "",
                                             can_focus: true,
                                             x_expand: true });
        ShellEntry.addContextMenu(this._passwordEntry, { isPassword: true });
        this._passwordEntry.clutter_text.connect('activate', Lang.bind(this, this._onEntryActivate));
        this._passwordEntry.clutter_text.set_password_char('\u25cf'); // ● U+25CF BLACK CIRCLE
        this._passwordBox.add_child(this._passwordEntry);
        this.setInitialKeyFocus(this._passwordEntry);

        this._errorMessageLabel = new St.Label({ style_class: 'prompt-dialog-error-label',
                                                 text: _GS("Sorry, that didn’t work. Please try again.") });
        this._errorMessageLabel.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
        this._errorMessageLabel.clutter_text.line_wrap = true;
        this._errorMessageLabel.hide();
        content.messageBox.add_child(this._errorMessageLabel);
        
        this._emptyPasswordErrorMessageLabel = new St.Label({ style_class: 'prompt-dialog-error-label',
                                                              text: _("A password is requested,\notherwise you should check “%s”")
                                                                    .format(_GTK("_Anonymous").replace(" (_D)", "").replace("_", "")) });
        this._emptyPasswordErrorMessageLabel.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
        this._emptyPasswordErrorMessageLabel.clutter_text.line_wrap = true;
        this._emptyPasswordErrorMessageLabel.hide();
        content.messageBox.add_child(this._emptyPasswordErrorMessageLabel);

        if (flags & Gio.AskPasswordFlags.SAVING_SUPPORTED) {
            this._rememberChoice = new CheckBox.CheckBox();
            this._rememberChoice.getLabelActor().text = _GS("Remember Password");
            this._rememberChoice.actor.checked =
                global.settings.get_boolean(REMEMBER_MOUNT_PASSWORD_KEY);
            content.messageBox.add_child(this._rememberChoice.actor);
        } else {
            this._rememberChoice = null;
        }
        
        if (this._anonymousChoice)
            this._anonymousChoice.actor.connect('clicked', Lang.bind(this, this._onAnonymousClicked));

        let buttons = [{ label: _GS("Cancel"),
                         action: Lang.bind(this, this._onCancelButton),
                         key: Clutter.KEY_Escape
                       },
                       { label: _GS("Unlock"),
                         action: Lang.bind(this, this._onUnlockButton),
                         default: true
                       }];

        this.setButtons(buttons);
    },
    
    _onAnonymousClicked: function() {
        let anonymous = this._anonymousChoice.actor.checked;
        let opacity = anonymous ? 120 : 255;
        
        if (anonymous)
            this._emptyPasswordErrorMessageLabel.hide();
        
        this._passwordEntry.clutter_text.editable = !anonymous;
        this._passwordEntry.clutter_text.selectable = !anonymous;
        [this._passwordEntry, this._passwordLabel].concat(this._rememberChoice ? [this._rememberChoice.actor] : [])
                                                  .forEach(actor => {
                                                      actor.reactive = actor.can_focus = !anonymous;
                                                      actor.set_opacity(opacity);
                                                  });
    },

    reaskPassword: function() {
        this._passwordEntry.set_text('');
        this._errorMessageLabel.show();
    },

    _onCancelButton: function() {
        this.emit('response', -1, '', false, false, false, false, 0);
    },

    _onUnlockButton: function() {
        this._onEntryActivate();
    },
    
    _onPasswordChanged: function() {
        if (!this._passwordEntry.get_text())
            return;

        this._emptyPasswordErrorMessageLabel.hide();

        if (this._passwordChangedHandlerId) {
            this._passwordEntry.disconnect(this._passwordChangedHandlerId);
            this._passwordChangedHandlerId = 0;
        }
    },

    _onEntryActivate: function() {
        if (!this._passwordEntry.get_text() &&
            (!this._anonymousChoice || !this._anonymousChoice.actor.checked)) {
            
            this._emptyPasswordErrorMessageLabel.show();
            
            if (!this._passwordChangedHandlerId)
                this._passwordChangedHandlerId = this._passwordEntry.clutter_text.connect('text-changed', Lang.bind(this, this._onPasswordChanged));
            
            return;
        }
        
        global.settings.set_boolean(REMEMBER_MOUNT_PASSWORD_KEY,
            this._rememberChoice && this._rememberChoice.actor.checked);
        this.emit('response', 1,
            this._passwordEntry.get_text(),
            this._anonymousChoice &&
            this._anonymousChoice.actor.checked,
            this._rememberChoice &&
            this._rememberChoice.actor.checked,
            false, false, 0);
    }
});

const MountPasswordDialog336 = new Mang.Class({
    Name: UUID + '-MountPasswordDialog336',
    Extends: ModalDialog.ModalDialog,
    ParentConstrParams: [{ styleClass: 'prompt-dialog', shouldFadeIn: true, shouldFadeOut: true }],
    Signals: { 'response': { param_types: [GObject.TYPE_INT,
                                           GObject.TYPE_STRING,
                                           GObject.TYPE_BOOLEAN,
                                           GObject.TYPE_BOOLEAN,
                                           GObject.TYPE_BOOLEAN,
                                           GObject.TYPE_BOOLEAN,
                                           GObject.TYPE_UINT] } },
    
    _init: function(message, icon, flags) {
        let strings = message.split('\n');
        let title = strings.shift() || null;
        let description = strings.shift() || null;
        this.callParent('_init', { styleClass: 'prompt-dialog', shouldFadeIn: true, shouldFadeOut: true });
        
        let disksApp = Shell.AppSystem.get_default().lookup_app('org.gnome.DiskUtility.desktop');
        
        let content = new Dialog.MessageDialogContent({ title: title, description: description });
        
        let passwordGridLayout = new Clutter.GridLayout({ orientation: Clutter.Orientation.VERTICAL });
        let passwordGrid = new St.Widget({
            style_class: 'prompt-dialog-password-grid',
            layout_manager: passwordGridLayout,
        });
        passwordGridLayout.hookup_style(passwordGrid);

        let rtl = passwordGrid.get_text_direction() === Clutter.TextDirection.RTL;
        let curGridRow = 0;

        if (flags & Gio.AskPasswordFlags.TCRYPT) {
            this._hiddenVolume = new CheckBox.CheckBox(_GS("Hidden Volume"));
            content.add_child(this._hiddenVolume);

            this._systemVolume = new CheckBox.CheckBox(_GS("Windows System Volume"));
            content.add_child(this._systemVolume);

            this._keyfilesCheckbox = new CheckBox.CheckBox(_GS("Uses Keyfiles"));
            this._keyfilesCheckbox.connect("clicked", this._onKeyfilesCheckboxClicked.bind(this));
            content.add_child(this._keyfilesCheckbox);

            this._keyfilesLabel = new St.Label({ visible: false });
            this._keyfilesLabel.clutter_text.set_markup(
                _GS("To unlock a volume that uses keyfiles, use the <i>%s</i> utility instead.").format(disksApp.get_name())
            );
            this._keyfilesLabel.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
            this._keyfilesLabel.clutter_text.line_wrap = true;
            content.add_child(this._keyfilesLabel);

            this._pimEntry = new St.PasswordEntry({
                style_class: 'prompt-dialog-password-entry',
                hint_text: _GS('PIM Number'),
                can_focus: true,
                x_expand: true,
            });
            this._pimEntry.clutter_text.connect('activate', this._onEntryActivate.bind(this));
            ShellEntry.addContextMenu(this._pimEntry);

            if (rtl)
                passwordGridLayout.attach(this._pimEntry, 1, curGridRow, 1, 1);
            else
                passwordGridLayout.attach(this._pimEntry, 0, curGridRow, 1, 1);
            curGridRow += 1;
        } else {
            this._hiddenVolume = null;
            this._systemVolume = null;
            this._pimEntry = null;
        }
        
        this._passwordEntry = new St.PasswordEntry({
            style_class: 'prompt-dialog-password-entry',
            hint_text: _GS('Password'),
            can_focus: true,
            x_expand: true,
        });
        this._passwordEntry.clutter_text.connect('activate', this._onEntryActivate.bind(this));
        this.setInitialKeyFocus(this._passwordEntry);
        ShellEntry.addContextMenu(this._passwordEntry);

        this._workSpinner = new Animation.Spinner(ShellMountOperation.WORK_SPINNER_ICON_SIZE, {
            animate: true,
        });

        if (rtl) {
            passwordGridLayout.attach(this._workSpinner, 0, curGridRow, 1, 1);
            passwordGridLayout.attach(this._passwordEntry, 1, curGridRow, 1, 1);
        } else {
            passwordGridLayout.attach(this._passwordEntry, 0, curGridRow, 1, 1);
            passwordGridLayout.attach(this._workSpinner, 1, curGridRow, 1, 1);
        }
        curGridRow += 1;

        let warningBox = new St.BoxLayout({ vertical: true });

        let capsLockWarning = new ShellEntry.CapsLockWarning();
        warningBox.add_child(capsLockWarning);

        this._errorMessageLabel = new St.Label({
            style_class: 'prompt-dialog-error-label',
            opacity: 0,
        });
        this._errorMessageLabel.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
        this._errorMessageLabel.clutter_text.line_wrap = true;
        warningBox.add_child(this._errorMessageLabel);
        
        this._emptyPasswordErrorMessageLabel = new St.Label({ style_class: 'prompt-dialog-error-label',
                                                              text: _("A password is requested,\notherwise you should check “%s”")
                                                                    .format(_GTK("_Anonymous").replace(" (_D)", "").replace("_", "")) });
        this._emptyPasswordErrorMessageLabel.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
        this._emptyPasswordErrorMessageLabel.clutter_text.line_wrap = true;
        this._emptyPasswordErrorMessageLabel.hide();
        warningBox.add_child(this._emptyPasswordErrorMessageLabel);

        passwordGridLayout.attach(warningBox, 0, curGridRow, 2, 1);

        content.add_child(passwordGrid);
        
        if (flags & Gio.AskPasswordFlags.ANONYMOUS_SUPPORTED) {
            this._anonymousChoice = new CheckBox.CheckBox(_GTK("_Anonymous").replace(" (_D)", "").replace("_", ""));
            this._anonymousChoice.checked = false;
            this._anonymousChoice.connect('clicked', this._onAnonymousClicked.bind(this));
            content.add_child(this._anonymousChoice);
        } else {
            this._anonymousChoice = null;
        }
        
        if (flags & Gio.AskPasswordFlags.SAVING_SUPPORTED) {
            this._rememberChoice = new CheckBox.CheckBox(_GS("Remember Password"));
            this._rememberChoice.checked =
                global.settings.get_boolean(ShellMountOperation.REMEMBER_MOUNT_PASSWORD_KEY);
            content.add_child(this._rememberChoice);
        } else {
            this._rememberChoice = null;
        }

        this.contentLayout.add_child(content);

        this._defaultButtons = [{
            label: _GS("Cancel"),
            action: this._onCancelButton.bind(this),
            key: Clutter.KEY_Escape,
        }, {
            label: _GS("Unlock"),
            action: this._onUnlockButton.bind(this),
            default: true,
        }];

        this._usesKeyfilesButtons = [{
            label: _GS("Cancel"),
            action: this._onCancelButton.bind(this),
            key: Clutter.KEY_Escape,
        }, {
            label: _GS("Open %s").format(disksApp.get_name()),
            action: this._onOpenDisksButton.bind(this),
            default: true,
        }];

        this.setButtons(this._defaultButtons);
    },
    
    _onAnonymousClicked: function() {
        let anonymous = this._anonymousChoice.checked;
        let opacity = anonymous ? 120 : 255;
        
        if (anonymous)
            this._emptyPasswordErrorMessageLabel.hide();
        
        this._passwordEntry.clutter_text.editable = !anonymous;
        this._passwordEntry.clutter_text.selectable = !anonymous;
        [this._passwordEntry].concat(this._rememberChoice ? [this._rememberChoice] : [])
                             .forEach(actor => {
                                 actor.reactive = actor.can_focus = !anonymous;
                                 actor.set_opacity(opacity);
                             });
    },
    
    reaskPassword: function() {
        this._workSpinner.stop();
        this._passwordEntry.set_text('');
        this._errorMessageLabel.text = _GS('Sorry, that didn’t work. Please try again.');
        this._errorMessageLabel.opacity = 255;

        Util.wiggle(this._passwordEntry);
    },

    _onCancelButton: function() {
        this.emit('response', -1, '', false, false, false, false, 0);
    },

    _onUnlockButton: function() {
        this._onEntryActivate();
    },
    
    _onPasswordChanged: function() {
        if (!this._passwordEntry.get_text())
            return;

        this._emptyPasswordErrorMessageLabel.hide();

        if (this._passwordChangedHandlerId) {
            this._passwordEntry.disconnect(this._passwordChangedHandlerId);
            this._passwordChangedHandlerId = 0;
        }
    },

    _onEntryActivate: function() {
        let pim = 0;
        if (this._pimEntry !== null) {
            pim = this._pimEntry.get_text();

            if (isNaN(pim)) {
                this._pimEntry.set_text('');
                this._errorMessageLabel.text = _GS('The PIM must be a number or empty.');
                this._errorMessageLabel.opacity = 255;
                return;
            }

            this._errorMessageLabel.opacity = 0;
        }
        
        if (!pim && !this._passwordEntry.get_text() &&
            (!this._anonymousChoice || !this._anonymousChoice.checked)) {
            
            this._emptyPasswordErrorMessageLabel.show();
            
            if (!this._passwordChangedHandlerId)
                this._passwordChangedHandlerId = this._passwordEntry.clutter_text.connect('text-changed',this._onPasswordChanged.bind(this));
            
            return;
        }

        global.settings.set_boolean(ShellMountOperation.REMEMBER_MOUNT_PASSWORD_KEY,
            this._rememberChoice && this._rememberChoice.checked);

        this._workSpinner.play();
        this.emit('response', 1,
            this._passwordEntry.get_text(),
            this._anonymousChoice &&
            this._anonymousChoice.checked,
            this._rememberChoice &&
            this._rememberChoice.checked,
            this._hiddenVolume &&
            this._hiddenVolume.checked,
            this._systemVolume &&
            this._systemVolume.checked,
            parseInt(pim));
    },

    _onKeyfilesCheckboxClicked: function() {
        let useKeyfiles = this._keyfilesCheckbox.checked;
        this._passwordEntry.reactive = !useKeyfiles;
        this._passwordEntry.can_focus = !useKeyfiles;
        this._passwordEntry.clutter_text.editable = !useKeyfiles;
        this._passwordEntry.clutter_text.selectable = !useKeyfiles;
        this._pimEntry.reactive = !useKeyfiles;
        this._pimEntry.can_focus = !useKeyfiles;
        this._pimEntry.clutter_text.editable = !useKeyfiles;
        this._pimEntry.clutter_text.selectable = !useKeyfiles;
        this._rememberChoice.reactive = !useKeyfiles;
        this._rememberChoice.can_focus = !useKeyfiles;
        this._keyfilesLabel.visible = useKeyfiles;
        this.setButtons(useKeyfiles ? this._usesKeyfilesButtons : this._defaultButtons);
    },

    _onOpenDisksButton: function() {
        let app = Shell.AppSystem.get_default().lookup_app('org.gnome.DiskUtility.desktop');
        if (app) {
            app.activate();
        } else {
            Main.notifyError(
                _GS("Unable to start %s").format(app.get_name()),
                _GS("Couldn’t find the %s application").format(app.get_name())
            );
        }
        this._onCancelButton();
    }
});

const MountPasswordDialog = GS_VERSION < '3.36.0' ? MountPasswordDialog334 : MountPasswordDialog336;

var ConfirmDialog = new Mang.Class({
    Name: UUID + '-ConfirmDialog',
    Extends: ModalDialog.ModalDialog,
    ParentConstrParams: [{ styleClass: 'prompt-dialog', destroyOnClose: true, shouldFadeIn: true, shouldFadeOut: true }],

    _init: function(params) {
        params = Params.parse(params, { iconName: 'dialog-question-symbolic',
                                        title: null,
                                        subtitle: null,
                                        body: null,
                                        cancelText: "Cancel",
                                        confirmText: "Ok",
                                        confirmAction: () => {},
                                        cancelAction: () => {} });
        this.callParent('_init', { styleClass: 'prompt-dialog',
                                   destroyOnClose: true,
                                   shouldFadeIn: true,
                                   shouldFadeOut: true });
                      
        let icon = new Gio.ThemedIcon({ name: params.iconName });
        let content = new MessageDialogContent({ icon: icon, title: params.title, subtitle: params.subtitle, body: params.body });
        content._title.clutter_text.line_wrap = true;
        content._title.clutter_text.ellipsize = 0;
        this.contentLayout.add_child(content);
        
        this.cancelButton = this.addButton({ label: params.cancelText,
                                             action: () => {
                                                  this.close();
                                                  params.cancelAction();
                                              },
                                             default: true,
                                             key: Clutter.KEY_Escape });
                         
        this.confirmButton = this.addButton({ label: params.confirmText,
                                              action: () => {
                                                  this.close();
                                                  params.confirmAction();
                                              } });
        this.confirmButton.add_style_class_name('files-view-confirm-button');
    }
});

// Forked from https://gitlab.gnome.org/GNOME/gnome-shell/blob/3.26.2/js/ui/dialog.js
// doesn't exist in version < 3.26
var MessageDialogContent = new Lang.Class({
    Name: UUID + '-MessageDialogContent',
    Extends: St.BoxLayout,
    Properties: {
        'icon': GObject.ParamSpec.object('icon', 'icon', 'icon',
                                         GObject.ParamFlags.READWRITE |
                                         GObject.ParamFlags.CONSTRUCT,
                                         Gio.Icon.$gtype),
        'title': GObject.ParamSpec.string('title', 'title', 'title',
                                          GObject.ParamFlags.READWRITE |
                                          GObject.ParamFlags.CONSTRUCT,
                                          ''),
        'subtitle': GObject.ParamSpec.string('subtitle', 'subtitle', 'subtitle',
                                             GObject.ParamFlags.READWRITE |
                                             GObject.ParamFlags.CONSTRUCT,
                                             ''),
        'body': GObject.ParamSpec.string('body', 'body', 'body',
                                         GObject.ParamFlags.READWRITE |
                                         GObject.ParamFlags.CONSTRUCT,
                                         '')
    },

    _init: function(params) {
        this._icon = new St.Icon({ y_align: Clutter.ActorAlign.START });
        this._title = new St.Label({ style_class: 'headline' });
        this._subtitle = new St.Label();
        this._body = new St.Label();

        ['icon', 'title', 'subtitle', 'body'].forEach(prop => {
            this['_' + prop].add_style_class_name('message-dialog-' + 'prop');
        });

        this._subtitle.clutter_text.set_ellipsize(Pango.EllipsizeMode.NONE);
        this._subtitle.clutter_text.set_line_wrap(true);
        this._body.clutter_text.set_ellipsize(Pango.EllipsizeMode.NONE);
        this._body.clutter_text.set_line_wrap(true);
        

        if (!params.hasOwnProperty('style_class'))
            params.style_class = 'message-dialog-main-layout';

        this.parent(params);

        this.messageBox = new St.BoxLayout({ style_class: 'message-dialog-content',
                                             x_expand: true,
                                             vertical: true });

        this.messageBox.add_child(this._title);
        this.messageBox.add_child(this._subtitle);
        this.messageBox.add_child(this._body);

        this.add_child(this._icon);
        this.add_child(this.messageBox);
    },

    get icon() {
        return this._icon.gicon;
    },

    get title() {
        return this._title.text;
    },

    get subtitle() {
        return this._subtitle.text;
    },

    get body() {
        return this._body.text;
    },

    set icon(icon) {
        this._icon.set_gicon(icon);
        this._icon.visible = (icon != null);
        this.notify('icon');
    },

    set title(title) {
        this._setLabel(this._title, 'title', title);
    },

    set subtitle(subtitle) {
        this._setLabel(this._subtitle, 'subtitle', subtitle);
    },

    set body(body) {
        this._setLabel(this._body, 'body', body);
    },

    _setLabel: function(label, prop, value) {
        label.set_text(value || '');
        label.visible = (value != null);
        this.notify(prop);
    },

    insertBeforeBody: function(actor) {
        this.messageBox.insert_child_below(actor, this._body);
    }
});


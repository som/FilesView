/*
 * Copyright 2018 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2018 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 6 */

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const GnomeDesktop = imports.gi.GnomeDesktop;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Signals = imports.signals;
const System = imports.system;

const ExtensionUtils = imports.misc.extensionUtils;
const Main = imports.ui.main;
const MessageTray = imports.ui.messageTray;

const Me = ExtensionUtils.getCurrentExtension();
const Misc = Me.imports.misc;

const _ = imports.gettext.domain(Me.metadata['gettext-domain']).gettext;
const _N = imports.gettext.domain('nautilus').gettext;

const MAX_CHILDREN_COUNT = 10000; // .cache/fontconfig ...
const MAX_THUMBNAILS = 200;
const MAX_THUMBNAILS_WARNING = 30;
const UUID = Me.uuid.replace(/@/gi, '_at_').replace(/[^a-z0-9+_-]/gi, '_');

var countFolderChildren = function(enumerator) {
    let n = 0;
    let maxChildrenCountReached = false;
    let childFileInfo = enumerator.next_file(null);
    
    while (childFileInfo) {
        if (n == MAX_CHILDREN_COUNT) {
            maxChildrenCountReached = true;
            break;
        }
        if (n % 1000 == 0)
            System.gc();
        n++;
        childFileInfo = enumerator.next_file(null);
    }
    return [n, maxChildrenCountReached];
};

var FolderManager = new Lang.Class({
    Name: UUID + '-FolderManager',
    
    _init : function(directory) {
        this.settings = Me.settings;
        this.directory = directory;
        this.files = [];
        this.mountFiles = [];
        this.allFiles = [];
        
        try {
            this.directoryMonitor = this.directory.monitor_directory(Gio.FileMonitorFlags.WATCH_MOVES, null);
        } catch(e) {
            // can not monitoring smb ...
        }
        
        if (this.directoryMonitor) {
            this.directoryChangedHandler = this.directoryMonitor.connect('changed', (monitor, file, otherFile, eventType) => {
                if (this.directory.query_exists(null)) {
                    this._onDirectoryChanged(file, otherFile, eventType);
                } else {
                    this.emit('directory-deleted');
                }
            });
        } else {
            this.fileManagerWorkingHandler = Main.overview.fileManager.connect('working-changed', () => {
                // when directories are not monitored (network...), view must be manually redisplayed
                 if (!Main.overview.fileManager.isWorking)
                     this.updateFolder();
            });
        }
        
        // monitor mounts
        this.mountsDisplayedInDirectory = this.directory && this.directory.get_path() &&
                                          this.directory.get_path() == GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DESKTOP) &&
                                          this.settings.get_boolean('show-devices-desktop');
        this.volumeMonitor = Gio.VolumeMonitor.get();
        
        this.mountRemovedHandler = this.volumeMonitor.connect('mount-removed', (obj, mount) => {
            if (!this.directory.query_exists(null)) {
                this.emit('directory-deleted');
            } else if (this.mountsDisplayedInDirectory) {
                this._removeMount(mount);
            }
        });
        
        //add mounts if it's Desktop folder
        if (this.mountsDisplayedInDirectory) {
            let mounts = this.volumeMonitor.get_mounts();
            mounts.forEach(mount => this._addMount(mount, true));
            
            this.mountAddedHandler = this.volumeMonitor.connect('mount-added', (obj, mount) => this._changeMount(mount));
            this.mountChangedHandler = this.volumeMonitor.connect('mount-changed', (obj, mount) => this._addMount(mount));
        }
    },
    
    disable: function() {
        if (this.volumeMonitor && this.mountRemovedHandler) {
            this.volumeMonitor.disconnect(this.mountRemovedHandler);
            this.mountRemovedHandler = null;
        }
        if (this.volumeMonitor && this.mountAddedHandler) {
            this.volumeMonitor.disconnect(this.mountAddedHandler);
            this.mountAddedHandler = null;
        }
        if (this.volumeMonitor && this.mountChangedHandler) {
            this.volumeMonitor.disconnect(this.mountChangedHandler);
            this.mountChangedHandler = null;
        }        
        
        if (this.directoryMonitor && this.directoryChangedHandler) {
            this.directoryMonitor.disconnect(this.directoryChangedHandler);
            this.directoryMonitor.cancel();
        }
        if (this.fileManagerWorkingHandler) {
            Main.overview.fileManager.disconnect(this.fileManagerWorkingHandler);
            this.fileManagerWorkingHandler = null;
        }
    },
    
    _removeMount: function(mount) {
        let mountFile = mount.get_default_location();
        let mountFileIndex = this.mountFiles.map(file => file.get_uri()).indexOf(mountFile.get_uri());
        if (mountFileIndex == -1)
            return;
        
        this.mountFiles.splice(mountFileIndex, 1);
        this.allFiles = [this.directory].concat(this.files, this.mountFiles);
        this.emit('file-removed', mountFile);
    },
    
    _addMount: function(mount, notToEmit) {
        // avoid double devices
        if (mount.is_shadowed())
            return;
        let mountFile = mount.get_default_location();
        // to hide blank cd for example
        // file.is_native() excludes networks mount (ftp, smb, ...)
        if (!mountFile.get_path())
            return;
        
        mountFile.isDesktopMount = true;
        mountFile.mountIcon = mount.get_icon();
        mountFile.mountName = mount.get_name();
        
        this.mountFiles.push(mountFile);
        this.allFiles = [this.directory].concat(this.files, this.mountFiles);
        let allIndex = this.allFiles.indexOf(mountFile);
        if (!notToEmit)
            this.emit('file-added', mountFile, allIndex);
    },
    
    _changeMount: function(mount) {
        this._removeMount(mount);
        this._addMount(mount);
    },
    
    _onDirectoryChanged: function(file, otherFile, eventType) {
        if (!file || !file.get_uri() || (otherFile && !otherFile.get_uri()))
            return;
        if (eventType == Gio.FileMonitorEvent.CHANGES_DONE_HINT || eventType == Gio.FileMonitorEvent.PRE_UNMOUNT || eventType == Gio.FileMonitorEvent.ATTRIBUTE_CHANGED) {
            return;
        } else if (eventType == Gio.FileMonitorEvent.CHANGED || eventType == Gio.FileMonitorEvent.RENAMED) {
            if (this.files.map(a => a.get_uri()).indexOf(file.get_uri()) != -1) {
                this._removeFile(file);
                this._addFile(eventType == Gio.FileMonitorEvent.RENAMED ? otherFile : file);
                
            // here file is a temporary file (i.e. '.goutputstream-...') and otherFile is the file to update
            } else if (eventType == Gio.FileMonitorEvent.RENAMED) {
                this._removeFile(otherFile);
                this._addFile(otherFile);
            }
        } else if (eventType == Gio.FileMonitorEvent.DELETED || eventType == Gio.FileMonitorEvent.MOVED_OUT || eventType == Gio.FileMonitorEvent.UNMOUNTED) {
            this._removeFile(file);
        } else if (eventType == Gio.FileMonitorEvent.CREATED || eventType == Gio.FileMonitorEvent.MOVED_IN) {
            this._addFile(file);
        }
    },
    
    _removeFile: function(file) {
        let fileIndex = this.files.map(a => a.get_uri()).indexOf(file.get_uri());
        if (fileIndex == -1)
            return;
        
        this.files.splice(fileIndex, 1);
        this.allFiles = [this.directory].concat(this.files, this.mountFiles);
        this.emit('file-removed', file);
    },
    
    _addFile: function(file) {
        if (!file.query_exists(null))
            return;
        
        // need .info and .isDir in _sortFiles()
        try {
            file.info = file.query_info('*', Gio.FileQueryInfoFlags.NONE, null);
        } catch(e) {
            if (GLib.quark_to_string(e.domain) == 'g-vfs-error-quark') {
            // GLib.Error g-vfs-error-quark: Cache invalid, retry (internally handled)
                try {
                    file.info = this.file.query_info('*', Gio.FileQueryInfoFlags.NONE, null);
                } catch(e) { }
            }
            
            if (!file.info) {
                return;
            }
        }
        file.isDir = file.info.get_file_type() == Gio.FileType.DIRECTORY;
        this.files.push(file);
        this._sortFiles();
        
        this.allFiles = [this.directory].concat(this.files, this.mountFiles);
        let allIndex = this.allFiles.indexOf(file);
        this.emit('file-added', file, allIndex);
    },
    
    updateFolder: function() {
        this._listFiles();
        this._sortFiles();
        this.allFiles = [this.directory].concat(this.files, this.mountFiles);
        this.directory.isGoParent = true;
        this.emit('changed');
    },
    
    resortFiles: function() {
        this._sortFiles();
        this.allFiles = [this.directory].concat(this.files, this.mountFiles);
        this.directory.isGoParent = true;
        this.emit('changed');
    },
    
    _listFiles: function() {
        this.files = [];
        let showHiddenFiles = this.settings.get_boolean('show-hidden-files');
        
        let enumerator;
        //"try" prevent permission error : "Gio.IOErrorEnum: Error opening directory"
        try {
            enumerator = this.directory.enumerate_children('*', Gio.FileQueryInfoFlags.NONE, null);
        } catch(e) {
            if (e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.PERMISSION_DENIED)) {
                Main.notify(_N("Don’t have permission to access the requested location."), this.directory.get_path());
            } else {
                Main.notify(_N("Unable to access the requested location."), e.message);
            }
            // wait until the view is initialized to cancel directory opening (at the end of FilesDisplay.openNewDirectory())
            this.hasListingError = true;
            return;
        }
        
        //we get a list of gfile in this.files
        let i = 0;
        let fileInfo = enumerator.next_file(null);
        while (fileInfo) {
            if (i % 1000 == 0)
                System.gc();
            let file = enumerator.get_child(fileInfo);
            file.info = fileInfo;
            file.isDir = fileInfo.get_file_type() == Gio.FileType.DIRECTORY;
            
            // ignore hidden files if required
            if (showHiddenFiles || (!fileInfo.get_is_hidden() && !(file.get_basename().endsWith("~") && !file.isDir))) {
                this.files[i] = file;
                i = i + 1;
            }
            
            fileInfo = enumerator.next_file(null);
        }
        enumerator.close(null);
    },
    
    _sortFiles: function() {
        let syncNautilusSorting = this.settings.get_boolean('sync-nautilus-sorting');
        let sortFoldersBeforeFiles = this.settings.get_boolean('sort-folders-before-files');
        
        let [sortedBy, sortReversed] = ['name', false];
        if (syncNautilusSorting) {
            let directorySortingInfo = this.directory.query_info('metadata::nautilus-icon-view-sort-by,metadata::nautilus-icon-view-sort-reversed', Gio.FileQueryInfoFlags.NONE, null);
            sortedBy = directorySortingInfo.get_attribute_string('metadata::nautilus-icon-view-sort-by');
            sortReversed = directorySortingInfo.get_attribute_string('metadata::nautilus-icon-view-sort-reversed') == 'true' ? true : false;
        } else {
            sortedBy = this.settings.get_string('sort-by');
            sortReversed = this.settings.get_boolean('sort-reversed');
        }
        
        if (sortedBy == 'size') {
            for (let i = 0; i < this.files.length; i++) {
                if (!this.files[i].isDir || this.files[i].childrenCount)
                    continue;
                
                // no count for network mounts
                if (!this.files[i].is_native()) {
                    this.files[i].childrenCount = 0;
                    continue;
                }
                
                try {
                    let childEnumerator = this.files[i].enumerate_children('', Gio.FileQueryInfoFlags.NONE, null);
                    [this.files[i].childrenCount, this.files[i].maxChildrenCountReached] = countFolderChildren(childEnumerator);
                    childEnumerator.close(null);
                } catch(e) {
                    this.files[i].childrenCount = 0;
                }
            }
        }
        
        //sort with "." and without uppercase
        this.files.sort(function(a,b) {
            // unused
            let sortedByExtension = false;
            if (sortedByExtension) {
                let aSplit = a.get_basename().split(".");
                let aExt = aSplit.length > 1 && aSplit[aSplit.length - 2] ? aSplit[aSplit.length - 1] : "";
                let bSplit = b.get_basename().split(".");
                let bExt = bSplit.length > 1 && bSplit[bSplit.length - 2] ? bSplit[bSplit.length - 1] : "";
                
                if (aExt != bExt && (aExt || bExt))
                    return aExt.localeCompare(bExt);
            }
            
            if (sortedBy == 'type') {
                // set empty value to directory type in order to have directories first
                let aType = a.info.get_file_type() == Gio.FileType.DIRECTORY ? '' : a.info.get_content_type();
                let bType = b.info.get_file_type() == Gio.FileType.DIRECTORY ? '' : b.info.get_content_type();
                if (aType != bType)
                    return aType.localeCompare(bType);
            }
            
            if (sortedBy == 'modification date') {
                let aTime = a.info.get_attribute_uint64('time::modified');
                let bTime = b.info.get_attribute_uint64('time::modified');
                return aTime - bTime;
            }
            
            if (sortedBy == 'size') {
                // divide childrenNumber in order not to mix directories and other files
                let aSize = a.isDir ? a.childrenCount/1000 : a.info.get_size();
                let bSize = b.isDir ? b.childrenCount/1000 : b.info.get_size();
                return aSize - bSize;
            }
            
            if ((a.get_basename()[0] === ".") && (b.get_basename()[0] !== "."))
                return 1;
            else if ((a.get_basename()[0] !== ".") && (b.get_basename()[0] === "."))
                return -1;
            else
                return a.get_basename().localeCompare(b.get_basename());
        });
                  
        if (sortReversed)        
            this.files.reverse();
        
        if (sortFoldersBeforeFiles) {
            this.files.sort(function(a,b) {
                if (a.isDir && !b.isDir)
                    return -1;
                else if (!a.isDir && b.isDir)
                    return 1;
                else
                    return 0;
            });
        }
    },
    
    sortBy: function(criterion) {
        if (this.settings.get_boolean('sync-nautilus-sorting')) {
            this.directory.set_attribute_string('metadata::nautilus-icon-view-sort-by', criterion, Gio.FileQueryInfoFlags.NONE, null);
            let sortReversedString = (criterion == 'modification date' || criterion == 'size') ? 'true' : 'false';
            this.directory.set_attribute_string('metadata::nautilus-icon-view-sort-reversed', sortReversedString, Gio.FileQueryInfoFlags.NONE, null);
            this.resortFiles();
        } else {
            this.settings.set_string('sort-by', criterion);
            let sortReversed = (criterion == 'modification date' || criterion == 'size') ? true : false;
            this.settings.set_boolean('sort-reversed', sortReversed);
        }
        this.notify(_("Sorted by %s").format(_(criterion)));
    },
    
    reverseSorting: function() {
        if (this.settings.get_boolean('sync-nautilus-sorting')) {
            let directorySortingInfo = this.directory.query_info('metadata::nautilus-icon-view-sort-by,metadata::nautilus-icon-view-sort-reversed', Gio.FileQueryInfoFlags.NONE, null);
            
            // Nautilus contraints 'metadata::nautilus-icon-view-sort-reversed' for 'type' and 'size'
            let sortedBy = directorySortingInfo.get_attribute_string('metadata::nautilus-icon-view-sort-by');
            if (sortedBy == 'size' || sortedBy == 'type')
                return;
            
            let sortReversedString = directorySortingInfo.get_attribute_string('metadata::nautilus-icon-view-sort-reversed') == 'true' ? 'false' : 'true';
            this.directory.set_attribute_string('metadata::nautilus-icon-view-sort-reversed', sortReversedString, Gio.FileQueryInfoFlags.NONE, null);
            this.resortFiles();
        } else {
            this.settings.set_boolean('sort-reversed', !this.settings.get_boolean('sort-reversed'));
        }
        this.notify(_("Sort order reversed"));
    },
    
    eraseSortMetadata: function() {
        if (this.settings.get_boolean('sync-nautilus-sorting')) {
            this.directory.set_attribute('metadata::nautilus-icon-view-sort-by', Gio.FileAttributeType.INVALID, null, Gio.FileQueryInfoFlags.NONE, null);
            this.directory.set_attribute('metadata::nautilus-icon-view-sort-reversed', Gio.FileAttributeType.INVALID, null, Gio.FileQueryInfoFlags.NONE, null);
            this.resortFiles();
            this.notify(_("Sort metadata of %s erased").format(this.directory.get_basename()));
        }
    },
    
    notify: function(msg, details) {
        if (!this.notificationSource) {
            this.notificationSource = new MessageTray.SystemNotificationSource();
            this.notificationSource.connect('destroy', () => this.notificationSource = null);
        }
            
        if (!Main.messageTray.contains(this.notificationSource))
            Main.messageTray.add(this.notificationSource);
        
        let notification = null;
        if (this.notificationSource.notifications.length == 0) {
            notification = new MessageTray.Notification(this.notificationSource, msg, details);
            notification.setTransient(true);
        } else {
            notification = this.notificationSource.notifications[0];
            notification.update(msg, details);
            notification.setTransient(true);
        }
        
        if (this.notificationSource.notifications.indexOf(notification) == -1)
            if (this.notificationSource.showNotification)
                this.notificationSource.showNotification(notification);
            else
                this.notificationSource.notify(notification); // GS 3.34-
    },
    
    generateThumbnails: function(large) {
        let size = large ? GnomeDesktop.DesktopThumbnailSize.LARGE : GnomeDesktop.DesktopThumbnailSize.NORMAL;
        // 'new GnomeDesktop.DesktopThumbnailFactory(size)' doesn't work
        let factory = GnomeDesktop.DesktopThumbnailFactory.new(size);
        
        let filesToThumbnailize = this.files.filter(file => {
            return !file.isDir &&
                   (!file.info.has_attribute('thumbnail::path') || !file.info.get_attribute_boolean('thumbnail::is-valid')) &&
                   factory.can_thumbnail(file.get_uri(), file.info.get_content_type(), file.info.get_modification_date_time().to_unix());
        });
        
        let thumbnailize = () => {
            filesToThumbnailize.forEach(file => {
                // using async (timeout) doesn't seem to affect velocity
                Mainloop.idle_add(() => {
                    let pixbuf = factory.generate_thumbnail(file.get_uri(), file.info.get_content_type());
                    if (pixbuf) {
                        factory.save_thumbnail(pixbuf, file.get_uri(), file.info.get_modification_date_time().to_unix());
                        this._removeFile(file);
                        this._addFile(file);
                    }
                    
                    return GLib.SOURCE_REMOVE;
                });
            });
        };
        
        if (filesToThumbnailize.length >= MAX_THUMBNAILS) {
            return;
        } else if (filesToThumbnailize.length >= MAX_THUMBNAILS_WARNING) {
            let confirmThumbnailizeDialog = new Misc.ConfirmDialog({ iconName: 'dialog-warning-symbolic',
                                                                     title: _N("Thumbnails"),
                                                                     body: _N("Show thumbnails:") + " " + filesToThumbnailize.length,
                                                                     cancelText: _N("Cancel"),
                                                                     confirmText: _N("_OK").replace(" (_O)", "").replace("(_O)", "").replace("_", ""),
                                                                     confirmAction: thumbnailize });
            confirmThumbnailizeDialog.open();
        } else {
            thumbnailize();
        }
    }
});
Signals.addSignalMethods(FolderManager.prototype);

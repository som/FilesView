/*
 * Copyright 2018 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2018 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 6 */

const ByteArray = imports.byteArray;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Signals = imports.signals;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Convenience = Me.imports.convenience;

const MAX_RECENTS = 72;
const MAX_RECENT_FOLDERS = 24;
const DECREMENT = 1 / (MAX_RECENT_FOLDERS - 1);
const FIRST_INCREMENT = 0.5;
const INCREMENT = 1;
const MIN_FOLDER_OPEN_TIME = 5; // minimum open time in seconds to add the folder to recent items
const SAVE_TIMEOUT = 60 * 1000; // 60 seconds
const UUID = Me.uuid.replace(/@/gi, '_at_').replace(/[^a-z0-9+_-]/gi, '_');

const RecentFolderInfo = new Lang.Class({
    Name: UUID + '-RecentFolderInfo',
    
    // data: { uri, modified }
    _init: function(data) {
        this.data = data;
        this.data.count = FIRST_INCREMENT;
    },
    
    existsAndIsDir: function() {
        // query_file_type return Gio.FileType.UNKOWN if the file does not exist
        return Gio.File.new_for_uri(this.data.uri).query_file_type(Gio.FileQueryInfoFlags.NONE, null) == Gio.FileType.DIRECTORY;
    },
    
    get_modified: function() {
        return this.data.modified;
    },
    
    set_modified: function(timestamp) {
        this.data.modified = timestamp;
        this.data.count += INCREMENT ;
    },
    
    decrement: function() {
        this.data.count -= DECREMENT;
    },
    
    get_count: function() {
        return this.data.count;
    },
    
    get_uri: function() {
        return this.data.uri;
    }
});

/*
 * Gtk.RecentManager plus recent and frequent open folders
 *
 * Goal:
 * Give the user the capacity to find recent and frequent open folders in the 'Recent' tab.
 *
 * Implementation:
 * 'open' means 'displayed during MIN_FOLDER_OPEN_TIME seconds or more'.
 * Each time a folder is open, it is added to a list if not already present, its count is incremented by 1
 * and others list folders are decremented by (1 / (MAX_RECENT_FOLDERS - 1)).
 * Folders with a negative count are removed.
 * So the list is a mix between recent-open folders and less-recent-but-frequent folders and
 * should keep a length lower than MAX_RECENT_FOLDERS.
 * Finally the list is merged with GTK recent files to be displayed in the 'Recent' tab.
 *
 * Privacy:
 * The list is saved in the user data directory (near the 'recently-used.xbel' GTK file)
 * or not according to the GNOME privacy settings.
*/
var RecentManager = new Lang.Class({
    Name: UUID + '-RecentManager',
    
    _init: function() {
        this.gtkManager = Gtk.RecentManager.get_default();
        this.gtkManager.connect('changed', () => this.emit('changed'));
        this.folderItems = [];
        
        this.persistentFile = Gio.File.new_for_path(GLib.build_filenamev([GLib.get_user_data_dir(), Me.metadata['recently-open-persistent-filename']]));
        
        this.privacySettings = Convenience.getSettings('org.gnome.desktop.privacy');
        this.privacySettingsHandler = this.privacySettings.connect('changed::remember-recent-files', Lang.bind(this, this._updatePersistence));
        this._updatePersistence(); 
        
        this._loadPersistent();
    },
    
    get_items: function() {
        let folderItems = this.folderItems.filter(item => item.existsAndIsDir());
        let folderUris = folderItems.map(item => item.get_uri());
        
        let fileItems = this.gtkManager.get_items()
                                       .filter(item => item.exists() && !item.get_private_hint() && folderUris.indexOf(item.get_uri()) == -1)
                                       .sort((a, b) => b.get_modified() - a.get_modified())
                                       .slice(0, MAX_RECENTS - folderItems.length);
        
        return fileItems.concat(folderItems)
                        .sort((a, b) => b.get_modified() - a.get_modified());
    },
    
    remove_item: function(uri) {
        let index = this.folderItems.map(item => item.get_uri()).indexOf(uri);
        if (index != -1) {
            this.folderItems = this.folderItems.filter(item => item.get_uri() != uri);
            this._savePersistent();
            this.emit('changed');
        } else {
            this.gtkManager.remove_item(uri);
        }
    },
    
    addPending: function(uri) {
        // uri may be null (case where directory is the start directory).
        // timestamp in seconds in order to follow Gtk.RecentManager.
        let timestamp = Math.round(Date.now() / 1000);
        
        if (this.pendingUri && timestamp - this.timestamp > MIN_FOLDER_OPEN_TIME) {
            this.folderItems.forEach(item => item.decrement());
            
            let index = this.folderItems.map(item => item.get_uri()).indexOf(this.pendingUri);
            if (index != -1) {
                this.folderItems[index].set_modified(this.timestamp);
            } else {
                let item = new RecentFolderInfo({ uri: this.pendingUri, modified: this.timestamp });
                this.folderItems.push(item);
            }
            
            this.folderItems = this.folderItems.filter(item => item.get_count() > 0);
            this._savePersistent();
            this.emit('changed');
        }
        
        this.pendingUri = uri;
        this.timestamp = timestamp;
    },
    
    _deletePersistentFile: function() {
        if (this.saveTimeout) {
            Mainloop.source_remove(this.saveTimeout);
            this.saveTimeout = null;
        }
        
        if (this.persistentFile.query_exists(null))
            try {
                this.persistentFile.delete(null);
            } catch(e) {
                logError(e);
            }
    },
    
    _updatePersistence: function() {
        if (this.privacySettings.settingsSchema.has_key('remember-recent-files') && this.privacySettings.get_boolean('remember-recent-files')) {
            this.persistence = true;
        } else {
            this.persistence = false;
            this._deletePersistentFile();
        }
    },
    
    _savePersistent: function() {
        if (!this.persistence)
            return;
        
        if (this.saveTimeout)
            Mainloop.source_remove(this.saveTimeout);
        
        this.saveTimeout = Mainloop.timeout_add(SAVE_TIMEOUT, () => {
            // 1 line per folder item
            let contents = '[\n  ' + this.folderItems.map(item => JSON.stringify(item.data)).join(',\n  ') + '\n]';
            try {
                this.persistentFile.replace_contents(contents, null, false, Gio.FileCreateFlags.NONE, null);
            } catch(e) {
                logError(e);
            }
            this.saveTimeout = null;
            return false;
        });
    },
    
    _loadPersistent: function() {
        if (!this.persistence || !this.persistentFile.query_exists(null))
            return;
        
        let [success, contents] = this.persistentFile.load_contents(null);
        if (!success)
            return;
        
        if (contents instanceof Uint8Array)
            contents = ByteArray.toString(contents);
        
        this.folderItems = JSON.parse(contents).map(data => new RecentFolderInfo(data));
    },
    
    disable: function() {
        this.privacySettings.disconnect(this.privacySettingsHandler);
        this._deletePersistentFile();
    }
});
Signals.addSignalMethods(RecentManager.prototype);

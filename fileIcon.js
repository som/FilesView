/*
 * Copyright 2018 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2018 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 6 */

/*
 * This is bases on GNOME Shell ui.appDisplay.js/part2
 * https://gitlab.gnome.org/GNOME/gnome-shell/blob/master/js/ui/appDisplay.js
 */

const Clutter = imports.gi.Clutter;
const Gdk = imports.gi.Gdk;
const GdkPixbuf = imports.gi.GdkPixbuf;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Signals = imports.signals;
const Shell = imports.gi.Shell;
const St = imports.gi.St;

const Config = imports.misc.config;
const DND = imports.ui.dnd;
const ExtensionUtils = imports.misc.extensionUtils;
const Main = imports.ui.main;
const Params = imports.misc.params;
const PopupMenu = imports.ui.popupMenu;
const Util = imports.misc.util;

const Me = ExtensionUtils.getCurrentExtension();
const FolderManager = Me.imports.folderManager;
const Mang = Me.imports.mang;
const Misc = Me.imports.misc;
const Gettext = imports.gettext.domain(Me.metadata['gettext-domain']);
const _ = Gettext.gettext;
const _n = Gettext.ngettext;
const _N = imports.gettext.domain('nautilus').gettext;
const _GS = imports.gettext.domain('gnome-shell').gettext;

const GS_VERSION = Config.PACKAGE_VERSION;
const MainOverviewControls = GS_VERSION < '3.36.0' ? Main.overview._controls : Main.overview._overview._controls;
const IconGrid = GS_VERSION < '3.37.0' ? imports.ui.iconGrid : Me.imports.ui.iconGrid336;
const UUID = Me.uuid.replace(/@/gi, '_at_').replace(/[^a-z0-9+_-]/gi, '_');

// St.DirectionType or Gtk.DirectionType
const DirectionType = {
    TAB_FORWARD: 0,
    TAB_BACKWARD: 1,
    UP: 2,
    DOWN: 3,
    LEFT: 4,
    RIGHT: 5
}

const MENU_POPUP_TIMEOUT = 600;
const LONG_LABEL_HOVER_TIMEOUT = 0.125;
const ICON_ZOOM_TIME = 0.25;
const ICON_ZOOM_SCALE = 1.3;
const THUMB_ZOOM_SCALE = 1.6;
const SIDE_CONTROLS_ANIMATION_TIME = 0.16; // OverviewControls.SIDE_CONTROLS_ANIMATION_TIME

const PARENT_ICON_FACTOR = 0.8;

const MAX_PATH_LENGTH = 50;
const MAX_NAME_LENGTH = 75;
const REDUCE_TEXT_AT_BEGINNING = false;

const getShortText = function(longText, maxLength, reduceAtTheBeginning) {
    if (reduceAtTheBeginning)
        return longText.length > maxLength ? "…" + longText.slice(-(maxLength - 1),200) : longText;
    else
        return longText.length > maxLength ? longText.slice(0, (maxLength - 1)) + "…" : longText;
};

// very abstract class to share methods between FileIcon, TagIcon and VolumeIcon
const Icon = new Lang.Class({
    Name: UUID + '-Icon',
    Abstract: true,
    
    _init : function(labelText, styleClass, iconParams) {
        this.filesDisplay = Main.overview.viewSelector.filesDisplay;
        
        this.actor = new St.Button({ style_class: 'app-well-app' + styleClass,
                                     reactive: true,
                                     button_mask: St.ButtonMask.ONE | St.ButtonMask.TWO,
                                     can_focus: true });
        if (GS_VERSION < '3.35')
            this.actor.x_fill = this.actor.y_fill = true;

        this._iconContainer = new St.Widget({ layout_manager: new Clutter.BinLayout(),
                                              x_expand: true, y_expand: true});

        this.actor.set_child(this._iconContainer);

        this.actor._delegate = this;
        
        this.iconSizeFactor = this.settings.get_double('view-icon-size-factor');
        if (this.iconSizeFactor > 1)
            this.actor.add_style_class_name('files-view-expanded-button');
        this.zoomScale = this.settings.get_string('icons-hover') == 'all' ? ICON_ZOOM_SCALE : 1;
        
        this.icon = new IconGrid.BaseIcon(labelText, iconParams);
        
        // GS 3.32- : this.icon.actor, GS 3.32+ this.icon
        this.iconActor = GS_VERSION < '3.32.0' ? this.icon.actor : this.icon;
        if (this.icon.label)
            this.actor.label_actor = this.icon.label;

        this._iconContainer.add_child(this.iconActor);
        
        this.actor.connect('destroy', Lang.bind(this, this._onDestroy));
        this.actor.connect('clicked', Lang.bind(this, this._onClicked));
        this.actor.connect('button-press-event', Lang.bind(this, this._countClicks));
        this.actor.connect('key-press-event', Lang.bind(this, this._onKeyPress));
        
        this.filesViewFadeInHandler = Main.overview.connect('files-view-item-drag-begin', () => {
            if (this.actor.is_mapped() && this.actor.get_opacity() != 128) {
                Mang.removeTweens(this.actor);
                Mang.addTween(this.actor, { opacity: 128,
                                            time: SIDE_CONTROLS_ANIMATION_TIME,
                                            transition: 'easeOutQuad' });
            }
        });
        this.filesViewFadeOutHandler = Main.overview.connect('files-view-item-drag-end', () => {
            if (this.actor.is_mapped() && this.actor.get_opacity() != 255) {
                Mang.removeTweens(this.actor);
                Mang.addTween(this.actor, { opacity: 255,
                                            time: SIDE_CONTROLS_ANIMATION_TIME,
                                            transition: 'easeOutQuad' });
            }
            if (this.setOpacityLowHandler) {
                this.DNDSource.disconnect(this.setOpacityLowHandler);
                this.setOpacityLowHandler = null;
                if (this.place && this.actor.get_parent().label.get_opacity() != 0)
                    this.actor.get_parent().hideLabel();
            }
        });
    },
    
    _onDestroy: function() {
        Main.overview.disconnect(this.filesViewFadeInHandler);
        Main.overview.disconnect(this.filesViewFadeOutHandler);
        
        if (this.setOpacityLowHandler) {
            this.DNDSource.disconnect(this.setOpacityLowHandler);
            this.setOpacityLowHandler = null;
        }
        
        if (this.longLabel) {
            this.longLabel.hide();
            let parent = this.longLabel.get_parent();
            if (parent)
                parent.remove_actor(this.longLabel);
            this.longLabelShown = false;
        }
        
        if (this._removeMenuTimeout)
            this._removeMenuTimeout();
    },
    
    hideLongLabel: function() {
        this.longLabel.hide();
        this.icon.label.opacity = 255;
        let parent = this.longLabel.get_parent();
        if (parent)
            parent.remove_actor(this.longLabel);
        this.longLabelShown = false;
    },
    
    showLongLabel: function() {
        Main.layoutManager.overviewGroup.add_actor(this.longLabel);
        this.icon.label.opacity = 0;
        let [x, y] = this.icon.label.get_transformed_position();
        let [a, b] = this.actor.get_transformed_position();
        let [width, height] = this.longLabel.get_transformed_size();
        this.longLabel.set_size(Math.min(width, 2*this.filesDisplay.iconSize*0.75), 3*height);
        let actorWidth = this.actor.allocation.x2 - this.actor.allocation.x1;
        let labelWidth = this.longLabel.get_width();
        let xOffset = Math.floor((actorWidth - labelWidth) / 2);
        this.longLabel.set_position(a + xOffset, y);
        this.longLabel.show();
        this.longLabelShown = true;
    },
        
    // longLabel is not created in _init because we need filesDisplay.iconSize (from _createIcon)
    _createLongLabel: function() {
        // not do it twice (refreshIcon)
        if (this.longLabel)
            return;
        
        // if label is ellipsized, show long label on hover
        this.longLabelTimeout = null;
        this.longLabelShown = false;
        if (this.filesDisplay.iconSize && this.icon.label.get_clutter_text().get_preferred_width(-1)[1] > this.icon.label.get_clutter_text().get_width()) {
            this.longLabel = new St.Label({ text: this.labelText, style: 'text-align:center' });
            this.longLabel.get_clutter_text().set_line_wrap(true);
            this.longLabel.get_clutter_text().set_line_wrap_mode(1);
            this.longLabel.hide();
            
            // if icon is zoomable, show/hideLongLabel is called on zoom/unzoom completed
            if (this.zoomScale != 1)
                return;
            
            this.actor.connect('notify::hover', () => {
                if (this.actor.hover && !this.longLabelShown && !this.longLabelTimeout) {
                    this.longLabelTimeout = Mainloop.timeout_add_seconds(LONG_LABEL_HOVER_TIMEOUT, () => {
                        this.showLongLabel();
                        Mainloop.source_remove(this.longLabelTimeout);
                        this.longLabelTimeout =  null;
                    });
                } else if (this.longLabelTimeout) {
                        Mainloop.source_remove(this.longLabelTimeout);
                        this.longLabelTimeout = null;
                } else if (this.longLabelShown) {
                        this.hideLongLabel();
                }
            });
        }
    },
    
    // implemented in FileIcon
    _getEmblemedGicon: function(gicon) {
        return gicon;
    },
    
    //It stores iconSize in filesDisplay to have the good size from the very first pass and prevent "pulse" effect due to resizing
    _createIcon: function(iconSize) {
        iconSize = this.iconSizeFactor*iconSize;
        let temp = iconSize;
        if (this.filesDisplay.iconSize && !this.icon.secondPass)
            iconSize = this.filesDisplay.iconSize;
        if (this.icon.secondPass)
                this.filesDisplay.iconSize = temp;
                
        let gicon = this._getGicon();
        let icon;
        
        if (this.hasThumbnail) {
            // gicon is a pixbuf (for thumbnail), so we can get width and height
            let ratio = gicon.width / gicon.height;
            let h, w, s;
            if (ratio >= 1 ) {
                s = Math.min(ratio, 16/9);
                [h, w] = [iconSize * s / ratio, iconSize * s];
            } else {
                s = Math.min(1 / ratio, 16/9);
                [h, w] = [iconSize * s, iconSize * s * ratio];
            }
            icon = new St.Icon({ gicon: this._getEmblemedGicon(gicon),
                                 // store bigger icon than required to anticipate hover-zoom and prevent blur
                                 icon_size: iconSize * s * this.zoomScale,
                                 // use St style rather than Clutter height/width/...
                                 // because scaling (hdpi screen) is done in St.
                                 style: 'width:' + w*0.70 + 'px;' + 'height:' + h*0.70 + 'px;' +
                                        'margin-top:' + h*0.15 + 'px;' + 'margin-bottom:' + h*0.15 + 'px;' });
        } else {
            icon = new St.Icon({ gicon: this._getEmblemedGicon(gicon),
                                 // store bigger icon than required to anticipate hover-zoom and prevent blur
                                 icon_size: iconSize * this.zoomScale,
                                 style: 'width:' + iconSize + 'px;' + 'height:' + iconSize + 'px;' });
        }
        
        this._createLongLabel();
        this._animateIcon();
        
        this.icon.secondPass = true;
        return icon;
    },
    
    _createParentIcon: function(iconSize) {
        iconSize = this.iconSizeFactor*iconSize;
        let temp = iconSize;
        if (this.filesDisplay.iconSize && !this.icon.secondPass)
            iconSize = this.filesDisplay.iconSize;
        if (this.icon.secondPass)
                this.filesDisplay.iconSize = temp;
        
        let gicon = new Gio.ThemedIcon({ name: 'go-previous-symbolic' });
        let icon = new St.Icon({ gicon: gicon,
                                 icon_size: iconSize * PARENT_ICON_FACTOR,
                                 style : 'padding:' + ((1 - PARENT_ICON_FACTOR) / 2) * iconSize + 'px;' });
        
        this.icon.secondPass = true;
        return icon;
    },
    
    // icon is not animated in _init because we need this.hasThumbnail (from _createIcon)
    _animateIcon: function() {
        // not do it twice (refreshIcon)
        if (this.iconAnimated)
            return;
        
        if (this.zoomScale != 1) {
            this.actor.connect('enter-event', Lang.bind(this, this._zoom));
            this.actor.connect('leave-event', Lang.bind(this, this._unZoom));
            this.iconAnimated = true;
        }
    },
    
    _zoom: function() {
        // prevent icon background change when hovering
        this.iconActor.set_style('background:none;');
        
        let [width, height] = this.icon.icon.get_transformed_size();
        this.icon.icon.set_pivot_point(0.5, 0.5);
        let scaledHeight = height * this.zoomScale;
        
        Mang.removeTweens(this.icon.icon);
        Mang.removeTweens(this.icon.label);
        Mang.addTween(this.icon.icon,
                      { time: this.zoomed ? 0 : ICON_ZOOM_TIME,
                        scale_x: this.zoomScale,
                        scale_y: this.zoomScale,
                        transition: 'easeOutQuad' });
        Mang.addTween(this.icon.label,
                      { time: this.zoomed ? 0 : ICON_ZOOM_TIME,
                        translation_x: 0,
                        translation_y: (scaledHeight - height) / 2,
                        transition: 'easeOutQuad',
                        onComplete: () => {
                            if (this.longLabel)
                                this.showLongLabel();
                        } });
        this.zoomed = true;
    },
    
    _unZoom: function() {
        Mang.removeTweens(this.icon.icon);
        Mang.removeTweens(this.icon.label);
        if (this.longLabel)
            this.hideLongLabel();
        
        Mang.addTween(this.icon.icon,
                      { time: ICON_ZOOM_TIME,
                        scale_x: 1,
                        scale_y: 1,
                        transition: 'easeOutQuad' });
        Mang.addTween(this.icon.label,
                      { time: ICON_ZOOM_TIME,
                        translation_x: 0,
                        translation_y: 0,
                        transition: 'easeOutQuad' });
        if (this.zoomId && this.actor)
            this.actor.disconnect(this.zoomId);
        
        this.zoomed = false;
    },
    
    // if it is the first item of the view, give the focus to search entry on Shift+Tab pressed
    // if it is in the first row of the view, give the focus to search entry on Up pressed
    _onKeyPress: function(actor, event) {
        if (event.get_key_symbol() == Clutter.KEY_ISO_Left_Tab) {
            let visibleChildren = Main.overview.viewSelector.filesDisplay.getCurrentView()._grid._getVisibleChildren();
            if (visibleChildren.indexOf(actor) == 0) {
                Main.overview.viewSelector.filesSearchEntry.focusSearch();
                return Clutter.EVENT_STOP;
            }
        } else if (event.get_key_symbol() == Clutter.KEY_Up) {
            let grid = Main.overview.viewSelector.filesDisplay.getCurrentView()._grid;
            let visibleChildren = grid._getVisibleChildren();
            let childrenPerRow = grid._childrenPerPage / grid._rowsPerPage;
            let childrenOnFirstRow = visibleChildren.slice(0, childrenPerRow);
            if (childrenOnFirstRow.indexOf(actor) != -1) {
                Main.overview.viewSelector.filesSearchEntry.focusSearch();
                return Clutter.EVENT_STOP;
            }
        }
        return Clutter.EVENT_PROPAGATE;
    },
    
    _countClicks: function(actor, event) {
        this.clickCount = event.get_click_count();
        return Clutter.EVENT_PROPAGATE;
    },
    
    _getIsDoubleClick: function() {
        if (this.clickCount < 2) {
            this.actor.add_style_pseudo_class('active');
            let leaveEventHandler = this.actor.connect('leave-event', () => {
                this.actor.remove_style_pseudo_class('active');
                this.actor.disconnect(leaveEventHandler);
            });
            return false;
        }
        
        return true;
    },
    
    _onClicked: function(actor, button) {
        // to use doubleClick:
        // let isDoubleClick = this._getIsDoubleClick();
        // if (!isDoubleClick)
        //     return;
    },
    
    _removeMenuTimeout: function() {
        if (this._menuTimeoutId) {
            Mainloop.source_remove(this._menuTimeoutId);
            this._menuTimeoutId = 0;
        }
    },
    
    _setPopupTimeout: function() {
        this._removeMenuTimeout();
        this._menuTimeoutId = Mainloop.timeout_add(MENU_POPUP_TIMEOUT,
            Lang.bind(this, function() {
                this._menuTimeoutId = 0;
                this.popupMenu();
                return GLib.SOURCE_REMOVE;
            }));
        GLib.Source.set_name_by_id(this._menuTimeoutId, '[gnome-shell] this.popupMenu');
    },

    _onLeaveEvent: function(actor, event) {
        this.actor.fake_release();
        this._removeMenuTimeout();
    },
    
    _onButtonPress: function(actor, event) {
        let button = event.get_button();
        if (button == 1) {
            this._setPopupTimeout();
        } else if (button == 3) {
            this.popupMenu();
            return Clutter.EVENT_STOP;
        }
        return Clutter.EVENT_PROPAGATE;
    },

    _onTouchEvent: function (actor, event) {
        if (event.type() == Clutter.EventType.TOUCH_BEGIN)
            this._setPopupTimeout();

        return Clutter.EVENT_PROPAGATE;
    },
    
    _onKeyboardPopupMenu: function() {
        this.popupMenu();
        this._menu.actor.navigate_focus(null, DirectionType.TAB_FORWARD, false);
    },
    
    _onMenuPoppedDown: function() {
        this.actor.sync_hover();
        this.emit('menu-state-changed', false);
    },
    
    _addMenu: function() {
        this.actor.connect('leave-event', Lang.bind(this, this._onLeaveEvent));
        this.actor.connect('button-press-event', Lang.bind(this, this._onButtonPress));
        this.actor.connect('touch-event', Lang.bind(this, this._onTouchEvent));
        this.actor.connect('popup-menu', Lang.bind(this, this._onKeyboardPopupMenu));
        
        this._menu = null;
        this._menuManager = new PopupMenu.PopupMenuManager(GS_VERSION < '3.33.0' ? this : this.actor);
        this._menuTimeoutId = 0;
    },
    
    popupMenu: function() {
        this._removeMenuTimeout();
        this.actor.fake_release();

        if (this._draggable)
            this._draggable.fakeRelease();

        if (!this._menu) {
            this._menu = new FileIconMenu(this);
            this._menu.connect('open-state-changed', (menu, isPoppedUp) => {
                let currentView = this.filesDisplay.getCurrentView();
                if (!isPoppedUp) {
                    // prevent persistent zoomed icon on menu popped down (by clicking outside the icon)
                    this.actor.sync_hover();
                    if (this.zoomed && !this.actor.get_hover())
                        this._unZoom();
                    if (currentView._scrollView && currentView._panAction)
                        currentView._scrollView.add_action(currentView._panAction);
                    this._onMenuPoppedDown();
                } else {
                    // prevent the view to scroll on menu popped down
                    // use currentView instead of this.source because this.source is not necessarily
                    // the displayed view (case where this is a sidebar icon).
                    // moreover the source view may have been deleted if this is a sidebar icon menu
                    if (currentView._scrollView && currentView._panAction)
                        currentView._scrollView.remove_action(currentView._panAction);
                }
            });
            let id = Main.overview.connect('hiding', () => {
                this._menu.close();
            });
            this.actor.connect('destroy', () => {
                Main.overview.disconnect(id);
            });

            this._menuManager.addMenu(this._menu);
        }

        this.emit('menu-state-changed', true);

        this.actor.set_hover(true);
        this._menu.popup();
        this._menuManager.ignoreRelease();
        this.emit('sync-tooltip');

        return false;
    },
    
    animateLaunch: function() {
        this.icon.animateZoomOut();
    },
    
    shouldShowTooltip: function() {
        return this.actor.hover && (!this._menu || !this._menu.isOpen);
    }
});

var VolumeIcon = new Lang.Class({
    Name: UUID + '-VolumeIcon',
    Extends: Icon,
    
    _init : function(place) {
        this.settings = Me.settings;
        this.place = place;
        this.volume = place.volume;
        
        this.labelText = '';
        
        let iconParams = {};
        iconParams.setSizeManually = true;
        iconParams.showLabel = false;
        iconParams.createIcon = Lang.bind(this, this._createPlaceIcon);
        
        this.parent(this.labelText, '', iconParams);
        
        this._addMenu();
    },
    
    _createPlaceIcon: function(iconSize) {
        let icon = new St.Icon({ gicon: this.place.icon, icon_size: iconSize });
        return icon;
    },
    
    _onClicked: function(actor, button) {
        Misc.mountVolume(this.volume, () => {
            let mount = this.volume.get_mount();
            if (mount)
                this.filesDisplay.openNewDirectory(mount.get_root());
        });
    },
    
    handleDragOver: function(source, actor, x, y, time) {
        if (source === this)
            return DND.DragMotionResult.CONTINUE;
        
        // catch sources that are not a file icon (e.g. dtp appIcons) or sidebar place
        if (!source.file || source.place)
            return DND.DragMotionResult.CONTINUE;
        
        // do nothing if source was already over this
        if (source.previousDNDTarget && source.previousDNDTarget == this) {
            return DND.DragMotionResult.CONTINUE;
        }
        
        source.previousDNDTarget = this;
        // notify previous target that source is not over it anymore
        source.emit('set-opacity-low');
        
        return DND.DragMotionResult.CONTINUE;
    }
});
Signals.addSignalMethods(VolumeIcon.prototype);

// simple icon, without menu and DnD
var TagIcon = new Lang.Class({
    Name: UUID + '-TagIcon',
    Extends: Icon,
    
    _init : function(tag, isGoParent, isGoTags, isGoFavorites) {
        this.settings = Me.settings;
        this.tag = tag;
        this.isGoParent = isGoParent;
        this.isGoTags = isGoTags;
        this.isGoFavorites = isGoFavorites;
        
        let styleClass = isGoParent || isGoTags || isGoFavorites ? ' files-view-parent-button' : '';
        
        this.labelText = isGoParent ? '' :
                         isGoTags ? _("Tags") :
                         isGoFavorites ? _("Favorites") :
                         tag;
        
        let iconParams = {};
        iconParams.setSizeManually = true;
        iconParams.showLabel = true;
        iconParams.createIcon = isGoParent ? Lang.bind(this, this._createParentIcon) :
                                Lang.bind(this, this._createIcon);
        
        this.parent(this.labelText, styleClass, iconParams);
    },
    
    _getGicon: function() {
        if (this.gicon)
            return this.gicon;
        
        this.gicon = this.isGoTags ? Main.overview.favoritesManager.tagGicon :
                     this.isGoFavorites ? new Gio.ThemedIcon({ name: 'starred-symbolic' }) :
                     new Gio.ThemedIcon({ name: 'folder' });
        
        return this.gicon;
    },
    
    _onClicked: function(actor, button) {
        if (this.settings.get_boolean('double-click-to-open')) {
            if (!this._getIsDoubleClick())
                return;
        }
        
        if (this.isGoTags)
            this.filesDisplay.openAllTagsView();
        else if (this.isGoFavorites)
            this.filesDisplay.openFavoritesView();
        else if (this.isGoParent)
            this.filesDisplay.openAllTagsView();
        else
            this.filesDisplay.openNewTagView(this.tag);
    }
});

// icon that represent a file, with menu and DnD
var FileIcon = new Lang.Class({
    Name: UUID + '-FileIcon',
    Extends: Icon,
    
    _init : function(file, source, place) {
        this.settings = Me.settings;
        this.file = file;
        this.source = source;
        this.isGoParent = this.file.isGoParent;
        this.isDesktopMount = this.file.isDesktopMount;
        this.place = place;
        this.exists = this.file.query_exists(null);
        this.isNotMountedBookmark = this.place && !this.exists;
        
        try {
            if (!this.isNotMountedBookmark)
                this.fileInfo = this.file.info ? this.file.info : this.file.query_info('*', Gio.FileQueryInfoFlags.NONE, null);
        } catch(e) {
            if (GLib.quark_to_string(e.domain) == 'g-vfs-error-quark') {
            // error with smb in sidebar :
            // GLib.Error g-vfs-error-quark: Cache invalid, retry (internally handled)
                try {
                    this.fileInfo = this.file.query_info('*', Gio.FileQueryInfoFlags.NONE, null);
                } catch(e) { }
            }
            
            if (!this.fileInfo) {
                logError(e, Me.metadata.uuid);
                this.hasError = true;
                return;
            }
        }
        
        if (this.fileInfo) {
            let scheme = this.file.get_uri_scheme();
            this.isTrashed = scheme == 'trash';
            this.isDir = this.fileInfo.get_file_type() == Gio.FileType.DIRECTORY;
            this.isSymlink = this.fileInfo.get_is_symlink();
            this.hasParent = this.file.has_parent(null);
            this.hasPath = this.file.get_path() ? true : false;
            this.isExecutable = this.fileInfo.get_attribute_boolean('access::can-execute');
            this.isDeletable = this.fileInfo.get_attribute_boolean('access::can-delete') || !this.fileInfo.has_attribute('access::can-delete');
            this.isReadable = this.fileInfo.get_attribute_boolean('access::can-read') || !this.fileInfo.has_attribute('access::can-read');
            this.isWritable = this.fileInfo.get_attribute_boolean('access::can-write') || !this.fileInfo.has_attribute('access::can-write');
            this.isFavorite = Main.overview.favoritesManager.getIsFavorite(this.file.get_uri());
            let contentType = this.fileInfo.get_content_type();
            this.isImage = contentType && contentType.indexOf('image') != -1 && contentType.indexOf('application') == -1;
            this.isXml = contentType && contentType.indexOf('xml') != -1;
            this.isLauncher = this.getIsLauncher();
        }
        
        if (this.place) {
            [this.isTrashable, this.trashText, this.trashAction, this.trashIconName] = [this.place.isTrashable, this.place.trashText, this.place.trashAction, this.place.trashIconName];
        } else {
            this.isTrashable = this.fileInfo.get_attribute_boolean('access::can-trash') || !this.fileInfo.has_attribute('access::can-trash');
            this.trashText = this.isTrashable ? _("Move to Trash") : null;
            this.trashAction = this.isTrashable ? (() => {
                if (!Main.overview.fileManager.hasTrash)
                    return;
                // no animation for drag and drop and go-parent button
                if (this.actor.get_opacity() == 255 && !this.isGoParent)
                    this.animateTrashedIcon();
                Main.overview.fileManager.onTrashClicked(file);
            }) : null;
            this.trashIconName = 'user-trash-symbolic';
        }
        
        let styleClass = this.isGoParent && this.file.get_uri() == 'file:///' ? ' files-view-parent-button-disable' :
                         this.isGoParent ? ' files-view-parent-button' :
                         this.place ? ' files-view-place-button' :
                         '';
        
        this.labelText = this.place ? this.place.name :
                         this.isGoParent ? "" :
                         this.file.mountName ? this.file.mountName :
                         this.isLauncher ? this.desktopAppInfo.get_display_name() :
                         this.fileInfo.get_display_name();
        
        let iconParams = {};
        iconParams.setSizeManually = true;
        iconParams.showLabel = this.place ? false : true;
        iconParams.createIcon = this.place ? Lang.bind(this, this._createPlaceIcon) :
                                this.isGoParent ? Lang.bind(this, this._createParentIcon) :
                                Lang.bind(this, this._createIcon);
        
        // stock the right _createIcon() in this._createIconFunction to use it in getDragActor()
        this._createIconFunction = iconParams.createIcon;
        
        this.parent(this.labelText, styleClass, iconParams);
        
        this.favoritesChangedHandler = Main.overview.favoritesManager.connect('changed', Lang.bind(this, this.refreshIcon));
        
        // Menu
        
        this._addMenu();
        
        // Drag and Drop
        
        let isInFolderView = this.source.directory ? true : false;
        let isDraggable = (isInFolderView && !this.isGoParent && this.exists && this.hasPath) ||
                          (this.place && this.isTrashable);
        if (isDraggable) {
            this._draggable = DND.makeDraggable(this.actor);
            
            this.dragBeginHandler = this._draggable.connect('drag-begin', () => {
                this._removeMenuTimeout();
                if (this.zoomed)
                    this._unZoom();
                Main.overview.emit('files-view-item-drag-begin', this);
                if (Main.overview.viewSelector.fadeIn)
                    // GS 3.32-
                    Main.overview.viewSelector.fadeIn();
                // Query setting one time, at the beginning
                this.fileManagingIsOff = (!this.settings.get_boolean('file-manager') || Main.overview.fileManager.isWorking);
                this._dragMonitor = {
                    dragMotion: Lang.bind(this, this._onDragMotion)
                };
                DND.addDragMonitor(this._dragMonitor);
            });
            this.dragCancelledHandler = this._draggable.connect('drag-cancelled', () => {
                Main.overview.emit('files-view-item-drag-cancelled', this);
            });
            this.dragEndHandler = this._draggable.connect('drag-end', () => {
                Main.overview.emit('files-view-item-drag-end', this);
                DND.removeDragMonitor(this._dragMonitor);
            });
        }
    },
    
    _onDestroy: function() {
        Main.overview.favoritesManager.disconnect(this.favoritesChangedHandler);
        
        if (this._draggable) {
            this._draggable.disconnect(this.dragBeginHandler);
            this._draggable.disconnect(this.dragCancelledHandler);
            this._draggable.disconnect(this.dragEndHandler);
            this._draggable.actor.destroy();
            this._draggable = null;
        }
        
        this.parent();
    },
    
    _onDragMotion: function(dragEvent) {
        // test whether the target is not an icon
        if ((this.source._gridGrid && dragEvent.targetActor == this.source._gridGrid) ||
            (this.source._gridActor && dragEvent.targetActor == this.source._gridActor) ||
            (this.source._scrollView && dragEvent.targetActor == this.source._scrollView) ||
            (MainOverviewControls._sidebarSlider && dragEvent.targetActor == MainOverviewControls._sidebarSlider.actor) ||
            (dragEvent.targetActor == Main.overview._overview)) {
            this.previousDNDTarget = null;
            this.emit('set-opacity-low');
        }
        return DND.DragMotionResult.CONTINUE;
    },
    
    _getCanAcceptDrop: function(source) {
        if (source === this)
            return false;
        // catch sources that are not a file icon (e.g. dtp appIcons)
        // or that are a sidebar place
        if (!source.file || source.place)
            return false;
        if (source.fileManagingIsOff)
            return false;
        if (!this.isDir || !this.exists || !this.hasPath || this.isGoParent)
            return false;
        if (source.file.has_parent(this.file))
            return false;
        
        return true;
    },
    
    handleDragOver: function(source, actor, x, y, time) {
        if (!this._getCanAcceptDrop(source))
            // do not return NO_DROP because it stop recursive calls and prevent _draggable._updateDragOver (see dnd.js)
            // from calling sidebar.handleDragOver after icon handleDragOver
            return DND.DragMotionResult.CONTINUE;
        
        // do nothing if source was already over this
        if (source.previousDNDTarget && source.previousDNDTarget == this)
            return this.isDir ? DND.DragMotionResult.MOVE_DROP : DND.DragMotionResult.CONTINUE;
        
        source.previousDNDTarget = this;
        // notify previous target that source is not over it anymore
        source.emit('set-opacity-low');
        
        if (this.gicon && this.gicon instanceof Gio.ThemedIcon) {
            let names = this.gicon.get_names();
            if (names.length && (names[0].indexOf('inode-directory') != -1 ||
                                 (names[0].indexOf('folder') != -1 && names[0].indexOf('symbolic') == -1) ||
                                 names[0] == 'folder-symbolic' ||
                                 names[0] == 'folder-remote-symbolic' ||
                                 names[0] == 'user-desktop' ||
                                 names[0] == 'user-home')) {
                this.giconNamesBeforePrepending = names;
                if (names[0].indexOf('symbolic') != -1)
                    this.gicon.prepend_name('folder-open-symbolic');
                else
                    this.gicon.prepend_name('folder-open');
                this.icon._onIconThemeChanged();
            }
        }
        
        this.actor.set_opacity(255);
        if (this.place) {
            Mang.removeTweens(this.actor.get_parent().label);
            this.actor.get_parent().showLabel();
        }
        this.DNDSource = source;
        this.setOpacityLowHandler = source.connect('set-opacity-low', () => {
            if (this.giconNamesBeforePrepending) {
                this.gicon = Gio.ThemedIcon.new_from_names(this.giconNamesBeforePrepending);
                this.icon._onIconThemeChanged();
                this.giconNamesBeforePrepending = null;
            }
            this.actor.set_opacity(128);
            if (this.place) {
                Mang.removeTweens(this.actor.get_parent().label);
                this.actor.get_parent().hideLabel();
            }
            if (this.setOpacityLowHandler) {
                source.disconnect(this.setOpacityLowHandler);
                this.setOpacityLowHandler = null;
            }
        });
            
        return this.isDir ? DND.DragMotionResult.MOVE_DROP : DND.DragMotionResult.CONTINUE;
    },

    acceptDrop: function(source, actor, x, y, time) {
        if (!this._getCanAcceptDrop(source))
            return false;
        
        if (this.file.equal(source.file) || this.file.has_prefix(source.file)) {
            Main.notify(_N("You cannot move a folder into itself."), _N("The destination folder is inside the source folder."));
            return false;
        }
        
        Main.overview.fileManager.onDndMove(source.file, this.file);
        this._animateDigest();
            
        return true;
    },
    
    _getGicon: function() {
        if (this.gicon)
            return this.gicon;
        
        if (this.file.mountIcon) {
            this.gicon = this.file.mountIcon;
        } else if (this.isLauncher && this.desktopAppInfo.get_icon()) {
            this.gicon = this.desktopAppInfo.get_icon();
        } else if (this.fileInfo.has_attribute('thumbnail::path') && this.fileInfo.get_attribute_boolean('thumbnail::is-valid')) {
            let thumbnailPath = this.fileInfo.get_attribute_as_string('thumbnail::path');
            let thumbnailFile = Gio.File.new_for_path(thumbnailPath);
            //check thumbnail exists
            if (thumbnailFile.query_exists(null)) {
                this.gicon = GdkPixbuf.Pixbuf.new_from_file(thumbnailPath);
                this.hasThumbnail = true;
            } else {
                this.gicon = this.fileInfo.get_icon();
                if (this.fileInfo.has_attribute('metadata::custom-icon-name') && this.gicon instanceof Gio.ThemedIcon) {
                    let customIconName = this.fileInfo.get_attribute_string('metadata::custom-icon-name');
                    this.gicon.prepend_name(customIconName);
                } else if (this.fileInfo.has_attribute('metadata::custom-icon')) {
                    let customIconUri = this.fileInfo.get_attribute_string('metadata::custom-icon');
                    let customIconFile = Gio.File.new_for_uri(customIconUri);
                    if (customIconFile.query_exists(null))
                        this.gicon = new Gio.FileIcon({ file: customIconFile });
                }
            }
        } else {
            this.gicon = this.fileInfo.get_icon();
            if (this.fileInfo.has_attribute('metadata::custom-icon-name') && this.gicon instanceof Gio.ThemedIcon) {
                let customIconName = this.fileInfo.get_attribute_string('metadata::custom-icon-name');
                this.gicon.prepend_name(customIconName);
            } else if (this.fileInfo.has_attribute('metadata::custom-icon')) {
                let customIconUri = this.fileInfo.get_attribute_string('metadata::custom-icon');
                let customIconFile = Gio.File.new_for_uri(customIconUri);
                if (customIconFile.query_exists(null))
                    this.gicon = new Gio.FileIcon({ file: customIconFile });
            }
        }
        
        // add fallbacks if the theme has not the icon
        if (this.gicon instanceof Gio.ThemedIcon) {
            this.gicon.append_name('stock_unknown');
            this.gicon.append_name('text-x-script');
        }
        
        // update zoomScale for thumbs
        if (this.settings.get_string('icons-hover') != 'none' && this.hasThumbnail)
            this.zoomScale = THUMB_ZOOM_SCALE;
        
        return this.gicon;
    },
    
    _getEmblemedGicon: function(gicon) {
        let emblemedGicon = Gio.EmblemedIcon.new(gicon, null);
        if (this.isSymlink) {
            let emblemIcon = Gio.ThemedIcon.new('emblem-symbolic-link');
            emblemedGicon.add_emblem(Gio.Emblem.new(emblemIcon));
        }
        if (!this.isWritable) {
            let emblemIcon = Gio.ThemedIcon.new('emblem-readonly');
            emblemedGicon.add_emblem(Gio.Emblem.new(emblemIcon));
        }
        if (!this.isReadable) {
            let emblemIcon = Gio.ThemedIcon.new('emblem-unreadable');
            emblemedGicon.add_emblem(Gio.Emblem.new(emblemIcon));
        }
        if (this.isFavorite) {
            let emblemIcon = Gio.ThemedIcon.new('emblem-favorite');
            emblemedGicon.add_emblem(Gio.Emblem.new(emblemIcon));
        }
        // add "clock" emblem if the file has been changed for the past 24 hours
        let timeChanged = this.fileInfo.get_attribute_uint64('time::changed');
        if ((Date.now() - timeChanged*1000 < 3600*1000) && !this.isDir) {
            let emblemIcon = Gio.ThemedIcon.new('emblem-urgent');
            emblemedGicon.add_emblem(Gio.Emblem.new(emblemIcon));
        }
        return emblemedGicon;
    },
    
    _getPlaceGicon: function() {
        if (!this.gicon)
            this.gicon = this.place.icon;
        
        return this.gicon;
    },
    
    _createPlaceIcon: function(iconSize) {
        if (this.place.kind == 'bookmarks') {
            let gicon = this._getPlaceGicon();
            
            if (this.isNotMountedBookmark) {
                let file = Me.dir.get_child('data').get_child('icons').get_child('emblem-not-mounted.svg');
                let emblemIcon = new Gio.FileIcon({ file: file });
                gicon = Gio.EmblemedIcon.new(this._getPlaceGicon(), Gio.Emblem.new(emblemIcon));
            }
            
            return new BookmarkIconTexture({ iconSize: iconSize, gicon: gicon, initial: this.place.name.length ? this.place.name[0] : '' });
        } else {
            let icon = new St.Icon({ gicon: this._getPlaceGicon(), icon_size: iconSize });
            return icon;
        }
    },
    
    refreshIcon: function() {
        if (this.isFavorite === undefined)
            this.isFavorite = false;
        
        if (this.isFavorite == Main.overview.favoritesManager.getIsFavorite(this.file.get_uri()))
            return;
        
        this.isFavorite = !this.isFavorite;
        
        if (this.longLabelTimeout) {
            Mainloop.source_remove(this.longLabelTimeout);
            this.longLabelTimeout = null;
        }
        if (this.longLabelShown) {
            this.hideLongLabel();
        }
        
        this.icon._onIconThemeChanged();
        
        if (this.longLabel && this.actor.hover)
            this.showLongLabel();
        if (this.zoomed) {
            this._zoom();
        }
    },
    
    _animateDigest: function() {
        if (this.giconNamesBeforePrepending) {
            this.gicon = Gio.ThemedIcon.new_from_names(this.giconNamesBeforePrepending);
            this.icon._onIconThemeChanged();
            this.giconNamesBeforePrepending = null;
        }
        
        this.icon.icon.set_pivot_point(0.5, 0.5);
        
        Mang.addTween(this.icon.icon,
                      { time: ICON_ZOOM_TIME/2,
                        scale_x: 0.9,
                        scale_y: 1.1,
                        rotation_angle_x: 0,
                        rotation_angle_y: 4,
                        rotation_angle_z: -1,
                        transition: 'easeInOutQuad',
                        onComplete: () => {
                            Mang.addTween(this.icon.icon, { time: ICON_ZOOM_TIME/2,
                                                            scale_x: 1.2,
                                                            scale_y: 1.2,
                                                            rotation_angle_x: 4,
                                                            rotation_angle_y: 0,
                                                            rotation_angle_z: 1,
                                                            transition: 'easeInOutQuad',
                                                            onComplete: () => {
                                                                Mang.addTween(this.icon.icon, { time: ICON_ZOOM_TIME*1.5,
                                                                                                scale_x: 1.0,
                                                                                                scale_y: 1.0,
                                                                                                rotation_angle_x: 0,
                                                                                                rotation_angle_y: 0,
                                                                                                rotation_angle_z: 0,
                                                                                                transition: 'easeOutElastic',
                                                                                                onComplete: () => {} });
                                                            }});
                        }});
    },
    
    animateTrashedIcon: function() {
        let icon = this.icon.icon;
        if (!MainOverviewControls._sidebarSlider.sidebar)
            return;
        let trash = MainOverviewControls._sidebarSlider.sidebar._showTrashIcon.icon.icon;
        
        let [width, height] = icon.get_transformed_size();
        let [x, y] = icon.get_transformed_position();
        let [trashWidth, trashHeight] = trash.get_transformed_size();
        let [trashX, trashY] = trash.get_transformed_position();
        
        let trashedIconClone = new St.Icon({ gicon: this.icon.icon.get_gicon(),
                                             icon_size: this.icon.icon.get_icon_size() });
        trashedIconClone.set_size(width, height);
        trashedIconClone.set_position(x, y);
        trashedIconClone.set_pivot_point(0.5, 0.5);
        trashedIconClone.opacity = 255;
        Main.layoutManager.overviewGroup.add_actor(trashedIconClone);
        
        Mang.addTween(trashedIconClone,
                      { time: 1,
                        scale_x: 0.6 * trashWidth / width,
                        scale_y: 0.6 * trashHeight / height,
                        rotation_angle_z: -450,
                        transition: 'easeInOutSine',
                        onComplete: () => {
                            trashedIconClone.opacity = 0;
                            Main.layoutManager.overviewGroup.remove_actor(trashedIconClone);
                            trashedIconClone.destroy(); } });
        
        Mang.addTween(trashedIconClone,
                      { time: 1,
                        x: trashX - (width - trashWidth)/2,
                        transition: 'easeOutQuad' });
        
        Mang.addTween(trashedIconClone,
                      { time: 1,
                        y: trashY - (height - trashHeight)/2,
                        transition: 'easeInQuart' });
        
    },
    
    _onKeyPress: function(actor, event) {
        let [symbol, control, shift] = [event.get_key_symbol(), event.has_control_modifier(), event.has_shift_modifier()];
        if ((symbol == Clutter.KEY_Return || symbol == Clutter.KEY_KP_Enter) && control) {
            let nautilusShellAppInfo = Shell.AppSystem.get_default().lookup_app('org.gnome.Nautilus.desktop').get_app_info();
            nautilusShellAppInfo.launch([this.file], null);
            Main.overview.hide();
            return Clutter.EVENT_STOP;
        } else if (symbol == Clutter.KEY_Delete && shift) {
            if (this.isDeletable && !this.place)
                this.deleteAction();
            return Clutter.EVENT_STOP;
        } else if (symbol == Clutter.KEY_Delete) {
            if (this.isTrashable)
                this.trashAction();
            return Clutter.EVENT_STOP;
        } else if (symbol == Clutter.KEY_c && control) {
            if (this.isReadable && this.hasPath && !this.place)
                Main.overview.fileManager.onCopyClicked(this.file, false);
            return Clutter.EVENT_STOP;
        } else if (symbol == Clutter.KEY_x && control) {
            if (this.isDeletable && this.hasPath && !this.place)
                Main.overview.fileManager.onCutClicked(this.file, false);
            return Clutter.EVENT_STOP;
        } else if (symbol == Clutter.KEY_v && control) {
            if (this.isDir && this.isWritable && this.hasPath)
                Main.overview.fileManager.onPasteClicked(this.file);
            return Clutter.EVENT_STOP;
        } else if ((symbol == Clutter.KEY_m || symbol == Clutter.KEY_M) && control && shift) {
            if (this.hasParent && !this.place && !this.isTrashed) {
                let parentFileInfo = this.file.get_parent().query_info('access::can-write', Gio.FileQueryInfoFlags.NONE, null);
                if (parentFileInfo.get_attribute_boolean('access::can-write') || !parentFileInfo.has_attribute('access::can-write'))
                    Main.overview.fileManager.onNewSymbolicLinkClicked(this.file);
            }
            return Clutter.EVENT_STOP;
        }
        
        return this.parent(actor, event);
    },
    
    _onClicked: function(actor, button) {
        this._removeMenuTimeout();
        
        if (this.settings.get_boolean('double-click-to-open') && !this.place) {
            if (!this._getIsDoubleClick())
                return;
        }
        
        if (this.isDir) {
            let [x_, y_, mods] = global.get_pointer();
            let ctrlPressed = (mods & Clutter.ModifierType.CONTROL_MASK) != 0;
            if (ctrlPressed) {
                let nautilusShellAppInfo = Shell.AppSystem.get_default().lookup_app('org.gnome.Nautilus.desktop').get_app_info();
                nautilusShellAppInfo.launch([this.file], null);
                Mainloop.idle_add(() => Main.overview.hide());
                return;
            }
        }
        
        if (this.isGoParent) {
            this.filesDisplay.openParentDirectory();
            return;
        }
        
        if (this.isDir && !this.isSymlink) {
            if (this.isDesktopMount)
                this.filesDisplay.openNewDesktopMountDirectory(this.file);
            else
                this.filesDisplay.openNewDirectory(this.file);
            return;
        }
        
        if (this.isNotMountedBookmark) {
            // maybe the enclosing volume is mounted now
            if (this.file.query_exists(null)) {
                if (this.file.query_file_type(Gio.FileQueryInfoFlags.NONE, null) == Gio.FileType.DIRECTORY)
                    this.filesDisplay.openNewDirectory(this.file);
            } else {
                Misc.mountEnclosingVolume(this.file, () => {
                    this.filesDisplay.openNewDirectory(this.file);
                });
            }
            return;
        }
        
        if (this.isLauncher) {
            try {
                this.desktopAppInfo.launch([], global.create_app_launch_context(0, -1));
                Main.overview.hide();
            } catch(e) {
                Main.notify(_GS("Failed to launch “%s”").format(this.fileInfo.get_display_name()), e.message);
            }
            return;
        }
        
        Misc.openFile(this.file);
    },
    
    deleteAction: function() {
        new Misc.ConfirmDialog({
            iconName: 'dialog-warning-symbolic',
            title: _N("Are you sure you want to permanently delete “%s”?").format(this.fileInfo.get_display_name()),
            body: _N("If you delete an item, it will be permanently lost."),
            cancelText: _N("Cancel"),
            confirmText: _N("_Delete Permanently").replace(" (_D)", "").replace("(_D)", "").replace("_", ""),
            confirmAction: () => Main.overview.fileManager.onDeleteClicked(this.file)
        }).open();
    },
    
    getIsLauncher: function() {
        if (this.fileInfo.get_content_type() != 'application/x-desktop')
            return false;
        if (!this.hasPath)
            return false;
        
        // even if 'run-launchers' on clicked is disabled, keep desktopAppInfo to add launch capabilities to menu
        this.desktopAppInfo = Gio.DesktopAppInfo.new_from_filename(this.file.get_path());
        if (!this.desktopAppInfo)
            return false;
        
        if (!this.hasParent ||
            (this.file.get_parent().get_path() != GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DESKTOP) && this.settings.get_string('run-launchers') == 'desktop-dir'))
            return false;
        if (this.settings.get_string('run-launchers') == 'none')
            return false;
        
        return true;
    },
    
    getDragActor: function() {
        return this._createIconFunction(this.settings.get_double('sidebar-icon-size'));
    },
    
    getDragActorSource: function() {
        return this.icon.icon;
    }
});
Signals.addSignalMethods(FileIcon.prototype);

// Draw the initial of the file name on the 'folder' or 'folder-symbolic' icon
const BookmarkIconTexture = new Lang.Class({
    Name: UUID + '-BookmarkIconTexture',
    Extends: St.DrawingArea,

    _init: function(params) {
        this.parent({ style: 'width:' + params.iconSize + 'px;' + 'height:' + params.iconSize + 'px;' });
        
        params = Params.parse(params, { gicon: null, iconName: '', iconSize: null, initial: '' });
        if (params.gicon)
            this.gicon = params.gicon;
        else
            this.gicon = new Gio.ThemedIcon({ name: params.iconName });
        this.iconSize = params.iconSize;
        this.initial = params.initial;
        
        let themeContextHandler = St.ThemeContext.get_for_stage(global.stage).connect('changed', () => this.surface = null);
        
        this.connect('destroy', () => St.ThemeContext.get_for_stage(global.stage).disconnect(themeContextHandler));
        this.connect('repaint', Lang.bind(this, this._repaint));
    },
    
    _updateSurface: function() {
        // try to use the background color of the sidebar as text color
        this.clutterTextColor = MainOverviewControls._sidebarSlider.sidebar._container.get_theme_node().get_background_color();
        if (this.clutterTextColor.alpha < 120)
            this.clutterTextColor = Clutter.Color.new(30, 30, 30, 255);
        else
            this.clutterTextColor.alpha = 255;
        
        let node = this.get_theme_node();
        let font = node.get_font();
        this.font = { family: font.get_family().split(',')[0],
                      style: font.get_style() == 2 ? 1 : ( font.get_style() == 1 ? 2 : 0),
                      weight: font.get_weight() > 500 ? 1 : 0 };
        
        let clutterColor = node.get_icon_colors().foreground;
        // convert Clutter color to Gdk RGBA
        let gdkColor = new Gdk.RGBA({ red: clutterColor.red / 255,
                                      green: clutterColor.green / 255,
                                      blue: clutterColor.blue / 255,
                                      alpha: clutterColor.alpha / 255 });
        
        // use this.iconSize (from this.get_surface_size()), that is not the same as params.iconSize when there is scaling
        let iconInfo = Gtk.IconTheme.get_default().lookup_by_gicon(this.gicon, this.iconSize, Gtk.IconLookupFlags.FORCE_SIZE);
        let pixbuf = iconInfo.load_symbolic(gdkColor, null, null, null)[0];
        
        this.surface = Gdk.cairo_surface_create_from_pixbuf(pixbuf, 1, null);
    },
    
    _getSurface: function() {
        if (!this.surface)
            this._updateSurface();
        
        return this.surface;
    },
    
    _repaint: function(area) {
        let cr = area.get_context();
        let [width, height] = area.get_surface_size();
        this.iconSize = Math.min(width, height);
        
        let surface = this._getSurface();
        
        if (!surface)
            return;
        
        // paint original icon
        cr.setSourceSurface(surface, 0, 0);
        cr.paint();
        
        Clutter.cairo_set_source_color(cr, this.clutterTextColor);
        cr.selectFontFace(this.font.family, this.font.style, this.font.weight);
        
        cr.moveTo(0.4*width, 0.75*height);
        
        cr.setFontSize(0.4*height);
        cr.showText(this.initial);
        
        cr.$dispose();
    },
    
    // for minimal compatibility with StIcon
    
    set_gicon: function(gicon) {
        if (gicon.equal(this.gicon))
            return;
        
        this.gicon = gicon;
        this.surface = null;
        this.queue_repaint();
    },
    
    set_icon_size: function(iconSize) {
        if (iconSize != this.iconSize)
            return;
        
        this.iconSize = iconSize;
        this.surface = null;
        this.queue_repaint();
    },
    
    set_icon_name: function(iconName) {
        let gicon = new Gio.ThemedIcon({ name: iconName });
        this.set_gicon(gicon);
    },
    
    get_gicon: function() {
        return this.gicon;
    },
    
    get_icon_size: function() {
        return this.iconSize;
    }
});

var FileIconMenu = new Mang.Class({
    Name: UUID + '-FileIconMenu',
    Extends: PopupMenu.PopupMenu,
    ParentConstrParams: [[0, 'actor'], 0.5, (Clutter.get_default_text_direction() == Clutter.TextDirection.RTL) ? St.Side.RIGHT : St.Side.LEFT],
    
    // minimal source: { actor:..., file:... }
    _init: function(source) {
        this.settings = Me.settings;
        let side = St.Side.LEFT;
        if (Clutter.get_default_text_direction() == Clutter.TextDirection.RTL)
            side = St.Side.RIGHT;

        this.callParent('_init', source.actor, 0.5, side);
        
        // We want to keep the item hovered while the menu is up
        this.blockSourceEvents = true;

        this.source = source;
        this.filesDisplay = Main.overview.viewSelector.filesDisplay;
        if (Main.overview.fileManager)
            this.fileManager = Main.overview.fileManager;
        this.apps = [];

        this.actor.add_style_class_name('files-view-file-icon-menu');
        this.connect('open-state-changed', this._onOpenStateChanged.bind(this));
        this.actor.connect('popup-menu', () => this.close(2));

        // Chain our visibility and lifecycle to that of the source
        this._sourceMappedId = source.actor.connect('notify::mapped', () => {
            if (!source.actor.mapped)
                this.close();
        });
        source.actor.connect('destroy', () => {
            source.actor.disconnect(this._sourceMappedId);
            this.destroy();
        });

        Main.layoutManager.overviewGroup.add_actor(this.actor);
    },
    
    destroy: function() {
        Main.layoutManager.overviewGroup.remove_actor(this.actor);
        this.disconnectHandlers();
        this.callParent('destroy');
    },
    
    disconnectHandlers: function() {
        if (this.cancellableHandler) {
            this.fileManager.disconnect(this.cancellableHandler);
            this.cancellableHandler = null;
        }
        if (this.undoHandler) {
            this.fileManager.disconnect(this.undoHandler);
            this.undoHandler = null;
        }
        if (this.capturedEventId) {
            global.stage.disconnect(this.capturedEventId);
            this.capturedEventId = null;
        }
        if (this.capturedEventId2) {
            global.stage.disconnect(this.capturedEventId2);
            this.capturedEventId2 = null;
        }
    },
    
    _onOpenStateChanged: function(menu, open) {
        let workArea = Main.layoutManager.getWorkAreaForMonitor(Main.layoutManager.primaryIndex);
        let scaleFactor = St.ThemeContext.get_for_stage(global.stage).scale_factor;
        let maxHeight = Math.round(0.95 * workArea.height / scaleFactor);
        this.actor.set_style('max-height:' + maxHeight + 'px;');
    },
    
    _appendSeparator: function() {
        let separator = new PopupMenu.PopupSeparatorMenuItem();
        this.addMenuItem(separator);
    },
    
    _populateOnOpen: function(menu, callback) {
        menu.openOld = menu.open;
        menu.open = (animate) => {
            if (!menu.isOpen && !menu.populated)
                Lang.bind(this, callback)(menu);
            menu.populated = true;
            menu.openOld(animate);
        }
    },

    popup: function(activatingButton) {
        this._redisplay();
        this.open();
    },
    
    // avoid menu opening when navigating in the grid with right arrow key
    _onKeyPress: function(actor, event) {
        if (this.isOpen)
            this.callParent('_onKeyPress', actor, event);
    },
    
    _volumeRedisplay: function() {
        let volume = this.source.volume;
        let name = this.source.place.name;
        let shortName = getShortText(name, MAX_NAME_LENGTH, REDUCE_TEXT_AT_BEGINNING);
        let nameItem = new MenuItem(shortName, null,{ hover: false, activate: false, can_focus: false });
        nameItem.label.set_style('font-weight:bold');
        this.addMenuItem(nameItem);
        
        let uuid = volume.get_uuid();
        let uuidItem = new MenuItem(uuid, null, { hover: false, activate: false });
        uuidItem.addInfoSectionStyle();
        this.addMenuItem(uuidItem);
        
        let drive = volume.get_drive();
        if (drive) {
            let driveShortName = getShortText(drive.get_name(), MAX_PATH_LENGTH, REDUCE_TEXT_AT_BEGINNING);
            let driveNameItem = new MenuItem(driveShortName + "  💽", null, { hover: false, activate: false });
            driveNameItem.addInfoSectionStyle();
            this.addMenuItem(driveNameItem);
        }
        
        this._appendSeparator();
        
        let mountItem = new MenuItem(_("Mount"), 'media-playback-start-symbolic');
        this.addMenuItem(mountItem);
        mountItem.connect('activate', () => {
            Misc.mountVolume(volume);
        });
        
        if (drive && drive.can_stop()) {
            let hasMount = false;
            let volumes = drive.get_volumes();
            volumes.forEach(vol => {
                if (vol.get_mount())
                    hasMount = true;
            });
            
            if (!hasMount) {
                let stopItem = new MenuItem(_("Power Off"), 'system-shutdown-symbolic');
                this.addMenuItem(stopItem);
                stopItem.connect('activate', () => {
                    Misc.stopDrive(drive);
                });
            }
        }
    },
    
    _notMountedBookmarkRedisplay: function() {
        let file = this.source.file;
        let name = this.source.place.name;
        let shortName = getShortText(name, MAX_NAME_LENGTH, REDUCE_TEXT_AT_BEGINNING);
        let nameItem = new MenuItem(shortName, null,{ hover: false, activate: false, can_focus: false });
        nameItem.label.set_style('font-weight:bold');
        this.addMenuItem(nameItem);
        
        let uri = file.get_uri();
        let shortUri = getShortText(uri, MAX_PATH_LENGTH, REDUCE_TEXT_AT_BEGINNING);
        let uriItem = new MenuItem(shortUri, null, { hover: false, activate: false });
        uriItem.addInfoSectionStyle();
        if (shortUri != uri) {
            uriItem.longText = uri;
            nameItem.addExpandIcon();
            nameItem.connect('expand-text', (nameItem) => {
                uriItem.expandText();
            });
        }
        this.addMenuItem(uriItem);
        
        this._appendSeparator();
        
        if (!file.query_exists(null)) {
            let mountItem = new MenuItem(_("Mount"), 'media-playback-start-symbolic');
            this.addMenuItem(mountItem);
            mountItem.connect('activate', () => {
                Misc.mountEnclosingVolume(file);
            });
        }
        
        let trashItem = new MenuItem(this.source.trashText, this.source.trashIconName);
        trashItem.connect('activate', Lang.bind(this.source, this.source.trashAction));
        this.addMenuItem(trashItem);
    },
    
    _trashRedisplay: function() {
        // this method is used by both 'go-parent' icon menu in trash folder view and sidebar showTrashIcon menu
        
        let trashFileInfo = this.source.file.query_info('trash::item-count,standard::display-name', Gio.FileQueryInfoFlags.NONE, null);
        let name = trashFileInfo.get_display_name();
        let trashCount = trashFileInfo.get_attribute_uint32('trash::item-count');
        
        let nameItem = new MenuItem(name, null, { hover: false, activate: false, can_focus: false });
        nameItem.label.set_style('font-weight:bold');
        this.addMenuItem(nameItem);
        
        if (trashCount) {
            let trashCountLabel = trashCount + " " + _n("file", "files", trashCount);
            let trashCountItem = new MenuItem(trashCountLabel, null, { hover: false, activate: false });
            trashCountItem.addInfoSectionStyle();
            this.addMenuItem(trashCountItem);
        }
        
        this._appendSeparator();
        
        let nautilusShellApp = Shell.AppSystem.get_default().lookup_app('org.gnome.Nautilus.desktop');
        if (nautilusShellApp) {
            let nautilusApp = nautilusShellApp.get_app_info();
            let goNautilusItem = new MenuItem(_("Open with %s").format(nautilusApp.get_display_name()), nautilusApp.get_icon());
            this.addMenuItem(goNautilusItem);
            goNautilusItem.connect('activate', () => {
                nautilusApp.launch_uris(['trash:///'], null);
                Main.overview.hide();
            });
        }
        
        if (this.settings.get_boolean('file-manager') && !Main.overview.fileManager.isWorking && trashCount) {
            let emptyTrashItem = new MenuItem(_("Empty Trash"), 'edit-clear-all-symbolic');
            this.addMenuItem(emptyTrashItem);
            emptyTrashItem.connect('activate', () => {
                new Misc.ConfirmDialog({
                    iconName: 'dialog-warning-symbolic',
                    title: _N("Empty all items from Trash?"),
                    body: _N("All items in the Trash will be permanently deleted."),
                    cancelText: _N("Cancel"),
                    confirmText: _N("Empty _Trash").replace(" (_T)", "").replace("(_T)", "").replace("_", ""),
                    confirmAction: () => Main.overview.fileManager.onEmptyTrashClicked()
                }).open();
            });
        }
    },
    
    // here we build all the menu
    // use symbolic icons, except for application icons (terminal, nautilus, open with ...)
    _redisplay: function() {
        this.removeAll();
        this.disconnectHandlers();
        
        if (this.source.volume) {
            this._volumeRedisplay();
            return;
        }
        
        if (this.source.isNotMountedBookmark) {
            this._notMountedBookmarkRedisplay();
            return;
        }
        
        if (this.source.file.get_uri() == 'trash:///') {
            this._trashRedisplay();
            return;
        }
        
        let infoSection = new PopupMenu.PopupMenuSection();
        this.addMenuItem(infoSection);
        this._appendSeparator();
        let actionSection = new PopupMenu.PopupMenuSection();
        this.addMenuItem(actionSection);
        this._appendSeparator();
        let managerSection = new PopupMenu.PopupMenuSection();
        this.addMenuItem(managerSection);
            
        // name item
        // other items can add an expand icon to nameItem. On activated, short paths become long paths (see MenuItem)
        let name = this.source.place ? this.source.place.name : this.source.fileInfo.get_display_name();
        let shortName = getShortText(name, MAX_NAME_LENGTH, REDUCE_TEXT_AT_BEGINNING);
        let nameItem = new MenuItem(shortName, null,{ hover: false, activate: false, can_focus: false });
        nameItem.label.set_style('font-weight:bold');
        infoSection.addMenuItem(nameItem);
        
        let filePath = this.source.file.is_native() ? this.source.file.get_path() : decodeURI(this.source.file.get_uri());
        if (this.source.fileInfo.has_attribute('standard::target-uri')) {
            let targetUri = this.source.fileInfo.get_attribute_string('standard::target-uri');
            try {
                filePath = GLib.filename_from_uri(targetUri)[0];
            } catch(e) {
                filePath = decodeURI(targetUri);
            }
        } else if (this.source.isTrashed) {
            // if trash file is not local, uri == 'trash:///%5c...%5c...'
            if (this.source.file.get_uri().indexOf('trash:///%5C') == 0)
                filePath = decodeURI(decodeURI(this.source.file.get_uri()).slice(9)).replace(/\\/g,'/');
            else
                filePath = GLib.get_home_dir() + '/.local/share/Trash/files/' + decodeURI(this.source.file.get_uri().slice(9));
        }
        
        // path item
        // short path when it's too long
        if (filePath != name) {
            let fileShortPath = getShortText(filePath, MAX_PATH_LENGTH, REDUCE_TEXT_AT_BEGINNING);
            let pathItem = new MenuItem(fileShortPath, null, { hover: false, activate: false });
            pathItem.addInfoSectionStyle();
            if (fileShortPath != filePath) {
                pathItem.longText = filePath;
                nameItem.addExpandIcon();
                nameItem.connect('expand-text', (nameItem) => {
                    pathItem.expandText();
                });
            }
            infoSection.addMenuItem(pathItem);
        }
        
        // target path item (for symbolic links)
        if (this.source.isSymlink) {
            let targetPath = this.source.fileInfo.get_symlink_target();
            let targetShortPath = getShortText(targetPath, MAX_PATH_LENGTH, REDUCE_TEXT_AT_BEGINNING);
            let targetPathItem = new MenuItem(targetShortPath + "  🔗", null, { hover: false, activate: false });
            targetPathItem.addInfoSectionStyle();
            if (targetShortPath != targetPath) {
                targetPathItem.longText = targetPath + "  🔗";
                nameItem.addExpandIcon();
                nameItem.connect('expand-text', () => {
                    targetPathItem.expandText();
                });
            }
            infoSection.addMenuItem(targetPathItem);
        }
        
        // origin path item & deletion date item (for trashed files)
        if (this.source.isTrashed && this.source.hasParent && this.source.file.get_parent().get_uri() == 'trash:///') {
            this.originPath = this.source.fileInfo.get_attribute_as_string('trash::orig-path');
            let originShortPath = getShortText(this.originPath, MAX_PATH_LENGTH, REDUCE_TEXT_AT_BEGINNING);
            let originPathItem = new MenuItem(originShortPath + "  ♻️", null, { hover: false, activate: false });
            originPathItem.addInfoSectionStyle();
            if (originShortPath != this.originPath) {
                originPathItem.longText = this.originPath + "  ♻️";
                nameItem.addExpandIcon();
                nameItem.connect('expand-text', () => {
                    originPathItem.expandText();
                });
            }
            infoSection.addMenuItem(originPathItem);
            
            let deletionDate = this.source.fileInfo.get_deletion_date();
            let deletionDateLabel = Util.formatTime(deletionDate);
            let deletionDateItem = new MenuItem(deletionDateLabel + '  🚮', null, { hover: false, activate: false } );
            deletionDateItem.addInfoSectionStyle();
            infoSection.addMenuItem(deletionDateItem);
        }
        
        // modified date item
        if (!this.source.isDir && !this.source.isTrashed && this.source.fileInfo.has_attribute('time::modified')) {
            let modifTime = this.source.fileInfo.get_attribute_uint64('time::modified');
            let modifDateLabel = Util.formatTime(GLib.DateTime.new_from_unix_local(modifTime));
            let modifDateItem = new MenuItem(modifDateLabel + "  📝", null, { hover: false, activate: false } );
            modifDateItem.addInfoSectionStyle();
            infoSection.addMenuItem(modifDateItem);
        }
        
        // filesytem size and type item
        if (!this.source.hasParent) {
            let fsInfo = this.source.file.query_filesystem_info('filesystem::', null);
            
            if (fsInfo.has_attribute('filesystem::size') && fsInfo.has_attribute('filesystem::used')) {
                let size = fsInfo.get_attribute_uint64('filesystem::size');
                let sizeLabel = GLib.format_size(size);
                let used = fsInfo.get_attribute_uint64('filesystem::used');
                let usedLabel = GLib.format_size(used);
                
                let diskUsageItem = new MenuItem(usedLabel + " / " + sizeLabel, null, { hover: false, activate: false });
                diskUsageItem.addInfoSectionStyle();
                infoSection.addMenuItem(diskUsageItem);
            }
            
            if (fsInfo.has_attribute('filesystem::type')) {
                let fsType = fsInfo.get_attribute_string('filesystem::type');
                let fsTypeItem = new MenuItem(fsType, null, { hover: false, activate: false });
                fsTypeItem.addInfoSectionStyle();
                infoSection.addMenuItem(fsTypeItem);
            }
            
        }
        
        // file size item
        if (!this.source.isDir && this.source.fileInfo.has_attribute('standard::size')) {
            let fileSize = this.source.fileInfo.get_size();
            let fileSizeLabel = GLib.format_size(fileSize);
            let fileSizeItem = new MenuItem(fileSizeLabel, null, { hover: false, activate: false });
            fileSizeItem.addInfoSectionStyle();
            infoSection.addMenuItem(fileSizeItem);
        }
        
        // directory children count item
        // display "..." first and then update
        // is_native() excludes network mounts
        if (!this.source.place && this.source.isDir && this.source.file.is_native()) {
            let childrenCountLabel = "..." + " " + _n("file", "files", 2);
            let childrenCountItem = new MenuItem(childrenCountLabel, null, { hover: false, activate: false });
            childrenCountItem.addInfoSectionStyle();
            infoSection.addMenuItem(childrenCountItem);
            
            if (this.source.file.childrenCount) {
                let childrenCount = this.source.file.childrenCount;
                let plus = this.source.file.maxChildrenCountReached ? "+" : "";
                childrenCountLabel = childrenCount + plus + " " + _n("file", "files", childrenCount);
                childrenCountItem.label.set_text(childrenCountLabel);
            } else {
                let countCallback = ((sourceObject, res) => {
                    let childEnumerator = sourceObject.enumerate_children_finish(res);
                    [sourceObject.childrenCount, sourceObject.maxChildrenCountReached] = FolderManager.countFolderChildren(childEnumerator);
                    childEnumerator.close(null);
                    
                    let plus = sourceObject.maxChildrenCountReached ? "+" : "";
                    childrenCountLabel =  sourceObject.childrenCount + plus + " " + _n("file", "files", sourceObject.childrenCount);
                    childrenCountItem.label.set_text(childrenCountLabel);
                });
                
                try {
                    this.source.file.enumerate_children_async('', Gio.FileQueryInfoFlags.NONE, 0, null, countCallback);
                } catch(e) {
                    this.source.file.childrenCount = 0;
                }
            }
        }
        
        this.favoritesManager = Main.overview.favoritesManager;
        this.isIndexedByTracker = this.favoritesManager.getIsIndexedByTracker(this.source.file.get_uri());
        
        // favorite item
        if (!this.favoritesManager.trackerUsed || this.isIndexedByTracker) {
            this.addToFavoritesFunc = function() {
                this.favoritesManager.addToFavorites(this.source.file.get_uri(), () => {
                    this.favoriteItem.label.set_text(_("Remove from favorites"));
                    this.favoriteItem.setIcon('starred-symbolic');
                    this.favoriteFunc = this.removeFromFavoritesFunc;
                });
            };
            this.removeFromFavoritesFunc = function() {
                this.favoritesManager.removeFromFavorites(this.source.file.get_uri(), () => {
                    if (this.filesDisplay.currentIndex != 2) {
                        this.favoriteItem.label.set_text(_("Add to favorites"));
                        this.favoriteItem.setIcon('non-starred-symbolic');
                        this.favoriteFunc = this.addToFavoritesFunc;
                    }
                });
            };
            
            if (this.favoritesManager.getIsFavorite(this.source.file.get_uri())) {
                this.favoriteItem = new MenuItem(_("Remove from favorites"), 'starred-symbolic');
                this.favoriteFunc = this.removeFromFavoritesFunc;
            } else {
                this.favoriteItem = new MenuItem(_("Add to favorites"), 'non-starred-symbolic');
                this.favoriteFunc = this.addToFavoritesFunc;
            }
                
            this.favoriteItem.closeMenuAfterActivating = false;
            this.favoriteItem.connect('activate-without-closing', () => { this.favoriteFunc(); });
            actionSection.addMenuItem(this.favoriteItem);
        }
        
        // bookmark item
        if (!this.source.place && this.source.isDir && MainOverviewControls._sidebarSlider.sidebar) {
            let isBookmark = MainOverviewControls._sidebarSlider.placesManager.getIsBookmark(this.source.file);
            let [bookmarkText, bookmarkIcon] = isBookmark ? [_("Remove from bookmarks"), 'edit-clear-symbolic'] : [_("Add to bookmarks"), 'bookmark-new-symbolic'];
        
            let bookmarkItem = new MenuItem(bookmarkText, bookmarkIcon);
            bookmarkItem.closeMenuAfterActivating = false;
            actionSection.addMenuItem(bookmarkItem);
            bookmarkItem.connect('activate-without-closing', () => {
                if (isBookmark) {
                    MainOverviewControls._sidebarSlider.placesManager.removeBookmark(this.source.file);
                    bookmarkItem.label.set_text(_("Add to bookmarks"));
                    bookmarkItem.setIcon('bookmark-new-symbolic');
                    isBookmark = !isBookmark;
                } else {
                    MainOverviewControls._sidebarSlider.placesManager.insertBookmark(this.source.file);
                    bookmarkItem.label.set_text(_("Remove from bookmarks"));
                    bookmarkItem.setIcon('edit-clear-symbolic');
                    isBookmark = !isBookmark;
                }
            });
        }
        
        // recent item
        if (this.filesDisplay.currentIndex == 0) {
            let recentItem = new MenuItem(_("Remove from recent"), 'document-open-recent-symbolic');
            actionSection.addMenuItem(recentItem);
            recentItem.connect('activate', () => {
                this.filesDisplay.getCurrentView().manager.remove_item(this.source.file.get_uri());
            });
        }
        
        // background item
        if (this.source.isImage || this.source.isXml) {
            this.bgSetting = new Gio.Settings({ schema_id: 'org.gnome.desktop.background' });
            let bgLabel, bgIconName;
            if (this.oldBgUri && (this.bgSetting.get_string('picture-uri') == this.source.file.get_uri())) {
                bgLabel = _("Remove as wallpaper");
                bgIconName = 'preferences-desktop-wallpaper';
            } else {
                bgLabel = _("Set as wallpaper");
                bgIconName = 'preferences-desktop-wallpaper-symbolic';
            }
            
            this.bgItem = new MenuItem(bgLabel, bgIconName);
            this.bgItem.closeMenuAfterActivating = false;
            actionSection.addMenuItem(this.bgItem);
            this.bgItem.connect('activate-without-closing', () => {
                if (this.oldBgUri && (this.bgSetting.get_string('picture-uri') == this.source.file.get_uri())) {
                    this.bgSetting.set_string('picture-uri', this.oldBgUri);
                    this.oldBgUri = null;
                    this.bgItem.label.set_text(_("Set as wallpaper"));
                    this.bgItem.setIcon('preferences-desktop-wallpaper-symbolic');
                } else {
                    this.oldBgUri = this.bgSetting.get_string('picture-uri');
                    this.bgSetting.set_string('picture-uri', this.source.file.get_uri());
                    this.bgItem.label.set_text(_("Remove as wallpaper"));
                    this.bgItem.setIcon('preferences-desktop-wallpaper');
                }
            });
        }
        
        // location item
        if (this.filesDisplay.currentIndex != 1 && this.source.hasParent) {
            let locationItem = new MenuItem(_("Open item location"), 'mark-location-symbolic');
            actionSection.addMenuItem(locationItem);
            locationItem.connect('activate', () => {
                this.filesDisplay.openNewDirectory(this.source.file.get_parent());
            });
        }
        
        // clipboard item
        this.clipboardItem = new MenuItem(_("Copy path to clipboard"), 'edit-copy-symbolic');
        this.clipboardItem.closeMenuAfterActivating = false;
        actionSection.addMenuItem(this.clipboardItem);
        this.clipboardItem.connect('activate-without-closing', () => {
            // encode if 'path' is an uri
            let path = GLib.uri_parse_scheme(filePath) ? encodeURI(filePath) : filePath;
            St.Clipboard.get_default().set_text(St.ClipboardType.CLIPBOARD, path);
            this.clipboardItem.setIcon('edit-paste-symbolic');
        });
        
        let contentType = this.source.fileInfo.get_content_type();
        if (!contentType)
            return;
        
        // tag item
        if (this.isIndexedByTracker) {
            let tagListItem = new PopupMenu.PopupSubMenuMenuItem(_("Tags"), true);
            tagListItem.icon.set_gicon(this.favoritesManager.tagGicon);
            actionSection.addMenuItem(tagListItem);
            this._populateOnOpen(tagListItem.menu, this._populateTagListSubMenu);
        } else if (this.favoritesManager.getCanIndex(this.source.file, this.source.isTrashed, this.source.isDir, this.source.hasParent)) {
            let indexItem = new MenuItem(_("Index in Tracker"), this.favoritesManager.tagGicon);
            indexItem.closeMenuAfterActivating = false;
            actionSection.addMenuItem(indexItem);
            indexItem.connect('activate-without-closing', () => {
                if (this.source.isDir) {
                    new Misc.ConfirmDialog({
                        iconName: 'dialog-warning-symbolic',
                        title: _("Are you sure you want to recursively index “%s” content in Tracker database?").format(name),
                        cancelText: _GS("Cancel"),
                        confirmText: _("Index"),
                        confirmAction: () => this.favoritesManager.index(this.source.file, () => this._redisplay())
                    }).open();
                } else {
                    this.favoritesManager.index(this.source.file, () => this._redisplay());
                }
            });
        }
        
        // "open with…" item
        let openWithItem = new PopupMenu.PopupSubMenuMenuItem(_("Open with…"), true);
        openWithItem.icon.icon_name = 'document-open-symbolic';
        actionSection.addMenuItem(openWithItem);
        this._populateOnOpen(openWithItem.menu, this._populateOpenWithSubMenu);
        
        // run item
        let runItem = new PopupMenu.PopupSubMenuMenuItem(_("Run"), true);
        runItem.icon.icon_name = 'application-x-executable-symbolic';
        actionSection.addMenuItem(runItem);
        let runItemActor = GS_VERSION < '3.33.0' ? runItem.actor : runItem;
        
        if (this.source.desktopAppInfo)
            this._populateOnOpen(runItem.menu, this._populateRunSubMenuWithDesktop);
        else if (!this.source.isDir && this.source.isExecutable && this.source.hasPath)
            this._populateOnOpen(runItem.menu, this._populateRunSubMenuWithExecutable);
        else
            runItemActor.hide();
        
        // example: some folders on webDav on which you cannot query info
        // do not use GLib.FileTest.EXISTS because it return trashed files and broken symlinks as not existing too
        if (!this.source.exists) {
            return;
        }
        
        if (this.settings.get_boolean('file-manager') && !Main.overview.fileManager.isWorking) {
            // rename items
            if (this.source.hasParent && !this.source.place &&
               (this.source.fileInfo.get_attribute_boolean('access::can-rename') || !this.source.fileInfo.has_attribute('access::can-rename'))) {
                
                let renameItem = new PopupMenu.PopupSubMenuMenuItem(_("Rename"), true);
                renameItem.icon.icon_name = 'document-edit-symbolic';
                managerSection.addMenuItem(renameItem);
                
                this._populateOnOpen(renameItem.menu, (menu) => {
                    // function to test whether a file with same name exists alongside
                    let renameTest = (text) => {
                        if (text.length == 0)
                            return false;
                        let testFile = this.source.file.get_parent().get_child_for_display_name(text);
                        if (!testFile)
                            return false;
                        let exists = GLib.file_test(testFile.get_path(), GLib.FileTest.EXISTS);
                        return exists;
                    };
                    
                    let editName = this.source.fileInfo.get_attribute_string('standard::edit-name');
                    let renameActivateCallback = (text) => {
                        this.close();
                        if (text != editName)
                            this.fileManager.onRenameClicked(this.source.file, text);
                    };
                    
                    let renameEntryItem = new EntryItem({ initialText: editName,
                                                          entryActivateCallback: renameActivateCallback,
                                                          invalidTextTests: [renameTest],
                                                          invalidStrings: ['/'] });
                    menu.addMenuItem(renameEntryItem);
                });
            }
            
            // new folder items
            if (this.source.isDir && this.source.isWritable) {
                let newFolderItem = new PopupMenu.PopupSubMenuMenuItem(_("New folder"), true);
                newFolderItem.icon.icon_name = 'folder-new-symbolic';
                managerSection.addMenuItem(newFolderItem);
                
                this._populateOnOpen(newFolderItem.menu, (menu) => {
                    // function to test if a file with same name exists in the folder
                    let newFolderTest = (text) => {
                        if (text.length == 0)
                            return false;
                        let testFile = this.source.file.get_child_for_display_name(text);
                        if (!testFile)
                            return false;
                        let exists = GLib.file_test(testFile.get_path(), GLib.FileTest.EXISTS);
                        return exists;
                    };
                    
                    let newFolderActivateCallback = (text) => {
                        this.close();
                        this.fileManager.onNewFolderClicked(this.source.file, text);
                    };
                    
                    let newFolderEntryItem = new EntryItem({ hintText: _("Type new folder name here…"),
                                                             entryActivateCallback: newFolderActivateCallback,
                                                             invalidTextTests: [newFolderTest],
                                                             invalidStrings: ['/'] });
                    menu.addMenuItem(newFolderEntryItem);
                });
            }
            
            // new document item
            if (this.source.isDir && this.source.isWritable) {
                let newDocItem = new PopupMenu.PopupSubMenuMenuItem(_("New document"), true);
                newDocItem.icon.icon_name = 'document-new-symbolic';
                managerSection.addMenuItem(newDocItem);
                this._populateOnOpen(newDocItem.menu, this._populateNewDocumentMenu);
            }
            
            // new symbolic link item
            if (this.source.hasParent && !this.source.place && !this.source.isTrashed) {
                let parentFileInfo = this.source.file.get_parent().query_info('access::can-write', Gio.FileQueryInfoFlags.NONE, null);
                if (parentFileInfo.get_attribute_boolean('access::can-write') || !parentFileInfo.has_attribute('access::can-write')) {
                    let symbolicLinkItem = new MenuItem(_("New symbolic link"), 'insert-link-symbolic');
                    managerSection.addMenuItem(symbolicLinkItem);
                    symbolicLinkItem.connect('activate', () => {
                        this.fileManager.onNewSymbolicLinkClicked(this.source.file);
                    });
                }
            }
            
            if (!this.source.place && this.source.isReadable && this.source.hasPath) {
                // check if "Control" key is pressed
                let [x, y, mods] = global.get_pointer();
                this.ctrlPressed = (mods & Clutter.ModifierType.CONTROL_MASK) != 0;
                
                // copy item
                this.copyItem = new MenuItem(_("Copy"), 'edit-copy-symbolic');
                managerSection.addMenuItem(this.copyItem);
                this.copyItem.connect('activate', () => {
                    this.fileManager.onCopyClicked(this.source.file, this.ctrlPressed && this.canAddToCopy);
                });
                
                if (this.source.isDeletable) {
                    // Cut Item
                    this.cutItem = new MenuItem(_("Cut"), 'edit-cut-symbolic');
                    managerSection.addMenuItem(this.cutItem);
                    this.cutItem.connect('activate', () => {
                        this.fileManager.onCutClicked(this.source.file, this.ctrlPressed && this.canAddToCut);
                    });
                }
                
                // inspired from system.js
                this.capturedEventId = global.stage.connect('captured-event', (actor, event) => {
                    let type = event.type();
                    if (type == Clutter.EventType.KEY_PRESS || type == Clutter.EventType.KEY_RELEASE) {
                        let key = event.get_key_symbol();
                        if (key == Clutter.KEY_Control_L || key == Clutter.KEY_Control_R) {
                            if (type == Clutter.EventType.KEY_PRESS) {
                                if (this.canAddToCopy)
                                    this.copyItem.label.set_text(_("Add to copy"));
                                if (this.canAddToCut && this.cutItem)
                                    this.cutItem.label.set_text(_("Add to cut"));
                                this.ctrlPressed = true;
                            } else {
                                this.copyItem.label.set_text(_("Copy"));
                                if (this.cutItem)
                                    this.cutItem.label.set_text(_("Cut"));
                                this.ctrlPressed = false;
                            }
                        }
                    }
                    return Clutter.EVENT_PROPAGATE;
                });
            }
            
            // paste item
            // synchronized with the clipboard
            if (this.source.isDir && this.source.isWritable && this.source.hasPath) {
                this.pasteItem = new MenuItem(_("Paste into folder"), 'edit-paste-symbolic');
                this.pasteItem.itemActor.visible = false;
                managerSection.addMenuItem(this.pasteItem);
                this.pasteItem.connect('activate', () => {
                    this.fileManager.onPasteClicked(this.source.file);
                });
            }
            
            // check clipboard before showing addToCopy, addToCut and paste items
            if (this.copyItem || this.cutItem || this.pasteItem) {
                St.Clipboard.get_default().get_text(St.ClipboardType.CLIPBOARD, (clipBoard, text) => {
                    let [valid, isCut, uris] = this.fileManager.parseClipboardText(text);
                    
                    // check if file, or a parent, or a child, is already in the clipboard
                    let conflict = false;
                    if (valid) {
                        for (let i = 0; i < uris.length; i++) {
                            let testFile = Gio.File.new_for_uri(uris[i]);
                            if (this.source.file.has_prefix(testFile) || testFile.has_prefix(this.source.file) || this.source.file.equal(testFile)) {
                                conflict = true;
                                break;      
                            }
                        }
                    }
                    
                    this.canAddToCopy = valid && !conflict && !isCut;
                    if (this.canAddToCopy && this.ctrlPressed && this.copyItem)
                        this.copyItem.label.set_text(_("Add to copy"));
                        
                    this.canAddToCut = valid && !conflict && isCut;
                    if (this.canAddToCut && this.ctrlPressed && this.cutItem)
                        this.cutItem.label.set_text(_("Add to cut"));
                        
                    if (this.pasteItem)
                        this.pasteItem.itemActor.visible = valid;
                });
            }
        }
        
        // trash and delete items, not only about file trash operation, but also about bookmarks and devices in sidebar
        if (!this.source.isTrashed && (this.source.isTrashable || this.source.isDeletable) &&
            ((this.settings.get_boolean('file-manager') && !Main.overview.fileManager.isWorking) || this.source.place)) {
            
            if (this.source.isTrashable) {
                this.trashItem = new MenuItem(this.source.trashText, this.source.trashIconName, { reactive: this.fileManager.hasTrash || this.source.place ? true : false });
                this.trashItem.connect('activate', Lang.bind(this.source, this.source.trashAction));
                managerSection.addMenuItem(this.trashItem);
            }
            // delete item appears when shift is pressed
            if (this.source.isDeletable && !this.source.place) {
                let deleteItem = new MenuItem(_("Delete"), 'edit-clear-symbolic');
                deleteItem.connect('activate', Lang.bind(this.source, this.source.deleteAction));
                managerSection.addMenuItem(deleteItem);
                
                // check if "Shift" key is pressed
                let [x, y, mods] = global.get_pointer();
                this.shiftPressed = (mods & Clutter.ModifierType.SHIFT_MASK) != 0;
                if (this.trashItem)
                    this.trashItem.itemActor.visible = !this.shiftPressed;
                deleteItem.itemActor.visible = this.shiftPressed;
                
                this.capturedEventId2 = global.stage.connect('captured-event', (actor, event) => {
                    let type = event.type();
                    if (type == Clutter.EventType.KEY_PRESS || type == Clutter.EventType.KEY_RELEASE) {
                        let key = event.get_key_symbol();
                        if (key == Clutter.KEY_Shift_L || key == Clutter.KEY_Shift_R) {
                            this.shiftPressed = (type == Clutter.EventType.KEY_PRESS);
                            if (this.trashItem)
                                this.trashItem.itemActor.visible = !this.shiftPressed;
                            deleteItem.itemActor.visible = this.shiftPressed;
                        }
                    }
                    return Clutter.EVENT_PROPAGATE;
                });
            }
            
        } else if (this.source.isTrashed && this.originPath && this.settings.get_boolean('file-manager') && !Main.overview.fileManager.isWorking) {
            let restoreItem = new MenuItem(_("Restore from Trash"), 'edit-undo-symbolic');
            managerSection.addMenuItem(restoreItem);
            let originFile = Gio.File.new_for_path(this.originPath);
            restoreItem.connect('activate', () => {
                this.fileManager.onRestoreFromTrashClicked(this.source.file, originFile);
            });
            
            let deleteItem = new MenuItem(_("Delete from Trash"), 'edit-clear-symbolic');
            managerSection.addMenuItem(deleteItem);
            deleteItem.connect('activate', () => {
                new Misc.ConfirmDialog({
                    iconName: 'dialog-warning-symbolic',
                    title: _N("Are you sure you want to permanently delete “%s”?").format(name),
                    body: _N("If you delete an item, it will be permanently lost."),
                    cancelText: _N("Cancel"),
                    confirmText: _N("_Delete").replace(" (_D)", "").replace("(_D)", "").replace("_", ""),
                    confirmAction: () => this.fileManager.onDeleteFromTrashClicked(this.source.file)
                }).open();
            });
        }
        
    },
    
    _populateTagListSubMenu: function(menu) {
        let tags = this.favoritesManager.allTagsManager.getTags(this.source.file.get_uri());
        
        let onTagRemoved = (removedTag, item) => {
            let index = menu._getMenuItems().indexOf(item);
            item.destroy();
            if (index >= menu._getMenuItems().length)
                index--;
            menu._getMenuItems()[index].grab_key_focus();
            tags = tags.filter(tag => tag != removedTag);
        };
        
        let onTagAdded = (tag) => {
            let item = new MenuItem(tag, 'edit-delete');
            menu.addMenuItem(item);
            item.closeMenuAfterActivating = false;
            item.connect('activate-without-closing', () => {
                this.favoritesManager.allTagsManager.removeTag(tag, this.source.file.get_uri(), (removedTag) => onTagRemoved(removedTag, item));
            });
            tags.push(tag);
        };
        
        let newTagActivateCallback = (text) => {
            // remove spaces at the start and at the end.
            text = text.replace(/^\s+/g, '').replace(/\s+$/g, '');
            
            if (text != '')
                this.favoritesManager.allTagsManager.addTag(text, this.source.file.get_uri(), onTagAdded);
        };
        
        // function to test if the file has already the tag
        let newTagTest = (text) => {
            return tags.indexOf(text) != -1;
        };
        
        let newTagEntryItem = new EntryItem({ hintText: _("Add tag…"),
                                              entryActivateCallback: newTagActivateCallback,
                                              invalidTextTests: [newTagTest],
                                              invalidStrings: [],
                                              primaryIconName: 'insert-text' });
        menu.addMenuItem(newTagEntryItem);
        
        tags.forEach(tag => {
            let item = new MenuItem(tag, 'edit-delete');
            menu.addMenuItem(item);
            item.closeMenuAfterActivating = false;
            item.connect('activate-without-closing', () => {
                this.favoritesManager.allTagsManager.removeTag(tag, this.source.file.get_uri(), (removedTag) => onTagRemoved(removedTag, item));
            });
        });
    },
     
    _populateOpenWithSubMenu: function(menu) {
        let contentType = this.source.fileInfo.get_content_type();
        let appInfos = Gio.AppInfo.get_recommended_for_type(contentType).concat(Gio.AppInfo.get_fallback_for_type(contentType));
        
        // sometimes default app is not in recommended/all apps list
        let defaultAppInfo = Gio.AppInfo.get_default_for_type(contentType, false);
        if (defaultAppInfo) {
            appInfos = appInfos.filter(appInfo => !appInfo.equal(defaultAppInfo));
            appInfos.unshift(defaultAppInfo);
        }
        
        appInfos.forEach(appInfo => {
            let item = new MenuItem(appInfo.get_display_name(), appInfo.get_icon());
            menu.addMenuItem(item);
            item.connect('activate', () => {
                appInfo.launch([this.source.file], null);
                Main.overview.hide();
            });
        });
        
        if (this.source.isDir) {
            // Do not use Gio.AppInfo.get_all() because it's very expansive (20ms)
            // but prefer Shell.AppSystem.get_default() that has a cache.
            
            // goNautilusItem if Nautilus is not in "open with…" list
            let nautilusShellApp = Shell.AppSystem.get_default().lookup_app('org.gnome.Nautilus.desktop');
            if (appInfos.map(appInfo => appInfo.get_id()).indexOf('org.gnome.Nautilus.desktop') == -1 && nautilusShellApp) {
                let nautilusAppInfo = nautilusShellApp.get_app_info();
                let goNautilusItem = new MenuItem(nautilusAppInfo.get_display_name(), nautilusAppInfo.get_icon());
                menu.addMenuItem(goNautilusItem);
                goNautilusItem.connect('activate', () => {
                    nautilusAppInfo.launch([this.source.file], null);
                    Main.overview.hide();
                });
            }
            
            // goBaobabItem if Baobab is not in "open with…" list
            let baobabShellApp = Shell.AppSystem.get_default().lookup_app('org.gnome.baobab.desktop');
            if (appInfos.map(appInfo => appInfo.get_id()).indexOf('org.gnome.baobab.desktop') == -1 && baobabShellApp != -1) {
                let baobabAppInfo = baobabShellApp.get_app_info();
                let goBaobabItem = new MenuItem(baobabAppInfo.get_display_name(), baobabAppInfo.get_icon());
                menu.addMenuItem(goBaobabItem);
                goBaobabItem.connect('activate', () => {
                    baobabAppInfo.launch([this.source.file], null);
                    Main.overview.hide();
                });
            }
            
            let terminalShellApp = Shell.AppSystem.get_default().lookup_app('org.gnome.Terminal.desktop') ||
                                   Shell.AppSystem.get_default().lookup_app('gnome-terminal.desktop'); // old ubuntu
            if (terminalShellApp) {
                let terminalAppInfo = terminalShellApp.get_app_info();
                let goTerminalItem = new MenuItem(terminalAppInfo.get_display_name(), terminalAppInfo.get_icon());
                menu.addMenuItem(goTerminalItem);
                goTerminalItem.connect('activate', () => {
                    Util.spawnCommandLine("gnome-terminal --window --working-directory=" + GLib.shell_quote(this.source.file.get_path()));
                    Main.overview.hide();
                });
            }
            
            let tilixShellApp = Shell.AppSystem.get_default().lookup_app('com.gexperts.Tilix.desktop');
            if (tilixShellApp) {
                let tilixAppInfo = tilixShellApp.get_app_info();
                let goTilixItem = new MenuItem(tilixAppInfo.get_display_name(), tilixAppInfo.get_icon());
                menu.addMenuItem(goTilixItem);
                goTilixItem.connect('activate', () => {
                    Util.spawnCommandLine("tilix --working-directory=" + GLib.shell_quote(this.source.file.get_path()));
                    Main.overview.hide();
                });
            }
        }
        
        if (menu.length == 0) {
            menu.addMenuItem(new PopupMenu.PopupMenuItem(_("No application for this file")));
        }
    },
    
    _launchFromCommand: function(needsTerminal) {
        let flags = needsTerminal ? Gio.AppInfoCreateFlags.NEEDS_TERMINAL : Gio.AppInfoCreateFlags.NONE;
        let appInfo = Gio.AppInfo.create_from_commandline(GLib.shell_quote(this.source.file.get_path()), null, flags);
        try {
            appInfo.launch([], global.create_app_launch_context(0, -1));
            Main.overview.hide();
        } catch(e) {
            Main.notify(_GS("Execution of “%s” failed:").format(this.source.fileInfo.get_display_name()), e.message);
        }
    },
    
    _populateRunSubMenuWithExecutable: function(menu) {
        let executeTermItem = new MenuItem(_("Run in a terminal"), 'utilities-terminal');
        menu.addMenuItem(executeTermItem);
        executeTermItem.connect('activate', () => this._launchFromCommand(true));
                             
        let executeBGItem = new MenuItem(_("Run in the background"), 'system-run');
        menu.addMenuItem(executeBGItem);
        executeBGItem.connect('activate', () => this._launchFromCommand(false));
    },
    
    _populateRunSubMenuWithDesktop: function(menu) {
        let desktopAppInfo = this.source.desktopAppInfo;
            
        let icon = desktopAppInfo.get_icon();
        if (!icon)
            icon = this.source.gicon;
        
        let desktopFirstItem = new MenuItem(desktopAppInfo.get_display_name(), icon);
        menu.addMenuItem(desktopFirstItem);
        desktopFirstItem.connect('activate', () => {
            try {
                desktopAppInfo.launch([], global.create_app_launch_context(0, -1));
                Main.overview.hide();
            } catch(e) {
                Main.notify(_GS("Failed to launch “%s”").format(this.source.fileInfo.get_display_name()), e.message);
            }
        });
        
        let actions = desktopAppInfo.list_actions();
        actions.forEach(action => {
            let desktopActionItem = new MenuItem(desktopAppInfo.get_action_name(action), icon);
            menu.addMenuItem(desktopActionItem);
            desktopActionItem.connect('activate', () => {
                try {
                    desktopAppInfo.launch_action(action, global.create_app_launch_context(0, -1));
                    Main.overview.hide();
                } catch(e) {
                    Main.notify(_GS("Failed to launch “%s”").format(this.source.fileInfo.get_display_name()), e.message);
                }
            });
        });
    },
    
    _populateNewDocumentMenu: function(menu) {
        let templatesDirPath = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_TEMPLATES);
        let templatesDir = Gio.File.new_for_path(templatesDirPath);
        let templates = [];
        // populate templates
        this.fileManager.getDocs(templatesDir, templates);
        
        for (let i = 0; i < templates.length; i++) {
            try {
                let template = templates[i];
                let info = template.query_info('standard::icon,standard::display-name', Gio.FileQueryInfoFlags.NONE, null);
                let subItem = new MenuItem(info.get_display_name(), info.get_icon());
                subItem.connect('activate', () => {
                    this.fileManager.onNewDocClicked(template, this.source.file);
                });
                menu.addMenuItem(subItem);
            } catch(e) {
                if (e instanceof Gio.IOErrorEnum)
                    return;
                throw e;
            }
        }
    }
});

var MenuItem = new Mang.Class({
    Name: UUID + '-MenuItem',
    Extends: PopupMenu.PopupBaseMenuItem,
    ParentConstrParams: [[2]],
    Signals: { 'activate-without-closing': { param_types: [Clutter.Event.$gtype] },
               'expand-text': {} },

    _init: function(text, icon, params) {
        this.callParent('_init', params);
        
        this.itemActor = GS_VERSION < '3.33.0' ? this.actor : this;
        
        // update parent menu scrollView adjustment when navigating with keyboard.
        // do not overwrite _onKeyFocusIn or setActive methods
        // (vfunc_key_focus_in or 'set active' in 3.33+)
        // to ensure all GS version support
        this.itemActor.connect('key-focus-in', () => this._updateAdjustment());
        this.itemActor.connect('popup-menu', () => this._getTopMenu().close(2));

        if (icon) {
            this._icon = new St.Icon({ style_class: 'popup-menu-icon' });
            this.itemActor.add_child(this._icon);
            this.setIcon(icon);
        }
        
        if (text) {
            this.label = new St.Label({ text: text });
            this.itemActor.add_child(this.label);
            this.itemActor.label_actor = this.label;
        }

        this.closeMenuAfterActivating = true;
        this.text = text;
        this.longText = null;
        if (params && params.hover)
            this._hoverable = params.hover;
    },
    
    addInfoSectionStyle: function() {
        this.itemActor.add_style_class_name('files-view-info-section-popup-menu-item');
        this.setSensitive(false);
    },

    setIcon: function(icon) {
        // The 'icon' parameter can be either a Gio.Icon or a string.
        // I removed the Gio.Icon check because of issue #10 and replace it by try-catch
        if (typeof icon != 'string' /*&& icon instanceof GObject.Object && GObject.type_is_a(icon, Gio.Icon)*/) {
            try {
            this._icon.gicon = icon;
            } catch(e) {
                return;
            }
        } else
            this._icon.icon_name = icon;
    },
    
    addExpandIcon: function() {
        if (this._expandIcon)
            return;
        
        this.itemActor.add_child(new St.Bin({ style_class: 'popup-menu-item-expander', x_expand: true }));
        this._expandIcon = new St.Icon({ style_class: 'popup-menu-arrow', icon_name: 'zoom-in-symbolic',
                                         y_expand: true, y_align: Clutter.ActorAlign.CENTER });
        this._expandButton = new St.Button({ style_class: 'files-view-expand-button', child: this._expandIcon, can_focus: true });
        this._expandButton.connect('clicked', () => {
            this._expandIcon.icon_name = this._expandIcon.icon_name == 'zoom-in-symbolic' ? 'zoom-out-symbolic' : 'zoom-in-symbolic';
            this.emit('expand-text');
        });
        this.itemActor.add_child(this._expandButton);
    },
    
    expandText: function() {
        if (this.label.get_text() == this.text) {
            this.label.set_text(this.longText);
        } else {
            this.label.set_text(this.text);
        }
    },
    
    // if this.closeMenuAfterActivating is false, the menu is not closed after activating the item
    // and we can only use 'activate-without-closing' for callbacks
    activate: function(event) {
        this.emit('activate-without-closing', event);
        if (this.closeMenuAfterActivating)
            // this.callParent() emits 'activate'
            this.callParent('activate', event);
        if (this._expandIcon) {
            this._expandIcon.icon_name = this._expandIcon.icon_name == 'zoom-in-symbolic' ? 'zoom-out-symbolic' : 'zoom-in-symbolic';
            this.emit('expand-text');
        }
    },
    
    // based on ApplicationsButton.scrollToButton , https://gitlab.gnome.org/GNOME/gnome-shell-extensions/blob/master/extensions/apps-menu/extension.js
    // change: 10 -> 5
    _updateAdjustment: function() {
        if (!this._parent || !this._parent.actor || !this._parent.actor.vscroll)
            return;
        
        let box = this._parent.actor;
        
        let adjustment = box.get_vscroll_bar().get_adjustment();
        let boxAlloc = box.get_allocation_box();
        let currentScrollValue = adjustment.get_value();
        let boxHeight = boxAlloc.y2 - boxAlloc.y1;
        let buttonAlloc = this.itemActor.get_allocation_box();
        let newScrollValue = currentScrollValue;
        if (currentScrollValue > buttonAlloc.y1 - 5)
            newScrollValue = buttonAlloc.y1 - 5;
        if (boxHeight + currentScrollValue < buttonAlloc.y2 + 5)
            newScrollValue = buttonAlloc.y2 - boxHeight + 5;
        if (newScrollValue != currentScrollValue)
            adjustment.set_value(newScrollValue);
    },
    
    grab_key_focus: function() {
        if (this.itemActor == this)
            this.callParent('grab_key_focus');
        else
            this.itemActor.grab_key_focus();
    }
});

// based on searchItem.js, https://github.com/leonardo-bartoli/gnome-shell-extension-Recents
const EntryItem = new Mang.Class({
    Name: UUID + '-EntryItem',
    Extends: PopupMenu.PopupBaseMenuItem,
    ParentConstrParams: [{ activate: false, reactive: true, can_focus: false }],
    
    _init: function(params) {
        this.params = Params.parse(params, { initialText: null, hintText: null,
                                             entryActivateCallback: (text) => {},
                                             invalidTextTests: [], invalidStrings: [],
                                             primaryIconName: null });
        this.callParent('_init', {
            activate: false,
            reactive: true,
            can_focus: false
        });
        
        this.itemActor = GS_VERSION < '3.33.0' ? this.actor : this;
        this.itemActor.connect('popup-menu', () => this._getTopMenu().close(2));
        
        this._entry = new St.Entry({
            name: 'FilesViewMenuItemEntry',
            style_class: 'search-entry files-view-item-entry',
            track_hover: true,
            reactive: true,
            can_focus: true,
            x_expand: true
        });
        
        if (this.params.hintText)
            this._entry.set_hint_text(this.params.hintText);
        if (this.params.initialText)
            this._entry.set_text(this.params.initialText);
        
        this.itemActor.add_child(this._entry);
        
        if (this.params.primaryIconName)
            this._entry.set_primary_icon(new St.Icon({ style_class: 'search-entry-icon',
                                                       icon_name: this.params.primaryIconName }));
        
        this._entry.clutter_text.connect('text-changed', Lang.bind(this, this._onTextChanged));
        this._entry.clutter_text.connect('activate', Lang.bind(this, this._onTextActivated));

        this._clearIcon = new St.Icon({
            style_class: 'search-entry-icon',
            icon_name: 'edit-clear-symbolic'
        });
        this._entry.connect('secondary-icon-clicked', Lang.bind(this, this._reset));
        if (this._entry.get_text().length)
            this._entry.set_secondary_icon(this._clearIcon);
    },
    
    _setError: function(hasError) {
        if (hasError)
            this._entry.add_style_class_name('files-view-item-entry-error');
        else
            this._entry.remove_style_class_name('files-view-item-entry-error');
    },
    
    _reset: function() {
        this._entry.text = '';
        this._entry.clutter_text.set_cursor_visible(true);
        this._entry.clutter_text.set_selection(0, 0);
        this._setError(false);
    },
    
    _onTextActivated: function(clutterText) {
        let text = clutterText.get_text();
        if (text.length == 0)
            return;
        if (this.getIsInvalid())
            return;
        this._reset();
        this.params.entryActivateCallback(text);
    },
    
    _onTextChanged: function(clutterText) {
        let text = this._entry.get_text();
        this._entry.set_secondary_icon(text.length ? this._clearIcon : null);
        
        if (text.length)
            this._setError(this.getIsInvalid());
    },
    
    getIsInvalid: function() {
        for (let i = 0; i < this.params.invalidTextTests.length; i++) {
            if (this.params.invalidTextTests[i](this._entry.text))
                return true;
        }
        
        for (let i = 0; i < this.params.invalidStrings.length; i++) {
            if (this._entry.text.indexOf(this.params.invalidStrings[i]) != -1)
                return true;
        }
        
        return false;
    },

    grab_key_focus: function() {
        this._entry.grab_key_focus();
    }
});



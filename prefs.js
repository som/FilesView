/*
 * Copyright 2018 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2018 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 6 */

const Gdk = imports.gi.Gdk;
const GLib = imports.gi.GLib;
const GObject = imports.gi.GObject;
const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const Mainloop = imports.mainloop;

let Tracker = null;
try {
    if (imports.misc.config.PACKAGE_VERSION > '3.37')
        throw new Error();
    
    Tracker = imports.gi.Tracker;
    // check libtracker version >= 1.1 since Tracker.check_version() is buggy
    // (query issue with 1.0 version)
    if (!Tracker.Resource)
        Tracker = null;
} catch(e) {
    // tracker not found
}

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Convenience = Me.imports.convenience;
const Metadata = Me.metadata;

const _ = imports.gettext.domain(Metadata['gettext-domain']).gettext;
const _GTK = imports.gettext.domain('gtk30').gettext;
const MARGIN = 10;
const UUID = Me.uuid.replace(/@/gi, '_at_').replace(/[^a-z0-9+_-]/gi, '_');

function init() {
    Convenience.initTranslations();
}


function buildPrefsWidget() {
    let topStack = new TopStack();
    let switcher = new Gtk.StackSwitcher({halign: Gtk.Align.CENTER, visible: true, stack: topStack});
    Mainloop.idle_add(() => {
        let window = topStack.get_toplevel();
        window.resize(720,500);
        window.set_type_hint(Gdk.WindowTypeHint.NORMAL);
        let headerBar = window.get_titlebar();
        headerBar.custom_title = switcher;
        
        let menu = new Gio.Menu();
        menu.append(_("Preferences"), 'win.prefs');
        menu.append(_("Shortcuts"), "win.shortcuts");
        menu.append(_("About"), "win.about");
        
        let actionGroup = new Gio.SimpleActionGroup();
        let prefsAction = new Gio.SimpleAction({ name: 'prefs' });
        prefsAction.connect('activate', () => {
            topStack.set_visible_child(topStack.prefsPage);
        });
        actionGroup.add_action(prefsAction);
        let shortcutsAction = new Gio.SimpleAction({ name: 'shortcuts' });
        shortcutsAction.connect('activate', () => {
            topStack.set_visible_child(topStack.shortcutsPage);
        });
        actionGroup.add_action(shortcutsAction);
        let aboutAction = new Gio.SimpleAction({ name: 'about' });
        aboutAction.connect('activate', () => {
            topStack.set_visible_child(topStack.aboutPage);
        });
        actionGroup.add_action(aboutAction);
        window.insert_action_group('win',actionGroup);
        
        let file = Me.dir.get_child('data').get_child('icons').get_child('files-view.png');
        let icon = new Gio.FileIcon({ file: file });
        let image = new Gtk.Image({ gicon: icon, icon_size: Gtk.IconSize.LARGE_TOOLBAR, visible: true });
        
        let button = new Gtk.MenuButton({ visible: true, image: image, menu_model: menu });
        button.get_style_context().add_class('titlebutton appmenu');
        headerBar.pack_start(button);
        
        return GLib.SOURCE_REMOVE;
    });
    
    topStack.show_all();

    return topStack;
}

const capitalize = function(string) {
    return string.replace(/^\D/, function(c) { return c.toLocaleUpperCase(); });
};

const TopStack = new GObject.Class({
    Name: UUID + '-TopStack',
    Extends: Gtk.Stack,
    
    _init: function(params) {
        this.parent({ transition_type: 1, transition_duration: 500, expand: true });
        this.prefsPage = new PrefsPage();
        this.add_titled(this.prefsPage, 'prefs', _("Preferences"));
        this.shortcutsPage = new ShortcutsPage();
        this.add_titled(this.shortcutsPage, 'shortcuts', _("Shortcuts"));
        this.aboutPage = new AboutPage();
        this.add_titled(this.aboutPage, 'about', _("About"));
    }
});

const AboutPage = new GObject.Class({
    Name: UUID + '-AboutPage',
    Extends: Gtk.ScrolledWindow,

    _init: function(params) {
        this.parent();

        let vbox= new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, margin: MARGIN*3 });
        this.add(vbox);
        
        let imagePath = Me.dir.get_child('data').get_child('icons').get_child('files-view.png').get_path();
        let image = new Gtk.Image({ file: imagePath, pixel_size: 96 });
        let name = "<b><big> " + _(Metadata.name) + "</big></b>";
        let version = _("Version %f").format(Metadata.version);
        let description = _(Metadata.description);
        let link = "<span><a href=\"" + Metadata.url + "\">" + Metadata.url + "</a></span>";
        let licenseName = _GTK("GNU General Public License, version 3 or later");
        let licenseLink = "https://www.gnu.org/licenses/gpl-3.0.html";
        let license = "<small>" + _GTK("This program comes with absolutely no warranty.\nSee the <a href=\"%s\">%s</a> for details.").format(licenseLink, licenseName) + "</small>";
        
        let imageBox = new Gtk.Box();
        imageBox.set_center_widget(image);
        vbox.add(imageBox);
        
        let aboutLabel = new Gtk.Label({ wrap: true, justify: 2, use_markup: true, label:
            name + "\n\n" + version + "\n\n" + description + "\n\n" + link + "\n\n" + license + "\n" });
        
        vbox.add(aboutLabel);
        
        let creditBox = new Gtk.Box({ orientation: Gtk.Orientation.HORIZONTAL, margin: 2*MARGIN });
        let leftBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let rightBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let leftLabel = new Gtk.Label({ wrap: true, valign: 1, halign: 2, justify: 1, use_markup: true, label: "<small>" + _GTK("Created by") + "</small>" });
        let rightLabel = new Gtk.Label({ wrap: true, valign: 1, halign: 1, justify: 0, use_markup: true, label: "<small><a href=\"https://codeberg.org/abak\">Abakkk</a></small>" });
        leftBox.pack_start(leftLabel, false, false, 0);
        rightBox.pack_start(rightLabel, false, false, 0);
        creditBox.pack_start(leftBox, true, true, 5);
        creditBox.pack_start(rightBox, true, true, 5);
        vbox.add(creditBox);
        
        if (_("translator-credits") != "translator-credits" && _("translator-credits") != "") {
            leftBox.pack_start(new Gtk.Label(), false, false, 0);
            rightBox.pack_start(new Gtk.Label(), false, false, 0);
            leftLabel = new Gtk.Label({ wrap: true, valign: 1, halign: 2, justify: 1, use_markup: true, label: "<small>" + _GTK("Translated by") + "</small>" });
            rightLabel = new Gtk.Label({ wrap: true, valign: 1, halign: 1, justify: 0, use_markup: true, label: "<small>" + _("translator-credits") + "</small>" });
            leftBox.pack_start(leftLabel, false, false, 0);
            rightBox.pack_start(rightLabel, false, false, 0);
        }
    }
    
});

const PrefsPage = new GObject.Class({
    Name: UUID + '-PrefsPage',
    Extends: Gtk.Box,
    
    _init: function(params) {
        this.parent({ orientation: Gtk.Orientation.HORIZONTAL });
        
        this.sidebar = new Gtk.StackSidebar();
        let styleContext = this.sidebar.get_child().get_style_context();
        styleContext.add_class('view');
        try {
            let provider = new Gtk.CssProvider();
            let data = ".view  { min-width: 8em; }";
            provider.load_from_data(data);
            styleContext.add_provider(provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
        } catch(e) {}
        this.add(this.sidebar);
        
        this.stack = new Gtk.Stack();
        this.stack.add_titled(new FilesPrefsPage(), 'files', _("Files"));
        this.stack.add_titled(new ViewPrefsPage(), 'view', _("View"));
        this.stack.add_titled(new TogglesPrefsPage(), 'toggles', _("Toggles"));
        this.add(this.stack);
        this.sidebar.set_stack(this.stack);
    }
});

const FilesPrefsPage = new GObject.Class({
    Name: UUID + '-FilesPrefsPage',
    Extends: Gtk.ScrolledWindow,

    _init: function(params) {
        this.parent();

        this.settings = Convenience.getSettings();
        
        let box = new Gtk.Box({ orientation: Gtk.Orientation.HORIZONTAL});
        this.add(box);
        
        box.add(new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, hexpand: true }));
        let listBox = new Gtk.ListBox({ selection_mode: 0, hexpand: true, margin_top: 2*MARGIN, margin_bottom: 2*MARGIN });
        box.add(listBox);
        box.add(new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, hexpand: true }));
        
        let styleContext = listBox.get_style_context();
        styleContext.add_class('background');
        
        
        let fileManagerBox = new Gtk.Box({ margin: MARGIN });
        let fileManagerLabelBox = new Gtk.Box({ valign: 3, orientation: Gtk.Orientation.VERTICAL });
        let fileManagerLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Enable file manager") });
        let fileManagerLabel2 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, use_markup: true, label: "<small>" + _("Add the conveniences of a file manager in popup menus and by drag-and-drop") + "</small>" });
        fileManagerLabel2.get_style_context().add_class("dim-label");
        fileManagerLabelBox.pack_start(fileManagerLabel1, true, true, 0);
        fileManagerLabelBox.pack_start(fileManagerLabel2, true, true, 0);
        let fileManagerSwitch = new Gtk.Switch({ valign: 3 });
        this.settings.bind("file-manager", fileManagerSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        fileManagerBox.pack_start(fileManagerLabelBox, true, true, 4);
        fileManagerBox.pack_start(fileManagerSwitch, false, false, 4);
        listBox.add(fileManagerBox);
        this.addSeparator(listBox);
            
        let viewBox = new Gtk.Box({ margin: MARGIN });
        let viewLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Start view") });
        let viewCombo = new Gtk.ComboBoxText({ valign: 3 });
        viewCombo.append('previous', _("Where I left"));
        viewCombo.append('recent', _("Recent"));
        viewCombo.append('folder', _("Folder"));
        viewCombo.append('tags', Tracker ? _("Tags") : _("Favorites"));
        this.settings.bind("start-view", viewCombo, "active-id", Gio.SettingsBindFlags.DEFAULT);
        viewBox.pack_start(viewLabel, true, true, 4);
        viewBox.pack_start(viewCombo, false, false, 4);
        listBox.add(viewBox);
        
        
        let startBox = new Gtk.Box({ margin: MARGIN });
        let startLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Start directory") });
        let startButton = new Gtk.FileChooserButton({ valign: 3, title: _('Select directory'), action: Gtk.FileChooserAction.SELECT_FOLDER });
        let startPath = this.settings.get_string('start-path');
        if (startPath == '')
            startPath = GLib.get_home_dir();
        startButton.set_filename(startPath);
        startButton.connect('file-set', () => {
            this.settings.set_string('start-path', startButton.get_filename());
        });
        startBox.pack_start(startLabel, true, true, 4);
        startBox.pack_start(startButton, false, false, 4);
        listBox.add(startBox);
        this.addSeparator(listBox);
        
        let hiddenBox = new Gtk.Box({ margin: MARGIN });
        let hiddenLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Show hidden files") });
        let hiddenSwitch = new Gtk.Switch({ valign: 3 });
        this.settings.bind("show-hidden-files", hiddenSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        hiddenBox.pack_start(hiddenLabel, true, true, 4);
        hiddenBox.pack_start(hiddenSwitch, false, false, 4);
        listBox.add(hiddenBox);
        
        let notMountedDevicesBox = new Gtk.Box({ margin: MARGIN });
        let notMountedDevicesLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Show not-mounted devices in sidebar") });
        let notMountedDevicesSwitch = new Gtk.Switch({ valign: 3 });
        this.settings.bind("show-not-mounted-devices-sidebar", notMountedDevicesSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        notMountedDevicesBox.pack_start(notMountedDevicesLabel, true, true, 4);
        notMountedDevicesBox.pack_start(notMountedDevicesSwitch, false, false, 4);
        listBox.add(notMountedDevicesBox);
        
        let desktopPath = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DESKTOP);
        let desktopFolderName = GLib.path_get_basename(desktopPath);
        
        let desktopDevicesBox = new Gtk.Box({ margin: MARGIN });
        let desktopDevicesLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Show removable devices in %s").format(desktopFolderName) });
        let desktopDevicesSwitch = new Gtk.Switch({ valign: 3 });
        this.settings.bind("show-devices-desktop", desktopDevicesSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        desktopDevicesBox.pack_start(desktopDevicesLabel, true, true, 4);
        desktopDevicesBox.pack_start(desktopDevicesSwitch, false, false, 4);
        listBox.add(desktopDevicesBox);
        
        let launcherBox = new Gtk.Box({ margin: MARGIN });
        let launcherLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, use_markup: true, label: _("Launch <i>.desktop</i> files on clicked") });
        let launcherCombo = new Gtk.ComboBoxText({ valign: 3 });
        launcherCombo.append('none',_("Never"));
        launcherCombo.append('desktop-dir',_("Only in %s").format(desktopFolderName));
        launcherCombo.append('everywhere',_("Everywhere"));
        this.settings.bind("run-launchers", launcherCombo, "active-id", Gio.SettingsBindFlags.DEFAULT);
        launcherBox.pack_start(launcherLabel, true, true, 4);
        launcherBox.pack_start(launcherCombo, false, false, 4);
        listBox.add(launcherBox);
        this.addSeparator(listBox);
        
        let favoritesSyncBox = new Gtk.Box({ margin: MARGIN });
        let favoritesSyncLabelBox = new Gtk.Box({ valign: 3, orientation: Gtk.Orientation.VERTICAL });
        let favoritesSyncLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Synchronize favorites (starred files) with Nautilus") });
        let favoritesSyncLabel2 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, use_markup: true, label: "<small>" + _("Tracker is required") + "</small>" });
        favoritesSyncLabel2.get_style_context().add_class("dim-label");
        favoritesSyncLabelBox.pack_start(favoritesSyncLabel1, true, true, 0);
        favoritesSyncLabelBox.pack_start(favoritesSyncLabel2, true, true, 0);
        let favoritesSyncSwitch = new Gtk.Switch({ valign: 3 });
        this.settings.bind("sync-nautilus-favorites", favoritesSyncSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        favoritesSyncBox.pack_start(favoritesSyncLabelBox, true, true, 4);
        favoritesSyncBox.pack_start(favoritesSyncSwitch, false, false, 4);
        listBox.add(favoritesSyncBox);
        if (!Tracker)
            favoritesSyncSwitch.set_sensitive(false);
        this.addSeparator(listBox);
        
        let sortSyncBox = new Gtk.Box({ margin: MARGIN });
        let sortSyncLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Synchronize file sorting with Nautilus") });
        let sortSyncSwitch = new Gtk.Switch({ valign: 3 });
        this.settings.bind("sync-nautilus-sorting", sortSyncSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        sortSyncBox.pack_start(sortSyncLabel, true, true, 4);
        sortSyncBox.pack_start(sortSyncSwitch, false, false, 4);
        listBox.add(sortSyncBox);
        
        let sortByBox = new Gtk.Box({ margin: MARGIN });
        let sortByLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Sort files by") });
        let sortByCombo = new Gtk.ComboBoxText({ valign: 3 });
        sortByCombo.append('name',capitalize(_("name")));
        sortByCombo.append('modification date',capitalize(_("date")));
        sortByCombo.append('size',capitalize(_("size")));
        sortByCombo.append('type',capitalize(_("type")));
        this.settings.bind("sort-by", sortByCombo, "active-id", Gio.SettingsBindFlags.DEFAULT);
        sortByBox.pack_start(sortByLabel, true, true, 4);
        sortByBox.pack_start(sortByCombo, false, false, 4);
        listBox.add(sortByBox);
        this.settings.bind("sync-nautilus-sorting", sortByBox, "sensitive", Gio.SettingsBindFlags.GET + Gio.SettingsBindFlags.INVERT_BOOLEAN);
        
        let sortReversedBox = new Gtk.Box({ margin: MARGIN });
        let sortReversedLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Decreasing sort order") });
        let sortReversedSwitch = new Gtk.Switch({ valign: 3 });
        this.settings.bind("sort-reversed", sortReversedSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        sortReversedBox.pack_start(sortReversedLabel, true, true, 4);
        sortReversedBox.pack_start(sortReversedSwitch, false, false, 4);
        listBox.add(sortReversedBox);
        this.settings.bind("sync-nautilus-sorting", sortReversedBox, "sensitive", Gio.SettingsBindFlags.GET + Gio.SettingsBindFlags.INVERT_BOOLEAN);
        
        let sortSeparatedBox = new Gtk.Box({ margin: MARGIN });
        let sortSeparatedLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Sort folders before files") });
        let sortSeparatedSwitch = new Gtk.Switch({ valign: 3 });
        this.settings.bind("sort-folders-before-files", sortSeparatedSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        sortSeparatedBox.pack_start(sortSeparatedLabel, true, true, 4);
        sortSeparatedBox.pack_start(sortSeparatedSwitch, false, false, 4);
        listBox.add(sortSeparatedBox);
        
        let children = listBox.get_children();
        for (let i = 0; i < children.length; i++) {
            if (children[i].activatable)
                children[i].set_activatable(false);
        }
    },
    
    addSeparator: function(container) {
        let separatorRow = new Gtk.ListBoxRow({ sensitive: false });
        separatorRow.add(new Gtk.Separator({ margin: MARGIN }));
        container.add(separatorRow);
    }
});

const ViewPrefsPage = new GObject.Class({
    Name: UUID + '-ViewPrefsPage',
    Extends: Gtk.ScrolledWindow,

    _init: function(params) {
        this.parent();

        this.settings = Convenience.getSettings();
        this.handlers = [];
        this.connect('destroy', () => {
            this.handlers.forEach(handler => handler[0].disconnect(handler[1]));
        });
        
        let box = new Gtk.Box({ orientation: Gtk.Orientation.HORIZONTAL });
        this.add(box);
        
        box.add(new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, hexpand: true }));
        let listBox = new Gtk.ListBox({ selection_mode: 0, hexpand: true, margin_top: 2*MARGIN, margin_bottom: 2*MARGIN });
        box.add(listBox);
        box.add(new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, hexpand: true }));
        
        let styleContext = listBox.get_style_context();
        styleContext.add_class('background');
        
        
        let sidebarBox = new Gtk.Box({ margin: MARGIN });
        let sidebarLabelBox = new Gtk.Box({ valign: 3, orientation: Gtk.Orientation.VERTICAL });
        let sidebarLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Places instead of applications in dash") });
        let sidebarLabel2 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, use_markup: true, label: "<small>" + _("The dash becomes a Nautilus-like sidebar") + "</small>" });
        sidebarLabel2.get_style_context().add_class("dim-label");
        sidebarLabelBox.pack_start(sidebarLabel1, true, true, 0);
        sidebarLabelBox.pack_start(sidebarLabel2, true, true, 0);
        let sidebarSwitch = new Gtk.Switch({ valign: 3 });
        this.settings.bind("sidebar", sidebarSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        sidebarBox.pack_start(sidebarLabelBox, true, true, 4);
        sidebarBox.pack_start(sidebarSwitch, false, false, 4);
        listBox.add(sidebarBox);
        
        
        let secondSidebarBox = new Gtk.Box({ margin: MARGIN });
        let secondSidebarLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Split sidebar") });
        let secondSidebarSwitch = new Gtk.Switch({ valign: 3 });
        this.settings.bind("second-sidebar", secondSidebarSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        secondSidebarBox.pack_start(secondSidebarLabel, true, true, 4);
        secondSidebarBox.pack_start(secondSidebarSwitch, false, false, 4);
        listBox.add(secondSidebarBox);
        this.settings.bind("sidebar", secondSidebarBox, "sensitive", Gio.SettingsBindFlags.GET);
        
        
        let sidebarSymbolicIconsBox = new Gtk.Box({ margin: MARGIN });
        let sidebarSymbolicIconsLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Symbolic icons in sidebar") });
        let sidebarSymbolicIconsSwitch = new Gtk.Switch({ valign: 3 });
        this.settings.bind("sidebar-symbolic-icons", sidebarSymbolicIconsSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        sidebarSymbolicIconsBox.pack_start(sidebarSymbolicIconsLabel, true, true, 4);
        sidebarSymbolicIconsBox.pack_start(sidebarSymbolicIconsSwitch, false, false, 4);
        listBox.add(sidebarSymbolicIconsBox);
        this.settings.bind("sidebar", sidebarSymbolicIconsBox, "sensitive", Gio.SettingsBindFlags.GET);
        
        
        let sidebarIconSizeBox = new Gtk.Box({ margin: MARGIN });
        let sidebarIconSizeLabelBox = new Gtk.Box({ valign: 3, orientation: Gtk.Orientation.VERTICAL });
        let sidebarIconSizeLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Icon size in sidebar") });
        let sidebarIconSizeLabel2 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, use_markup: true, label: "<small>" + "16, 22, 24, 32, 48, 64 (px)" + "</small>" });
        sidebarIconSizeLabel2.get_style_context().add_class("dim-label");
        sidebarIconSizeLabelBox.pack_start(sidebarIconSizeLabel1, true, true, 0);
        sidebarIconSizeLabelBox.pack_start(sidebarIconSizeLabel2, true, true, 0);
        let sidebarIconSizeSpin = Gtk.SpinButton.new_with_range(16,64,4);
        sidebarIconSizeSpin.set_valign(3);
        this.settings.bind("sidebar-icon-size", sidebarIconSizeSpin, "value", Gio.SettingsBindFlags.DEFAULT);
        sidebarIconSizeBox.pack_start(sidebarIconSizeLabelBox, true, true, 4);
        sidebarIconSizeBox.pack_start(sidebarIconSizeSpin, false, false, 4);
        listBox.add(sidebarIconSizeBox);
        this.settings.bind("sidebar", sidebarIconSizeBox, "sensitive", Gio.SettingsBindFlags.GET);
        this.addSeparator(listBox);
        
        
        this.globalAnimationRow = new Gtk.ListBoxRow();
        this.globalAnimationBox = new Gtk.Box({ margin: MARGIN });
        let globalAnimationLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, use_markup: true, label: "<small>" + "⚠ " + _("GNOME Shell animations are disabled") + "</small>" });
        globalAnimationLabel.get_style_context().add_class("dim-label");
        this.globalAnimationBox.pack_start(globalAnimationLabel, true, true, 4);
        this.globalAnimationRow.add(this.globalAnimationBox);
        listBox.add(this.globalAnimationRow);
        
        // globalAnimationBox appears when global animation is disabled and extension animation enabled
        this.globalSettings = Convenience.getSettings('org.gnome.desktop.interface');
        this.warningEnabled = true;
        this.setWarning = () => {
            let warningNeeded = !this.globalSettings.get_boolean('enable-animations') && (this.settings.get_boolean('fade-in-browsing') || this.settings.get_string('icons-hover') != 'none');
            if (warningNeeded && !this.warningEnabled) {
                this.globalAnimationRow.add(this.globalAnimationBox);
                this.warningEnabled = true;
            } else if (!warningNeeded && this.warningEnabled) {
                this.globalAnimationRow.remove(this.globalAnimationBox);
                this.warningEnabled = false;
            }
        };
        // need to map globalAnimationBox before removing it. If not, it can't be added later.
        this.globalAnimationHandler = this.globalAnimationBox.connect('map', () => {
            this.setWarning();
            this.globalAnimationBox.disconnect(this.globalAnimationHandler);
        });
        this.handlers.push([this.globalSettings, this.globalSettings.connect('changed::enable-animations', () => {
            this.setWarning();
        })]);
        this.handlers.push([this.settings, this.settings.connect('changed::fade-in-browsing', () => {
            this.setWarning();
        })]);
        this.handlers.push([this.settings, this.settings.connect('changed::icons-hover', () => {
            this.setWarning();
        })]);
        
            
        let fadeBox = new Gtk.Box({ margin: MARGIN });
        let fadeLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Fade-in animation when browsing folders") });
        let fadeSwitch = new Gtk.Switch({ valign: 3 });
        this.settings.bind("fade-in-browsing", fadeSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        fadeBox.pack_start(fadeLabel, true, true, 4);
        fadeBox.pack_start(fadeSwitch, false, false, 4);
        listBox.add(fadeBox);
        
        
        let hoverBox = new Gtk.Box({ margin: MARGIN });
        let hoverLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Animate icons when hovering") });
        let hoverCombo = new Gtk.ComboBoxText({ valign: 3 });
        hoverCombo.append('none',_("None"));
        hoverCombo.append('thumbnails',_("Only thumbnails"));
        hoverCombo.append('all',_("All"));
        this.settings.bind("icons-hover", hoverCombo, "active-id", Gio.SettingsBindFlags.DEFAULT);
        hoverBox.pack_start(hoverLabel, true, true, 4);
        hoverBox.pack_start(hoverCombo, false, false, 4);
        listBox.add(hoverBox);
        this.addSeparator(listBox);
        
        
        let viewIconSizeBox = new Gtk.Box({ margin: MARGIN });
        let viewIconSizeLabelBox = new Gtk.Box({ valign: 3, orientation: Gtk.Orientation.VERTICAL });
        let viewIconSizeLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Icon size in view") });
        let viewIconSizeLabel2 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, use_markup: true, label: "<small>" + _("Factor 1 is apps view icon size") + "</small>" });
        viewIconSizeLabel2.get_style_context().add_class("dim-label");
        viewIconSizeLabelBox.pack_start(viewIconSizeLabel1, true, true, 0);
        viewIconSizeLabelBox.pack_start(viewIconSizeLabel2, true, true, 0);
        let viewIconSizeSpin = Gtk.SpinButton.new_with_range(0.5,1.5,0.05);
        viewIconSizeSpin.set_valign(3);
        this.settings.bind("view-icon-size-factor", viewIconSizeSpin, "value", Gio.SettingsBindFlags.DEFAULT);
        viewIconSizeBox.pack_start(viewIconSizeLabelBox, true, true, 4);
        viewIconSizeBox.pack_start(viewIconSizeSpin, false, false, 4);
        listBox.add(viewIconSizeBox);
        
        
        let columnsBox = new Gtk.Box({ margin: MARGIN });
        let columnsLabelBox = new Gtk.Box({ valign: 3, orientation: Gtk.Orientation.VERTICAL });
        let columnsLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Maximum number of columns") });
        let columnsLabel2 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, use_markup: true, label: "<small>" + _("default") + " 6</small>" });
        columnsLabel2.get_style_context().add_class("dim-label");
        columnsLabelBox.pack_start(columnsLabel1, true, true, 0);
        columnsLabelBox.pack_start(columnsLabel2, true, true, 0);
        let columnsSpin = Gtk.SpinButton.new_with_range(1,14,1);
        columnsSpin.set_valign(3);
        this.settings.bind("max-columns", columnsSpin, "value", Gio.SettingsBindFlags.DEFAULT);
        columnsBox.pack_start(columnsLabelBox, true, true, 4);
        columnsBox.pack_start(columnsSpin, false, false, 4);
        listBox.add(columnsBox);
        
        
        let rowsBox = new Gtk.Box({ margin: MARGIN });
        let rowsLabelBox = new Gtk.Box({ valign: 3, orientation: Gtk.Orientation.VERTICAL });
        let rowsLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Minimum number of rows") });
        let rowsLabel2 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, use_markup: true, label: "<small>" + _("default") + " 4</small>" });
        rowsLabel2.get_style_context().add_class("dim-label");
        rowsLabelBox.pack_start(rowsLabel1, true, true, 0);
        rowsLabelBox.pack_start(rowsLabel2, true, true, 0);
        let rowsSpin = Gtk.SpinButton.new_with_range(2,12,1);
        rowsSpin.set_valign(3);
        this.settings.bind("min-rows", rowsSpin, "value", Gio.SettingsBindFlags.DEFAULT);
        rowsBox.pack_start(rowsLabelBox, true, true, 4);
        rowsBox.pack_start(rowsSpin, false, false, 4);
        listBox.add(rowsBox);
        
        
        let doubleClickBox = new Gtk.Box({ margin: MARGIN });
        let doubleClickLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Double click to open items") });
        let doubleClickSwitch = new Gtk.Switch({ valign: 3 });
        this.settings.bind("double-click-to-open", doubleClickSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        doubleClickBox.pack_start(doubleClickLabel, true, true, 4);
        doubleClickBox.pack_start(doubleClickSwitch, false, false, 4);
        listBox.add(doubleClickBox);
        
        
        let children = listBox.get_children();
        for (let i = 0; i < children.length; i++) {
            if (children[i].activatable)
                children[i].set_activatable(false);
        }
    },
    
    addSeparator: function(container) {
        let separatorRow = new Gtk.ListBoxRow( {sensitive: false });
        separatorRow.add(new Gtk.Separator({ margin: MARGIN }));
        container.add(separatorRow);
    }
});

const TogglesPrefsPage = new GObject.Class({
    Name: UUID + '-TogglesPrefsPage',
    Extends: Gtk.ScrolledWindow,

    _init: function(params) {
        this.parent();

        this.settings = Convenience.getSettings();
        this.handlers = [];
        this.connect('destroy', () => {
            this.handlers.forEach(handler => handler[0].disconnect(handler[1]));
        });
        
        let box = new Gtk.Box({ orientation: Gtk.Orientation.HORIZONTAL });
        this.add(box);
        
        box.add(new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, hexpand: true }));
        let listBox = new Gtk.ListBox({ selection_mode: 0, hexpand: true, margin_top: 2*MARGIN, margin_bottom: 2*MARGIN });
        box.add(listBox);
        box.add(new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, hexpand: true }));
        
        let styleContext = listBox.get_style_context();
        styleContext.add_class('background');
        
        
        let toggleFilesViewkeybindings = { 'toggle-files-view': _("Shortcut to display Files view") };
        let toggleFilesViewKeybindingsWidget = new KeybindingsWidget(toggleFilesViewkeybindings, this.settings);
        toggleFilesViewKeybindingsWidget.margin = MARGIN;
        listBox.add(toggleFilesViewKeybindingsWidget);
        
        
        let overviewSwitchingBox = new Gtk.Box({ margin: MARGIN });
        let overviewSwitchingLabelBox = new Gtk.Box({ valign: 3, orientation: Gtk.Orientation.VERTICAL });
        let overviewSwitchingLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Improve overview switching") });
        let overviewSwitchingLabel2 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, use_markup: true, label: "<small>" + _("Change the behaviour of Super+A and Super+S inside the overview to facilitate switching between views") + "</small>" });
        overviewSwitchingLabel2.get_style_context().add_class("dim-label");
        overviewSwitchingLabelBox.pack_start(overviewSwitchingLabel1, true, true, 0);
        overviewSwitchingLabelBox.pack_start(overviewSwitchingLabel2, true, true, 0);
        let overviewSwitchingSwitch = new Gtk.Switch({ valign: 3 });
        this.settings.bind("improve-overview-switching", overviewSwitchingSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        overviewSwitchingBox.pack_start(overviewSwitchingLabelBox, true, true, 4);
        overviewSwitchingBox.pack_start(overviewSwitchingSwitch, false, false, 4);
        listBox.add(overviewSwitchingBox);
        this.addSeparator(listBox);
        
        
        let cornerBox = new Gtk.Box({ margin: MARGIN });
        let cornerLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Add a hot corner") });
        let cornerCombo = new Gtk.ComboBoxText({ valign: 3 });
        cornerCombo.append('none',_("None"));
        cornerCombo.append('top-left',_("Top left"));
        cornerCombo.append('top-right',_("Top right"));
        cornerCombo.append('bottom-left',_("Bottom left"));
        cornerCombo.append('bottom-right',_("Bottom right"));
        this.settings.bind("hot-corner", cornerCombo, "active-id", Gio.SettingsBindFlags.DEFAULT);
        cornerBox.pack_start(cornerLabel, true, true, 4);
        cornerBox.pack_start(cornerCombo, false, false, 4);
        listBox.add(cornerBox);
        
        
        let cornerSensitivityBox = new Gtk.Box({ margin: MARGIN });
        let cornerSensitivityLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Hot corner sensitivity") });
        let cornerSensitivityScale = Gtk.Scale.new_with_range(Gtk.Orientation.HORIZONTAL, 0, 1, 0.01);
        cornerSensitivityScale.set_valign(3);
        cornerSensitivityScale.set_round_digits(2);
        cornerSensitivityScale.add_mark(0.5, Gtk.PositionType.BOTTOM, _("default"));
        this.settings.bind("hot-corner-sensitivity", cornerSensitivityScale.adjustment, "value", Gio.SettingsBindFlags.DEFAULT);
        cornerSensitivityBox.pack_start(cornerSensitivityLabel, true, true, 4);
        cornerSensitivityBox.pack_start(cornerSensitivityScale, true, true, 4);
        listBox.add(cornerSensitivityBox);
        cornerSensitivityBox.set_sensitive(this.settings.get_string('hot-corner') == 'none' ? false : true);
        this.handlers.push([this.settings, this.settings.connect('changed::hot-corner', () => {
            cornerSensitivityBox.set_sensitive(this.settings.get_string('hot-corner') == 'none' ? false : true);
        })]);
        this.addSeparator(listBox);
        
        
        let dashBox = new Gtk.Box({ margin: MARGIN });
        let dashLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Add toggle button in dash/dock") });
        let dashCombo = new Gtk.ComboBoxText({ valign: 3 });
        dashCombo.append('none',_("None"));
        dashCombo.append('above',_("Above"));
        dashCombo.append('below',_("Below"));
        this.settings.bind("dash-button", dashCombo, "active-id", Gio.SettingsBindFlags.DEFAULT);
        dashBox.pack_start(dashLabel, true, true, 4);
        dashBox.pack_start(dashCombo, false, false, 4);
        listBox.add(dashBox);
        
        
        let dashCustomBox = new Gtk.Box({ margin: MARGIN });
        let dashCustomLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Custom button")});
        let dashCustomRightBox = new Gtk.Box({ valign: 3, spacing: 8 });
        let dashCustomButtonImage = new Gtk.Image({ icon_name: 'emblem-system-symbolic' });
        let dashCustomButton = new Gtk.Button({ image: dashCustomButtonImage, valign: Gtk.Align.CENTER });
        dashCustomButton.get_style_context().add_class("circular");
        dashCustomButton.connect('clicked', () => new ToggleCustomButtonDialog(this.get_toplevel(), 'dash'));
        let dashCustomSwitch = new Gtk.Switch({ valign: Gtk.Align.CENTER });
        this.settings.bind("dash-custom-button", dashCustomSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        this.settings.bind("dash-custom-button", dashCustomButton, "sensitive", Gio.SettingsBindFlags.GET);
        dashCustomRightBox.pack_start(dashCustomButton, false, false, 4);
        dashCustomRightBox.pack_start(dashCustomSwitch, false, false, 4);
        dashCustomBox.pack_start(dashCustomLabel, true, true, 4);
        dashCustomBox.pack_start(dashCustomRightBox, false, false, 4);
        listBox.add(dashCustomBox);
        dashCustomBox.set_sensitive(this.settings.get_string('dash-button') == 'none' ? false : true);
        this.handlers.push([this.settings, this.settings.connect('changed::dash-button', () => {
            dashCustomBox.set_sensitive(this.settings.get_string('dash-button') == 'none' ? false : true);
        })]);
        this.addSeparator(listBox);
        
        
        let panelBox = new Gtk.Box({ margin: MARGIN });
        let panelLabelBox = new Gtk.Box({ valign: 3, orientation: Gtk.Orientation.VERTICAL });
        let panelLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Add toggle button in panel") });
        let panelLabel2 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, use_markup: true, label: "<small>" + _("right panel is status area") + "</small>" });
        panelLabel2.get_style_context().add_class("dim-label");
        panelLabelBox.pack_start(panelLabel1, true, true, 0);
        panelLabelBox.pack_start(panelLabel2, true, true, 0);
        let panelCombo = new Gtk.ComboBoxText({ valign: 3 });
        panelCombo.append('none',_("None"));
        panelCombo.append('left',_("To the left"));
        panelCombo.append('center',_("To the center"));
        panelCombo.append('right',_("To the right"));
        this.settings.bind("panel-button", panelCombo, "active-id", Gio.SettingsBindFlags.DEFAULT);
        panelBox.pack_start(panelLabelBox, true, true, 4);
        panelBox.pack_start(panelCombo, false, false, 4);
        listBox.add(panelBox);
        
        
        let panelPositionBox = new Gtk.Box({ margin: MARGIN });
        let panelPositionLabelBox = new Gtk.Box({ valign: 3, orientation: Gtk.Orientation.VERTICAL });
        let panelPositionLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Panel button accurate position") });
        let panelPositionLabel2 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, use_markup: true, label: "<small>" + _("0 for left, -1 for right") + "</small>" });
        panelPositionLabel2.get_style_context().add_class("dim-label");
        panelPositionLabelBox.pack_start(panelPositionLabel1, true, true, 0);
        panelPositionLabelBox.pack_start(panelPositionLabel2, true, true, 0);
        let panelPositionSpin = Gtk.SpinButton.new_with_range(-1,99,1);
        panelPositionSpin.set_valign(3);
        this.settings.bind("panel-button-position", panelPositionSpin, "value", Gio.SettingsBindFlags.DEFAULT);
        panelPositionBox.pack_start(panelPositionLabelBox, true, true, 4);
        panelPositionBox.pack_start(panelPositionSpin, false, false, 4);
        listBox.add(panelPositionBox);
        panelPositionBox.set_sensitive(this.settings.get_string('panel-button') == 'none' ? false : true);
        this.handlers.push([this.settings, this.settings.connect('changed::panel-button', () => {
            panelPositionBox.set_sensitive(this.settings.get_string('panel-button') == 'none' ? false : true);
        })]);
        
        
        let panelCustomBox = new Gtk.Box({ margin: MARGIN });
        let panelCustomLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Custom button")});
        let panelCustomRightBox = new Gtk.Box({ valign: 3, spacing: 8 });
        let panelCustomButtonImage = new Gtk.Image({ icon_name: 'emblem-system-symbolic' });
        let panelCustomButton = new Gtk.Button({ image: panelCustomButtonImage, valign: Gtk.Align.CENTER });
        panelCustomButton.get_style_context().add_class("circular");
        panelCustomButton.connect('clicked', () => new ToggleCustomButtonDialog(this.get_toplevel(), 'panel'));
        let panelCustomSwitch = new Gtk.Switch({ valign: Gtk.Align.CENTER });
        this.settings.bind("panel-custom-button", panelCustomSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        this.settings.bind("panel-custom-button", panelCustomButton, "sensitive", Gio.SettingsBindFlags.GET);
        panelCustomRightBox.pack_start(panelCustomButton, false, false, 4);
        panelCustomRightBox.pack_start(panelCustomSwitch, false, false, 4);
        panelCustomBox.pack_start(panelCustomLabel, true, true, 4);
        panelCustomBox.pack_start(panelCustomRightBox, false, false, 4);
        listBox.add(panelCustomBox);
        panelCustomBox.set_sensitive(this.settings.get_string('panel-button') == 'none' ? false : true);
        this.handlers.push([this.settings, this.settings.connect('changed::panel-button', () => {
            panelCustomBox.set_sensitive(this.settings.get_string('panel-button') == 'none' ? false : true);
        })]);
        
        
        let children = listBox.get_children();
        for (let i = 0; i < children.length; i++) {
            if (children[i].activatable)
                children[i].set_activatable(false);
        }
    },
    
    addSeparator: function(container) {
        let separatorRow = new Gtk.ListBoxRow({ sensitive: false });
        separatorRow.add(new Gtk.Separator({ margin: MARGIN }));
        container.add(separatorRow);
    }
});

const ToggleCustomButtonDialog = new GObject.Class({
    Name: UUID + '-ToggleCustomButtonDialog',
    Extends: Gtk.Dialog,
    
    _init: function(toplevel, type) {
        this.parent({ title: _("Custom button"),
                      transient_for: toplevel,
                      modal: true });
        
        this.settings = Convenience.getSettings();
        
        let listBox = new Gtk.ListBox({ selection_mode: 0, hexpand: true, margin_top: 2*MARGIN, margin_bottom: 2*MARGIN });
        this.get_content_area().add(listBox);
        
        listBox.get_style_context().add_class('background');
        
        
        if (type == 'panel') {
            let panelAppearanceBox = new Gtk.Box({ margin: MARGIN });
            let panelAppearanceLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Panel button appearance") });
            let radioBox = new Gtk.Box({ valign: 3, orientation: Gtk.Orientation.HORIZONTAL });
            let iconRadio = new Gtk.RadioButton({ label: _("Icon") });
            iconRadio.connect('toggled', (widget) => {
                if (widget.get_active())
                    this.settings.set_string('panel-button-appearance', 'icon');
            });
            let textRadio = new Gtk.RadioButton({ group: iconRadio, label: _("Text") });
            textRadio.connect('toggled', (widget) => {
                if (widget.get_active())
                    this.settings.set_string('panel-button-appearance', 'text');
            });
            if (this.settings.get_string('panel-button-appearance') == 'icon')
                iconRadio.set_active(true);
            else
                textRadio.set_active(true);
            radioBox.pack_start(iconRadio, true, true, 4);
            radioBox.pack_start(textRadio, true, true, 4);
            panelAppearanceBox.pack_start(panelAppearanceLabel, true, true, 4);
            panelAppearanceBox.pack_start(radioBox, false, false, 0);
            listBox.add(panelAppearanceBox);
        }
        
        
        let iconNameBox = new Gtk.Box({ margin: MARGIN });
        let iconNameLabelBox = new Gtk.Box({ valign: 3, orientation: Gtk.Orientation.VERTICAL });
        let iconNameLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Icon name") });
        let iconNameLabel2 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, use_markup: true, label: "<small>" + _("default") + ": <i>folder-symbolic</i>" + "</small>" });
        iconNameLabel2.get_style_context().add_class("dim-label");
        iconNameLabelBox.pack_start(iconNameLabel1, true, true, 0);
        iconNameLabelBox.pack_start(iconNameLabel2, true, true, 0);
        let iconNameEntry = new Gtk.Entry({ hexpand: true, valign: 3 });
        iconNameEntry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, this.settings.get_string(type + "-icon-name"));
        this.settings.bind(type + "-icon-name", iconNameEntry, "text", Gio.SettingsBindFlags.DEFAULT);
        this.settings.bind(type + "-icon-name", iconNameEntry, "secondary-icon-name", Gio.SettingsBindFlags.DEFAULT);
        iconNameBox.pack_start(iconNameLabelBox, true, true, 4);
        iconNameBox.pack_start(iconNameEntry, true, true, 4);
        listBox.add(iconNameBox);
        
        
        let visuBox = new Gtk.Box({ margin: MARGIN });
        let visuLabelBox = new Gtk.Box({ valign: 3, orientation: Gtk.Orientation.VERTICAL });
        let visuLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Visualize icons and their name") });
        let visuLabel2 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, use_markup: true, label: "<small>" + _("gtk3-icon-browser") + "</small>" });
        visuLabel2.get_style_context().add_class("dim-label");
        visuLabelBox.pack_start(visuLabel1, true, true, 0);
        visuLabelBox.pack_start(visuLabel2, true, true, 0);
        let visuButton = new Gtk.Button({ label: _("Open"), valign: 3 });
        visuButton.connect('clicked', function() {
            GLib.spawn_command_line_sync("gtk3-icon-browser");
        });
        visuBox.pack_start(visuLabelBox, true, true, 4);
        visuBox.pack_start(visuButton, false, false, 4);
        listBox.add(visuBox);
        visuBox.connect("realize", () => {
        try {
            GLib.spawn_command_line_sync("gtk3-icon-browser --help");
        } catch(e) {
            visuButton.set_sensitive(false);
        }});
        
        
        let children = listBox.get_children();
        for (let i = 0; i < children.length; i++) {
            if (children[i].activatable)
                children[i].set_activatable(false);
        }
        
        this.connect('response', (dialog, id) => dialog.destroy());
        this.show_all();
    }
});

/*this code comes from Sticky Notes View by Sam Bull, https://extensions.gnome.org/extension/568/notes/ */
const KeybindingsWidget = new GObject.Class({
    Name: UUID + '-KeybindingsWidget',
    Extends: Gtk.Box,

    _init: function(keybindings, settings) {
        this.parent();
        this.set_orientation(Gtk.Orientation.VERTICAL);

        this._keybindings = keybindings;
        this._settings = settings;

        this._columns = {
            NAME: 0,
            ACCEL_NAME: 1,
            MODS: 2,
            KEY: 3
        };

        this._store = new Gtk.ListStore();
        this._store.set_column_types([
            GObject.TYPE_STRING,
            GObject.TYPE_STRING,
            GObject.TYPE_INT,
            GObject.TYPE_INT
        ]);

        this._tree_view = new Gtk.TreeView({
            model: this._store,
            hexpand: false,
            vexpand: false
        });
        this._tree_view.set_activate_on_single_click(false);
        this._tree_view.get_selection().set_mode(Gtk.SelectionMode.SINGLE);

        let action_renderer = new Gtk.CellRendererText();
        let action_column = new Gtk.TreeViewColumn({
            title: _(""),
            expand: true,
        });
        action_column.pack_start(action_renderer, true);
        action_column.add_attribute(action_renderer, 'text', 1);
        this._tree_view.append_column(action_column);
               
        let keybinding_renderer = new Gtk.CellRendererAccel({
            editable: true,
            accel_mode: Gtk.CellRendererAccelMode.GTK
        });
        keybinding_renderer.connect('accel-edited', (renderer, iter, key, mods) => {
            let value = Gtk.accelerator_name(key, mods);
            let [success, iterator ] =
                this._store.get_iter_from_string(iter);

            if(!success) {
                printerr(_("Can't change keybinding"));
            }

            let name = this._store.get_value(iterator, 0);

            this._store.set(
                iterator,
                [this._columns.MODS, this._columns.KEY],
                [mods, key]
            );
            this._settings.set_strv(name, [value]);
        });

        let keybinding_column = new Gtk.TreeViewColumn({
            title: _(""),
        });
        keybinding_column.pack_end(keybinding_renderer, false);
        keybinding_column.add_attribute(
            keybinding_renderer,
            'accel-mods',
            this._columns.MODS
        );
        keybinding_column.add_attribute(
            keybinding_renderer,
            'accel-key',
            this._columns.KEY
        );
        this._tree_view.append_column(keybinding_column);
        this._tree_view.columns_autosize();
        this._tree_view.set_headers_visible(false);

        this.add(this._tree_view);
        this.keybinding_column = keybinding_column;
        this.action_column = action_column;

        this._refresh();
    },

    _refresh: function() {
        this._store.clear();

        for(let settings_key in this._keybindings) {
            let [key, mods] = Gtk.accelerator_parse(
                this._settings.get_strv(settings_key)[0]
            );

            let iter = this._store.append();
            this._store.set(iter,
                [
                    this._columns.NAME,
                    this._columns.ACCEL_NAME,
                    this._columns.MODS,
                    this._columns.KEY
                ],
                [
                    settings_key,
                    this._keybindings[settings_key],
                    mods,
                    key
                ]
            );
        }
    }
});

const ShortcutsPage = new GObject.Class({
    Name: UUID + '-ShortcutsPage',
    Extends: Gtk.ScrolledWindow,

    _init: function(params) {
        this.parent();
        
        let box = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        this.add(box);
        
        let shortcutsLabel = new Gtk.Label({ wrap: true, xalign: 0, use_markup: true, justify: 2, label: "<b>" + _("Internal shortcuts") + "</b>" + "\n" + "<small>" + _("Not editable") + "</small>" });
        let shortcutsFrame = new Gtk.Frame({ valign: 1, shadow_type: 0,
                                    label_widget: shortcutsLabel, label_yalign: 0.5, label_xalign: 0.5,
                                    margin: MARGIN*2 });
        box.add(shortcutsFrame);
        let shortcutsListBox = new Gtk.ListBox({ selection_mode: 0, margin: MARGIN });
        shortcutsFrame.add(shortcutsListBox);
        
        
        let hiddenBox = new Gtk.Box({ margin: MARGIN });
        let hiddenLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Show/hide hidden files") });
        let hiddenLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + <b>H</b>" });
        hiddenBox.pack_start(hiddenLabel1, true, true, 4);
        hiddenBox.pack_start(hiddenLabel2, false, false, 4);
        shortcutsListBox.add(hiddenBox);
        this.addSeparator(shortcutsListBox);
        
        let sortReversedBox = new Gtk.Box({ margin: MARGIN });
        let sortReversedLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Reverse sort order") });
        let sortReversedLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + <b>F2</b>" });
        sortReversedBox.pack_start(sortReversedLabel1, true, true, 4);
        sortReversedBox.pack_start(sortReversedLabel2, false, false, 4);
        shortcutsListBox.add(sortReversedBox);
        
        let sortByNameBox = new Gtk.Box({ margin: MARGIN });
        let sortByNameLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Sort by %s").format(_("name")) });
        let sortByNameLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + <b>F3</b>" });
        sortByNameBox.pack_start(sortByNameLabel1, true, true, 4);
        sortByNameBox.pack_start(sortByNameLabel2, false, false, 4);
        shortcutsListBox.add(sortByNameBox);
        
        let sortByTypeBox = new Gtk.Box({ margin: MARGIN });
        let sortByTypeLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Sort by %s").format(_("type")) });
        let sortByTypeLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + <b>F4</b>" });
        sortByTypeBox.pack_start(sortByTypeLabel1, true, true, 4);
        sortByTypeBox.pack_start(sortByTypeLabel2, false, false, 4);
        shortcutsListBox.add(sortByTypeBox);
        
        let sortByDateBox = new Gtk.Box({ margin: MARGIN });
        let sortByDateLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Sort by %s").format(_("date")) });
        let sortByDateLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + <b>F5</b>" });
        sortByDateBox.pack_start(sortByDateLabel1, true, true, 4);
        sortByDateBox.pack_start(sortByDateLabel2, false, false, 4);
        shortcutsListBox.add(sortByDateBox);
        
        let sortBySizeBox = new Gtk.Box({ margin: MARGIN });
        let sortBySizeLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Sort by %s").format(_("size")) });
        let sortBySizeLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + <b>F6</b>" });
        sortBySizeBox.pack_start(sortBySizeLabel1, true, true, 4);
        sortBySizeBox.pack_start(sortBySizeLabel2, false, false, 4);
        shortcutsListBox.add(sortBySizeBox);
        
        let eraseMetadataBox = new Gtk.Box({ margin: MARGIN });
        let eraseMetadataLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Erase sort metadata (if sorting is synchronized)") });
        let eraseMetadataLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + <b>F7</b>" });
        eraseMetadataBox.pack_start(eraseMetadataLabel1, true, true, 4);
        eraseMetadataBox.pack_start(eraseMetadataLabel2, false, false, 4);
        shortcutsListBox.add(eraseMetadataBox);
        this.addSeparator(shortcutsListBox);
        
        let undoBox = new Gtk.Box({ margin: MARGIN });
        let undoLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Undo operation") });
        let undoLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + <b>Z</b>" });
        undoBox.pack_start(undoLabel1, true, true, 4);
        undoBox.pack_start(undoLabel2, false, false, 4);
        shortcutsListBox.add(undoBox);
        
        let redoBox = new Gtk.Box({ margin: MARGIN });
        let redoLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Redo operation") });
        let redoLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Shift</b> + <b>Ctrl</b> + <b>Z</b>" });
        redoBox.pack_start(redoLabel1, true, true, 4);
        redoBox.pack_start(redoLabel2, false, false, 4);
        shortcutsListBox.add(redoBox);
        this.addSeparator(shortcutsListBox);
        
        let thumbnailLargeBox = new Gtk.Box({ margin: MARGIN });
        let thumbnailLargeLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Generate missing thumbnails") });
        let thumbnailLargeLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + <b>T</b>" });
        thumbnailLargeBox.pack_start(thumbnailLargeLabel1, true, true, 4);
        thumbnailLargeBox.pack_start(thumbnailLargeLabel2, false, false, 4);
        shortcutsListBox.add(thumbnailLargeBox);
        
        let thumbnailNormalBox = new Gtk.Box({ margin: MARGIN });
        let thumbnailNormalLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Generate missing thumbnails (small size)") });
        let thumbnailNormalLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Shift</b> + <b>Ctrl</b> + <b>T</b>" });
        thumbnailNormalBox.pack_start(thumbnailNormalLabel1, true, true, 4);
        thumbnailNormalBox.pack_start(thumbnailNormalLabel2, false, false, 4);
        shortcutsListBox.add(thumbnailNormalBox);
        this.addSeparator(shortcutsListBox);
        
        let searchEntryBox = new Gtk.Box({ margin: MARGIN });
        let searchEntryLabel = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, use_markup: true, label: "<u>" + _("Search entry") + "</u>" + " :" });
        searchEntryBox.pack_start(searchEntryLabel, true, true, 4);
        shortcutsListBox.add(searchEntryBox);
        
        let currentLocationBox = new Gtk.Box({ margin: MARGIN });
        let currentLocationLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Enter current location") });
        let currentLocationLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + <b>L</b>" });
        currentLocationBox.pack_start(currentLocationLabel1, true, true, 4);
        currentLocationBox.pack_start(currentLocationLabel2, false, false, 4);
        shortcutsListBox.add(currentLocationBox);
        
        let rootLocationBox = new Gtk.Box({ margin: MARGIN });
        let rootLocationLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Enter root location") });
        let rootLocationLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>/</b>" });
        rootLocationBox.pack_start(rootLocationLabel1, true, true, 4);
        rootLocationBox.pack_start(rootLocationLabel2, false, false, 4);
        shortcutsListBox.add(rootLocationBox);
        
        let homeLocationBox = new Gtk.Box({ margin: MARGIN });
        let homeLocationLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Enter home location") });
        let homeLocationLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>~/</b>" });
        homeLocationBox.pack_start(homeLocationLabel1, true, true, 4);
        homeLocationBox.pack_start(homeLocationLabel2, false, false, 4);
        shortcutsListBox.add(homeLocationBox);
        
        let tagBox = new Gtk.Box({ margin: MARGIN });
        let tagLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Search a tag") });
        let tagLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>#</b>"});
        tagBox.pack_start(tagLabel1, true, true, 4);
        tagBox.pack_start(tagLabel2, false, false, 4);
        shortcutsListBox.add(tagBox);
        
        let completionBox = new Gtk.Box({ margin: MARGIN });
        let completionLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Apply completion") });
        let completionLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Tab</b>" });
        completionBox.pack_start(completionLabel1, true, true, 4);
        completionBox.pack_start(completionLabel2, false, false, 4);
        shortcutsListBox.add(completionBox);
        
        let historyBox = new Gtk.Box({ margin: MARGIN });
        let historyLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Navigate through the history") });
        let historyLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + ⬆" + "<small> " + _("or") + "</small>" + " <b>Ctrl</b> + ⬇"});
        historyBox.pack_start(historyLabel1, true, true, 4);
        historyBox.pack_start(historyLabel2, false, false, 4);
        shortcutsListBox.add(historyBox);
        this.addSeparator(shortcutsListBox);
        
        let parentBox = new Gtk.Box({ margin: MARGIN });
        let parentLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Go to parent folder") });
        let parentLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Alt</b> + ⬆" });
        parentBox.pack_start(parentLabel1, true, true, 4);
        parentBox.pack_start(parentLabel2, false, false, 4);
        shortcutsListBox.add(parentBox);
        
        let homeBox = new Gtk.Box({ margin: MARGIN });
        let homeLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Go to home folder") });
        let homeLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Alt</b> + ↖" });
        homeBox.pack_start(homeLabel1, true, true, 4);
        homeBox.pack_start(homeLabel2, false, false, 4);
        shortcutsListBox.add(homeBox);
        
        let previousBox = new Gtk.Box({ margin: MARGIN });
        let previousLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Go to previous folder") });
        let previousLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: _("<b>BackSpace</b>") + "  " + "<small>" + _("or") + "</small>" + "  <b>Alt</b> + ⬅" });
        previousBox.pack_start(previousLabel1, true, true, 4);
        previousBox.pack_start(previousLabel2, false, false, 4);
        shortcutsListBox.add(previousBox);
        this.addSeparator(shortcutsListBox);
        
        let switchLeftBox = new Gtk.Box({ margin: MARGIN });
        let switchLeftLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Switch to left tab") });
        let switchLeftLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + ⬅" + "<small> " + _("or") + "</small>" + "  <b>Ctrl</b> + <b>Page</b>⬇" });
        switchLeftBox.pack_start(switchLeftLabel1, true, true, 4);
        switchLeftBox.pack_start(switchLeftLabel2, false, false, 4);
        shortcutsListBox.add(switchLeftBox);
        
        let switchRightBox = new Gtk.Box({ margin: MARGIN });
        let switchRightLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Switch to right tab") });
        let switchRightLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + <b>Tab</b> " + "<small>" + _("or") + "</small>" + " <b>Ctrl</b> + ➡" + "<small> " + _("or") + "</small>" + "  <b>Ctrl</b> + <b>Page</b>⬆" });
        switchRightBox.pack_start(switchRightLabel1, true, true, 4);
        switchRightBox.pack_start(switchRightLabel2, false, false, 4);
        shortcutsListBox.add(switchRightBox);
        this.addSeparator(shortcutsListBox);
        
        let moreColumnsBox = new Gtk.Box({ margin: MARGIN });
        let moreColumnsLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Increase max-columns number") });
        let moreColumnsLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + ➕" });
        moreColumnsBox.pack_start(moreColumnsLabel1, true, true, 4);
        moreColumnsBox.pack_start(moreColumnsLabel2, false, false, 4);
        shortcutsListBox.add(moreColumnsBox);

        let lessColumnsBox = new Gtk.Box({ margin: MARGIN });
        let lessColumnsLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Decrease max-columns number") });
        let lessColumnsLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + ➖" });
        lessColumnsBox.pack_start(lessColumnsLabel1, true, true, 4);
        lessColumnsBox.pack_start(lessColumnsLabel2, false, false, 4);
        shortcutsListBox.add(lessColumnsBox);
        
        let moreRowsBox = new Gtk.Box({ margin: MARGIN });
        let moreRowsLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Increase min-rows number") });
        let moreRowsLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Alt</b> + ➕" });
        moreRowsBox.pack_start(moreRowsLabel1, true, true, 4);
        moreRowsBox.pack_start(moreRowsLabel2, false, false, 4);
        shortcutsListBox.add(moreRowsBox);
        
        let lessRowsBox = new Gtk.Box({ margin: MARGIN });
        let lessRowsLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Decrease min-rows number") });
        let lessRowsLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Alt</b> + ➖" });
        lessRowsBox.pack_start(lessRowsLabel1, true, true, 4);
        lessRowsBox.pack_start(lessRowsLabel2, false, false, 4);
        shortcutsListBox.add(lessRowsBox);
        this.addSeparator(shortcutsListBox);
        
        let dashBox = new Gtk.Box({ margin: MARGIN });
        let dashLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Show/split/hide the sidebar") });
        let dashLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>F9</b>" });
        dashBox.pack_start(dashLabel1, true, true, 4);
        dashBox.pack_start(dashLabel2, false, false, 4);
        shortcutsListBox.add(dashBox);
        this.addSeparator(shortcutsListBox);
        
        let refreshBox = new Gtk.Box({ margin: MARGIN });
        let refreshLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Refresh view") });
        let refreshLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + <b>R</b>  " + "<small>" + _("or") + "</small>" + "  <b>F5</b>" });
        refreshBox.pack_start(refreshLabel1, true, true, 4);
        refreshBox.pack_start(refreshLabel2, false, false, 4);
        shortcutsListBox.add(refreshBox);
        this.addSeparator(shortcutsListBox);
        
        let nautilusBox = new Gtk.Box({ margin: MARGIN });
        let nautilusLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Open in Nautilus") });
        let nautilusLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + " + _("Left click") });
        nautilusBox.pack_start(nautilusLabel1, true, true, 4);
        nautilusBox.pack_start(nautilusLabel2, false, false, 4);
        shortcutsListBox.add(nautilusBox);
        
        let controlBox = new Gtk.Box({ margin: MARGIN });
        let controlLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Allow to copy/cut many files") });
        let controlLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + " + _("Popup menu open") });
        controlBox.pack_start(controlLabel1, true, true, 4);
        controlBox.pack_start(controlLabel2, false, false, 4);
        shortcutsListBox.add(controlBox);
        
        let shiftBox = new Gtk.Box({ margin: MARGIN });
        let shiftLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Allow to delete a file") });
        let shiftLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Shift</b> + " + _("Popup menu open") });
        shiftBox.pack_start(shiftLabel1, true, true, 4);
        shiftBox.pack_start(shiftLabel2, false, false, 4);
        shortcutsListBox.add(shiftBox);
        
        let copyBox = new Gtk.Box({ margin: MARGIN });
        let copyLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Copy") });
        let copyLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + <b>C</b>" });
        copyBox.pack_start(copyLabel1, true, true, 4);
        copyBox.pack_start(copyLabel2, false, false, 4);
        shortcutsListBox.add(copyBox);
        
        let cutBox = new Gtk.Box({ margin: MARGIN });
        let cutLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Cut") });
        let cutLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + <b>X</b>" });
        cutBox.pack_start(cutLabel1, true, true, 4);
        cutBox.pack_start(cutLabel2, false, false, 4);
        shortcutsListBox.add(cutBox);
        
        let pasteBox = new Gtk.Box({ margin: MARGIN });
        let pasteLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Paste into folder") });
        let pasteLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Ctrl</b> + <b>V</b>" });
        pasteBox.pack_start(pasteLabel1, true, true, 4);
        pasteBox.pack_start(pasteLabel2, false, false, 4);
        shortcutsListBox.add(pasteBox);
        
        let trashBox = new Gtk.Box({ margin: MARGIN });
        let trashLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Move to Trash") });
        let trashLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Delete</b>" });
        trashBox.pack_start(trashLabel1, true, true, 4);
        trashBox.pack_start(trashLabel2, false, false, 4);
        shortcutsListBox.add(trashBox);
        
        let deleteBox = new Gtk.Box({ margin: MARGIN });
        let deleteLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("Delete") });
        let deleteLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Shift</b> + <b>Delete</b>" });
        deleteBox.pack_start(deleteLabel1, true, true, 4);
        deleteBox.pack_start(deleteLabel2, false, false, 4);
        shortcutsListBox.add(deleteBox);
        
        let symlinkBox = new Gtk.Box({ margin: MARGIN });
        let symlinkLabel1 = new Gtk.Label({ halign: 1, wrap: true, xalign: 0, label: _("New symbolic link") });
        let symlinkLabel2 = new Gtk.Label({ wrap: true, use_markup: true, label: "<b>Shift</b> + <b>Ctrl</b> + <b>M</b>" });
        symlinkBox.pack_start(symlinkLabel1, true, true, 4);
        symlinkBox.pack_start(symlinkLabel2, false, false, 4);
        shortcutsListBox.add(symlinkBox);
        
        this.addSeparator(shortcutsListBox);
    },

    addSeparator: function(container) {
        let separatorRow = new Gtk.ListBoxRow({sensitive: false});
        separatorRow.add(new Gtk.Separator({ margin_left: MARGIN, margin_right: MARGIN}));
        container.add(separatorRow);
    }
});


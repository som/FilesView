/*
 * Copyright 2018 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2018 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 6 */

const Clutter = imports.gi.Clutter;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const GObject = imports.gi.GObject;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Shell = imports.gi.Shell;
const Signals = imports.signals;
const St = imports.gi.St;

const Config = imports.misc.config;
const Dash = imports.ui.dash;
const DND = imports.ui.dnd;
const ExtensionUtils = imports.misc.extensionUtils;
const IconGrid = imports.ui.iconGrid;
const Main = imports.ui.main;
const OverviewControls = imports.ui.overviewControls;
const PopupMenu = imports.ui.popupMenu;
const ShellMountOperation = imports.ui.shellMountOperation;

const Me = ExtensionUtils.getCurrentExtension();
const FileIcon = Me.imports.fileIcon;
const Mang = Me.imports.mang;

const _GSE = imports.gettext.domain('gnome-shell-extensions').gettext;
const _N = imports.gettext.domain('nautilus').gettext;
const _ = imports.gettext.domain(Me.metadata['gettext-domain']).gettext;

const GS_VERSION = Config.PACKAGE_VERSION;
const MainOverviewControls = GS_VERSION < '3.36.0' ? Main.overview._controls : Main.overview._overview._controls;
const MainOverviewControlsActor = GS_VERSION < '3.36.0' ? MainOverviewControls.actor : MainOverviewControls;
const OverviewControls334 = GS_VERSION < '3.36.0' ? null : Me.imports.ui.overviewControls334;
const Tweener = GS_VERSION < '3.37.0' ? imports.ui.tweener : imports.tweener.tweener;

const getActor = function(object) {
    return object instanceof Clutter.Actor ? object : object.actor;
};

const SCROLL_TIME = 0.2;
const SCROLL_TIMEOUT = 0.3;
const SIDE_CONTROLS_ANIMATION_TIME = 0.16; // old OverviewControls.SIDE_CONTROLS_ANIMATION_TIME (GS < 3.34)
const DASH_ANIMATION_TIME = 0.2; // old Dash.DASH_ANIMATION_TIME (GS < 3.34)
const Sidebars = { ALONE: 0, FIRST: 1, SECOND: 2, HIDDEN: 3 };
const UUID = Me.uuid.replace(/@/gi, '_at_').replace(/[^a-z0-9+_-]/gi, '_');

// Just handle cases where there is no sidebar
const SlideLayout = new Lang.Class({
    Name: UUID + '-SlideLayout',
    Extends: GS_VERSION < '3.36.0' ? OverviewControls.SlideLayout : OverviewControls334.SlideLayout,

    vfunc_get_preferred_width: function(container, forHeight) {
        if (!container.get_first_child())
            return [0,0];
        else
            return this.parent(container, forHeight);
    },

    vfunc_allocate: function(container, box, flags) {
        if (!container.get_first_child())
            return;
        else
            GS_VERSION < '3.37' ? this.parent(container, box, flags) : this.parent(container, box);
    },
    
    // from GS 3.32
    set translationX(value) {
        this._translationX = value;
        this.layout_changed();
    },
    
    get translationX() {
        return this._translationX;
    }
});

/*
 * Based on DashSlider, SlidingControls._init and ControlsManager in overviewControls.js.
 * Add this.box which can contain two sidebars.
 * Sidebars are not added in _init.
 * 
 * slideIn, slideOut and _updateTranslation:
 * ctrlAltTab manager only shows visible actors, so hide inactive dash/sidebar
 * (show before starting translation and hide on translation completed).
 */
var SidebarSlider = new Lang.Class({
    Name: UUID + '-SidebarSlider',
    
    _init: function() {
        this._visible = true;
        this._inDrag = false;
        
        this.settings = Me.settings;
        let symbolicIcons = this.settings.get_boolean('sidebar-symbolic-icons');
        let showNotMountedDevices = this.settings.get_boolean("show-not-mounted-devices-sidebar");
        this.placesManager = new PlacesManager(symbolicIcons, showNotMountedDevices);
        
        this.layout = new SlideLayout();
        this.actor = new St.Widget({ layout_manager: this.layout, style_class: 'overview-controls', clip_to_allocation: true,
                                     x_align: Clutter.ActorAlign.START, x_expand: true, y_expand: false });
        
        MainOverviewControlsActor.add_child(this.actor);
    },
    
    addSidebars: function() {
        let hasSecondSidebar = this.settings.get_boolean('second-sidebar');
        
        this.box = new St.BoxLayout({ vertical: false, clip_to_allocation: true, x_expand: true });
        this.actor.add_actor(this.box);
        
        this.sidebar = new Sidebar(hasSecondSidebar ? Sidebars.FIRST : Sidebars.ALONE, this.placesManager);
        this.box.add_actor(this.sidebar.actor);
        this.iconSizeChangedHandler = this.sidebar.connect('icon-size-changed', Lang.bind(this, this._updateSlide));
        
        this.sidebarSecond = new Sidebar(hasSecondSidebar ? Sidebars.SECOND : Sidebars.HIDDEN, this.placesManager);
        this.box.add_actor(this.sidebarSecond.actor);
        
        this.hidingHandler = Main.overview.connect('hiding', Lang.bind(this, this.slideOut));
        this.pageChangedHandler = Main.overview.viewSelector.connect('page-changed', Lang.bind(this, this._setVisibility));
        this.pageEmptyHandler = Main.overview.viewSelector.connect('page-empty', Lang.bind(this, this.pageEmpty));
        
        this.emit('sidebar-updated');
    },
    
    removeSidebars: function() {
        if (!this.sidebar)
            return;
        
        Main.overview.disconnect(this.hidingHandler);
        Main.overview.viewSelector.disconnect(this.pageChangedHandler);
        Main.overview.viewSelector.disconnect(this.pageEmptyHandler);
        getActor(MainOverviewControls.dash).visible = true;
        
        this.sidebarSecond.disable();
        this.sidebarSecond = null;
        
        this.sidebar.disconnect(this.iconSizeChangedHandler);
        this.sidebar.disable();
        this.sidebar = null;
        
        this.box.destroy();
    },
    
    updateSidebars: function() {
        if (!this.sidebar)
            return;
        
        let hasSecondSidebar = this.settings.get_boolean('second-sidebar');
        this.sidebar.updateId(hasSecondSidebar ? Sidebars.FIRST : Sidebars.ALONE);
        this.sidebarSecond.updateId(hasSecondSidebar ? Sidebars.SECOND : Sidebars.HIDDEN);
        this.placesManager._updatePlaces();
    },
    
    _getSlide: function() {
        if (this._visible || this._inDrag)
            return 1;
        else
            return 0;
    },
    
    _updateSlide: function() {
        Tweener.addTween(this.layout, { slideX: this._getSlide(),
                                        time: SIDE_CONTROLS_ANIMATION_TIME,
                                        transition: 'easeOutQuad' });
    },
    
    getVisibleWidth: function() {
        let child = this.actor.get_first_child();
        let [, , natWidth, ] = child.get_preferred_size();
        return natWidth;
    },

    _getTranslation: function() {
        let child = this.actor.get_first_child();
        let direction = OverviewControls.getRtlSlideDirection(this.layout.slideDirection, child);
        let visibleWidth = this.getVisibleWidth();

        if (direction == OverviewControls.SlideDirection.LEFT)
            return - visibleWidth;
        else
            return visibleWidth;
    },

    _updateTranslation: function() {
        let translationStart = 0;
        let translationEnd = 0;
        let translation = this._getTranslation();

        let shouldShow = (this._getSlide() > 0);
        if (shouldShow) {
            translationStart = translation;
        } else {
            translationEnd = translation;
        }

        if (this.layout.translationX == translationEnd)
            return;
        if (this.currentAnimation)
            return;
        
        this.currentAnimation = true;
        this.layout.translationX = translationStart;
        Tweener.addTween(this.layout, { translationX: translationEnd,
                                        delay: SIDE_CONTROLS_ANIMATION_TIME,
                                        time: SIDE_CONTROLS_ANIMATION_TIME,
                                        transition: 'easeOutQuad',
                                        onComplete: () => {
                                            this.currentAnimation = false;
                                            this.actor.visible = this._visible;
                                            // Do not hide the dash if the dash is a not-left-side dash-to-dock (GS 3.36+)
                                            let dashIsDock = getActor(MainOverviewControls.dash)._position !== undefined;
                                            let dashIsLeftDock = dashIsDock && getActor(MainOverviewControls.dash)._position == St.Side.LEFT;
                                            if (!dashIsDock || dashIsLeftDock)
                                                getActor(MainOverviewControls.dash).visible = !this._visible;
                                            this.emit('sidebar-translation-finished');
                                        }});
    },
    
    slideIn: function() {
        this._visible = true;
        this.actor.visible = true;
    },

    slideOut: function() {
        this._visible = false;
        getActor(MainOverviewControls.dash).visible = true;
        this._updateTranslation();
    },

    pageEmpty: function() {
        this.layout.slideX = this._getSlide();
        this._updateTranslation();
    },
    
    disable: function() {
        MainOverviewControlsActor.remove_child(this.actor);
        this.actor.destroy();
        
        this.placesManager.destroy();
    },

    _setVisibility: function() {
        if (!Main.overview.visible ||
            (Main.overview.animationInProgress && !Main.overview.visibleTarget))
            return;

        let activePage = Main.overview.viewSelector._activePage;
        let sidebarVisible = (activePage == Main.overview.viewSelector._filesPage);
        
        if (sidebarVisible)
            this.slideIn();
        else
            this.slideOut();
    },
    
    redisplayIcons: function() {
        this.sidebar.redisplayIcons();
        this.sidebarSecond.redisplayIcons();
        
        this.placesManager.symbolicIcons = this.settings.get_boolean('sidebar-symbolic-icons');
        this.placesManager._updateSpecials();
        this.placesManager._updateMounts();
        this.placesManager._reloadBookmarks();
    },
    
    adjustIconSize: function() {
        this.sidebar.adjustIconSize();
        this.sidebarSecond.adjustIconSize();
    },
    
    updateShowNotMountedDevices: function() {
        this.placesManager.showNotMountedDevices = this.settings.get_boolean("show-not-mounted-devices-sidebar");
        this.placesManager._updateMounts();
        this.placesManager._reloadBookmarks();
    }
});
Signals.addSignalMethods(SidebarSlider.prototype);

const getFileFromSource = function(source) {
    if (source instanceof FileIcon.FileIcon) {
        return source.file;
    } else {
        return null;
    }
};

// Forked from dash.js
const ShowTrashIcon = new Mang.Class({
    Name: UUID + '-ShowTrashIcon',
    Extends: Dash.DashItemContainer,
    Signals: { 'menu-state-changed': { param_types: [GObject.TYPE_BOOLEAN] } },

    _init: function(symbolicIcons) {
        this.callParent('_init');
        this.symbolicSuffix = symbolicIcons ? '-symbolic' : '';

        this.button = new St.Button({ style_class: 'show-apps show-trash',
                                      track_hover: true,
                                      can_focus: true,
                                      toggle_mode: false });
        this.button.connect('clicked', Lang.bind(Main.overview.viewSelector.filesDisplay, Main.overview.viewSelector.filesDisplay.openTrash));
        
        this.trashFile = Gio.File.new_for_uri('trash:///');
        
        this._iconActor = null;
        this.icon = new IconGrid.BaseIcon(_N("Trash"),
                                           { setSizeManually: true,
                                             showLabel: false,
                                             createIcon: Lang.bind(this, this._createIcon) });
        
        if (this.trashFile.query_exists(null)) {
            this.trashMonitor = this.trashFile.monitor_directory(Gio.FileMonitorFlags.NONE, null);
            this.trashMonitor.connect('changed', Lang.bind(this, this.syncIcon));
        }
        
        // GS 3.32- : this.icon.actor, GS 3.32+ this.icon
        this.button.add_actor(GS_VERSION < '3.32.0' ? this.icon.actor : this.icon);
        this.button._delegate = this;

        this.setChild(this.button);
        this.setDragIcon(null);
        
        this.button.connect('button-press-event', Lang.bind(this, this._onButtonPress));
        this.button.connect('popup-menu', Lang.bind(this, this._onKeyboardPopupMenu));
        
        this.button.connect('destroy', () => {
            if (this.trashMonitor)
                this.trashMonitor.cancel();
        });
        
        this._menu = null;
        this._menuManager = new PopupMenu.PopupMenuManager(GS_VERSION < '3.33.0' ? { actor: this.button } : this.button);
    },
    
    _onButtonPress: function(actor, event) {
        let button = event.get_button();
        if (button == 3) {
            this.popupMenu();
            return Clutter.EVENT_STOP;
        }
        return Clutter.EVENT_PROPAGATE;
    },
    
    _onKeyboardPopupMenu: function() {
        this.popupMenu();
        this._menu.actor.navigate_focus(null, Gtk.DirectionType.TAB_FORWARD, false);
    },
    
    _onMenuPoppedDown: function() {
		this.button.sync_hover();
		this.emit('menu-state-changed', false);
	},
    
    popupMenu: function() {
        if (!this._menu) {
            this._menu = new FileIcon.FileIconMenu({ actor: this.button, file: this.trashFile });
            this._menu.connect('open-state-changed', (menu, isPoppedUp) => {
				if (!isPoppedUp) this._onMenuPoppedDown();
			});
			this._menuManager.addMenu(this._menu);
        }
        
        this.emit('menu-state-changed', true);
        
		this.button.set_hover(true);
		this._menu._redisplay();
		this._menu.open();
		this._menuManager.ignoreRelease();
		return false;
    },

    _createIcon: function(size) {
        if (this.trashFile.query_exists(null)) {
            this.trashFileInfo = this.trashFile.query_info('trash::item-count', Gio.FileQueryInfoFlags.NONE, null);
            this.trashCount = this.trashFileInfo.get_attribute_uint32('trash::item-count');
        }
        
        let iconName = this.trashCount ? 'user-trash-full' + this.symbolicSuffix : 'user-trash' + this.symbolicSuffix;
        this._iconActor = new St.Icon({ icon_name: iconName,
                                        icon_size: size,
                                        style_class: 'show-trash-icon',
                                        can_focus: true,
                                        track_hover: true });
        return this._iconActor;
    },
    
    syncIcon: function() {
        if (this.trashFile.query_exists(null)) {
            this.trashFileInfo = this.trashFile.query_info('trash::item-count', Gio.FileQueryInfoFlags.NONE, null);
            this.trashCount = this.trashFileInfo.get_attribute_uint32('trash::item-count');
        }
        
        if (this.trashCount)
            this.icon.icon.set_icon_name('user-trash-full' + this.symbolicSuffix);
        else
            this.icon.icon.set_icon_name('user-trash' + this.symbolicSuffix);
    },
    
    _getCanRemove: function(source) {
        return source && source.isTrashable &&
               ((source.settings.get_boolean('file-manager') && !Main.overview.fileManager.isWorking) || source.place);
    },

    setDragIcon: function(source) {
        let canRemove = this._getCanRemove(source);

        this.button.set_hover(canRemove);
        if (this._iconActor)
            this._iconActor.set_hover(canRemove);
            
        if (canRemove) {
            this.setLabelText(source.trashText);
        } else
            this.setLabelText(_N("Trash"));
    },

    handleDragOver: function(source, actor, x, y, time) {
        source.previousDNDTarget = null;
        // notify previous target that source is not over it anymore, see FileIcon.HandleDragOver
        source.emit('set-opacity-low');
        return this._getCanRemove(source) ? DND.DragMotionResult.MOVE_DROP : DND.DragMotionResult.NO_DROP;
    },

    acceptDrop: function(source, actor, x, y, time) {
        if (!this._getCanRemove(source))
            return false;
        source.trashAction();

        return true;
    }
});

const DragPlaceholderItem = new Mang.Class({
    Name: UUID + '-DragPlaceholderItem',
    Extends: Dash.DashItemContainer,

    _init: function(dash) {
        this.callParent('_init');
        this.dash = dash;
        this.bin = new St.Bin({ style_class: 'placeholder' });
        this.bin._delegate = this;
        this.setChild(this.bin);
        // DragPlaceholder is not a place (for dash._redisplay())
        this.place = null;
    },
    
    acceptDrop: function(source, actor, x, y, time) {
        let file = getFileFromSource(source);
        if (!file)
            return false;
        if (source.place) {
            let pos = this.dash._dragPlaceholderPos;
            if (pos <= this.dash._places.length - 1) {
                let file2 = this.dash._places[pos].file;
                this.dash.placesManager.moveBookmarkBefore(file, file2);
            } else {
                let file2 = this.dash._places[pos - 1].file;
                this.dash.placesManager.moveBookmarkAfter(file, file2);
            }
        } else if (!source.isDir) {
            return false;
        } else {
            this.dash.placesManager.insertBookmark(file);
        }

        return true;
    },
    
    handleDragOver: function(source, actor, x, y, time) {
        source.previousDNDTarget = null;
        // notify previous target that source is not over it anymore, see FileIcon.HandleDragOver
        source.emit('set-opacity-low');
        
        let file = getFileFromSource(source);
        if (!file)
            return DND.DragMotionResult.NO_DROP;
        else if (source.place)
            return DND.DragMotionResult.MOVE_DROP;
        else if (!source.isDir)
            return DND.DragMotionResult.NO_DROP;
        else
            return DND.DragMotionResult.COPY_DROP;
    }
});

// Forked from DashActor of dash.js.
// Just handle the case where there is no showTrashButton.
const SidebarActor = new Lang.Class({
    Name: UUID + '-SidebarActor',
    Extends: St.Widget,

    _init: function() {
        let layout = new Clutter.BoxLayout({ orientation: Clutter.Orientation.VERTICAL });
        this.parent({ name: 'dash',
                      layout_manager: layout,
                      clip_to_allocation: true });
    },

    vfunc_allocate: function(box, flags) {
        let contentBox = this.get_theme_node().get_content_box(box);
        let availWidth = contentBox.x2 - contentBox.x1;

        GS_VERSION < '3.37' ? this.set_allocation(box, flags) : this.set_allocation(box);

        let children = this.get_children();
        let scrollView = children[0];
        
        let showTrashButton;
        let [showTrashMinHeight, showTrashNatHeight] = [0, 0];
        if (children.length > 1) {
            showTrashButton = children[1];
            [showTrashMinHeight, showTrashNatHeight] = showTrashButton.get_preferred_height(availWidth);
        }
        
        // add a padding-bottom to scrollView if 'scrollable is active'.
        let [filesIconMinHeight, filesIconNatHeight] = scrollView.get_preferred_height(availWidth);
        if (filesIconNatHeight > contentBox.y2 - showTrashNatHeight - contentBox.y1)
            scrollView.add_style_pseudo_class('scrollable-active');
        else
            scrollView.remove_style_pseudo_class('scrollable-active');
        
        let childBox = new Clutter.ActorBox();
        childBox.x1 = contentBox.x1;
        childBox.y1 = contentBox.y1;
        childBox.x2 = contentBox.x2;
        childBox.y2 = contentBox.y2 - showTrashNatHeight;
        GS_VERSION < '3.37' ? scrollView.allocate(childBox, flags) : scrollView.allocate(childBox);
        
        if (showTrashButton) {
            childBox.y1 = contentBox.y2 - showTrashNatHeight;
            childBox.y2 = contentBox.y2;
            GS_VERSION < '3.37' ? showTrashButton.allocate(childBox, flags) : showTrashButton.allocate(childBox);
        }
    },

    vfunc_get_preferred_height: function(forWidth) {
        let [a, natHeight] = this.parent(forWidth);

        let themeNode = this.get_theme_node();
        let adjustedForWidth = themeNode.adjust_for_width(forWidth);
        
        let children = this.get_children();
        let [minHeight, b] = [0, 0];
        if (children.length > 1) {
            let showTrashButton = children[1];
            [minHeight, b] = showTrashButton.get_preferred_height(adjustedForWidth);
        }
        
        [minHeight, b] = themeNode.adjust_preferred_height(minHeight, natHeight);

        return [minHeight, natHeight];
    }
});

// Based on original Dash and MyDash of Dash-to-Dock
const Sidebar = new Lang.Class({
    Name: UUID + '-Sidebar',

    _init : function(id, placesManager) {
        //add filesDisplay to connect sidebar with filesViews
        this.filesDisplay = Main.overview.viewSelector.filesDisplay;
        
        this.settings = Me.settings;
        this.symbolicIcons = this.settings.get_boolean('sidebar-symbolic-icons');
        this.iconSize = this.settings.get_double('sidebar-icon-size');
        this.placesManager = placesManager;
        
        this._shownInitially = false;

        this._dragPlaceholder = null;
        this._dragPlaceholderPos = -1;
        this._animatingPlaceholdersCount = 0;
        this._showLabelTimeoutId = 0;
        this._resetHoverTimeoutId = 0;
        this._labelShowing = false;

        this._container = new SidebarActor();
        this._scrollView = new St.ScrollView({
            name: 'SidebarScrollview',
            hscrollbar_policy: Gtk.PolicyType.NEVER,
            vscrollbar_policy: Gtk.PolicyType.NEVER,
            enable_mouse_scrolling: true,
            style_class: 'files-view-sidebar-scrollview'
        });
        
        // get vfade to ensureActorVisibleInScrollView()
        this.styleChangeHandler = this._scrollView.connect('style-changed', () => {
            this.vfade = this._scrollView.get_theme_node().get_length('-st-vfade-offset');
            this._scrollView.disconnect(this.styleChangeHandler);
        });
        
        this._box = new St.BoxLayout({ vertical: true, clip_to_allocation: false });
        this._box._delegate = this;
        this._scrollView.add_actor(this._box);
        this._container.add_actor(this._scrollView);
        this._container.set_offscreen_redirect(Clutter.OffscreenRedirect.ALWAYS);
        
        this.updateId(id);
        
        if (this.id == Sidebars.ALONE || this.id == Sidebars.FIRST) {
            this._showTrashIcon = new ShowTrashIcon(this.symbolicIcons);
            this._showTrashIcon.childScale = 1;
            this._showTrashIcon.childOpacity = 255;
            this._showTrashIcon.show(false);
            this._showTrashIcon.icon.setIconSize(this.iconSize);
            this._showTrashIcon.connect('menu-state-changed', (emitter, opened) => {
                this._itemMenuStateChanged(this._showTrashIcon, opened);
            });
            this._hookUpLabel(this._showTrashIcon);
            this._container.add_actor(this._showTrashIcon);
        }

        this.actor = new St.Bin({ child: this._container, x_expand: true });
        if (GS_VERSION < '3.35')
            this.actor.y_fill = true;
        
        // make places initialization async
        Mainloop.idle_add(() => {
            if (!this.placesManager.initialized)
                this.placesManager.initPlaces();
            this._places = this.placesManager.getPlaces();
            this.posFirstBookmark = this.placesManager.posFirstBookmark;
            this._queueRedisplay();
            
            this.placesManagerHandler = this.placesManager.connect('places-updated', (emitter, places, posFirstBookmark) => {
                // do a copy of places (with slice()) not to share the same array between the two sidebars
                this._places = places.slice();
                this.posFirstBookmark = posFirstBookmark;
                this._queueRedisplay();
            });
            
            return GLib.SOURCE_REMOVE;
        });
        
        this.itemDragBeginHandler = Main.overview.connect('files-view-item-drag-begin', Lang.bind(this, this._onDragBegin));
        this.itemDragEndHandler = Main.overview.connect('files-view-item-drag-end', Lang.bind(this, this._onDragEnd));
        this.itemDragCancelledHandler = Main.overview.connect('files-view-item-drag-cancelled', Lang.bind(this, this._onDragCancelled));
        
        this.updateCtrlAltTab();
    },
    
    updateId: function(id) {
        this.id = id;
        
        this._container.set_style_class_name(this.id == Sidebars.FIRST ? 'files-view-sidebar-first' :
                                             this.id == Sidebars.SECOND ? 'files-view-sidebar-second' :
                                             this.id == Sidebars.HIDDEN ? 'files-view-sidebar-hidden' :
                                             '');
        
        this.hasBookmarks = this.id == Sidebars.ALONE || this.id == Sidebars.SECOND;
        
        if (this.hasBookmarks && !this.mountChangedHandler) {
            this.mountChangedHandler = this.placesManager.connect('mount-changed', Lang.bind(this, this.onMountChanged));
        } else if (!this.hasBookmarks && this.mountChangedHandler) {
            this.placesManager.disconnect(this.mountChangedHandler);
            this.mountChangedHandler = null;
        }
        
        this._removeBookmarks();
        
        // Only when called from SidebarSlider.updateSidebars
        // (actor is not defined when updateId is called during initialization).
        if (this.actor)
            this.updateCtrlAltTab();
    },
    
    updateCtrlAltTab: function() {
        // Remove an actor that has not been added previously (Sidebars.HIDDEN) is not an error
        // since GS does nothing in this case.
        Main.ctrlAltTabManager.removeGroup(this.actor);
        
        if (this.id != Sidebars.HIDDEN)
            Main.ctrlAltTabManager.addGroup(this.actor, _GSE("Places") + (this.id == Sidebars.ALONE ? "" : this.id), 'user-bookmarks-symbolic');
    },
    
    disable: function() {
        // ensure _queueRedisplay is not called after
        this.isDisabled = true;
        
        if (this.placesManagerHandler) {
            this.placesManager.disconnect(this.placesManagerHandler);
            this.placesManagerHandler = null;
        }
        
        if (this.mountChangedHandler) {
            this.placesManager.disconnect(this.mountChangedHandler);
            this.mountChangedHandler = null;
        }
        
        Main.overview.disconnect(this.itemDragBeginHandler);
        Main.overview.disconnect(this.itemDragEndHandler);
        Main.overview.disconnect(this.itemDragCancelledHandler);
        Main.ctrlAltTabManager.removeGroup(this.actor);
    },
    
    _onDragBegin: function(emitter, source) {
    
        // If the source is a fileIcon of the view,
        //   create a static dragPlaceHolder that allows to add the source to the sidebar
        //   or to move the source inside the target.
        // If the source is a fileIcon of the sidebar,
        //   create dynamic dragPlaceHolders that allow to change the position of the source in the sidebar
        //   (like in the dash).
        
        if (!source.place && source.isDir && this.hasBookmarks) {
            this._createDragPlaceholder();
        }
        
        this._dragCancelled = false;
        this._dragMonitor = {
            dragMotion: Lang.bind(this, this._onDragMotion)
        };
        DND.addDragMonitor(this._dragMonitor);
    },

    _onDragCancelled: function(emitter, source) {
        this._dragCancelled = true;
        this._endDrag();
    },

    _onDragEnd: function(emitter, source) {
        if (this._dragCancelled)
            return;
        this._endDrag();
    },

    _endDrag: function() {
        this._clearDragPlaceholder();
        if (this._showTrashIcon)
            this._showTrashIcon.setDragIcon(null);
        DND.removeDragMonitor(this._dragMonitor);
    },

    _onDragMotion: function(dragEvent) {
        let file = getFileFromSource(dragEvent.source);
        if (!file)
            return DND.DragMotionResult.CONTINUE;
        let showTrashHovered = this._showTrashIcon && this._showTrashIcon.contains(dragEvent.targetActor);
        if (showTrashHovered) {
            this._clearDragPlaceholder();
            this._showTrashIcon.setDragIcon(dragEvent.source);
        } else {
            if (this._showTrashIcon)
                this._showTrashIcon.setDragIcon(null);
            if (!dragEvent.source.place && dragEvent.source.isDir && !this._dragPlaceholder && this.hasBookmarks)
                this._createDragPlaceholder();
        }

        return DND.DragMotionResult.CONTINUE;
    },

    _queueRedisplay: function() {
        // the timeout that initialize placesManager in _init may call _queueRedisplay after the sidebar is disabled
        if (this.isDisabled)
            return;
        if (this._workId)
            Main.queueDeferredWork(this._workId);
        else
            this._workId = Main.initializeDeferredWork(this._box, Lang.bind(this, this._redisplay));
    },

    _hookUpLabel: function(item, placeIcon) {
        item.child.connect('notify::hover', () => {
            this._syncLabel(item, placeIcon);
        });

        let id = Main.overview.connect('hiding', () => {
            this._labelShowing = false;
            item.hideLabel();
        });
        item.child.connect('destroy', () => {
            Main.overview.disconnect(id);
        });

        if (placeIcon) {
            placeIcon.connect('sync-tooltip', () => {
                this._syncLabel(item, placeIcon);
            });
        }
    },
    
    // update related bookmarks on mount changed
    // (fileIcon is not the same for mounted and not mounted files)
    onMountChanged: function(emitter, file) {
        let children = this._box.get_children().filter(actor => {
            return actor.child &&
                   actor.child._delegate &&
                   actor.child._delegate.place &&
                   actor.child._delegate.place.kind == 'bookmarks' &&
                   (actor.child._delegate.place.file.equal(file) || actor.child._delegate.place.file.has_prefix(file));
        });
        
        children.forEach(actor => actor.destroy());
    },

    _createPlaceItem: function(place) {
        if (place.kind == 'bookmarks') {
            place.isTrashable = true;
            place.trashText = _("Remove from bookmarks");
            place.trashAction = (() => this.placesManager.removeBookmark(place.file));
            place.trashIconName = 'edit-clear-symbolic';
        } else if (place.isRemovable()) {
            place.isTrashable = true;
            place.trashText = _("Eject");
            place.trashAction = (() => place.eject());
            place.trashIconName = 'media-eject-symbolic';
        } else if (place.eject ) {
            place.isTrashable = true;
            place.trashText = _("Unmount");
            place.trashAction = (() => place.eject());
            place.trashIconName = 'media-eject-symbolic';
        } else {
            place.isTrashable = false;
            place.trashText = null;
            place.trashAction = null;
            place.trashIconName = null;
        }
        
        let placeIcon = place.volume ? new FileIcon.VolumeIcon(place) : new FileIcon.FileIcon(place.file, {}, place);
        
        if (placeIcon.hasError)
            return null;
        
        // sync with file.query_info_finish of PlaceInfo.getIcon()
        place.connect('changed', () => {
            placeIcon.icon.icon.set_gicon(place.icon);
        });
        
        if (placeIcon._draggable) {
            placeIcon._draggable.connect('drag-begin',
                                       () => {
                                           placeIcon.actor.opacity = 50;
                                       });
            placeIcon._draggable.connect('drag-end',
                                       () => {
                                           placeIcon.actor.opacity = 255;
                                       });
        }

        placeIcon.connect('menu-state-changed',
                        (placeIcon, opened) => {
                            this._itemMenuStateChanged(item, opened);
                        });
                        
        // from Dash-to-Dock
        placeIcon.actor.connect('clicked', Lang.bind(this, this.ensureActorVisibleInScrollView));
        placeIcon.actor.connect('key-focus-in', Lang.bind(this, this.ensureActorVisibleInScrollView));
        placeIcon.actor.connect('notify::hover', () => {
            if (this._ensurePlaceIconVisibilityTimeout) {
                Mainloop.source_remove(this._ensurePlaceIconVisibilityTimeout);
                this._ensurePlaceIconVisibilityTimeout = null;
            }
            
            if (placeIcon.actor.hover) {
                if (this.ensureActorVisibleInScrollViewTweening) {
                    this.ensureActorVisibleInScrollView(placeIcon.actor);
                } else {
                    this._ensurePlaceIconVisibilityTimeout = Mainloop.timeout_add(SCROLL_TIMEOUT * 1000, () => {
                        this.ensureActorVisibleInScrollView(placeIcon.actor);
                        this._ensurePlaceIconVisibilityTimeout = null;
                        return GLib.SOURCE_REMOVE;
                    });
                }
            }
        });

        let item = new Dash.DashItemContainer();
        item.setChild(placeIcon.actor);

        // Override default PlaceIcon label_actor, now the
        // accessible_name is set at DashItemContainer.setLabelText
        placeIcon.actor.label_actor = null;
        item.setLabelText(place.name);

        placeIcon.icon.setIconSize(this.iconSize);
        this._hookUpLabel(item, placeIcon);
        return item;
    },
    
    // based on gnome-shell misc/util.js
    // fixes offset
    ensureActorVisibleInScrollView: function(actor) {
        actor = actor.get_parent();
        
        let adjustment = this._scrollView.vscroll.adjustment;
        let [value, lower, upper, stepIncrement, pageIncrement, pageSize] = adjustment.get_values();
        let offset = this.vfade;

        let box = actor.get_allocation_box();
        let y1 = box.y1, y2 = box.y2;

        if (y1 < value + offset)
            value = Math.max(0, y1 - offset);
        else if (y2 > value + pageSize - offset)
            value = Math.min(upper, y2 + offset - pageSize);
        else
            return;

        this.ensureActorVisibleInScrollViewTweening = true;
        Mang.removeTweens(adjustment);
        Mang.addTween(adjustment, { value: value, time: SCROLL_TIME, transition: 'easeOutQuad',
                                    onComplete: () => this.ensureActorVisibleInScrollViewTweening = false });
    },

    _itemMenuStateChanged: function(item, opened) {
        // When the menu closes, it calls sync_hover, which means
        // that the notify::hover handler does everything we need to.
        if (opened) {
            if (this._showLabelTimeoutId > 0) {
                Mainloop.source_remove(this._showLabelTimeoutId);
                this._showLabelTimeoutId = 0;
            }

            item.hideLabel();
            this._labelShowing = false;
        } else {
            this._syncLabel(item);
        }
    },

    _syncLabel: function(item, placeIcon) {
        let shouldShow = placeIcon ? placeIcon.shouldShowTooltip() : item.child.get_hover();

        if (shouldShow) {
            if (this._showLabelTimeoutId == 0) {
                let timeout = this._labelShowing ? 0 : Dash.DASH_ITEM_HOVER_TIMEOUT;
                this._showLabelTimeoutId = Mainloop.timeout_add(timeout,
                    () => {
                        this._labelShowing = true;
                        item.showLabel();
                        this._showLabelTimeoutId = 0;
                        return GLib.SOURCE_REMOVE;
                    });
                GLib.Source.set_name_by_id(this._showLabelTimeoutId, '[gnome-shell] item.showLabel');
                if (this._resetHoverTimeoutId > 0) {
                    Mainloop.source_remove(this._resetHoverTimeoutId);
                    this._resetHoverTimeoutId = 0;
                }
            }
        } else {
            if (this._showLabelTimeoutId > 0)
                Mainloop.source_remove(this._showLabelTimeoutId);
            this._showLabelTimeoutId = 0;
            item.hideLabel();
            if (this._labelShowing) {
                this._resetHoverTimeoutId = Mainloop.timeout_add(Dash.DASH_ITEM_HOVER_TIMEOUT,
                    () => {
                        this._labelShowing = false;
                        this._resetHoverTimeoutId = 0;
                        return GLib.SOURCE_REMOVE;
                    });
                GLib.Source.set_name_by_id(this._resetHoverTimeoutId, '[gnome-shell] this._labelShowing');
            }
        }
    },

    adjustIconSize: function() {
        // For the icon size, we only consider children which are "proper"
        // icons (i.e. ignoring drag placeholders) and which are not
        // animating out (which means they will be destroyed at the end of
        // the animation)
        let iconChildren = this._box.get_children().filter(actor => {
            return actor.child &&
                   actor.child._delegate &&
                   actor.child._delegate.icon &&
                   !actor.animatingOut;
        });
        if (this._showTrashIcon)
            iconChildren.push(this._showTrashIcon);

        let oldIconSize = this.iconSize;
        this.iconSize = this.settings.get_double('sidebar-icon-size');
        let scale = oldIconSize / this.iconSize;
        
        if (scale == 1)
            return;
            
        this.emit('icon-size-changed');
        for (let i = 0; i < iconChildren.length; i++) {
            let icon = iconChildren[i].child._delegate.icon;

            // Set the new size immediately, to keep the icons' sizes
            // in sync with this.iconSize
            icon.setIconSize(this.iconSize);

            // Don't animate the icon size change when the overview
            // is transitioning, not visible or when initially filling
            // the dash
            if (!Main.overview.visible || Main.overview.animationInProgress ||
                !this._shownInitially)
                continue;

            let [targetWidth, targetHeight] = icon.icon.get_size();

            // Scale the icon's texture to the previous size and
            // tween to the new size
            icon.icon.set_size(icon.icon.width * scale,
                               icon.icon.height * scale);

            Mang.addTween(icon.icon,
                          { width: targetWidth,
                            height: targetHeight,
                            time: DASH_ANIMATION_TIME,
                            transition: 'easeOutQuad' });
        }
    },
    
    redisplayIcons: function() {
        this._removeAll();
        
        this.symbolicIcons = this.settings.get_boolean('sidebar-symbolic-icons');
        if (this._showTrashIcon) {
            this._showTrashIcon.symbolicSuffix = this.symbolicIcons ? '-symbolic' : '';
            this._showTrashIcon.syncIcon();
        }
    },
    
    _removeAll: function() {
        this._box.get_children().filter(actor => {
            return actor.child &&
                   actor.child._delegate &&
                   actor.child._delegate.place;
        }).forEach(actor => actor.destroy());
    },
    
    _removeBookmarks: function() {
        this._box.get_children().filter(actor => {
            return actor.child &&
                   actor.child._delegate &&
                   actor.child._delegate.place &&
                   actor.child._delegate.place.kind == 'bookmarks';
        }).forEach(actor => actor.destroy());
    },

    _redisplay: function() {
        if (this.id == Sidebars.FIRST) {
            this._places.splice(this.posFirstBookmark);
        } else if (this.id == Sidebars.SECOND) {
            this._places.splice(0, this.posFirstBookmark);
            this.posFirstBookmark = 0;
        } else if (this.id == Sidebars.HIDDEN) {
            this._box.queue_relayout();
            return;
        }
        
        let children = this._box.get_children().filter(actor => {
            return actor.child &&
                   actor.child._delegate &&
                   actor.child._delegate.place;
        });
        // Places currently in the dash
        let oldPlaces = children.map(actor => actor.child._delegate.place);
        let oldUris = oldPlaces.map(place => place.file.get_uri());
        
        // Places emitted by place manager
        let newPlaces = this._places;
        let newUris = newPlaces.map(place => place.file.get_uri());

        let addedItems = [];
        let removedActors = [];

        let newIndex = 0;
        let oldIndex = 0;
        while (newIndex < newPlaces.length || oldIndex < oldPlaces.length) {
            let oldPlace = oldPlaces.length > oldIndex ? oldPlaces[oldIndex] : null;
            let newPlace = newPlaces.length > newIndex ? newPlaces[newIndex] : null;

            // No change at oldIndex/newIndex if uris and icons are equals
            if (oldPlace && newPlace && oldPlace.file.equal(newPlace.file)) {
                oldIndex++;
                newIndex++;
                continue;
            }

            // Place removed at oldIndex
            if (oldPlace && newUris.indexOf(oldPlace.file.get_uri()) == -1) {
                removedActors.push(children[oldIndex]);
                oldIndex++;
                continue;
            }

            // Place added at newIndex
            // '!oldPlace': handle case where there is several places with the same uri
            // (a priori impossible since duplicates are filtered in placeManager._reloadBookmarks())
            if (newPlace && (!oldPlace || oldUris.indexOf(newPlace.file.get_uri()) == -1)) {
                addedItems.push({ place: newPlace,
                                  item: this._createPlaceItem(newPlace),
                                  pos: newIndex });
                newIndex++;
                continue;
            }

            // Place moved
            let nextPlace = newPlaces.length > newIndex + 1 ? newPlaces[newIndex + 1]
                                                        : null;
            let insertHere = nextPlace && nextPlace.file.equal(oldPlace.file);
            let alreadyRemoved = removedActors.reduce((result, actor) => {
                let removedPlace = actor.child._delegate.place;
                return result || removedPlace.file.equal(newPlace.file);
            }, false);

            if (insertHere || alreadyRemoved) {
                let newItem = this._createPlaceItem(newPlace);
                addedItems.push({ place: newPlace,
                                  item: newItem,
                                  pos: newIndex + removedActors.length });
                newIndex++;
            } else {
                removedActors.push(children[oldIndex]);
                oldIndex++;
            }
        }
        
        // filter items that have no error (see _createPlaceItem())
        addedItems = addedItems.filter(item => item.item);

        for (let i = 0; i < addedItems.length; i++)
            this._box.insert_child_at_index(addedItems[i].item,
                                            addedItems[i].pos);

        for (let i = 0; i < removedActors.length; i++) {
            let item = removedActors[i];

            // Don't animate item removal when the overview is transitioning
            // or hidden
            if (Main.overview.visible && !Main.overview.animationInProgress)
                item.animateOutAndDestroy();
            else
                item.destroy();
        }

        this.adjustIconSize();

        // Skip animations on first run when adding the initial set
        // of items, to avoid all items zooming in at once

        let animate = this._shownInitially && Main.overview.visible &&
            !Main.overview.animationInProgress;

        if (!this._shownInitially)
            this._shownInitially = true;

        for (let i = 0; i < addedItems.length; i++) {
            addedItems[i].item.show(animate);
        }
        
        // Workaround for https://bugzilla.gnome.org/show_bug.cgi?id=692744
        // Without it, StBoxLayout may use a stale size cache
        this._box.queue_relayout();
    },
    
    _createDragPlaceholder: function() {
        let fadeIn;
        if (this._dragPlaceholder) {
            this._dragPlaceholder.destroy();
            fadeIn = false;
        } else {
            fadeIn = true;
        }
        this._dragPlaceholder = new DragPlaceholderItem(this);
        this._dragPlaceholder.child.set_width (this.iconSize);
        this._dragPlaceholder.child.set_height (this.iconSize);
        this._box.insert_child_at_index(this._dragPlaceholder, this.posFirstBookmark);
        this._dragPlaceholder.show(fadeIn);
    },

    _clearDragPlaceholder: function() {
        if (this._dragPlaceholder) {
            this._animatingPlaceholdersCount++;
            this._dragPlaceholder.animateOutAndDestroy();
            this._dragPlaceholder.connect('destroy', () => {
                this._animatingPlaceholdersCount--;
            });
            this._dragPlaceholder = null;
        }
        this._dragPlaceholderPos = -1;
    },

    handleDragOver : function(source, actor, x, y, time) {
        let children = this._box.get_children();
        let numChildren = children.length;
        let favPos = children.map(actor => actor.child._delegate).indexOf(source);
        
        if (favPos == -1 || favPos < this.posFirstBookmark)
            return DND.DragMotionResult.NO_DROP;
        
        // fom dash to dock, compatible with scrollbar
        let boxHeight = 0;
        for (let i = 0; i < numChildren; i++)
            boxHeight += children[i].height;

        // Keep the placeholder out of the index calculation; assuming that
        // the remove target has the same size as "normal" items, we don't
        // need to do the same adjustment there.
        if (this._dragPlaceholder) {
            boxHeight -= this._dragPlaceholder.height;
            numChildren--;
        }
        
        let offset = (this._dragPlaceholder && this._dragPlaceholder.y < y) ? -0.5 : 0;

        let pos = Math.round((y * numChildren  / boxHeight) + offset);

        if (pos != this._dragPlaceholderPos && pos >= this.posFirstBookmark && this._animatingPlaceholdersCount == 0) {
            this._dragPlaceholderPos = pos;

            // Don't allow positioning before or after self
            if (favPos != -1 && (pos == favPos || pos == favPos + 1)) {
                this._clearDragPlaceholder();
                return DND.DragMotionResult.CONTINUE;
            }

            // If the placeholder already exists, we just move
            // it, but if we are adding it, expand its size in
            // an animation
            let fadeIn;
            if (this._dragPlaceholder) {
                this._dragPlaceholder.destroy();
                fadeIn = false;
            } else {
                fadeIn = true;
            }

            this._dragPlaceholder = new DragPlaceholderItem(this);
            this._dragPlaceholder.child.set_width (this.iconSize);
            this._dragPlaceholder.child.set_height (this.iconSize);
            this._box.insert_child_at_index(this._dragPlaceholder,
                                            this._dragPlaceholderPos);
            this._dragPlaceholder.show(fadeIn);
        }

        // Remove the drag placeholder if we are not in the
        // "bookmark zone"
        if (pos < this.posFirstBookmark)
            this._clearDragPlaceholder();

        if (!this._dragPlaceholder)
            return DND.DragMotionResult.NO_DROP;

        return DND.DragMotionResult.COPY_DROP;
    },

    addBookmarkItem: function(file, pos) {
        let place = new PlaceInfo('bookmarks', file, file.get_basename(), null, this.symbolicIcons);
        let item = this._createPlaceItem(place);
        this._box.insert_child_at_index(item, pos);
        item.show(true);
    }
});
Signals.addSignalMethods(Sidebar.prototype);

// forked from placeDisplay.js from places-menu extension, https://gitlab.gnome.org/GNOME/gnome-shell-extensions/tree/master/extensions/places-menu
const SECTIONS = [
    'special',
    'devices',
    'network',
    'bookmarks'
];

const PlaceInfo = new Lang.Class({
    Name: UUID + '-PlaceInfo',

    _init: function(kind, file, name, icon, symbolicIcons) {
        // don't overwrite this.symbolicIcons, for other objects that extends PlaceInfo
        if (!this.symbolicIcons)
            this.symbolicIcons = symbolicIcons;
        this.kind = kind;
        this.file = file;
        this.name = name || this._getFileName();
        this.icon = icon ? new Gio.ThemedIcon({ name: icon }) : this.getIcon();
    },

    destroy: function() {
    },

    isRemovable: function() {
        return false;
    },

    getIcon: function() {
        this.file.query_info_async('standard::symbolic-icon,standard::icon', 0, 0, null,
                                   (file, result) => {
                                       try {
                                           let info = file.query_info_finish(result);
                                           this.icon = this.symbolicIcons ? info.get_symbolic_icon() : info.get_icon();
                                           this.emit('changed');
                                       } catch(e) {
                                           if (e instanceof Gio.IOErrorEnum)
                                               return;
                                           throw e;
                                       }
                                   });
        let symbolicSuffix = this.symbolicIcons ? '-symbolic' : '';

        // return a generic icon for this kind for now, until we have the
        // icon from the query info above
        switch (this.kind) {
        case 'network':
            return Gio.ThemedIcon.new_from_names(['folder-remote' + symbolicSuffix, 'folder' + symbolicSuffix]);
        case 'devices':
            return new Gio.ThemedIcon({ name: 'drive-harddisk' + symbolicSuffix });
        case 'special':
        case 'bookmarks':
        default:
            if (!this.file.is_native())
                return Gio.ThemedIcon.new_from_names(['folder-remote' + symbolicSuffix, 'folder' + symbolicSuffix]);
            else
                return new Gio.ThemedIcon({ name: 'folder' + symbolicSuffix });
        }
    },

    _getFileName: function() {
        try {
            let info = this.file.query_info('standard::display-name', 0, null);
            return info.get_display_name();
        } catch(e) {
            if (e instanceof Gio.IOErrorEnum)
                return this.file.get_basename();
            throw e;
        }
    }
});
Signals.addSignalMethods(PlaceInfo.prototype);

const RootInfo = new Lang.Class({
    Name: UUID + '-RootInfo',
    Extends: PlaceInfo,

    _init: function(symbolicIcons) {
        this.symbolicIcons = symbolicIcons;
        this.parent('devices', Gio.File.new_for_path('/'), _GSE("Computer"));
    },

    getIcon: function() {
        return Gio.ThemedIcon.new_from_names(this.symbolicIcons ?
                                             ['computer-symbolic', 'drive-harddisk-system-symbolic', 'drive-harddisk-symbolic'] :
                                             ['computer', 'drive-harddisk-system', 'drive-harddisk']);
    },

    destroy: function() {
        this.parent();
    }
});

const PlaceDeviceInfo = new Lang.Class({
    Name: UUID + '-PlaceDeviceInfo',
    Extends: PlaceInfo,

    _init: function(kind, mount, symbolicIcons) {
        this._mount = mount;
        this.symbolicIcons = symbolicIcons;
        this.parent(kind, mount.get_root(), mount.get_name());
    },

    getIcon: function() {
        let gicon = this.symbolicIcons ? this._mount.get_symbolic_icon() : this._mount.get_icon();
        let file = Me.dir.get_child('data').get_child('icons').get_child('emblem-mounted.svg');
        let emblemIcon = new Gio.FileIcon({ file: file });
        
        return Gio.EmblemedIcon.new(gicon, Gio.Emblem.new(emblemIcon));
    },
    
    isRemovable: function() {
        return this._mount.can_eject();
    },

    eject: function() {
        let mountOp = new ShellMountOperation.ShellMountOperation(this._mount);

        if (this._mount.can_eject())
            this._mount.eject_with_operation(Gio.MountUnmountFlags.NONE,
                                             mountOp.mountOp,
                                             null,
                                             Lang.bind(this, this._ejectFinish));
        else
            this._mount.unmount_with_operation(Gio.MountUnmountFlags.NONE,
                                               mountOp.mountOp,
                                               null,
                                               Lang.bind(this, this._unmountFinish));
    },

    _ejectFinish: function(mount, result) {
        try {
            mount.eject_with_operation_finish(result);
        } catch(e) {
            if (!e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.FAILED_HANDLED))
                this._reportFailure(e);
        }
    },

    _unmountFinish: function(mount, result) {
        try {
            mount.unmount_with_operation_finish(result);
        } catch(e) {
            if (!e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.FAILED_HANDLED))
                this._reportFailure(e);
        }
    },

    _reportFailure: function(exception) {
        let msg = _GSE("Ejecting drive “%s” failed:").format(this._mount.get_name());
        Main.notifyError(msg, exception.message);
    }
});

const PlaceVolumeInfo = new Lang.Class({
    Name: UUID + '-PlaceVolumeInfo',
    Extends: PlaceInfo,

    _init: function(kind, volume, symbolicIcons) {
        this.volume = volume;
        this.symbolicIcons = symbolicIcons;
        this.parent(kind, volume.get_activation_root(), volume.get_name());
        
        // Why passing an uuid as uri ??
        // It is a trick to have a unique uri that identify the place
        // since the GFile is only used in sidebar._redisplay()
        // (to check new places and update places list).
        // It avoids complete rewriting of the method.
        this.file = Gio.File.new_for_uri(volume.get_uuid());
    },

    getIcon: function() {
        let gicon = this.symbolicIcons ? this.volume.get_symbolic_icon() :  this.volume.get_icon();
        
        if (this.volume.get_mount()) {
            let file = Me.dir.get_child('data').get_child('icons').get_child('emblem-mounted.svg');
            let emblemIcon = new Gio.FileIcon({ file: file });
            gicon = Gio.EmblemedIcon.new(gicon, Gio.Emblem.new(emblemIcon));
        }
        
        return gicon;
    }
});

const DEFAULT_DIRECTORIES = [
    GLib.UserDirectory.DIRECTORY_DOCUMENTS,
    GLib.UserDirectory.DIRECTORY_PICTURES,
    GLib.UserDirectory.DIRECTORY_MUSIC,
    GLib.UserDirectory.DIRECTORY_DOWNLOAD,
    GLib.UserDirectory.DIRECTORY_VIDEOS,
];

const PlacesManager = new Lang.Class({
    Name: UUID + '-PlacesManager',
    
    // take initPlaces() out of _init() to allow async initialization in Sidebar
    _init: function(symbolicIcons, showNotMountedDevices) {
        this.symbolicIcons = symbolicIcons;
        this.showNotMountedDevices = showNotMountedDevices;
        this._places = {
            special: [],
            devices: [],
            bookmarks: [],
            network: [],
        };

        // Show devices, code more or less ported from nautilus-places-sidebar.c
        this._volumeMonitor = Gio.VolumeMonitor.get();
        this._connectVolumeMonitorSignals();

        this._bookmarksFile = this._findBookmarksFile();
        this._bookmarkTimeoutId = 0;
        this._bookmarksMonitor = null;

        if (this._bookmarksFile) {
            this._bookmarksMonitor = this._bookmarksFile.monitor_file(Gio.FileMonitorFlags.NONE, null);
            this._bookmarksHandler = this._bookmarksMonitor.connect('changed', () => {
                if (this._bookmarkTimeoutId > 0)
                    return;
                // Defensive event compression
                this._bookmarkTimeoutId = Mainloop.timeout_add(100, () => {
                    this._bookmarkTimeoutId = 0;
                    this._reloadBookmarks();
                    return false;
                });
            });
        }
    },
    
    initPlaces: function() {
        this.initialized = true;
        this._updateSpecials();
        this._updateMounts();
        this._reloadBookmarks();
    },

    destroy: function() {
        for (let i = 0; i < this._volumeMonitorSignals.length; i++)
            this._volumeMonitor.disconnect(this._volumeMonitorSignals[i]);
        
        if (this._bookmarksHandler)
            this._bookmarksMonitor.disconnect(this._bookmarksHandler);
        if (this._bookmarksMonitor)
            this._bookmarksMonitor.cancel();
        if (this._bookmarkTimeoutId)
            Mainloop.source_remove(this._bookmarkTimeoutId);
    },
    
    _connectVolumeMonitorSignals: function() {
        const signals = ['volume-added', 'volume-removed', 'volume-changed',
                         'mount-added', 'mount-removed', 'mount-changed',
                         'drive-connected', 'drive-disconnected', 'drive-changed'];

        this._volumeMonitorSignals = [];
        let func = Lang.bind(this, this._updateMounts);
        for (let i = 0; i < signals.length; i++) {
            let id = this._volumeMonitor.connect(signals[i], func);
            this._volumeMonitorSignals.push(id);
        }
    },
    
    getPlaces: function() {
        let places = [];
        for (let i=0; i < SECTIONS.length; i++) {
            let id = SECTIONS[i];
            if (i == 3)
                this.posFirstBookmark = places.length;
            places = places.concat(this._places[id]);
        }
        return places;
    },
    
    _updatePlaces: function() {
        let places = this.getPlaces();
        this.emit('places-updated', places, this.posFirstBookmark);
    },

    _updateSpecials: function() {
        this._places.special.forEach(function(p) { p.destroy(); });
        this._places.special = [];

        let homePath = GLib.get_home_dir();

        this._places.special.push(new PlaceInfo('special',
                                                Gio.File.new_for_path(homePath),
                                                _GSE("Home"),
                                                null,
                                                this.symbolicIcons));

        let specials = [];
        let dirs = DEFAULT_DIRECTORIES.slice();

        dirs.push(GLib.UserDirectory.DIRECTORY_DESKTOP);

        for (let i = 0; i < dirs.length; i++) {
            let specialPath = GLib.get_user_special_dir(dirs[i]);
            if (!specialPath || specialPath == homePath)
                continue;

            let file = Gio.File.new_for_path(specialPath), info;
            try {
                info = new PlaceInfo('special', file, null, null, this.symbolicIcons);
            } catch(e) {
                if (e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.NOT_FOUND))
                    continue;
                throw e;
            }

            specials.push(info);
        }

        specials.sort(function(a, b) {
            return GLib.utf8_collate(a.name, b.name);
        });
        this._places.special = this._places.special.concat(specials);

        this.emit('special-updated');
        this._updatePlaces();
    },

    _updateMounts: function(monitor, object) {
        if (object && object instanceof Gio.Mount)
            this.emit('mount-changed', object.get_root());
                
        let networkMounts = [];
        let networkVolumes = [];

        this._places.devices.forEach(function(p) { p.destroy(); });
        this._places.devices = [];
        this._places.network.forEach(function(p) { p.destroy(); });
        this._places.network = [];

        /* Add standard places */
        this._places.devices.push(new RootInfo(this.symbolicIcons));

        /* first go through all connected drives */
        let drives = this._volumeMonitor.get_connected_drives();
        for (let i = 0; i < drives.length; i++) {
            let volumes = drives[i].get_volumes();

            for(let j = 0; j < volumes.length; j++) {
                let identifier = volumes[j].get_identifier('class');
                if (identifier && identifier.indexOf('network') >= 0) {
                    networkVolumes.push(volumes[j]);
                } else {
                    let mount = volumes[j].get_mount();
                    if (mount)
                        this._addMount('devices', mount);
                    else if (this.showNotMountedDevices)
                        this._addVolume('devices', volumes[j]);
                }
            }
        }

        /* add all volumes that is not associated with a drive */
        let volumes = this._volumeMonitor.get_volumes();
        for(let i = 0; i < volumes.length; i++) {
            if(volumes[i].get_drive())
                continue;

            let identifier = volumes[i].get_identifier('class');
            if (identifier && identifier.indexOf('network') >= 0) {
                networkVolumes.push(volumes[i]);
            } else {
                let mount = volumes[i].get_mount();
                if (mount)
                    this._addMount('devices', mount);
                else if (this.showNotMountedDevices)
                    this._addVolume('devices', volumes[i]);
            }
        }

        /* add mounts that have no volume (/etc/mtab mounts, ftp, sftp,...) */
        let mounts = this._volumeMonitor.get_mounts();
        for(let i = 0; i < mounts.length; i++) {
            if(mounts[i].is_shadowed())
                continue;

            if(mounts[i].get_volume())
                continue;

            let root = mounts[i].get_default_location();
            if (!root.is_native()) {
                networkMounts.push(mounts[i]);
                continue;
            }
            this._addMount('devices', mounts[i]);
        }

        for (let i = 0; i < networkVolumes.length; i++) {
            let mount = networkVolumes[i].get_mount();
            if (mount) {
                networkMounts.push(mount);
                continue;
            }
            this._addVolume('network', networkVolumes[i]);
        }

        for (let i = 0; i < networkMounts.length; i++) {
            this._addMount('network', networkMounts[i]);
        }

        this.emit('devices-updated');
        this.emit('network-updated');
        this._updatePlaces();
    },

    _findBookmarksFile: function() {
        let paths = [
            GLib.build_filenamev([GLib.get_user_config_dir(), 'gtk-3.0', 'bookmarks']),
            GLib.build_filenamev([GLib.get_home_dir(), '.gtk-bookmarks']),
        ];

        for (let i = 0; i < paths.length; i++) {
            if (GLib.file_test(paths[i], GLib.FileTest.EXISTS))
                return Gio.File.new_for_path(paths[i]);
        }

        return null;
    },
    
    getIsBookmark: function(file) {
        let content = Shell.get_file_contents_utf8_sync(this._bookmarksFile.get_path());
        let lines = content.split('\n');
        let uris = lines.map(line => line.split(' ')[0]);
        let uri = file.get_uri();
        return uris.indexOf(uri) != -1;
    },
    
    insertBookmark: function(file) {
        let content = Shell.get_file_contents_utf8_sync(this._bookmarksFile.get_path());
        let lines = content.split('\n');
        // lines of bookmarksFile are in the form: uri +(optionnal) ' ' + label
        let uris = lines.map(line => line.split(' ')[0]);
        let uri = file.get_uri();
        // if file uri already in bookmarks, return
        if (uris.indexOf(uri) != -1)
            return;
        let displayName = '';
        if (!file.is_native()) {
            try {
                let info = file.query_info('standard::display-name', Gio.FileQueryInfoFlags.NONE, null);
                displayName = ' ' + info.get_display_name();
            } catch(e) {}
        }
        lines.splice(0, 0, uri + displayName);
        content = lines.join('\n');
        GLib.file_set_contents(this._bookmarksFile.get_path(),content);
    },
    
    removeBookmark: function(file) {
        let content = Shell.get_file_contents_utf8_sync(this._bookmarksFile.get_path());
        let lines = content.split('\n');
        let uris = lines.map(line => line.split(' ')[0]);
        let uri = file.get_uri();
        let pos = uris.indexOf(uri);
        if (pos == -1)
            return;
        lines.splice(pos, 1);
        content = lines.join('\n');
        GLib.file_set_contents(this._bookmarksFile.get_path(),content);
    },
    
    moveBookmarkBefore: function(file1, file2) {
        this._moveBookmark(file1, file2, 0);
    },
    
    moveBookmarkAfter: function(file1, file2) {
        this._moveBookmark(file1, file2, 1);
    },
    
    _moveBookmark: function(file1, file2, offset) {
        let content = Shell.get_file_contents_utf8_sync(this._bookmarksFile.get_path());
        let lines = content.split('\n');
        let uris = lines.map(line => line.split(' ')[0]);
        let uri1 = file1.get_uri();
        let uri2 = file2.get_uri();
        // remove bookmark from old position
        let pos1 = uris.indexOf(uri1);
        if (pos1 == -1)
            return;
        let line1 = lines[pos1];
        lines.splice(pos1, 1);
        // reload uris because lines changed
        uris = lines.map(line => line.split(' ')[0]);
        // put bookmark back to new position
        let pos2 = uris.indexOf(uri2);
        if (pos2 == -1)
            return;
        lines.splice(pos2 + offset, 0, line1);
        // save updated bookmarks file
        content = lines.join('\n');
        GLib.file_set_contents(this._bookmarksFile.get_path(),content);
    },

    _reloadBookmarks: function() {
        this._bookmarks = [];

        let content = Shell.get_file_contents_utf8_sync(this._bookmarksFile.get_path());
        let lines = content.split('\n');

        let bookmarks = [];
        for (let i = 0; i < lines.length; i++) {
            let line = lines[i];
            let components = line.split(' ');
            let bookmark = components[0];

            if (!bookmark)
                continue;

            let file = Gio.File.new_for_uri(bookmark);
            if (!file.query_exists(null) &&
                (file.is_native() || !this.showNotMountedDevices))
                continue;
            
            // Not to duplicate rootInfo
            if (file.get_path() == '/')
                continue;
            
            let duplicate = false;
            for (let i = 0; i < this._places.special.length; i++) {
                if (file.equal(this._places.special[i].file)) {
                    duplicate = true;
                    break;
                }
            }
            if (duplicate)
                continue;
            for (let i = 0; i < bookmarks.length; i++) {
                if (file.equal(bookmarks[i].file)) {
                    duplicate = true;
                    break;
                }
            }
            if (duplicate)
                continue;

            let label = null;
            if (components.length > 1)
                label = components.slice(1).join(' ');

            bookmarks.push(new PlaceInfo('bookmarks', file, label, null, this.symbolicIcons));
        }

        this._places.bookmarks = bookmarks;

        this.emit('bookmarks-updated');
        this._updatePlaces();
    },

    _addMount: function(kind, mount) {
        let devItem;

        try {
            devItem = new PlaceDeviceInfo(kind, mount, this.symbolicIcons);
        } catch(e) {
            if (e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.NOT_FOUND))
                return;
            throw e;
        }

        this._places[kind].push(devItem);
    },

    _addVolume: function(kind, volume) {
        if (!volume.get_name() || !volume.get_uuid() || !volume.can_mount())
            return;
        
        // escape duplicates
        let uris = this._places[kind].map(place => place.file.get_uri());
        if (uris.indexOf(volume.get_uuid()) != -1)
            return;
        
        let volItem;

        try {
            volItem = new PlaceVolumeInfo(kind, volume, this.symbolicIcons);
        } catch(e) {
            if (e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.NOT_FOUND))
                return;
            throw e;
        }

        this._places[kind].push(volItem);
    },

    get: function(kind) {
        return this._places[kind];
    }
});
Signals.addSignalMethods(PlacesManager.prototype);

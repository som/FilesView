/*
 * Copyright 2018 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2018 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 6, multistr: true */

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Lang = imports.lang;
const Signals = imports.signals;

const ExtensionUtils = imports.misc.extensionUtils;
const Main = imports.ui.main;

const Me = ExtensionUtils.getCurrentExtension();
const UUID = Me.uuid.replace(/@/gi, '_at_').replace(/[^a-z0-9+_-]/gi, '_');

let Tracker = null;
let IS_TRACKER3 = false;
try {
    // Tracker 3 does not support tags and Nautilus does not share its favorites anymore.
    if (imports.misc.config.PACKAGE_VERSION > '3.37')
        throw new Error();
    
    Tracker = imports.gi.Tracker;
    // check libtracker version since Tracker.check_version() is buggy
    if (!Tracker.Resource)
        Tracker = null;
    else if (Tracker.SparqlConnection.bus_new)
        IS_TRACKER3 = true;
} catch(e) {
    // tracker not found
}

const MINER_FS_BUS_NAME = IS_TRACKER3 ? 'org.freedesktop.Tracker3.Miner.Files' : 'org.freedesktop.Tracker1.Miner.Files';
const MINER_FS_OBJECT_PATH = IS_TRACKER3 ? '/org/freedesktop/Tracker3/Miner/Files' : '/org/freedesktop/Tracker1/Miner/Files';
const MINER_FS_IFACE_XML = IS_TRACKER3 ? '<?xml version="1.0" encoding="UTF-8"?>\
<node name="/">\
  <interface name="org.freedesktop.Tracker3.Miner">\
    <annotation name="org.freedesktop.DBus.GLib.CSymbol" value="_tracker_miner_dbus"/>\
    <method name="GetStatus">\
      <annotation name="org.freedesktop.DBus.GLib.Async" value="true"/>\
      <arg type="s" name="status" direction="out" />\
    </method>\
    <signal name="Progress">\
      <arg type="s" name="status" />\
      <arg type="d" name="progress" />\
      <arg type="i" name="remaining_time" />\
    </signal>\
  </interface>\
</node>' : '<?xml version="1.0" encoding="UTF-8"?>\
<node name="/">\
  <interface name="org.freedesktop.Tracker1.Miner">\
    <annotation name="org.freedesktop.DBus.GLib.CSymbol" value="_tracker_miner_dbus"/>\
    <method name="GetStatus">\
      <annotation name="org.freedesktop.DBus.GLib.Async" value="true"/>\
      <arg type="s" name="status" direction="out" />\
    </method>\
    <signal name="Progress">\
      <arg type="s" name="status" />\
      <arg type="d" name="progress" />\
      <arg type="i" name="remaining_time" />\
    </signal>\
  </interface>\
</node>';
const MINER_FS_INDEX_BUS_NAME = IS_TRACKER3 ? 'org.freedesktop.Tracker3.Miner.Files.Control' : 'org.freedesktop.Tracker1.Miner.Files';
const MINER_FS_INDEX_OBJECT_PATH = IS_TRACKER3 ? '/org/freedesktop/Tracker3/Miner/Files/Index' : '/org/freedesktop/Tracker1/Miner/Files/Index';
const MINER_FS_INDEX_IFACE_XML = IS_TRACKER3 ? '<?xml version="1.0" encoding="UTF-8"?>\
<node>\
  <interface name="org.freedesktop.Tracker3.Miner.Files.Index">\
    <method name="IndexLocation">\
      <arg type="s" name="file_uri" direction="in" />\
      <arg type="as" name="graphs" direction="in" />\
      <arg type="as" name="flags" direction="in">"\
        <doc:doc><doc:summary>Extension flags, no allowed values at the moment</doc:summary></doc:doc>\
      </arg>\
    </method>\
  </interface>\
</node>' : '<?xml version="1.0" encoding="UTF-8"?>\
<node>\
  <interface name="org.freedesktop.Tracker1.Miner.Files.Index">\
    <method name="IndexFile">\
      <arg type="s" name="file_uri" direction="in" />\
    </method>\
  </interface>\
</node>';

// Compatibility wrapper around Tracker.SparqlConnection
const SparqlConnection = function() {
    let connection = IS_TRACKER3 ?
                     Tracker.SparqlConnection.bus_new(MINER_FS_BUS_NAME, null, null) :
                     Tracker.SparqlConnection.get(null);
    
    if (!IS_TRACKER3) {
        // Tracker2: update_async(sparql, priority, cancellable, callback)
        // Tracker3: update_async(sparql, cancellable, callback)
        let updateAsyncFunction = connection.update_async;
        connection.update_async = function(sparql, cancellable, callback) {
            return Lang.bind(connection, updateAsyncFunction)(sparql, 0, cancellable, callback);
        };
        
        // Tracker2: no close method
        connection.close = function() {};
    }
    
    return connection;
};

// There is no more public MinerManager in Tracker3 so let's go through DBus (yippee)
const TrackerMinerManager = new Lang.Class({
    Name: UUID + '-TrackerMinerManager',
    
    get minerFsProxy() {
        if (this._minerFsProxy === undefined) {
            let connection = Gio.DBus.session;
            let Proxy = Gio.DBusProxy.makeProxyWrapper(MINER_FS_IFACE_XML);
            this._minerFsProxy = new Proxy(connection, MINER_FS_BUS_NAME, MINER_FS_OBJECT_PATH);
            if (!this._minerFsProxy.get_name_owner())
                this._minerFsProxy = null;
        }
        return this._minerFsProxy;
    },
    
    get minerFsIndexProxy() {
        if (this._minerFsIndexProxy === undefined) {
            let connection = Gio.DBus.session;
            let Proxy = Gio.DBusProxy.makeProxyWrapper(MINER_FS_INDEX_IFACE_XML);
            this._minerFsIndexProxy = new Proxy(connection, MINER_FS_INDEX_BUS_NAME, MINER_FS_INDEX_OBJECT_PATH);
            if (!this._minerFsIndexProxy.get_name_owner())
                this._minerFsIndexProxy = null;
        }
        return this._minerFsIndexProxy;
    },
    
    get isIdle() {
        if (!this.minerFsProxy)
            return false;
        
        // TODO: check GetStatusSync result validity
        // maybe do this.minerFsProxy.GetStatusRemote((result, error) => ...)
        try {
            return this.minerFsProxy.GetStatusSync() == 'Idle';
        } catch(e) {
            return false;
        }
    },
    
    index: function(file, callback) {
        if (!this.minerFsProxy || !this.minerFsIndexProxy)
            return;
        
        let minerManagerHandler = this.minerFsProxy.connectSignal('Progress', (proxy_, proc_, tupple) => {
            let [status, progress, remainingTime_] = tupple;
            if (status == 'Idle' && progress == 1) {
                this.minerFsProxy.disconnectSignal(minerManagerHandler);
                callback();
            }
        });
        
        if (IS_TRACKER3)
            this.minerFsIndexProxy.IndexLocationSync(file.get_uri(), [], []);
        else {
            this.minerFsIndexProxy.IndexFileSync(file.get_uri());
        }
    }
});

const fmt = function(string) {
    return GLib.str_to_ascii(string, null).toLowerCase();
};

/*
 * sparql queries come from Nautilus (https://gitlab.gnome.org/GNOME/nautilus/blob/master/src/nautilus-tag-manager.c)
 */

var FavoritesManager = new Lang.Class({
    Name: UUID + '-FavoritesManager',
    
    _init : function() {
        this.settings = Me.settings;
        this.favoriteUris = [];
        this.trackerUsed = false;
        this.connection = null;
        
        if (Tracker) {
            this.hasTracker = true;
            try {
                this.connection = new SparqlConnection();
            } catch(e) {
                logError(e, Me.metadata.uuid);
                this.hasTracker = false;
            }
            
            if (!this.connection) {
                logError(new Error('trackerSparqlConnection is null'), Me.metadata.uuid);
                this.hasTracker = false;
            } else {
                this.allTagsManager = new AllTagsManager(this.connection);
            }
            
            this.minerManager = new TrackerMinerManager();
        }
        
        this.updateTrackerUsed();
        
        let iconFile = Me.dir.get_child('data').get_child('icons').get_child('tag-symbolic.svg');
        this.tagGicon = new Gio.FileIcon({ file: iconFile });
        
        this.overviewShowingHandler = Main.overview.connect('showing', Lang.bind(this, this._onOverviewShowing));
    },
    
    // FIXME: need better event to update tracker favorites and keep synchronized with Nautilus
    _onOverviewShowing: function() {
        if (Main.overview.viewSelector._activePage == Main.overview.viewSelector._filesPage)
            this.updateFavoritesAsync();
    },
    
    updateTrackerUsed: function() {
        this.trackerUsed = this.settings.get_boolean('sync-nautilus-favorites') && this.hasTracker;
        this.updateFavorites();
    },
    
    updateFavorites: function() {
        let oldUris = this.favoriteUris.slice();
        this.favoriteUris = [];
        
        if (this.trackerUsed)
            this._getTrackerFavorites();
        else
            this._getInternalFavorites();
        
        if (this.favoriteUris.join() != oldUris.join())
            this.emit('changed');
    },
    
    _getInternalFavorites: function() {
        let favorites = this.settings.get_strv('favorite-files');
        
        for (let i = 0; i < favorites.length; i++) {
            // convert path to uri
            if (!GLib.uri_parse_scheme(favorites[i])) {
                let tempFile = Gio.File.new_for_path(favorites[i]);
                favorites[i] = tempFile.get_uri();
                this.settings.set_strv('favorite-files', favorites);
            }
            
            this.favoriteUris.push(favorites[i]);
        }
    },
    
    _getTrackerFavorites: function() {
        if (this.trackerBusy)
            return;
        
        let sparql = "SELECT ?url " +
                     "WHERE { ?urn nie:url ?url ; nao:hasTag <urn:gnome:nautilus:starred> }";
        
        let cursor;
        try {
            cursor = this.connection.query(sparql, null);
        } catch(e) {
            logError(e, Me.metadata.uuid);
        }
        
        if (!cursor) {
            logError(new Error('trackerSparqlCursor is null'), Me.metadata.uuid);
            return;
        }
        
        let test = cursor.next(null);
        while (test) {
            let uri = cursor.get_string(0)[0];
            this.favoriteUris.push(uri);
            
            test = cursor.next(null);
        }
        
        cursor.close();
    },
    
    // called on files-view shown (see displayManager), in order to be in sync with Nautilus
    updateFavoritesAsync: function(callback) {
        // It is not necessary to sync internal favorites
        if (!this.trackerUsed)
            return;
        
        this._getTrackerFavoritesAsync(callback);
    },
    
    _getTrackerFavoritesAsync: function(callback) {
        if (this.trackerBusy)
            return;
        
        let oldUris = this.favoriteUris.slice();
        this.favoriteUris = [];
        
        let sparql = "SELECT ?url " +
                     "WHERE { ?urn nie:url ?url ; nao:hasTag <urn:gnome:nautilus:starred> }";
        
        let queryCallback = ((connection, res) => {
            if (this.callbackCount)
                return;
            else
                this.callbackCount ++;
            
            let cursor;
            try {
                cursor = connection.query_finish(res);
            } catch(e) {
                logError(e, Me.metadata.uuid);
            }
            
            if (!cursor) {
                logError(new Error('trackerSparqlCursor is null'), Me.metadata.uuid);
                return;
            }
            
            let test = cursor.next(null);
            while (test) {
                let uri = cursor.get_string(0)[0];
                this.favoriteUris.push(uri);
                
                test = cursor.next(null);
            }
            
            cursor.close();
            this.trackerBusy = false;
            if (callback)
                callback(this.favoriteUris);
            
            if (this.favoriteUris.join() != oldUris.join())
                this.emit('changed');
        });
        
        // callback is called 2 times (because of SparqlConnection.get() ??)
        this.callbackCount = 0;
        this.trackerBusy = true;
        this.connection.query_async(sparql, null, queryCallback);
    },
    
    getIsIndexedByTracker: function(uri) {
        if (!this.hasTracker)
            return false;
        
        // check that the file is indexed
        // escape both uri and ?url to avoid quote issue
        let escapedUri = GLib.uri_escape_string(uri, null, true);
        if (!escapedUri)
            return false;
        
        let sparql = "SELECT ?url " +
                     "WHERE { ?urn a nfo:FileDataObject ; nie:url ?url . FILTER(ENCODE_FOR_URI(?url) IN ('" + escapedUri + "')) }";
        try {
            let cursor = this.connection.query(sparql, null);
            let isIndexed = cursor.next(null);
            cursor.close();
            return isIndexed;
        } catch(e) {
            logError(e, Me.metadata.uuid);
            return false;
        }
    },
    
    getIsFavorite: function(uri) {
        return this.favoriteUris.indexOf(uri) != -1;
    },
    
    addToFavorites: function(uri, callback) {
        if (this.trackerUsed)
            this._addToTrackerFavorites(uri, () => {
                // update to have a consistent sorting (by indexed date ?)
                // callback first since update takes time
                callback();
                this.updateFavorites();
            });
        else
            this._addToInternalFavorites(uri, () => {
                this.favoriteUris.push(uri);
                this.emit('changed');
                callback();
            });
    },
    
    _addToInternalFavorites: function(uri, callback) {
        let favorites = this.settings.get_strv('favorite-files');
        favorites.push(uri);
        this.settings.set_strv('favorite-files', favorites);
        callback();
    },
    
    // works with Tracker v1.10+ (GS 3.22+)
    _addToTrackerFavorites: function(uri, callback) {
        // escape both uri and ?url to avoid quote issue
        let escapedUri = GLib.uri_escape_string(uri, null, true);
        if (!escapedUri)
            return;
        
        let sparql = "INSERT DATA { <urn:gnome:nautilus:starred> a nao:Tag .}\n" +
                     "INSERT { ?urn nao:hasTag <urn:gnome:nautilus:starred> }" +
                     "WHERE { ?urn a nfo:FileDataObject ; nie:url ?url . FILTER(ENCODE_FOR_URI(?url) IN ('" + escapedUri + "')) }";
        
        let queryCallback = ((connection, res) => {
            try {
                connection.update_finish(res);
            } catch(e) {
                logError(e, Me.metadata.uuid);
                return;
            }
            
            callback();
        });
        
        this.connection.update_async(sparql, null, queryCallback);
    },
    
    removeUri: function(uri) {
        this.favoriteUris = this.favoriteUris.filter(a => a !== uri);
        if (!this.trackerUsed)
            this._removeFromInternalFavorites(uri);
    },
    
    removeFromFavorites: function(uri, callback) {
        if (this.trackerUsed)
            this._removeFromTrackerFavorites(uri, () => {
                this.favoriteUris = this.favoriteUris.filter(a => a !== uri);
                this.emit('changed');
                callback();
            });
        else
            this._removeFromInternalFavorites(uri, () => {
                this.favoriteUris = this.favoriteUris.filter(a => a !== uri);
                this.emit('changed');
                callback();
            });
    },
    
    _removeFromInternalFavorites: function(uri, callback) {
        let favorites = this.settings.get_strv('favorite-files');
        favorites = favorites.filter(a => a !== uri);
        this.settings.set_strv('favorite-files', favorites);
        if (callback)
            callback();
    },
    
    _removeFromTrackerFavorites: function(uri, callback) {
        let escapedUri = GLib.uri_escape_string(uri, null, true);
        if (!escapedUri)
            return;
        
        let sparql = "DELETE { ?urn nao:hasTag <urn:gnome:nautilus:starred> }" +
                     "WHERE { ?urn a nfo:FileDataObject ; nie:url ?url . FILTER(ENCODE_FOR_URI(?url) IN ('" + escapedUri + "')) }";
        
        let updateCallback = ((connection, res) => {
            try {
                connection.update_finish(res);
            } catch(e) {
                logError(e, Me.metadata.uuid);
                return;
            }
            
            if (callback)
                callback();
        });
        
        this.connection.update_async(sparql, null, updateCallback);
    },
    
    getCanIndex: function(file, isTrashed, isDir, hasParent) {
        if (!this.minerManager)
            return false;
        
        if (isTrashed)
            return false;
        
        if (!isDir && (!hasParent || !this.getIsIndexedByTracker(file.get_parent().get_uri())))
            return false;
        
        return this.minerManager.isIdle;
    },
    
    index: function(file, callback) {
        if (!this.minerManager)
            return;
        
        this.minerManager.index(file, callback);
    },
    
    disable: function() {
        Main.overview.disconnect(this.overviewShowingHandler);
        if (this.connection)
            this.connection.close();
    }
});
Signals.addSignalMethods(FavoritesManager.prototype);

const AllTagsManager = new Lang.Class({
    Name: UUID + '-AllTagsManager',
    
    _init : function(connection) {
        this.connection = connection;
        this.tags = [];
    },
    
    updateTags: function(callback) {
        if (this.trackerBusy)
            return;
        
        let oldTags = this.tags.slice();
        this.tags = [];
        
        let sparql = "SELECT DISTINCT ?label " +
                     "WHERE { ?urn nie:url ?url ; nao:hasTag ?tag . " +
                     "        ?tag nao:prefLabel ?label . " +
                     "  FILTER(STRSTARTS(?tag, 'urn:tag:')) . " +
                     "}";
        
        let queryCallback = ((connection, res) => {
            if (this.callbackCount)
                return;
            else
                this.callbackCount ++;
            
            let cursor;
            try {
                cursor = connection.query_finish(res);
            } catch(e) {
                logError(e, Me.metadata.uuid);
            }
            
            if (!cursor) {
                logError(new Error('trackerSparqlCursor is null'), Me.metadata.uuid);
                return;
            }
                
            let test = cursor.next(null);
            while (test) {
                this.tags.push(cursor.get_string(0)[0]);
                test = cursor.next(null);
            }
        
            cursor.close();
            this.trackerBusy = false;
            
            if (callback)
                callback();
            
            if (this.tags.join() != oldTags.join())
                this.emit('changed');
        });
        
        this.callbackCount = 0;
        this.trackerBusy = true;
        this.connection.query_async(sparql, null, queryCallback);
    },
    
    getTags: function(uri) {
        let escapedUri = GLib.uri_escape_string(uri, null, true);
        if (!escapedUri)
            return [];
        
        let sparql = "SELECT ?label " +
                     "WHERE { ?urn nie:url ?url ; nao:hasTag ?tag . " +
                     "        ?tag nao:prefLabel ?label . " +
                     "  FILTER(STRSTARTS(?tag, 'urn:tag:')) . " +
                     "  FILTER(ENCODE_FOR_URI(?url) IN ('" + escapedUri + "')) . " +
                     "}";
        
        try {
            let cursor = this.connection.query(sparql, null);
            let tags = [];
            
            let test = cursor.next(null);
            while (test) {
                tags.push(cursor.get_string(0)[0]);
                test = cursor.next(null);
            }
            
            cursor.close();
            return tags;
        } catch(e) {
            logError(e, Me.metadata.uuid);
            return [];
        }
    },
    
    // Does not work with Tracker v1.12- (GS 3.24-)
    addTag: function(tag, uri, callback) {
        let escapedUri = GLib.uri_escape_string(uri, null, true);
        let escapedTag = GLib.uri_escape_string(tag, null, true);
        let quoteEscapedTag = tag.replace("'", "%27");
        if (!escapedUri || !escapedTag)
            return;
        
        let sparql = "INSERT DATA { <urn:tag:" + escapedTag + "> a nao:Tag . }" +
                     "INSERT { ?urn nao:hasTag <urn:tag:" + escapedTag + "> . <urn:tag:" + escapedTag + "> nao:prefLabel ?label . }" +
                     "WHERE { ?urn a nfo:FileDataObject ; nie:url ?url . " +
                     "  FILTER(ENCODE_FOR_URI(?url) IN ('" + escapedUri + "')) . " +
                     "  BIND(REPLACE('" + quoteEscapedTag + "', '%27', \"'\") AS ?label) . " +
                     "}";
        
        let updateCallback = ((connection, res) => {
            try {
                connection.update_finish(res);
            } catch(e) {
                logError(e, Me.metadata.uuid);
                return;
            }
            
            callback(tag);
            
            
            // update allTagsManager tags and then update tagManager if it exists and has this tag
            let updateCallback = this.tagManager && this.tagManager.tag == tag ? Lang.bind(this.tagManager, this.tagManager.updateUris) : null;
            this.updateTags(updateCallback);
        });
        
        this.connection.update_async(sparql, null, updateCallback);
    },
    
    removeTag: function(tag, uri, callback) {
        let escapedUri = GLib.uri_escape_string(uri, null, true);
        let quoteEscapedTag = tag.replace("'", "%27");
        if (!escapedUri)
            return;
        
        let sparql = "DELETE { ?urn nao:hasTag ?tag }" +
                     "WHERE { ?urn a nfo:FileDataObject ; nie:url ?url . " +
                     "        ?tag nao:prefLabel ?label . " +
                     "  FILTER(ENCODE_FOR_URI(?url) IN ('" + escapedUri + "')) . " +
                     "  FILTER(?label = REPLACE('" + quoteEscapedTag + "', '%27', \"'\")) . " +
                     "}";
        
        let updateCallback = ((connection, res) => {
            try {
                connection.update_finish(res);
            } catch(e) {
                logError(e, Me.metadata.uuid);
                return;
            }
            
            callback(tag);
            
            let updateCallback = this.tagManager && this.tagManager.tag == tag ? Lang.bind(this.tagManager, this.tagManager.updateUris) : null;
            this.updateTags(updateCallback);
        });
        
        this.connection.update_async(sparql, null, updateCallback);
    },
    
    getNewTagManager: function(tag) {
        this.tagManager = new TagManager(this.connection, tag);
        return this.tagManager;
    },
    
    getIsTag: function(text) {
        return this.tags.indexOf(text) != -1;
    },
    
    completeTag: function(text, callback) {
        let completedText = text;
        
        let filteredTags = this.tags.filter(tag => fmt(tag).indexOf(fmt(text)) != -1);
        if (filteredTags.length == 1) {
            completedText = filteredTags[0];
        } else {
            filteredTags = this.tags.filter(tag => tag.indexOf(text) == 0);
            if (filteredTags.length == 1) {
                completedText = filteredTags[0];
            } else if (filteredTags.length) {
                let i = 0;
                while (filteredTags[0].length > i &&
                       filteredTags.filter(tag => tag.length > i && tag[i] == filteredTags[0][i]).length > 1)
                    i++;
                
                completedText = filteredTags[0].slice(0,i);
            }
        }
        
        callback(completedText);
    }
});
Signals.addSignalMethods(AllTagsManager.prototype);

const TagManager = new Lang.Class({
    Name: UUID + '-FavoritesManager',
    
    _init : function(connection, tag) {
        this.connection = connection;
        this.tag = tag;
        this.uris = [];
    },
    
    updateUris: function() {
        if (this.trackerBusy)
            return;
        
        let oldUris = this.uris.slice();
        this.uris = [];
        
        let quoteEscapedTag = this.tag.replace("'", "%27");
                
        let sparql = "SELECT ?url " +
                     "WHERE { ?urn nie:url ?url ; nao:hasTag ?tag . " +
                     "        ?tag nao:prefLabel ?label . " +
                     "    FILTER(?label = REPLACE('" + quoteEscapedTag + "', '%27', \"'\")) . " +
                     "}";
        
        let queryCallback = ((connection, res) => {
            if (this.callbackCount)
                return;
            else
                this.callbackCount ++;
            
            let cursor;
            try {
                cursor = connection.query_finish(res);
            } catch(e) {
                logError(e, Me.metadata.uuid);
            }
            
            if (!cursor) {
                logError(new Error('trackerSparqlCursor is null'), Me.metadata.uuid);
                return;
            }
            
            let test = cursor.next(null);
            while (test) {
                let uri = cursor.get_string(0)[0];
                this.uris.push(uri);
            
                test = cursor.next(null);
            }
            
            cursor.close();
            this.trackerBusy = false;
            
            if (this.uris.join() != oldUris.join())
                this.emit('changed');
        });
        
        this.callbackCount = 0;
        this.trackerBusy = true;
        this.connection.query_async(sparql, null, queryCallback);
    },
    
    removeUri: function(uri) {
        this.uris = this.uris.filter(a => a !== uri);
    }
});
Signals.addSignalMethods(TagManager.prototype);

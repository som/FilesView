��    ]           �      �  !   �       �   $  �   �     �	     �	     �	     �	     �	     
     9
     =
     Y
     _
     k
     x
     
     �
     �
     �
     �
  
   �
     �
  '        5  	   D     N     T  
   h     s     z     �     �     �  	   �  	   �     �     �               1     H     N     l     �     �     �     �     �  !   �     �     �          $     <     H     O     \     i     }     �     �     �     �     �     �     �  	     
        )     ;     V     m  
   }     �     �     �     �     �     �     �  	   �     �          !     .     ?     N     V  /   r     �     �    �  #   �       �   '  �   �     �     �     �     �  )   �  #        >     C  	   b     l     �     �     �  	   �     �     �     �  	     .        D  	   `  	   j     t     z     �     �     �     �     �     �     �                !     9     K     b     u     |     �     �     �     �     �     �     �          0     ?     V     m     |     �     �     �     �     �     �     �               -     I  	   g  
   q     |  -   �     �     �     �     �               "     +     9     E     R     f     n     �  	   �     �     �     �  :   �                               Y   L   .             5               F       T            D          4   K           <      I   )   W   1       "           =       :       X   U       [          !             ]   V   *   N   P   C      	   7          A          B   Z           \   ;   ,   2      G   '      E   R   /   -   @   O       Q      S      #          H                    0       6   8      9       ?   %   3   M              
       >   $   J   +   &          (                    .desktop files must be executable 0 for left, -1 for right <span size="small">This program comes with ABSOLUTELY NO WARRANTY.
See the <a href="https://www.gnu.org/licenses/old-licenses/gpl-2.0.html">GNU General Public License, version 2 or later</a> for details.</span> A files-view for GNOME Shell.

This extension displays a 'Files' view similar to the Applications view, whose design is taken from it (same layout, same style classes). It is accessed through the combination SUPER + F About Above Add a hot corner Add to favorites Add toggle button in dash/dock Add toggle button in panel All Animate icons when hovering Below Bottom left Bottom right Browse Copy path to clipboard Credits Decrease max-columns number Decrease min-rows number Enter location Everywhere Factor 1 is apps view icon size Fade-in animation when browsing folders Favorite Files Favorites Files Files View settings Files view Folder Go to home folder Go to parent folder Go to previous folder Icon Icon size Icon used Increase max-columns number Increase min-rows number Internal shortcuts Maximum number of columns Minimum number of rows Never No applications for this file No permission to open None Only in Only thumbnails Open Open location dialog Open the folder in Gnome-terminal Open the folder in Nautilus Open with ... Panel button accurate position Panel button appearance Preferences Recent Recent Files Refresh view Remove as wallpaper Remove favorites Run Run in a terminal Run in the background Run launchers on clicked Select directory Set as wallpaper Shortcut to display Files view Shortcuts Show Files Show hidden files Show/hide dash in overview Show/hide hidden files Start directory Start view Switch to left tab Switch to right tab Text To the center To the left To the right Toggle to Files view Top right Version Visualize icons and their name Where I left apply completion browse history default need gtk-3-examples package no longer exists and was removed from favorites not editable right panel is status area Project-Id-Version: Files View 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-06-17 10:46+0100
Last-Translator: Morgan Antonsson <morgan.antonsson@gmail.com>
Language-Team: 
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 .desktop-filer måste vara körbara 0 för vänster, -1 för höger <span size="small">Detta program kommer HELT UTAN GARANTI.
Se <a href="https://www.gnu.org/licenses/old-licenses/gpl-2.0.html">GNU General Public License, version 2 eller senare</a> för detaljer.</span> En filvy för GNOME Shell.

Detta tillägg visar en 'filvy' liknande programöversiktsvyn, vars utformning har återanvänts (layout och stilmallar). Vyn öppnas med tangentkombinationen SUPER + F Om Ovanför Ange aktivitetshörn Lägg till i favoriter Lägg till knapp i snabbstartspanel/docka Lägg till växlingsknapp i panelen Alla Animera ikoner under muspekare Nedanför Nederst till vänster Nederst till höger Bläddra Kopiera sökväg till urklipp Tack till Minska högsta antal kolumner Minska minsta antal rader Ange adress Överallt Faktor 1 är programöversiktsvyns ikonstorlek Animera öppning av katalog Favoriter Favoriter Filer Inställningar för filvy Filvy Katalog Gå till hemkatalog Gå till föräldrakatalog Gå till föregående katalog Ikon Ikonstorlek Ikon Öka högsta antal kolumner Öka minsta antal rader Interna genvägar Högsta antal kolumner Minsta antal rader Aldrig Program saknas för denna fil Saknar rättigheter att öppna Inga Endast i Endast miniatyrer Öppna Gå till addressfält Öppna katalog i Gnome-terminal Öppna katalog i Nautilus Öppna med ... Panelknappens position Panelknappens utseende Inställningar Tidigare Senaste filer Uppdatera vy Ta bort som skrivbordsbakgrund Ta bort från favoriter Kör Kör i en terminal Kör i bakgrunden Klicka för att köra program Välj katalog Ange som skrivbordsbakgrund Genväg för att öppna filvy Genvägar Visa filer Visa dolda filer Visa/dölj snabbstartspanelen i översiktsvyn Visa/dölj dolda filer Startkatalog Startvy Aktivera flik till vänster Aktivera flik till höger Text I mitten Till vänster Till höger Öppna filvy Överst till höger Version Visa ikoner och namn Senaste katalog tillämpa visa historik standard kräver paketet gtk-3-examples existerar inte längre och har tagits bort från favoriter kan ej redigeras höger panel är statusfältet 
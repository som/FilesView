# Files View

![](https://codeberg.org/som/FilesView/raw/branch/media/screenshots/screenshot1.jpg)

This extension displays a “Files” view similar to the Applications view, with the conveniences of a file manager. It is accessed through the combination `super + F`.
For those who are used to using the Applications view (`super + A`), this is quite natural.

## Features

* Moving the view in directories
* Sidebar with places
* Search entry with completion and history
* Drag and drop
* A "tab" for recent files
* Another for favorites, either specific to the extension ~~or those of Nautilus~~
* ~~Tag support (Tracker)~~
* Thumbnail display (if Nautilus has already generated them)
* File sorting
* Numerous internal shortcuts
* “read-only” mode, by disabling file manager

![](https://codeberg.org/som/FilesView/raw/branch/media/screenshots/screenshot2.jpg)

## Install

1. Download and decompress or clone the repository
2. Place the resulting directory in `~/.local/share/gnome-shell/extensions`
3. **Change the directory name** to `files-view@som.codeberg.org`
4. Xorg: type `alt + F2` and `r` to restart gnome-shell  
   Wayland: restart or re-login
5. Enable extension in gnome-tweaks or gnome-shell-extension-prefs
6. `super + F` to test
7. [https://codeberg.org/som/FilesView/issues](https://codeberg.org/som/FilesView/issues) to say it doesn't work

## Details

* Extension as Desktop substitute :

  * Configure “Start view” and “Start directory”
  * Mounted partitions and media access in Desktop folder
  * Launcher support

  ![](https://codeberg.org/som/FilesView/raw/branch/media/screenshots/desktop.jpg)

* The search entry lets you :

  * Search items in displayed view
  * Enter paths and uris (e.g. `network:///`)
  * Enter tags (by starting with "#")

  ![](https://codeberg.org/som/FilesView/raw/branch/media/screenshots/search.jpg)

Nautilus is a great file manager but most often it is used to only access files. Keeping it open is a waste of resources and opening/closing it each time we need a file is a waste of time. Moreover a file view is more suited to multiple workspaces usage.

## Known issue

Some distributions set CAP_SYS_NICE to gnome-shell to support new "rt-sheduler" feature. But it brings problems with gvfs in gnome-shell and Files View use it. If you can't access trash or remotes in the files view, you could try ```setcap -r /usr/bin/gnome-shell```.

[https://bugs.archlinux.org/task/62860](https://bugs.archlinux.org/task/62860)
[https://gitlab.gnome.org/GNOME/mutter/commit/dae2c1d420ed272710ac55b7a00f6787e5c0e762](https://gitlab.gnome.org/GNOME/mutter/commit/dae2c1d420ed272710ac55b7a00f6787e5c0e762)

## Credits

* GNOME Shell authors : a lot of code is picked from GNOME Shell
* [charlesg99](https://github.com/charlesg99) : ES6 class support ([`defineClass`](https://github.com/home-sweet-gnome/dash-to-panel/blob/master/utils.js)) from “Dash to panel” extension



/*
 * Copyright 2018 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2018 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 6 */

const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;

const Config = imports.misc.config;
const ExtensionUtils = imports.misc.extensionUtils;
const IconGrid = imports.ui.iconGrid;
const Main = imports.ui.main;
const OverviewControls = imports.ui.overviewControls;
const ViewSelector = imports.ui.viewSelector;
const ExtensionManager = Main.extensionManager ? Main.extensionManager : imports.ui.extensionSystem;

const Me = ExtensionUtils.getCurrentExtension();
const Buttons = Me.imports.buttons;
const Convenience = Me.imports.convenience;
const FavoritesManager = Me.imports.favoritesManager;
const FilesDisplay = Me.imports.filesDisplay;
const FileManager = Me.imports.fileManager;
const HotCorner = Me.imports.hotCorner;
const Search = Me.imports.search;
const Sidebar = Me.imports.sidebar;

const _ = imports.gettext.domain(Me.metadata['gettext-domain']).gettext;

const GS_VERSION = Config.PACKAGE_VERSION;
const MainOverviewControls = GS_VERSION < '3.36.0' ? Main.overview._controls : Main.overview._overview._controls;
const UUID = Me.uuid.replace(/@/gi, '_at_').replace(/[^a-z0-9+_-]/gi, '_');

const getDash = function() {
    return GS_VERSION < '3.36.0' ? Main.overview._dash : Main.overview.dash;
}

const _toggleAppsPageWrapper = function() {
    if (Main.overview.viewSelector._activePage == Main.overview.viewSelector._appsPage) {
        Main.overview.hide();
    } else {
        Main.overview.viewSelector._toggleAppsPage();
    }
};

const toggleWrapper = function() {
    if (Main.overview.isDummy)
        return;

    if (Main.overview.visible && Main.overview.viewSelector._activePage == Main.overview.viewSelector._workspacesPage) {
        Main.overview.hide();
    } else if (Main.overview.visible) {
        Main.overview.viewSelector._showAppsButton.checked = false;
        Main.overview.viewSelector._showPage(Main.overview.viewSelector._workspacesPage);
    } else
        Main.overview.toggle();
};

const DisplayManager = new Lang.Class({
    Name: UUID + '-DisplayManger',
    
    _init: function() {
        this.settings = Me.settings;
        this.internalShortcutsSettings = Convenience.getSettings(Me.metadata['settings-schema'] + '.internal-shortcuts');
        
        this.favoritesManager = new FavoritesManager.FavoritesManager();
        Main.overview.favoritesManager = this.favoritesManager;
        
        this.fileManager = new FileManager.FileManager();
        Main.overview.fileManager = this.fileManager;
        
        this.searchManager = new Search.SearchManager();
        Main.overview.viewSelector.filesSearchEntry = this.searchManager.searchEntry;
        
        this.filesDisplay = new FilesDisplay.FilesDisplay();
        // used in Main.overview.viewSelector._animateIn/_animateOut injections
        Main.overview.viewSelector.filesDisplay = this.filesDisplay;
        
        this.filesPage = Main.overview.viewSelector._addPage(this.filesDisplay.actor, _("Files"),'folder-symbolic');
        this.filesPage.hide();
        this.filesPage.toggle = Lang.bind(this, this.toggleFilesPage);
        this.filesDisplay.page = this.filesPage;  // Give reference to page for focus handling.
        Main.overview.viewSelector._filesPage = this.filesPage;
        
        this.sidebarSlider = new Sidebar.SidebarSlider();
        //useful to access sidebar from fileIconMenu
        MainOverviewControls._sidebarSlider = this.sidebarSlider;
        this.addSidebar();
        
        //bind prefs
        this.settingsHandler = [
            this.settings.connect('changed::improve-overview-switching', Lang.bind(this, this.improveOverviewSwitching)),
            this.settings.connect('changed::sidebar', Lang.bind(this, this.addSidebar)),
            this.settings.connect('changed::second-sidebar', Lang.bind(this, this.updateSidebar)),
            this.settings.connect('changed::sidebar-symbolic-icons',  Lang.bind(this.sidebarSlider, this.sidebarSlider.redisplayIcons)),
            this.settings.connect('changed::sidebar-icon-size', Lang.bind(this.sidebarSlider, this.sidebarSlider.adjustIconSize)),
            this.settings.connect('changed::show-not-mounted-devices-sidebar', Lang.bind(this.sidebarSlider, this.sidebarSlider.updateShowNotMountedDevices)),
            this.settings.connect('changed::panel-button', Lang.bind(this, this.addPanelButton)),
            this.settings.connect('changed::panel-button-position', Lang.bind(this, this.updatePanelButtonPosition)),
            this.settings.connect('changed::panel-custom-button', Lang.bind(this, this.addPanelButton)),
            this.settings.connect('changed::panel-button-appearance', Lang.bind(this, this.addPanelButton)),
            this.settings.connect('changed::panel-icon-name', Lang.bind(this, this.updatePanelIcon)),
            this.settings.connect('changed::dash-button', Lang.bind(this, this.addDashButton)),
            this.settings.connect('changed::dash-custom-button', Lang.bind(this, this.updateDashIcon)),
            this.settings.connect('changed::dash-icon-name', Lang.bind(this, this.updateDashIcon)),
            this.settings.connect('changed::hot-corner', Lang.bind(this, this.addHotCorner)),
            this.settings.connect('changed::hot-corner-sensitivity', Lang.bind(this, this.changeHotCornerSensitivity)),
            this.settings.connect('changed::view-icon-size-factor', Lang.bind(this.filesDisplay, this.filesDisplay.sizeRedisplay)),
            this.settings.connect('changed::max-columns', Lang.bind(this.filesDisplay, this.filesDisplay.updateViewLayout)),
            this.settings.connect('changed::min-rows', Lang.bind(this.filesDisplay, this.filesDisplay.updateViewLayout)),
            this.settings.connect('changed::icons-hover', Lang.bind(this.filesDisplay, this.filesDisplay.redisplay)),
            this.settings.connect('changed::show-devices-desktop', Lang.bind(this.filesDisplay, this.filesDisplay.redisplay)),
            this.settings.connect('changed::show-hidden-files', Lang.bind(this, this.updateFolderViews)),
            this.settings.connect('changed::run-launchers', Lang.bind(this, this.resortFolderViews)),
            this.settings.connect('changed::sync-nautilus-favorites', () => { this.favoritesManager.updateTrackerUsed(); this.filesDisplay.redisplay(); }),
            this.settings.connect('changed::sync-nautilus-sorting', Lang.bind(this, this.resortFolderViews)),
            this.settings.connect('changed::sort-by', Lang.bind(this, this.resortFolderViews)),
            this.settings.connect('changed::sort-reversed', Lang.bind(this, this.resortFolderViews)),
            this.settings.connect('changed::sort-folders-before-files', Lang.bind(this, this.resortFolderViews))
        ];
        
        this.pageChangedHandler = Main.overview.viewSelector.connect('page-changed', Lang.bind(this, this.onPageChanged));
        this.overviewShownHandler = Main.overview.connect('shown', Lang.bind(this, this.onOverviewShown));
        this.overviewHiddenHandler = Main.overview.connect('hidden', Lang.bind(this, this.removeKeybindings));
        Main.wm.addKeybinding('toggle-files-view',
                              this.settings,
                              Meta.KeyBindingFlags.NONE,
                              Shell.ActionMode.NORMAL |
                              Shell.ActionMode.OVERVIEW,
                              Lang.bind(this, this.toggleFilesPage));
        
        this.internalShortcuts = {
            'toggle-hidden-files': Lang.bind(this, this.toggleHiddenFiles),
            'undo-operation': Lang.bind(this, this.undoOperation),
            'redo-operation': Lang.bind(this, this.redoOperation),
            'go-parent-directory': Lang.bind(this.filesDisplay, this.filesDisplay.openParentDirectory),
            'go-previous-directory': Lang.bind(this.filesDisplay, this.filesDisplay.openPreviousDirectory),
            'go-home-directory': Lang.bind(this.filesDisplay, this.filesDisplay.openHomeDirectory),
            'switch-subview': Lang.bind(this.filesDisplay, this.filesDisplay.switchRight),
            'switch-subview-left': Lang.bind(this.filesDisplay, this.filesDisplay.switchLeft),
            'switch-subview-right': Lang.bind(this.filesDisplay, this.filesDisplay.switchRight),
            'enter-current-location': Lang.bind(this.filesDisplay, this.filesDisplay.enterCurrentLocationInSearch),
            'more-columns': Lang.bind(this, this.increaseColumnsNumber),
            'less-columns': Lang.bind(this, this.decreaseColumnsNumber),
            'more-rows': Lang.bind(this, this.increaseRowsNumber),
            'less-rows': Lang.bind(this, this.decreaseRowsNumber),
            'show-hide-sidebar': Lang.bind(this, this.showHideSidebar),
            'refresh-view': Lang.bind(this.filesDisplay, this.filesDisplay.redisplay),
            'sort-reversed-shortcut': Lang.bind(this.filesDisplay, this.filesDisplay.reverseSorting),
            'sort-by-name-shortcut': () => this.filesDisplay.sortBy('name'),
            'sort-by-type-shortcut': () => this.filesDisplay.sortBy('type'),
            'sort-by-date-shortcut': () => this.filesDisplay.sortBy('modification date'),
            'sort-by-size-shortcut': () => this.filesDisplay.sortBy('size'),
            'erase-sort-metadata-shortcut': Lang.bind(this.filesDisplay, this.filesDisplay.eraseSortMetadata),
            'generate-thumbnails-large': () => this.generateThumbnails(true),
            'generate-thumbnails-normal': () => this.generateThumbnails(false)
        };
        
        this.improveOverviewSwitching();
    },
    
    unImproveOverviewSwitching: function() {
        if (!this.overviewSwitchingIsImproved)
            return;
        
        this.overviewSwitchingIsImproved = false;
        Main.wm.removeKeybinding('toggle-application-view');
        Main.wm.removeKeybinding('toggle-overview');
        
        let shellKeybindingsSettings = Convenience.getSettings('org.gnome.shell.keybindings');
        Main.wm.addKeybinding('toggle-application-view',
                              shellKeybindingsSettings,
                              Meta.KeyBindingFlags.NONE,
                              Shell.ActionMode.NORMAL |
                              Shell.ActionMode.OVERVIEW,
                              Lang.bind(Main.overview.viewSelector, Main.overview.viewSelector._toggleAppsPage));

        Main.wm.addKeybinding('toggle-overview',
                              shellKeybindingsSettings,
                              Meta.KeyBindingFlags.NONE,
                              Shell.ActionMode.NORMAL |
                              Shell.ActionMode.OVERVIEW,
                              Lang.bind(Main.overview, Main.overview.toggle));
    },
    
    removePrefbindings: function() {
        for (let i = 0; i < this.settingsHandler.length; i++) {
            this.settings.disconnect(this.settingsHandler[i]);
        }
    },
    
    removeKeybindings: function() {
        if (this.activeKeybindings) {
            for (let key in this.internalShortcuts) {
                Main.wm.removeKeybinding(key);
            }
            this.activeKeybindings = false;
        }
    },
    
    removeSidebar: function() {
        if (this.sliderTranslationHandler)
            return;
        
        if (Main.overview.visible && this.sidebarSlider._visible) {
            this.sliderTranslationHandler = this.sidebarSlider.connect('sidebar-translation-finished', () => {
                this.sidebarSlider.removeSidebars();
                this.sidebarSlider.disconnect(this.sliderTranslationHandler);
                this.sliderTranslationHandler = null;
                let dashIsDock = (getDash().constructor.name.toLowerCase().indexOf('dashtodock') != -1);
                let dashIsLeftDock = dashIsDock && getDash()._position == 3; // 3 is St.Side.LEFT
                
                if (dashIsLeftDock) {
                    let dashtodockContainer = Main.uiGroup.get_children().find(child => { return child.name == 'dashtodockContainer'; });
                    if (dashtodockContainer) {
                        let dockedDash = dashtodockContainer._delegate || dashtodockContainer;
                        dockedDash._show();
                    }
                } else if (!dashIsDock && !MainOverviewControls._dashSlider._visible) {
                    MainOverviewControls._dashSlider._visible = true;
                    MainOverviewControls._dashSlider.pageEmpty();
                }
            });
            
            this.sidebarSlider.slideOut();
        } else {
            this.sidebarSlider.removeSidebars();
        }
    },

    removeHotCorner: function() {
        if (this.hotCorner) {
            this.hotCorner.disconnect(this.hotCornerHandler);
            this.hotCornerHandler = null;
            this.hotCorner.disable();
            this.hotCorner = null;
        }
    },
    
    removePanelButton: function() {
        if (this.panelButton) {
            this.panelButton.disable();
            this.panelButton = null;
        }
    },
    
    removeDashButton: function() {
        if (this.dashButton) {
            ExtensionManager.disconnect(this.extensionsChangedHandler);
            this.dashButton.dashActor.disconnect(this.dashDestroyedHandler);
            if (this.dashDestroyedTimeout) {
                Mainloop.source_remove(this.dashDestroyedTimeout);
                this.dashDestroyedTimeout = null;
            }
            this.dashButton.disable();
            this.dashButton = null;
        }
    },
    
    removeToggles: function() {
        this.removeHotCorner();
        this.removePanelButton();
        this.removeDashButton();
    },
    
    improveOverviewSwitching: function() {
        if (!this.settings.get_boolean('improve-overview-switching')) {
            this.unImproveOverviewSwitching();
            return;
        }
        
        this.overviewSwitchingIsImproved = true;
        Main.wm.removeKeybinding('toggle-application-view');
        Main.wm.removeKeybinding('toggle-overview');
        
        let shellKeybindingsSettings = Convenience.getSettings('org.gnome.shell.keybindings');
        Main.wm.addKeybinding('toggle-application-view',
                              shellKeybindingsSettings,
                              Meta.KeyBindingFlags.NONE,
                              Shell.ActionMode.NORMAL |
                              Shell.ActionMode.OVERVIEW,
                              _toggleAppsPageWrapper);
        Main.wm.addKeybinding('toggle-overview',
                              shellKeybindingsSettings,
                              Meta.KeyBindingFlags.NONE,
                              Shell.ActionMode.NORMAL |
                              Shell.ActionMode.OVERVIEW,
                              toggleWrapper);
    },
    
    //these keybindings are added when overview is shown and removed when overview is hidden, cf this._init
    //I could use 'key-press-event' rather than keybindings but Tab key isn't listened
    addKeybindings: function() {
        for (let key in this.internalShortcuts)
            Main.wm.addKeybinding(key,
                                  this.internalShortcutsSettings,
                                  Meta.KeyBindingFlags.NONE,
                                  Shell.ActionMode.OVERVIEW,
                                  this.internalShortcuts[key]);
        this.activeKeybindings = true;
    },
    
    onPageChanged: function() {
        if ((Main.overview.viewSelector._activePage == this.filesPage) && (Main.actionMode == Shell.ActionMode.OVERVIEW) && (!this.activeKeybindings)) {
            this.addKeybindings();
        } else if ((Main.overview.viewSelector._activePage != this.filesPage) && this.activeKeybindings) {
            this.removeKeybindings();
        }
        
        // that is for dash
        if ((Main.overview.viewSelector._activePage == this.filesPage) && (Main.actionMode == Shell.ActionMode.OVERVIEW)) {
            let dashIsDock = (getDash().constructor.name.toLowerCase().indexOf('dashtodock') != -1);
            let dashIsLeftDock = dashIsDock && getDash()._position == 3; // 3 is St.Side.LEFT
            if (!this.settings.get_boolean('sidebar') || dashIsDock && !dashIsLeftDock) {
                // solve dash-to-dock conflict
                if (dashIsDock) {
                    let dashtodockContainer = Main.uiGroup.get_children().find(child => { return child.name == 'dashtodockContainer'; });
                    if (dashtodockContainer) {
                        let dockedDash = dashtodockContainer._delegate || dashtodockContainer;
                        Mainloop.timeout_add(0, () => {
                            dockedDash._show();
                            return false;
                        });
                    }
                } else {
                    MainOverviewControls._dashSlider.slideIn();
                }
            } else if (this.settings.get_boolean('sidebar')) {
                // solve dash-to-dock conflict
                if (dashIsDock) {
                    let dashtodockContainer = Main.uiGroup.get_children().find(child => { return child.name == 'dashtodockContainer'; });
                    if (dashtodockContainer) {
                        let dockedDash = dashtodockContainer._delegate || dashtodockContainer;
                        Mainloop.timeout_add(0, () => {
                            dockedDash._removeAnimations();
                            dockedDash._animateOut(0,0);
                            return false;
                        });
                    }
                } else if (MainOverviewControls._dashSlider._visible) {
                    MainOverviewControls._dashSlider._visible = false;
                    MainOverviewControls._dashSlider._updateTranslation();
                }
            }
        }
    },
    
    onOverviewShown: function() {
        if ((Main.overview.viewSelector._activePage == this.filesPage) && !this.activeKeybindings)
            this.addKeybindings();
    },
    
    addSidebar: function() {
        if (!this.settings.get_boolean('sidebar')) {
            this.removeSidebar();
            return;
        }
        
        if (this.sliderTranslationHandler)
            return;
        
        if (Main.overview.visible) {
            let dashIsDock = (getDash().constructor.name.toLowerCase().indexOf('dashtodock') != -1);
            let dashIsLeftDock = dashIsDock && getDash()._position == 3; // 3 is St.Side.LEFT
            
            if (dashIsLeftDock) {
                let dashtodockContainer = Main.uiGroup.get_children().find(child => { return child.name == 'dashtodockContainer'; });
                if (dashtodockContainer) {
                    let dockedDash = dashtodockContainer._delegate || dashtodockContainer;
                    dockedDash._removeAnimations();
                    dockedDash._animateOut(0,0);
                }
            } else if (!dashIsDock && MainOverviewControls._dashSlider._visible) {
                MainOverviewControls._dashSlider._visible = false;
                MainOverviewControls._dashSlider.pageEmpty();
            }
            
            let sidebarUpdateHandler = this.sidebarSlider.connect('sidebar-updated', () => {
                this.sidebarSlider.slideIn();
                this.sidebarSlider._updateTranslation();
                this.sidebarSlider.disconnect(sidebarUpdateHandler);
            });
        }
        
        this.sidebarSlider.addSidebars();
    },
    
    updateSidebar: function() {
        if (this.sliderTranslationHandler)
            return;
        
        this.sidebarSlider.updateSidebars();
    },

    addHotCorner: function() {
        this.removeHotCorner();
        let position = this.settings.get_string('hot-corner');
        let sensitivity = this.settings.get_double('hot-corner-sensitivity');
        if (position !== 'none') {
            this.hotCorner = new HotCorner.HotCorner(position, sensitivity);
            this.hotCornerHandler = this.hotCorner.connect('hot-corner-event', Lang.bind(this, this.toggleFilesPage));
        }
    },
    
    changeHotCornerSensitivity: function() {
        if (this.hotCorner)
            this.hotCorner.setSensitivity(this.settings.get_double('hot-corner-sensitivity'));
    },
    
    addPanelButton: function() {
        this.removePanelButton();
        if (this.settings.get_string('panel-button') != 'none')
            this.panelButton = new Buttons.PanelButton(Lang.bind(this, this.toggleFilesPage), this.filesPage);
    },
    
    updatePanelButtonPosition: function() {
        if (this.panelButton)
            this.panelButton.updateButtonPosition();
    },
    
    updatePanelIcon: function() {
        if (this.panelButton)
            this.panelButton.updateIcon();
    },
    
    updateDashIcon: function() {
        if (this.dashButton)
            this.dashButton.updateIcon();
    },
    
    addDashButton: function() {
        let position = this.settings.get_string('dash-button');
        
        if (this.dashButton && position !== 'none') {
            this.dashButton.changeButtonPosition(position);
        } else if (position !== 'none') {
            this.dashButton = new Buttons.DashButton(position, Lang.bind(this, this.toggleFilesPage), this.filesPage);
            this.extensionsChangedHandler = ExtensionManager.connect('extension-state-changed', (evt, extension) => {
                if ((['dash-to-panel@jderose9.github.com', 'ubuntu-dock@ubuntu.com', 'dash-to-dock@micxgx.gmail.com'].indexOf(extension.uuid) !== -1) && (extension.state === 1) ) {
                    this.removeDashButton();
                    this.addDashButton();
                }
            });
            this.dashDestroyedHandler = this.dashButton.dashActor.connect('destroy', () => {
                ExtensionManager.disconnect(this.extensionsChangedHandler);
                this.dashButton = null;
                this.dashDestroyedTimeout = Mainloop.timeout_add(100, () => {
                    Mainloop.source_remove(this.dashDestroyedTimeout);
                    this.dashDestroyedTimeout = null;
                    this.addDashButton();
                });
            });
        } else {
            this.removeDashButton();
        }
    },
    
    addToggles: function() {
        this.addHotCorner();
        this.addPanelButton();
        this.addDashButton();
    },
    
    resortFolderViews: function() {
        if (this.filesDisplay.getFolderView())
            this.filesDisplay.getFolderView().resort();
        this.filesDisplay.removeOldFolderView();
    },
    
    updateFolderViews: function() {
        if (this.filesDisplay.getFolderView())
            this.filesDisplay.getFolderView().redisplay();
        this.filesDisplay.removeOldFolderView();
    },
    
    generateThumbnails: function(large) {
        if (this.filesDisplay.getFolderView())
            this.filesDisplay.getFolderView().generateThumbnails(large);
    },
    
    toggleHiddenFiles: function() {
        this.settings.set_boolean('show-hidden-files', !this.settings.get_boolean('show-hidden-files'));
    },
    
    increaseColumnsNumber: function() {
        this.settings.set_int('max-columns', Math.min(this.settings.get_int('max-columns') + 1, 14));
    },
    
    decreaseColumnsNumber: function() {
        this.settings.set_int('max-columns', Math.max(this.settings.get_int('max-columns') - 1, 1));
    },
    
    increaseRowsNumber: function() {
        this.settings.set_int('min-rows', Math.min(this.settings.get_int('min-rows') + 1, 12));
    },
    
    decreaseRowsNumber: function() {
        this.settings.set_int('min-rows', Math.max(this.settings.get_int('min-rows') - 1, 2));
    },
    
    undoOperation: function() {
        if (this.settings.get_boolean('file-manager') && this.fileManager.hasUndo && !this.fileManager.undoIsRedo)
            this.fileManager.onUndoClicked();
    },
    
    redoOperation: function() {
        if (this.settings.get_boolean('file-manager') && this.fileManager.hasUndo && this.fileManager.undoIsRedo)
            this.fileManager.onUndoClicked();
    },
    
    showHideSidebar: function() {
        if (this.settings.get_boolean('sidebar') && this.settings.get_boolean('second-sidebar')) {
            this.settings.set_boolean('sidebar', false);
        } else if (this.settings.get_boolean('sidebar')) {
            this.settings.set_boolean('second-sidebar', true);
        } else {
            this.settings.set_boolean('second-sidebar', false);
            this.settings.set_boolean('sidebar', true);
        }
    },
    
    toggleFilesPage: function(actor) {
        Main.overview.viewSelector.trueOldPage = Main.overview.viewSelector._activePage;
        let activePage = Main.overview.viewSelector._activePage;
        
        if (Main.overview.viewSelector._activePage !== this.filesPage) {
            this.filesDisplay.switchDefaultView();
            if (!Main.overview.visible) {
                Main.overview.viewSelector.requestedPage = this.filesPage;
                Main.overview.viewSelector.fromOverview = false;
                Main.overview.viewSelector._showAppsButton.checked = false;
                Main.overview.show();
            } else {
                Main.overview.viewSelector.trueNewPage = this.filesPage;
                Main.overview.viewSelector.fromOverview = true;
                Main.overview.viewSelector._showAppsButton.checked = false;
                Main.overview.viewSelector._showPage(this.filesPage);
            }
            
        } else {
            if (this.dashButton && actor && (actor == this.dashButton.showFilesIcon.toggleButton) && !this.dashButton.dashIsDock) {
                // go to the previous page;
                Main.overview.viewSelector._showAppsButton.checked = (this.previousPage == Main.overview.viewSelector._appsPage);
            } else
                Main.overview.hide();
        }
        
        this.previousPage = activePage;
    },
    
    disable: function() {
        if (this.dashTimeout)
            Mainloop.source_remove(this.dashTimeout);
        Main.overview.viewSelector.disconnect(this.pageChangedHandler);
        Main.overview.disconnect(this.overviewShownHandler);
        Main.overview.disconnect(this.overviewHiddenHandler);
        this.removeSidebar();
        this.sidebarSlider.disable();
        delete MainOverviewControls._sidebarSlider;
        this.removeToggles();
        this.removePrefbindings();
        this.removeKeybindings();
        this.unImproveOverviewSwitching();
        Main.wm.removeKeybinding('toggle-files-view');
        Main.ctrlAltTabManager.removeGroup(this.filesDisplay.actor);
        let viewSelectorActor = GS_VERSION < "3.36.0" ? Main.overview.viewSelector.actor : Main.overview.viewSelector;
        viewSelectorActor.remove_child(this.filesPage);
        delete Main.overview.viewSelector.filesDisplay;
        this.filesDisplay.disable();
        delete Main.overview.viewSelector._filesPage;
        this.filesPage.destroy();
        delete Main.overview.viewSelector.filesSearchEntry;
        this.searchManager.disable();
        delete Main.overview.fileManager;
        this.fileManager.disable();
        delete Main.overview.favoritesManager;
        this.favoritesManager.disable();
    }
});

const _animateIn = function(oldPage) {
    if (oldPage)
        oldPage.hide();

    this.emit('page-empty');

    this._activePage.show();
    
    this.showFilesButton = Buttons.getShowFilesButton();
    
    if (this._activePage == this._appsPage && oldPage == this._workspacesPage) {
        this._activePage.opacity = 255;
        this.appDisplay.animate(IconGrid.AnimationDirection.IN);
    } else if (this.filesDisplay && this._activePage == this._filesPage && this.trueOldPage == this._workspacesPage && this.showFilesButton && Main.overview.viewSelector.fromOverview) {
        this._activePage.opacity = 255;
        this.filesDisplay.animate(IconGrid.AnimationDirection.IN);
    } else {
        this._fadePageIn();
    }
};

const _animateOut = function(page) {
    let oldPage = page;
    
    this.showFilesButton = Buttons.getShowFilesButton();
    
    if (page == this._appsPage && this._activePage == this._workspacesPage && this.trueNewPage != this._filesPage && !Main.overview.animationInProgress) {
        this.appDisplay.animate(IconGrid.AnimationDirection.OUT, () => {
            this._animateIn(oldPage);
        });
    } else if (this.filesDisplay && page == this._filesPage && this._activePage == this._workspacesPage && this.showFilesButton && !Main.overview.animationInProgress) {
        this.filesDisplay.animate(IconGrid.AnimationDirection.OUT, () => {
            this._animateIn(oldPage);
        });
    } else {
        this._fadePageOut(page);
    }
    this.trueNewPage = null;
};

// shell injections to move cleanly from one overview page to another
// the shell overview system is designed to deal with 2 pages (showAppsButton has 2 states)
const inject = function() {
    // dash display
    MainOverviewControls._dashSlider.slideOut = function() {
        if (Main.overview.viewSelector._activePage != Main.overview.viewSelector._workspacesPage &&
            Main.overview.viewSelector._activePage != Main.overview.viewSelector._appsPage)
            return;
        Lang.bind(this, OverviewControls.SlidingControl.prototype.slideOut)();
    };
    
    // CtrlAltTab
    Main.overview.viewSelector._a11yFocusPage = function(page) {
        if (page.toggle) {
            if (!page.mapped)
                page.toggle();
            page.navigate_focus(null, 0, false); // 0 is St.DirectionType.TAB_FORWARD
            return;
        }
        Lang.bind(Main.overview.viewSelector, ViewSelector.ViewSelector.prototype._a11yFocusPage)(page);
    };
    
    // add filesPage animation
    // we don't want animation between appsPage and filesPage
    // requestedPage, trueOldPage, trueNewPage and fromOverview prop are added to viewSelector
    // closely linked to displayManager.toggleFilesPage()
    
    Main.overview.viewSelector._animateIn = _animateIn;
    Main.overview.viewSelector._animateOut = _animateOut;
    
    if (GS_VERSION < '3.37') {
        Main.overview.viewSelector.show = function() {
            if (this.requestedPage) {
                this.reset();
                this._workspacesDisplay.show(true);
                this._activePage = null;
                this._showPage(this.requestedPage);
                this.requestedPage = null;
                if (!this._workspacesDisplay.activeWorkspaceHasMaximizedWindows())
                    Main.overview.fadeOutDesktop();
            } else {
                Lang.bind(this, ViewSelector.ViewSelector.prototype.show)();
            }
        };
    } else {
        Main.overview.viewSelector.animateToOverview = function() {
        	if (this.requestedPage) {
                this.show();
                this.reset();
                this._workspacesDisplay.animateToOverview(true);
                this._activePage = null;
                this._showPage(this.requestedPage);
                this.requestedPage = null;
                if (!this._workspacesDisplay.activeWorkspaceHasMaximizedWindows())
                    Main.overview.fadeOutDesktop();
            } else {
                Lang.bind(this, ViewSelector.ViewSelector.prototype.animateToOverview)();
            }
        };
    }
    
    Main.overview.viewSelector.requestedPage = null;
    Main.overview.viewSelector.trueOldPage = null;
    Main.overview.viewSelector.trueNewPage = null;
    Main.overview.viewSelector.fromOverview = false;
};

function init() {
    Convenience.initTranslations();
}

let displayManager;
let startupCompleteHandler = null;

function enable() {
    if (ExtensionUtils.isOutOfDate(Me))
        log('GNOME Shell ' + Number.parseFloat(GS_VERSION) + ' is not supported.');
    
    Me.settings = Convenience.getSettings();
    displayManager = new DisplayManager();
    
    // if Gnome shell is starting, we wait until it is completed
    // to add icons because of dash extensions
    // and to do injections (war of extensions ..)
    // (e.g. dtp overwrites _animateIn and _animateOut for minor changes)
    
    if (Main.actionMode == Shell.ActionMode.NONE) {
        startupCompleteHandler = Main.layoutManager.connect('startup-complete', () => {
            displayManager.addToggles();
            inject();
            
            if (startupCompleteHandler) {
                Main.layoutManager.disconnect(startupCompleteHandler);
                startupCompleteHandler = null;
            }
        });
    } else {
        displayManager.addToggles();
        inject();
    }
}

function disable() {
    MainOverviewControls._dashSlider.slideOut = OverviewControls.SlidingControl.prototype.slideOut;
    Main.overview.viewSelector._a11yFocusPage = ViewSelector.ViewSelector.prototype._a11yFocusPage;
    Main.overview.viewSelector._animateIn = ViewSelector.ViewSelector.prototype._animateIn;
    Main.overview.viewSelector._animateOut = ViewSelector.ViewSelector.prototype._animateOut;
    if (GS_VERSION < '3.37')
        Main.overview.viewSelector.show = ViewSelector.ViewSelector.prototype.show;
    else
        Main.overview.viewSelector.animateToOverview = ViewSelector.ViewSelector.prototype.animateToOverview;
    
    delete Main.overview.viewSelector.requestedPage;
    delete Main.overview.viewSelector.trueOldPage;
    delete Main.overview.viewSelector.trueNewPage;
    delete Main.overview.viewSelector.fromOverview;
    
    if (startupCompleteHandler) {
        Main.layoutManager.disconnect(startupCompleteHandler);
        startupCompleteHandler = null;
    }
    
    displayManager.disable();
    displayManager = null;
    delete Me.settings;
}



/*
 * Copyright 2018 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2018 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 6 */
 
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gdk = imports.gi.Gdk;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Signals = imports.signals;

const ExtensionUtils = imports.misc.extensionUtils;
const Main = imports.ui.main;
const MessageTray = imports.ui.messageTray;
const Params = imports.misc.params;

const Me = ExtensionUtils.getCurrentExtension();
const UUID = Me.uuid.replace(/@/gi, '_at_').replace(/[^a-z0-9+_-]/gi, '_');

// use Nautilus locales for notification texts
const _N = imports.gettext.domain('nautilus').gettext;

/************** Semantics ******************
 * file: a file that is a directory or not *
 * dir: a file that is a directory         *
 * doc: a file that is not a directory     *
 *******************************************/
 
const getDisplayName = function(file) {
    try {
        let info = file.query_info('standard::display-name', 0, null);
        return info.get_display_name();
    } catch(e) {
        if (e instanceof Gio.IOErrorEnum)
            return file.get_basename();
        else
            return null;
    }
};
    
const getCopyName = function(file) {
    try {
        let info = file.query_info('standard::copy-name', 0, null);
        let name = info.get_attribute_string('standard::copy-name');
        if (name)
            return name;
        else
            return file.get_basename();
    } catch(e) {
        return file.get_basename();
    }
};

// 'readme.md' => ['readme','.md']
const stripExtension = function(name) {
    let index = name.lastIndexOf('.');
    if (index <= 0 || index == name.length -1)
        return [name, ''];
    else
        return [name.slice(0, index), name.slice(index)];
};

const formatName = function(name) {
    if (name.length <= 25)
        return name;
    
    return name.slice(0, 12) + "…" + name.slice(-12);
};

var FileManager = new Lang.Class({
    Name: UUID + '-FileManager',
    
    _init : function() {
        this.operations = [];
        this.cancellable = new Gio.Cancellable();
        this.undoText = _N("Undo");
        this.cancelText = _N("Cancel");
        this.isWorking = false;
        this.hasTrash = Gio.File.new_for_uri('trash:///').query_exists(null);
        
        this.notificationSender = new NotificationSender();
    },
    
    disable: function() {
        this.notificationSender.disable();
    },
    
    setIsWorking: function(isWorking) {
        if (isWorking == this.isWorking)
            return;
        this.isWorking = isWorking;
        this.emit('working-changed');
    },
    
    _reset: function() {
        this.finishedOperationsCount = 0;
        this.operations = [];
        this.undoText = _N("Undo");
        this.setIsWorking(true);
        this.errors = [];
        this.cancelledMessage = null;
    },
    
    get clipboard() {
        // Gtk.clipboard rather than St.Clipboard because we need to clear clipboard
        // content after cut&paste and St doesn't provides clipboard clearing method
        
        // Use a getter because Gdk.Display.get_default() is not available at GS startup (return null) under Wayland
        
        if (!this._clipboard)
            this._clipboard = Gtk.Clipboard.get_default(Gdk.Display.get_default());
        return this._clipboard;
    },
    
    // from https://gitlab.gnome.org/World/ShellExtensions/desktop-icons desktopGrid.js
    parseClipboardText: function(text) {
        if (!text)
            return [false, false, null];
        
        // Since 3.38 there is a line terminator character, that has to be removed with .trim().
        let lines = text.split('\n').map(line => line.trim());
        if (lines.length < 3)
            return [false, false, null];
        
        let [mime, action, uris] = [lines[0], lines[1], lines.slice(2)];
        if (mime.indexOf('x-special/nautilus-clipboard') !== 0)
            return [false, false, null];
        if (action.indexOf('copy') !== 0 && action.indexOf('cut') !== 0)
            return [false, false, null];
        let isCut = action == 'cut';
    
        /* Last line is empty due to the split */
        if (uris.length <= 1)
            return [false, false, null];
        /* Remove last line */
        uris.pop();

        return [true, isCut, uris];
    },
    
    getDocs: function(dir, docs) {
        try {
            let enumerator = dir.enumerate_children('standard::name,standard::type,access::can-read', Gio.FileQueryInfoFlags.NONE, null);
            let i = 0;
            let fileInfo = enumerator.next_file(null);
            while( fileInfo !== null) {
                if(fileInfo.get_file_type() == Gio.FileType.REGULAR && fileInfo.get_attribute_boolean('access::can-read')) {
                    docs.push(enumerator.get_child(fileInfo));
                }
                else if(fileInfo.get_file_type() == Gio.FileType.DIRECTORY  || fileInfo.get_file_type() == Gio.FileType.SYMBOLIC_LINK)
                    this.getDocs(enumerator.get_child(fileInfo), docs);
                
                i = i + 1;
                fileInfo = enumerator.next_file(null);
            }
            enumerator.close(null);
        } catch(e) {
            throw e;
        }
    },
    
    get hasUndo() {
        if (this.isWorking || !this.operations || this.operations.length == 0)
            return false;
            
        // if undo is done and there is errors, hide undo
        if (this.undoText == _N("Redo") && this.errors.length != 0)
            return false;
        
        for (let i = 0; i < this.operations.length; i++) {
            if (this.operations[i].hasUndo)
                return true;
        }
        return false;
    },
    
    get undoIconName() {
        if (this.undoText == _N("Undo"))
            return 'edit-undo-symbolic';
        else
            return 'edit-redo-symbolic';
    },
    
    get undoIsRedo() {
        return this.undoText == _N("Redo");
    },
    
    onUndoClicked: function() {
        if (this.isWorking)
            return;
        
        this.finishedOperationsCount = 0;
        this.errors = [];
        this.cancelledMessage = null;
        if (this.isWorking || !this.operations || this.operations.length == 0)
            return;
            
        this.undoText = (this.undoText == _N("Undo")) ? _N("Redo") : _N("Undo");
        
        for (let i = 0; i < this.operations.length; i++) {
            if (!this.operations[i].undo)
                continue;
            this.setIsWorking(true);
            this.operations[i].undo(this.cancellable);
        }
    },
    
    onCancelClicked: function() {
        this.cancellable.cancel();
        this.cancellable = new Gio.Cancellable();
    },
    
    onRenameClicked: function(file, newName) {
        if (this.isWorking)
            return;
        
        this.undoText = _N("Undo");
        this.rename(file, newName);
    },
    
    rename: function(file, newName) {
        this.operations = [];
        this.setIsWorking(true);
        this.errors = [];
        this.cancelledMessage = null;
        
        // remove spaces at the start and at the end.
        newName = newName.replace(/^\s+/g, '').replace(/\s+$/g, '');
        
        try {
            let newFile = file.set_display_name(newName, null);
            let name = getDisplayName(file);
            if (!name || !newFile)
                return;
            let renameUndo = () => { this.rename(newFile, name); };
            let operation = { undo: renameUndo, hasUndo: true };
            this.operations.push(operation); 
        } catch(e) {
            Main.notify(_N("The item could not be renamed."), e.message);
        }
        this.setIsWorking(false);
    },
    
    onDeleteClicked: function(file) {
        if (this.isWorking)
            return;
        
        this._reset();
        let operation = new FileOperation(null, null, this.cancellable);
        this.operations.push(operation);
        operation.connect('operation-finished', Lang.bind(this, this._onOperationFinished));
        operation.deleteOnly(file);
    },
    
    onTrashClicked: function(file) {
        if (this.isWorking || !this.hasTrash)
            return;
        
        this._reset();
        let operation = new TrashOperation(file, this.cancellable);
        operation.connect('operation-finished', Lang.bind(this, this._onOperationFinished));
        this.operations.push(operation);
        operation.trash();
    },
    
    onDeleteFromTrashClicked: function(file) {
        if (this.isWorking)
            return;
        
        this._reset();
        let operation = new TrashOperation(null, this.cancellable);
        operation.connect('operation-finished', Lang.bind(this, this._onOperationFinished));
        this.operations.push(operation);
        operation.deleteFromTrash(file);
    },
    
    onEmptyTrashClicked: function() {
        if (this.isWorking)
            return;
        
        this._reset();
        let operation = new TrashOperation(null, this.cancellable);
        operation.connect('operation-finished', Lang.bind(this, this._onOperationFinished));
        this.operations.push(operation);
        operation.emptyTrash();
    },
    
    onRestoreFromTrashClicked: function(file, originFile) {
        if (this.isWorking)
            return;
        
        this._reset();
        let operation = new TrashOperation(originFile, this.cancellable);
        operation.trashedFile = file;
        operation.connect('operation-progressed', Lang.bind(this, this._onOperationProgressed));
        operation.connect('operation-finished', Lang.bind(this, this._onOperationFinished));
        this.operations.push(operation);
        operation.restoreFromTrash();
    },
    
    onNewFolderClicked: function(parentDir, name) {
        if (this.isWorking)
            return;
        
        this._reset();
        
        let newDir = parentDir.get_child_for_display_name(name);
        while (newDir.query_exists(null))
            newDir = parentDir.get_child(newDir.get_basename() + _N(" (copy)"));
        
        let operation = new FileOperation(null, null, this.cancellable);
        operation.connect('operation-finished', Lang.bind(this, this._onOperationFinished));
        this.operations.push(operation);
        operation.createDir(null, newDir);
        
        // _onOperationFinished must be called manually because createDir() doesn't emit.
        // In undo/redo loop, no problem because _deleteSourceDirs does it.
        this._onOperationFinished(operation, operation.errors);
    },
    
    onNewDocClicked: function(templateDoc, parentDir) {
        if (this.isWorking)
            return;
        this._reset();
        
        let newDoc = parentDir.get_child(getCopyName(templateDoc));
        while (newDoc.query_exists(null)) {
            let [base, ext] = stripExtension(getCopyName(newDoc));
            newDoc = parentDir.get_child(base + _N(" (copy)") + ext);
        }
        
        let operation = new FileOperation(templateDoc, newDoc, this.cancellable);
        this.operations.push(operation);
        operation.connect('operation-progressed', Lang.bind(this, this._onOperationProgressed));
        operation.connect('operation-finished', Lang.bind(this, this._onOperationFinished));
        operation.copyFromTemplates();
    },
    
    onNewSymbolicLinkClicked: function(sourceFile) {
        if (this.isWorking)
            return;
        
        this._reset();
        let operation = new FileOperation(null, null, this.cancellable);
        operation.connect('operation-finished', Lang.bind(this, this._onOperationFinished));
        this.operations.push(operation);
        
        let destFile = sourceFile.get_parent().get_child(_N("Link to %s").format(sourceFile.get_basename()));
        if (destFile.query_exists(null)) {
            destFile = sourceFile.get_parent().get_child(_N("Another link to %s").format(sourceFile.get_basename()));
            let i = 3;
            while (destFile.query_exists(null)) {
                destFile = sourceFile.get_parent().get_child(_N("Link to %s").format(sourceFile.get_basename()) + ' ' + i);
                i++;
            }
        }
        
        operation.newSymbolicLink(sourceFile.get_path(), destFile);
    },
    
    onCopyClicked: function(file, noOverwrite) {
        if (!file.get_path())
            return;
        if (noOverwrite) {
            this.clipboard.request_text((clipboard, oldText) => {
                let text = oldText + file.get_uri() + '\n';
                this.clipboard.set_text(text, -1);
            });
        } else {
            let text = 'x-special/nautilus-clipboard\n' + 'copy' + '\n' + file.get_uri() + '\n';
            this.clipboard.set_text(text, -1);
        }
    },
    
    onCutClicked: function(file, noOverwrite) {
        if (!file.get_path())
            return;
        if (noOverwrite) {
            this.clipboard.request_text((clipboard, oldText) => {
                let text = oldText + file.get_uri() + '\n';
                this.clipboard.set_text(text, -1);
            });
        } else {
            let text = 'x-special/nautilus-clipboard\n' + 'cut' + '\n' + file.get_uri() + '\n';
            this.clipboard.set_text(text, -1);
        }
    },
    
    onPasteClicked: function(destDir) {
        if (this.isWorking)
            return;
        
        this.clipboard.request_text((clipboard, text) => {
            let [valid, isCut, uris] = this.parseClipboardText(text);
            if (!valid)
                return;
            
            let destDirPath = destDir.get_path();
            
            let toOperate = [];
            for (let i = 0; i < uris.length; i++) {
                let sourceFile = Gio.File.new_for_uri(uris[i]);
                let destFile = destDir.get_child(getCopyName(sourceFile));
                
                // rename destination file if already exists
                let isRegular;
                while (destFile.query_exists(null)) {
                    if (isRegular === undefined)
                        isRegular = sourceFile.query_file_type(Gio.FileQueryInfoFlags.NONE, null) == Gio.FileType.REGULAR;
                    let [base, ext] = isRegular ? stripExtension(getCopyName(destFile)) : [getCopyName(destFile), ""];
                    destFile = destDir.get_child(base + _N(" (copy)") + ext);
                }
                
                // prevent from copying folder in itself or a child (loop), something Gio is permissive with
                if (destFile.get_parent().equal(sourceFile) || destFile.get_parent().has_prefix(sourceFile)) {
                    if (isCut)
                        Main.notify(_N("You cannot move a folder into itself."), _N("The destination folder is inside the source folder."));
                    else
                        Main.notify(_N("You cannot copy a folder into itself."), _N("The destination folder is inside the source folder."));
                    continue;
                }
                
                toOperate.push([sourceFile, destFile]);
            }
            
            if (toOperate.length) {
                this._reset();
                
                for (let i = 0; i < toOperate.length; i++) {
                    let operation = new FileOperation(toOperate[i][0], toOperate[i][1], this.cancellable);
                    this.operations.push(operation);
                    operation.connect('operation-progressed', Lang.bind(this, this._onOperationProgressed));
                    operation.connect('operation-finished', Lang.bind(this, this._onOperationFinished));
                    
                    if (isCut)
                        operation.cut();
                    else
                        operation.copy();
                }
            }
            
            if (isCut) {
                clipboard.set_text("", -1);
                clipboard.clear();
            }
        });
    },
    
    onDndMove: function(sourceFile, destDir) {
        if (this.isWorking)
            return;
        
        this._reset();
        
        let destFile = destDir.get_child(getCopyName(sourceFile));
        // rename destination file if already exists
        let isRegular;
        while (destFile.query_exists(null)) {
            if (isRegular === undefined)
                isRegular = sourceFile.query_file_type(Gio.FileQueryInfoFlags.NONE, null) == Gio.FileType.REGULAR;
            let [base, ext] = isRegular ? stripExtension(getCopyName(destFile)) : [getCopyName(destFile), ""];
            destFile = destDir.get_child(base + _N(" (copy)") + ext);
        }
        
        let operation = new FileOperation(sourceFile, destFile, this.cancellable);
        this.operations.push(operation);
        operation.connect('operation-progressed', Lang.bind(this, this._onOperationProgressed));
        operation.connect('operation-finished', Lang.bind(this, this._onOperationFinished));
        operation.move();
    },
    
    _onOperationProgressed: function() {
        let text = "";
        
        this.operations.forEach(operation => {
            let [cumulCurrentNumBytes, cumulTotalNumBytes] = [0, 0];
            operation.progresses.forEach(progress => {
                cumulCurrentNumBytes += progress[0];
                cumulTotalNumBytes += progress[1];
            });
            
            if (cumulTotalNumBytes) {
                // split numbers and characters to set Monospace font family to numbers
                // separator is unbreakable space
                // .concat([""]) to ensure the length >= 2
                
                let currents = GLib.format_size(cumulCurrentNumBytes).split("\xa0").concat([""]);
                let totals = GLib.format_size(cumulTotalNumBytes).split("\xa0").concat([""]);
                
                text += text != "" ? "\n<small>" : "<small>";
                text += operation.sourceFile ? (formatName(operation.sourceFile.get_basename()) + "\xa0: ") : "";
                text += "<span font_family=\"Monospace\">" + currents[0] + "</span>" + "\xa0" + currents[1];
                text += " / ";
                text += "<span font_family=\"Monospace\">" + totals[0] + "</span>" + "\xa0" + totals[1];
                text += "</small>";
            }
        });
        
        text += text != "" ? "\n\n" : "";
        text += this.cancelText;
        
        this.emit('operations-progressed', text);
    },
    
    _onOperationFinished: function(operation, errors, cancelledMessage) {
        this.finishedOperationsCount++;
        this.errors = this.errors.concat(errors);
        if (cancelledMessage)
            this.cancelledMessage = cancelledMessage;
        
        if (this.finishedOperationsCount == this.operations.length) {
            this.setIsWorking(false);
            this.operations.forEach(operation => operation.resetProgress());
            
            if (this.errors[0]) {
                let msg = this.errors.join('\n');
                this.notificationSender.setMessage(_N("Oops! Something went wrong."),
                                                   { bannerText: msg, 
                                                     actionCallback: this.hasUndo ? Lang.bind(this, this.onUndoClicked) : null,
                                                     actionText: this.undoText,
                                                     forFeedback: true,
                                                     iconName: 'dialog-warning-symbolic',
                                                     critical: true,
                                                   });
                logError(new Error("Operation failed: \n" + msg), Me.metadata.uuid);
            } else {
                this.notificationSender.setMessage(this.cancelledMessage ? this.cancelledMessage : _N("All file operations have been successfully completed"),
                                                   { bannerText: null, 
                                                     actionCallback: this.hasUndo ? Lang.bind(this, this.onUndoClicked) : null,
                                                     actionText: this.undoText,
                                                     forFeedback: true,
                                                     iconName: 'emblem-ok-symbolic'
                                                   });
            }
        }
    }
});
Signals.addSignalMethods(FileManager.prototype);


const FileOperation = new Lang.Class({
    Name: UUID + '-FileOperation',
    
    _init : function(sourceFile, destFile, cancellable) {
        this.sourceFile = sourceFile;
        this.destFile = destFile;
        this.copyFlag = Gio.FileCopyFlags.ALL_METADATA;
        this.cancellable = cancellable;
        this.errors = [];
        this.cancelledMessage = null;
        this.isUndo = false;
        this.progressCallbacks = [];
        this.progresses = [];
        this._reset();
    },
    
    _reset: function() {
        this.movedFile = null;
        this.createdSymbolicLinks = [];
        this.deletedSymbolicLinks = [];
        [this.toCopyDirs, this.toCopyDocs] = [[], []];
        [this.copiedDirs, this.copiedDocs] = [[], []];
        [this.toDeleteDirs, this.toDeleteDocs] = [[], []];
        [this.deletedDirs, this.deletedDocs] = [[], []];
    },
    
    get hasUndo() {
        if (this.movedFile ||
            this.createdSymbolicLinks.length || this.deletedSymbolicLinks.length ||
            this.deletedDirs.length || this.deletedDocs.length ||
            this.copiedDocs.length || this.copiedDirs.length)
            
            return true;
        else
            return false;
    },
    
    undo: function(cancellable) {
        this.cancellable = cancellable;
        this.isUndo = true;
        this.errors = [];
        this.cancelledMessage = null;
        let oldMovedFile = this.movedFile;
        let oldCreatedSymbolicLinks = this.createdSymbolicLinks;
        let oldDeletedSymbolicLinks = this.deletedSymbolicLinks;
        let oldDeletedDirs = this.deletedDirs;
        let oldDeletedDocs = this.deletedDocs;
        let oldCopiedDocs = this.copiedDocs;
        let oldCopiedDirs = this.copiedDirs;
        
        this._reset();
        
        if (oldMovedFile) {
            [this.destFile, this.sourceFile] = oldMovedFile;
            this.move();
            return;
        }
        
        for (let i = 0; i < oldCreatedSymbolicLinks.length; i++) {
            let [targetFilePath, file] = oldCreatedSymbolicLinks[i];
            this._deleteSymbolicLink(targetFilePath, file);
        }
        
        for (let i = 0; i < oldCopiedDirs.length; i++) {
            let [destFile, sourceFile] = oldCopiedDirs[i];
            this.toDeleteDirs.push([sourceFile, destFile]);
        }
    
        for (let i = 0; i < oldCopiedDocs.length; i++) {
            let [destFile, sourceFile] = oldCopiedDocs[i];
            this.toDeleteDocs.push([sourceFile, destFile]);
        }
    
        for (let i = 0; i < oldDeletedDirs.length; i++) {
            let j = oldDeletedDirs.length - 1 - i;
            let [destFile, sourceFile] = oldDeletedDirs[j];
            this.toCopyDirs.push([sourceFile, destFile]);
            this.createDir(sourceFile, destFile);
        }
        
        for (let i = 0; i < oldDeletedSymbolicLinks.length; i++) {
            let [targetFilePath, destFile] = oldDeletedSymbolicLinks[i];
            this._createSymbolicLink(targetFilePath, destFile);
        }
    
        for (let i = 0; i < oldDeletedDocs.length; i++) {
            let j = oldDeletedDocs.length - 1 - i;
            let [destFile, sourceFile] = oldDeletedDocs[j];
            this.toCopyDocs.push([sourceFile, destFile]);
        }
        this._copyDocs();
            
    },
    
    resetProgress: function() {
        this.progressCallbacks = [];
        this.progresses = [];
    },
    
    createProgressCallback: function() {
        let id = this.progressCallbacks.length;
        let progressCallback = (currentNumBytes, totalNumBytes) => {
            this.progresses[id] = [currentNumBytes, totalNumBytes];
            this.emit('operation-progressed');
        };
        
        this.progressCallbacks.push(progressCallback);
        return progressCallback;
    },
    
    _onError: function(e) {
        if (e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.CANCELLED))
            this.cancelledMessage = e.message;
        else
            this.errors.push(e.message);
    },
    
    copyFromTemplates: function() {
        this.copyFlag = Gio.FileCopyFlags.NONE;
        this.copy();
    },
    
    copy: function() {
        this._copyDirs(this.sourceFile, this.destFile);
        this._copyDocs();
    },
    
    cut: function() {
        this.isCut = true;
        this.move();
    },
    
    move: function() {
        let sourceFileInfo = this.sourceFile.query_info('id::filesystem', Gio.FileQueryInfoFlags.NONE, null);
        let destParentFileInfo = this.destFile.get_parent().query_info('id::filesystem', Gio.FileQueryInfoFlags.NONE, null);
        let sourceFileFsId = sourceFileInfo.get_attribute_string('id::filesystem');
        let destParentFileFsId = destParentFileInfo.get_attribute_string('id::filesystem');
        
        // if destination is not in the same filesystem as source,
        // don't try move, copy directly (and delete if this.isCut)
        // (move has only a copy-delete callback for REGULAR files when filesystems are different)
        if ( sourceFileFsId != destParentFileFsId) {
            this.copy();
            return;
        }
    
        // try move (in the same filesystem), else copy (and delete if this.isCut)
        // no Cancellable because Gio.File.move() is not async
        try {
            let success = this.sourceFile.move(this.destFile, Gio.FileCopyFlags.ALL_METADATA, this.cancellable, this.createProgressCallback());
            if (success) {
                this.movedFile = [this.sourceFile, this.destFile];
            } else {
                this.errors.push(this.sourceFile.get_path() + " to " + this.destFile.get_parent().get_path() + ": " + _N("Error while moving."));
            }
            this.emit('operation-finished', this.errors);
        } catch(e) {
            // if source is a directory and source and destination are not in the same filesystem, also do copy and delete
            // (necessary ?)
            if (e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.WOULD_RECURSE)) {
                this.copy();
            } else {
                this._onError(e);
                this.emit('operation-finished', this.errors, this.cancelledMessage);
            }
        }
        
    },
    
    deleteOnly: function(file) {
        this.isDeleteOnly = true;
        this._enumerateFilesToDelete(file);
        this._deleteSourceFiles();
    },
    
    createDir: function(sourceFile, destFile) {
        try {
            let success = destFile.make_directory(this.cancellable);
            if (success) {
                this.copiedDirs.push([sourceFile, destFile]);
                return true;
            } else {
                this.errors.push("Fails to create directory " + sourceFile.get_path());
                return false;
            }
        } catch(e) {
            this._onError(e);
            return false;
        }
    },
    
    newSymbolicLink: function(sourceFilePath, destFile) {
        this._createSymbolicLink(sourceFilePath, destFile);
        this.emit('operation-finished', this.errors, this.cancelledMessage);
    },
    
    _createSymbolicLink: function(targetFilePath, destFile) {
        try {
            let success = destFile.make_symbolic_link(targetFilePath, this.cancellable);
            if (success) {
                this.createdSymbolicLinks.push([targetFilePath, destFile]);
                return true;
            } else {
                this.errors.push("Fails to create symbolic link " + destFile.get_path());
                return false;
            }
        } catch(e) {
            this._onError(e);
            return false;
        }
    },
    
    _deleteSymbolicLink: function(targetFilePath, file) {
        try {
            let success = file.delete(this.cancellable);
            if (success) {
                this.deletedSymbolicLinks.push([targetFilePath, file]);
            } else {
                this.errors.push("Fails to delete symbolic link " + file.get_path());
            }
        } catch(e) {
            this._onError(e);
        }
    },
    
    _enumerateFilesToDelete: function(file) {
        // if file is a directory or a symbolic link
        if (file.query_file_type(Gio.FileQueryInfoFlags.NONE, null) != Gio.FileType.DIRECTORY ||
            file.query_file_type(Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS, null) == Gio.FileType.SYMBOLIC_LINK) {
            
            this.toDeleteDocs.push([file, null]);
        } else {
            this.toDeleteDirs.push([file, null]);
            
            let enumerator;
            // prevent permission error : "Gio.IOErrorEnum: Error opening directory"
            try {
                enumerator = file.enumerate_children('standard::name', Gio.FileQueryInfoFlags.NONE, null);
            } catch(e) {
                this.errors.push(_N("Unable to access the requested location.") + " " + file.get_path() + ": " + e.message);
                return;
            }
            
            let i = 0;
            let fileInfo = enumerator.next_file(null);
            while( fileInfo !== null) {
                let child = enumerator.get_child(fileInfo);
                this._enumerateFilesToDelete(child);
                i = i + 1;
                fileInfo = enumerator.next_file(null);
            }
            
            enumerator.close(null);
        }
    },
    
    // create directories and symbolic link, and lists docs to be copied
    _copyDirs: function(sourceFile, destFile) {
        if (sourceFile.query_file_type(Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS, null) == Gio.FileType.SYMBOLIC_LINK) {
            let sourceInfo = sourceFile.query_info('standard::symlink-target', Gio.FileQueryInfoFlags.NONE, null);
            let targetFilePath = sourceInfo.get_symlink_target();
            let success = this._createSymbolicLink(targetFilePath, destFile);
            if (success && this.isCut)
                this._deleteSymbolicLink(targetFilePath, sourceFile);
        } else if (sourceFile.query_file_type(Gio.FileQueryInfoFlags.NONE, null) != Gio.FileType.DIRECTORY) {
            this.toCopyDocs.push([sourceFile, destFile]);
        } else {
            this.toCopyDirs.push([sourceFile, destFile]);
            
            // If destFile doesn't exist, create it
            // If destFile exist, but is not a directory, catch error
            if (!destFile.query_exists(null) || destFile.query_file_type(Gio.FileQueryInfoFlags.NONE, null) != Gio.FileType.DIRECTORY) {
                let success = this.createDir(sourceFile, destFile);
                if (!success)
                    return;
            }
            
            let enumerator;
            // prevent permission error : "Gio.IOErrorEnum: Error opening directory"
            try {
                enumerator = sourceFile.enumerate_children('standard::name', Gio.FileQueryInfoFlags.NONE, null);
            } catch(e) {
                this.errors.push(_N("Unable to access the requested location.") + " " + sourceFile.get_path() + ": " + e.message);
                return;
            }
            
            let i = 0;
            let fileInfo = enumerator.next_file(null);
            while( fileInfo !== null) {
                let sourceChild = enumerator.get_child(fileInfo);
                let destChild = destFile.get_child(getCopyName(sourceChild));
                this._copyDirs(sourceChild, destChild);
                i = i + 1;
                fileInfo = enumerator.next_file(null);
            }
            
            enumerator.close(null);
        }
    },
    
    _copyDocs: function() {
        this.finishedCopyCount = 0;
        let copyCallback = ((sourceObject, res) => {
            let destObject = this.toCopyDocs.filter(pair => { return pair[0] == sourceObject; })[0][1];
            try {
                if (sourceObject.copy_finish(res))
                    this.copiedDocs.push([sourceObject, destObject]);
                else
                    this.errors.push(sourceObject.get_path() + ": " + _N("Error while copying."));
            } catch(e) {
                this._onError(e);
            }
            this.finishedCopyCount++;
            
            if (this.finishedCopyCount == this.toCopyDocs.length) {
                if (this.isCut && !this.isUndo) {
                    [this.toDeleteDirs, this.toDeleteDocs] = [this.copiedDirs, this.copiedDocs];
                }
                this._deleteSourceFiles();
            }
        });
        
        if (this.toCopyDocs.length == 0) {
            if (this.isCut && !this.isUndo) {
                [this.toDeleteDirs, this.toDeleteDocs] = [this.copiedDirs, this.copiedDocs];
            }
            this._deleteSourceFiles();            
        }
        
        for (let i = 0; i < this.toCopyDocs.length; i++) {
            let [sourceFile, destFile] = this.toCopyDocs[i];
            if (sourceFile.copy_async)
                sourceFile.copy_async(destFile, this.copyFlag, 0, this.cancellable, this.createProgressCallback(), copyCallback);
            else {
                // no Gio.File.copy_async in GS version < 3.26
                try {
                    let success = sourceFile.copy(destFile, this.copyFlag, this.cancellable, this.createProgressCallback());
                    if (success)
                        this.copiedDocs.push([sourceFile, destFile]);
                    else
                        this.errors.push(sourceFile.get_path() + ": " + _N("Error while copying."));
                } catch(e) {
                    this._onError(e);
                }
                
                this.finishedCopyCount++;
                if (this.finishedCopyCount == this.toCopyDocs.length) {
                    if (this.isCut && !this.isUndo) {
                        [this.toDeleteDirs, this.toDeleteDocs] = [this.copiedDirs, this.copiedDocs];
                    }
                    this._deleteSourceFiles();
                }
            }
        }
    },
    
    _deleteSourceFiles: function() {
        let finishedDeletedDocsCount = 0;
        
        let deleteDocCallback = ((sourceObject, res) => {
            try {
                let destObject = this.toDeleteDocs.filter(pair => { return pair[0] == sourceObject; })[0][1];
                if (sourceObject.delete_finish(res)) {
                    this.deletedDocs.push([sourceObject, destObject]);
                } else
                    this.errors.push(_N("There was an error deleting the file “%s”.").format(sourceObject.get_path()));
            } catch(e) {
                this._onError(e);
            }
            finishedDeletedDocsCount++;
            if (finishedDeletedDocsCount == this.toDeleteDocs.length) {
                this._deleteSourceDirs();
            }
        });
        
        if (this.toDeleteDocs.length == 0)
            this._deleteSourceDirs();
            
        for (let i = 0; i < this.toDeleteDocs.length; i++) {
            let j = this.toDeleteDocs.length - 1 - i;
            let [sourceDoc, destDoc] = this.toDeleteDocs[j];
            sourceDoc.delete_async(0, this.cancellable, deleteDocCallback);
        }
    },
    
    _deleteSourceDirs: function() {
        for (let i = 0; i < this.toDeleteDirs.length; i++) {
            let j = this.toDeleteDirs.length - 1 - i;
            let [sourceDir, destDir] = this.toDeleteDirs[j];
            
            try {
                let success = sourceDir.delete(this.cancellable);
                if (success) {
                    this.deletedDirs.push([sourceDir, destDir]);
                } else
                    this.errors.push(_N("There was an error deleting the folder “%s”.").format(sourceDir.get_path()));
            } catch(e) {
                this._onError(e);
            }
        }
        if (this.isDeleteOnly)
            this._reset();
        this.emit('operation-finished', this.errors, this.cancelledMessage);
    }
});
Signals.addSignalMethods(FileOperation.prototype);

const TrashOperation = new Lang.Class({
    Name: UUID + '-TrashOperation',
    
    _init : function(file, cancellable) {
        this.trashedFile = null;
        this.errors = [];
        this.file = file; // the non-trashed file
        this.trashFile = Gio.File.new_for_uri('trash:///');
        this.cancellable = cancellable;
        this.cancelledMessage = null;
        this.progressCallbacks = [];
        this.progresses = [];
    },
    
    get hasUndo() {
        return (!this.file || this.errors[0]) ? false : true;
    },
    
    undo: function() {
        if (this.trashedFile)
            this.restoreFromTrash();
        else
            this.trash();
    },
    
    resetProgress: function() {
        this.progressCallbacks = [];
        this.progresses = [];
    },
    
    createProgressCallback: function() {
        let id = this.progressCallbacks.length;
        let progressCallback = (currentNumBytes, totalNumBytes) => {
            this.progresses[id] = [currentNumBytes, totalNumBytes];
            this.emit('operation-progressed');
        };
        
        this.progressCallbacks.push(progressCallback);
        return progressCallback;
    },
    
    _onError: function(e) {
        if (e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.CANCELLED))
            this.cancelledMessage = e.message;
        else
            this.errors.push(e.message);
    },
    
    deleteFromTrash: function(file) {
        this.toDeleteFiles = [file];
        this.deleteFiles();
    },
    
    emptyTrash: function() {
        this.toDeleteFiles = [];
                
        let enumerator = this.trashFile.enumerate_children('standard::,trash::orig-path,trash::deletion-date', Gio.FileQueryInfoFlags.NONE, null);
        let i = 0;
        let fileInfo = enumerator.next_file(null);
        while( fileInfo !== null) {
            this.toDeleteFiles.push(enumerator.get_child(fileInfo));
            i = i + 1;
            fileInfo = enumerator.next_file(null);
        }
        enumerator.close(null);
        
        this.deleteFiles();
    },
        
    deleteFiles: function() {
        let finishedDeletedFilesCount = 0;
        
        let callback = ((sourceObject, res) => {
            try {
                if (!sourceObject.delete_finish(res))
                    this.errors.push(_N("There was an error deleting the file “%s”.").format(getDisplayName(sourceObject)));
            } catch(e) {
                this._onError(e);
            }
            finishedDeletedFilesCount++;
            if (finishedDeletedFilesCount == this.toDeleteFiles.length) {
                this.emit('operation-finished', this.errors, this.cancelledMessage);
                this.toDeleteFiles = null;
            }
        });
        
        for (let i = 0; i < this.toDeleteFiles.length; i++) {
            this.toDeleteFiles[i].delete_async(0, this.cancellable, callback);
        }        
    },
    
    getTrashedFile: function() {
        try {
            let enumerator = this.trashFile.enumerate_children('standard::,trash::orig-path,trash::deletion-date', Gio.FileQueryInfoFlags.NONE, null);
            let i = 0;
            let fileInfo = enumerator.next_file(null);
            while( fileInfo !== null) {
                if (fileInfo.get_attribute_as_string("trash::orig-path") == this.file.get_path()) {
                    this.trashedFile = enumerator.get_child(fileInfo);
                    enumerator.close(null);
                    return this.trashedFile;
                }
                i = i + 1;
                fileInfo = enumerator.next_file(null);
            }
            enumerator.close(null);
        } catch(e) {
            this.errors.push(e.message);
        }
        this.trashedFile = null;
        return this.trashedFile;
    },
    
    restoreFromTrash: function() {
        this.errors = [];
        if (!this.trashedFile)
            return;
        try {
            let success = this.trashedFile.move(this.file, Gio.FileCopyFlags.ALL_METADATA, this.cancellable, this.createProgressCallback());
            if (success)
                this.trashedFile = null;
            else
                this.errors.push(_N("The item cannot be restored from trash"));            
        } catch(e) {
            this._onError(e);
        }
        this.emit('operation-finished', this.errors, this.cancelledMessage);
    },
    
    trash: function() {
        this.trashedFile = null;
        this.errors = [];
        
        let trashCallback = ((sourceObject, res) => {
            try {
                if (sourceObject.trash_finish(res)) {
                    this.trashedFile = this.getTrashedFile();
                } else
                    this.errors.push(sourceObject.get_path() + ": " + _N("Error while moving files to trash."));
            } catch(e) {
                this._onError(e);
            }
            this.emit('operation-finished', this.errors, this.cancelledMessage);
        });
        
        this.file.trash_async(0, this.cancellable, trashCallback);
    }
});
Signals.addSignalMethods(TrashOperation.prototype);


// forked from Overview.ShellInfo
const NotificationSender = new Lang.Class({
    Name: UUID + '-NotificationSender',

    _init: function() {
        this._source = null;
        this._undoCallback = null;
        
        // notifications prevent drag and drop
        this.dragBeginHandler = Main.overview.connect('files-view-item-drag-begin', Lang.bind(this, this._hideNotification));
    },
    
    disable: function() {
        if (this._source)
            this._source.destroy();
        Main.overview.disconnect(this.dragBeginHandler);
    },
    
    _hideNotification: function() {
        if (this._source && !this.isCritical)
            this._source.destroy();
    },

    _onActionClicked: function() {
        if (this._actionCallback)
            this._actionCallback();
        this._actionCallback = null;

        if (this._source)
            this._source.destroy();
    },

    setMessage: function(title, options) {
        options = Params.parse(options, { bannerText: null,
                                          actionText: _N("Undo"),
                                          actionCallback: null,
                                          forFeedback: false,
                                          critical: false,
                                          iconName: null // 'dialog-warning-symbolic', 'emblem-ok-symbolic'
                                        });

        if (!this._source) {
            this._source = new MessageTray.SystemNotificationSource();
            this._source.connect('destroy', () => {
                this._source = null;
            });
            Main.messageTray.add(this._source);
        }
        
        this._actionCallback = options.actionCallback;
        this.isCritical = options.critical;
        
        let gicon = options.iconName ? new Gio.ThemedIcon({ name: options.iconName }) : null;

        let notification = null;
        if (this._source.notifications.length == 0) {
            notification = new MessageTray.Notification(this._source, title, options.bannerText, { gicon: gicon });
            notification.setTransient(true);
            
            notification.setForFeedback(options.forFeedback);
            notification.setUrgency(options.critical ? MessageTray.Urgency.CRITICAL : MessageTray.Urgency.NORMAL);
            
            if (options.actionCallback)
                notification.addAction(options.actionText, Lang.bind(this, this._onActionClicked));
        } else {
            notification = this._source.notifications[0];
            
            notification.setForFeedback(options.forFeedback);
            notification.setUrgency(options.critical ? MessageTray.Urgency.CRITICAL : MessageTray.Urgency.NORMAL);
            
            notification.update(title, options.bannerText, { clear: true, gicon: gicon });
            
            if (options.actionCallback)
                notification.addAction(options.actionText, Lang.bind(this, this._onActionClicked));
            
            // it's not an error : new update (with clear: false) to show action button
            notification.update(title, options.bannerText, { clear: false, gicon: gicon });
        }
        
        notification.createBanner = function() {
            let banner = this.source.createBanner(this);
            let bannerActor = banner.add_style_class_name ? banner : banner.actor; // banner.actor: GS 3.34-
            bannerActor.add_style_class_name('files-view-manager-notification-banner');
            return banner;
        };
        
        if (this._source.showNotification)
            this._source.showNotification(notification);
        else
            this._source.notify(notification); // GS 3.34-
    }
});



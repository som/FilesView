/*
 * Copyright 2018 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2018 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 6 */

const Clutter = imports.gi.Clutter;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Lang = imports.lang;
const St = imports.gi.St;

const Config = imports.misc.config;
const ExtensionUtils = imports.misc.extensionUtils;
const Main = imports.ui.main;
const PopupMenu = imports.ui.popupMenu;
const ShellEntry = imports.ui.shellEntry;

const Me = ExtensionUtils.getCurrentExtension();
const Convenience = Me.imports.convenience;
const FileIcon = Me.imports.fileIcon;
const Mang = Me.imports.mang;
const Misc = Me.imports.misc;

// == Gtk/St.DirectionType
const DirectionType = { TAB_FORWARD: 0 ,TAB_BACKWARD: 1 };
const GS_VERSION = Config.PACKAGE_VERSION;
const MainOverviewSearchEntry = GS_VERSION < '3.36.0' ? Main.overview._searchEntry : Main.overview.searchEntry;
const _GS = imports.gettext.domain('gnome-shell').gettext;

const MAX_COMPLETION_MENU_ITEMS = 200;
const UUID = Me.uuid.replace(/@/gi, '_at_').replace(/[^a-z0-9+_-]/gi, '_');

// Based on GNOME Shell viewSelector
// (some code are moved to SearchManager)
// https://gitlab.gnome.org/GNOME/gnome-shell/blob/master/js/ui/viewSelector.js
const SearchEntry = new Lang.Class({
    Name: UUID + '-SearchEntry',
    Extends: St.Entry,
    Signals: { 'text-changed': {} },
    
    _init: function() {
        this.parent({ style_class: 'search-entry',
                      hint_text: _GS(GS_VERSION < '3.36.0' ? "Type to search…" : "Type to search"),
                      track_hover: true,
                      can_focus: true });
        
        this.settings = Me.settings;
        
        this._pathCompleter = new Gio.FilenameCompleter();
        this._pathCompleterHandler = this._pathCompleter.connect('got-completion-data', Lang.bind(this, this._onCompletionGot));
        
        this._historyManager = new HistoryManager(this);
        this._historyManager.lastItem();
        
        this._addContextMenu();
        
        this.searchActive = false;
        
        this.clutter_text.connect('text-changed', Lang.bind(this, this._onTextChanged));
        this.clutter_text.connect('activate', Lang.bind(this, this._onTextActivated));
        this.clutter_text.connect('key-press-event', Lang.bind(this, this._onKeyPress));
        
        this._iconClickedId = 0;
        this._capturedEventId = 0;
        
        this.connect('notify::mapped', Lang.bind(this, this._onMapped));
        this._stageKeyFocusHandler = global.stage.connect('notify::key-focus', Lang.bind(this, this._onStageKeyFocusChanged));
        
        this.set_primary_icon(new St.Icon({ style_class: 'search-entry-icon',
                                                   icon_name: 'edit-find-symbolic' }));
        this._clearIcon = new St.Icon({ style_class: 'search-entry-icon',
                                        icon_name: 'edit-clear-symbolic' });
        
        this.connect('destroy', Lang.bind(this, this._onDestroy));
    },
    
    _onDestroy: function() {
        global.stage.disconnect(this._stageKeyFocusHandler);
        if (this._capturedEventId > 0)
            global.stage.disconnect(this._capturedEventId);
        this._capturedEventId = 0;
        this._pathCompleter.disconnect(this._pathCompleterHandler);
        
        this._historyManager.disable();
        this._clearIcon.destroy();
    },
    
    _addContextMenu: function() {
        ShellEntry.addContextMenu(this);
        
        // Entry clutter_text loses it selection when opening menu,
        // so entry selection is stored at the start of the menu opening.
        // Copy item gets the former selection text on activated and
        // the entry regains all  former selection params on menu popped down.
        this.menu._updateCopyItem = Lang.bind(this.menu, function() {
            this.selection = this._entry.clutter_text.get_selection();
            this.selectionStart = this._entry.clutter_text.get_cursor_position();
            this.selectionEnd = this._entry.clutter_text.get_selection_bound();
            this._copyItem.setSensitive(!this._entry.clutter_text.password_char &&
                                        this.selection && this.selection != '');
        });
        this.menu._copyItem.connect('activate', Lang.bind(this.menu, function() {
            this._clipboard.set_text(St.ClipboardType.CLIPBOARD, this.selection);
        }));
        this.menu.connect('open-state-changed', (menu, isPoppedUp) => {
            if (!isPoppedUp)
                this.clutter_text.set_selection(menu.selectionStart, menu.selectionEnd);
        });
        
        // close popup-menu with popup-menu key
        this.menu.actor.connect('popup-menu', () => {
            if (this.menu.isOpen)
                this.menu.close(2);
        });
        this.menu._getMenuItems().forEach(item => {
            let itemActor = GS_VERSION < '3.33.0' ? item.actor : item;
            itemActor.connect('popup-menu', () => {
                if (item._getTopMenu().isOpen)
                    item._getTopMenu().close(2);
            });
        });
    },
    
    get view() {
        return Main.overview.viewSelector.filesDisplay.getCurrentView();
    },
    
    get directory() {
        return this.view.directory ? this.view.directory : null;
    },
    
    _searchCancelled: function() {
        if (this.clutter_text.text != '')
            this.reset();
    },
    
    reset: function() {
        if (Main.modalCount <= 1)
            global.stage.set_key_focus(null);

        this.text = '';

        this.clutter_text.set_cursor_visible(true);
        this.clutter_text.set_selection(0, 0);
    },
    
    _onStageKeyFocusChanged: function() {
        let focus = global.stage.get_key_focus();
        let appearFocused = this.contains(focus);
        let menuFocused = this.menu.actor.contains(focus);

        this.clutter_text.set_cursor_visible(appearFocused || menuFocused);

        if (appearFocused || menuFocused)
            this.add_style_pseudo_class('focus');
        else
            this.remove_style_pseudo_class('focus');
    },
    
    _onMapped: function() {
        if (this.mapped) {
            // Enable 'find-as-you-type'
            this._capturedEventId = global.stage.connect('captured-event', Lang.bind(this, this._onCapturedEvent));
            this.clutter_text.set_cursor_visible(true);
        } else {
            // Disable 'find-as-you-type'
            if (this._capturedEventId > 0)
                global.stage.disconnect(this._capturedEventId);
            this._capturedEventId = 0;
        }
    },
    
    _shouldTriggerSearch: function(symbol) {
        if (symbol == Clutter.KEY_Multi_key)
            return true;

        if (symbol == Clutter.KEY_BackSpace && this.searchActive)
            return true;

        let unicode = Clutter.keysym_to_unicode(symbol);
        if (unicode == 0)
            return false;

        if (String.fromCharCode(unicode).replace(/^\s+/g, '').replace(/\s+$/g, '') != '')
            return true;

        return false;
    },

    startSearch: function(event) {
        global.stage.set_key_focus(this.clutter_text);

        let synthEvent = event.copy();
        synthEvent.set_source(this.clutter_text);
        this.clutter_text.event(synthEvent, false);
    },
    
    _isActivated: function() {
        return this.clutter_text.text == this.get_text();
    },
    
    setLocation: function(directory) {
        let text = directory.is_native() ? directory.get_path() : directory.get_uri();
        if (text[text.length - 1] != '/')
            text += '/';
        this.focusSearch();
        this.clutter_text.set_text(text);
        this.remove_style_class_name('files-view-search-entry-valid');
    },
    
    _onTextChanged: function(se, prop) {
        this.emit('text-changed');
        
        if (this.clutter_text.get_text() == '#' && Main.overview.favoritesManager.allTagsManager)
            Main.overview.favoritesManager.allTagsManager.updateTags();
        
        this.remove_style_class_name('files-view-search-entry-error');
        if (this.clutter_text.get_text() != '' && this._getIsValid())
            this.add_style_class_name('files-view-search-entry-valid');
        else
            this.remove_style_class_name('files-view-search-entry-valid');
        
        this.searchActive = this.get_text() != '';

        if (this.searchActive) {
            this.set_secondary_icon(this._clearIcon);

            if (this._iconClickedId == 0)
                this._iconClickedId = this.connect('secondary-icon-clicked', Lang.bind(this, this.reset));
        } else {
            if (this._iconClickedId > 0) {
                this.disconnect(this._iconClickedId);
                this._iconClickedId = 0;
            }

            this.set_secondary_icon(null);
            this._searchCancelled();
        }
    },
    
    _getIsValid: function() {
        let text = this.clutter_text.get_text();
        
        if (text.indexOf('#') == 0 && Main.overview.favoritesManager.allTagsManager) {
            text = text.slice(1);
            return Main.overview.favoritesManager.allTagsManager.getIsTag(text);
        }
        
        if (text == '.' || text == '..')
            return false;
        
        text = this._prepend(text);
        
        if (GLib.uri_parse_scheme(text)) {
            let file = Gio.File.new_for_uri(text);
            return file.query_exists(null);
        } else if (text.indexOf('/') == 0) {
            let file = Gio.File.new_for_path(text);
            return file.query_exists(null);
        } else {
            return false;
        }
    },
    
    _onTextActivated: function() {
        let text = this.clutter_text.get_text();
        
        if (text.indexOf('#') == 0 && Main.overview.favoritesManager.allTagsManager) {
            text = text.slice(1);
            
            if (Main.overview.favoritesManager.allTagsManager.getIsTag(text))
                Main.overview.viewSelector.filesDisplay.openNewTagView(text);
            else
                this.add_style_class_name('files-view-search-entry-error');
            
            return;
        }
        
        text = this._prepend(text);
        
        if (text.indexOf('/') != 0 && !GLib.uri_parse_scheme(text))
            return;
        
        this._historyManager.addItem(text);
        let file = GLib.uri_parse_scheme(text) ? Gio.File.new_for_uri(text) : Gio.File.new_for_path(text);
        
        if (file.query_exists(null)) {
            Misc.openFile(file);
            this.reset();
        } else {
            this.add_style_class_name('files-view-search-entry-error');
        }
    },
    
    _prepend: function(text) {
        if (GLib.uri_parse_scheme(text))
            return text;
        if (text.indexOf('~/') == 0 || text == '~')
            return GLib.get_home_dir() + text.slice(1);
        if (text.indexOf('/') == 0)
            return text;
        
        if (this.directory && this.directory.get_path())
            return this.directory.resolve_relative_path(text).get_path();
            
        if (this.directory)
            return GLib.build_filenamev([this.directory.get_uri(), text]);
            
        return text;
    },
    
    _showCompletionMenu: function(relativePaths) {
        if (!this._completionMenu) {
            this._completionMenu = new SearchCompletionMenu(this);
        }
        
        let originalLength = relativePaths.length;
        relativePaths.splice(MAX_COMPLETION_MENU_ITEMS);
        
        this._completionMenu.removeAllCompletionActions();
        relativePaths.forEach((relativePath) => {
            let callback = () => {
                this.clutter_text.set_text(relativePath);
                this.clutter_text.set_cursor_position(-1);
                this.focusSearch();
            };
            
            let splits = relativePath.split('/');
            let [suffix, isDir] = splits[splits.length -1] ? [splits[splits.length - 1], false] : [splits[splits.length - 2] + '/', true];
            let hasChildren = isDir && Misc.hasChildren(Gio.File.new_for_path(this._prepend(relativePath)));
            
            this._completionMenu.addCompletionAction(suffix, callback, hasChildren);
        });
        
        if (originalLength > MAX_COMPLETION_MENU_ITEMS)
            this._completionMenu.add3DotsItem();
        
        this._completionMenu.open();
    },
    
    doTab: function() {
        this._onTabPressed();
    },
    
    reverseTab: function() {
        let text = this.clutter_text.get_text();
        if (text != '/' && text.slice(-1) == '/')
            text = text.slice(0, -1);
        
        let dirname = text == '.' ? '..' :
                      text == '..' ? '../..' :
                      text.slice(-2) == '..' ? text + '/..' :
                      GLib.path_get_dirname(text);
        
        // add condition to avoid some infinite "../../.. ..."
        if (this._prepend(dirname) != this._prepend(text)) {
            this.clutter_text.set_text(dirname == '/' ? '/' : dirname + '/');
            this.clutter_text.set_cursor_position(-1);
        }
        
        this._onTabPressed();
    },
    
    // Gio FilenameCompleter is very strange !
    // first case : the completer "knows" the directory where it's looking for a filename to complete
    //     -> it returns the result (_onTabPressed)
    // second case : the completer doesn't know the directory
    //     -> it returns null and sends the 'got-completion-data' signal when it is ready for a new request,
    //        the same request in fact (_onCompletionGot is the handler callback)
    // prefix is stored in this._prefix to be used by _onCompletionGot
    
    _onCompletionGot: function() {
        let text = this.clutter_text.get_text();
        
        let suffix = this._pathCompleter.get_completion_suffix(this._prefix);
        if (suffix && suffix.length > 0) {
            this.clutter_text.insert_text(suffix, -1);
            this.clutter_text.set_cursor_position(text.length + suffix.length);
        } else {
            let paths = this._pathCompleter.get_completions(this._prefix);
            if (paths.length > 1)
                this._showCompletionMenu(paths.map(path => text + path.slice(this._prefix.length)));
        }
    },
    
    _onTabPressed: function() {
        let text = this.clutter_text.get_text();
        
        // it's a tag
        if (text.indexOf('#') == 0 && Main.overview.favoritesManager.allTagsManager) {
            Main.overview.favoritesManager.allTagsManager.completeTag(text.slice(1), (completedText) => {
                this.clutter_text.set_text('#' + completedText);
                this.clutter_text.set_cursor_position(-1);
            });
            
            return;
        }
        
        let lastChar = text.slice(-1) == '/' ? '/' : '';
        this._prefix = this._prepend(text) + lastChar;
        
        // it's a directory
        if (lastChar == '/') {
            let directory = GLib.uri_parse_scheme(this._prefix) ? Gio.File.new_for_uri(this._prefix) : Gio.File.new_for_path(this._prefix);
            let childrenNames = Misc.getChildrenNames(directory);
            if (childrenNames && childrenNames.length) {
                this._showCompletionMenu(childrenNames.map(childName => text + childName));
            }
            
           return;
        }
        
        let suffix = this._pathCompleter.get_completion_suffix(this._prefix);
        // there is one suffix
        if (suffix && suffix.length > 0) {
            this.clutter_text.insert_text(suffix, -1);
            this.clutter_text.set_cursor_position(text.length + suffix.length);
        } else {
            let paths = this._pathCompleter.get_completions(this._prefix);
            // there is several suffixes
            if (paths.length > 1)
                this._showCompletionMenu(paths.map(path => text + path.slice(this._prefix.length)));
            
            // If no suffixes here, there is another chance to get a suffix in _onCompletionGot
        }
    },
    
    _onKeyPress: function(entry, event) {
        let symbol = event.get_key_symbol();
        let controlPressed = event.has_control_modifier();
        
        if (this._completionMenu && this._completionMenu.isOpen) {
            if (symbol == Clutter.KEY_Down) {
                this._completionMenu.getFocus();
                return Clutter.EVENT_STOP;
            }
            
            this._completionMenu.close();
            if (symbol == Clutter.KEY_Escape)
                return Clutter.EVENT_STOP;
        }
        
        if (symbol == Clutter.KEY_Escape) {
            if (this._isActivated()) {
                this.reset();
                return Clutter.EVENT_STOP;
            }
        } else if (symbol == Clutter.KEY_Up) {
            if (controlPressed)
                this._historyManager.setPrevItem(this.get_text());
            else
                Main.overview.viewSelector._activePage.navigate_focus(null, DirectionType.TAB_BACKWARD, false);
            return Clutter.EVENT_STOP;
        } else if (symbol == Clutter.KEY_Down) {
            if (controlPressed)
                this._historyManager.setNextItem(this.get_text());
            else
                Main.overview.viewSelector._activePage.navigate_focus(null, DirectionType.TAB_FORWARD, false);
            return Clutter.EVENT_STOP;
        } else if (symbol == Clutter.KEY_Tab) {
            if (this.searchActive)
                this._onTabPressed();
            else
                Main.overview.viewSelector._activePage.navigate_focus(null, DirectionType.TAB_FORWARD, false);
            return Clutter.EVENT_STOP;
        } else if (symbol == Clutter.KEY_ISO_Left_Tab) {
            Main.overview.viewSelector._activePage.navigate_focus(null, DirectionType.TAB_BACKWARD, false);
            return Clutter.EVENT_STOP;
        }
        
        return Clutter.EVENT_PROPAGATE;
    },
    
    _onCapturedEvent: function(actor, event) {
        // close completion menu when clicking outside
        if (this._completionMenu && this._completionMenu.isOpen) {
            if ((event.type() == Clutter.EventType.BUTTON_PRESS || event.type() == Clutter.EventType.TOUCH_BEGIN) &&
                (!event.get_source() || !this._completionMenu.box.contains(event.get_source()))) {
                this._completionMenu.close(2);
                return Clutter.EVENT_STOP;
            }
        }
        
        if (event.type() == Clutter.EventType.BUTTON_PRESS) {
            let source = event.get_source();
            if (source != this.clutter_text &&
                this.clutter_text.text == '' &&
                !this.clutter_text.has_preedit () &&
                !Main.layoutManager.keyboardBox.contains(source)) {
                // the user clicked outside after activating the entry, but
                // with no search term entered and no keyboard button pressed
                // - cancel the search
                this.reset();
            }
        }

        return Clutter.EVENT_PROPAGATE;
    },
    
    focusSearch: function() {
        this.grab_key_focus();
    }
});

var SearchManager = new Lang.Class({
    Name: UUID + '-SearchManager',
    
    _init: function() {
        this.searchEntry = new SearchEntry();
        this._searchEntryBin = new St.Bin({ child: this.searchEntry,
                                            x_align: GS_VERSION < '3.36.0' ? St.Align.MIDDLE : Clutter.ActorAlign.CENTER,
                                            visible: false });
        
        Main.overview._overview.insert_child_above(this._searchEntryBin, MainOverviewSearchEntry.get_parent());
        Main.ctrlAltTabManager.addGroup(this.searchEntry, _GS("Search"), 'edit-find-symbolic');
        
        this.pageChangedHandler = Main.overview.viewSelector.connect('page-changed', Lang.bind(this, this._onPageChanged));
        
        Main.overview.viewSelector._onStageKeyPressOld = Main.overview.viewSelector._onStageKeyPress;
        Main.overview.viewSelector._onStageKeyPress = Lang.bind(Main.overview.viewSelector, function(actor, event) {
            if (this._activePage == this._filesPage)
                return Clutter.EVENT_PROPAGATE;
            
            return this._onStageKeyPressOld(actor, event);
        });
        
        this._stageKeyPressId = 0;
        this._showinHandler = Main.overview.connect('showing', () => {
            this._stageKeyPressId = global.stage.connect('key-press-event', Lang.bind(this, this._onStageKeyPress));
        });
        this._hidingHandler = Main.overview.connect('hiding', () => {
            if (this._stageKeyPressId != 0) {
                global.stage.disconnect(this._stageKeyPressId);
                this._stageKeyPressId = 0;
            }
        });
    },
    
    disable: function() {
        Main.overview.viewSelector.disconnect(this.pageChangedHandler);
        Main.overview.disconnect(this._showinHandler);
        Main.overview.disconnect(this._hidingHandler);
        
        if (this._stageKeyPressId != 0) {
            global.stage.disconnect(this._stageKeyPressId);
            this._stageKeyPressId = 0;
        }
        
        Main.ctrlAltTabManager.removeGroup(this.searchEntry);
        Main.overview._overview.remove_child(this._searchEntryBin);
        this._searchEntryBin.destroy();
        
        Main.overview.viewSelector._onStageKeyPress = Main.overview.viewSelector._onStageKeyPressOld;
        delete Main.overview.viewSelector._onStageKeyPressOld;
    },
    
    _onStageKeyPress: function(actor, event) {
        if (Main.overview.viewSelector._activePage != Main.overview.viewSelector._filesPage)
            return Clutter.EVENT_PROPAGATE;
        
        // Ignore events while anything but the overview has
        // pushed a modal (system modals, looking glass, ...)
        if (Main.modalCount > 1)
            return Clutter.EVENT_PROPAGATE;

        let symbol = event.get_key_symbol();

        if (symbol == Clutter.KEY_Escape) {
            if (this.searchEntry.searchActive)
                this.searchEntry.reset();
            else
                Main.overview.hide();
            return Clutter.EVENT_STOP;
        } else if (this.searchEntry._shouldTriggerSearch(symbol)) {
            this.searchEntry.startSearch(event);
        } else if (!this.searchEntry.searchActive && !global.stage.key_focus) {
            if (symbol == Clutter.KEY_Tab || symbol == Clutter.KEY_Down) {
                Main.overview.viewSelector._activePage.navigate_focus(null, DirectionType.TAB_FORWARD, false);
                return Clutter.EVENT_STOP;
            } else if (symbol == Clutter.KEY_ISO_Left_Tab) {
                Main.overview.viewSelector._activePage.navigate_focus(null, DirectionType.TAB_BACKWARD, false);
                return Clutter.EVENT_STOP;
            }
        }
        return Clutter.EVENT_PROPAGATE;
    },
    
    _onPageChanged: function() {
        if (Main.overview.viewSelector._activePage == Main.overview.viewSelector._filesPage) {
            MainOverviewSearchEntry.get_parent().hide();
            this._searchEntryBin.show();
        } else {
            this._searchEntryBin.hide();
            MainOverviewSearchEntry.get_parent().show();
        }
    }
});

const HISTORY_LIMIT = 30;

// Based on GNOME Shell HistoryManager
// https://gitlab.gnome.org/GNOME/gnome-shell/blob/master/js/misc/history.js

const HistoryManager = new Lang.Class ({
    Name: UUID + '-HistoryManager',
    
    _init: function(entry) {
        this.settings = Me.settings;
        this._entry = entry;
        
        this._historyIndex = 0;
        
        this._privacySettings = Convenience.getSettings('org.gnome.desktop.privacy');
        this._privacySettingsHandler = this._privacySettings.connect('changed::remember-recent-files', Lang.bind(this, this._updateRemember));
        this._updateRemember();        
    },
    
    disable: function() {
        this._privacySettings.disconnect(this._privacySettingsHandler);
        if (this._historyHandler) {
            this.settings.disconnect(this._historyHandler);
            this._historyHandler = null;
        }
    },
    
    _updateRemember: function() {
        if (this._privacySettings.settingsSchema.has_key('remember-recent-files') && this._privacySettings.get_boolean('remember-recent-files')) {
            this._history = this.settings.get_strv('location-history');
            this._historyHandler = this.settings.connect('changed::location-history', Lang.bind(this, this._historyChanged));
            this._remember = true;
        } else {
            this._history = [];
            if (this._historyHandler) {
                this.settings.disconnect(this._historyHandler);
                this._historyHandler = null;
            }
            this._remember = false;
            this.settings.set_strv('location-history', []);
        }
    },

    _historyChanged: function() {
        this._history = this.settings.get_strv('location-history');
        this._historyIndex = this._history.length;
    },

    setPrevItem: function(text) {
        if (this._historyIndex <= 0)
            return false;

        if (text)
            this._history[this._historyIndex] = text;
        this._historyIndex--;
        this._indexChanged();
        return true;
    },

    setNextItem: function(text) {
        if (this._historyIndex >= this._history.length)
            return false;

        if (text)
            this._history[this._historyIndex] = text;
        this._historyIndex++;
        this._indexChanged();
        return true;
    },

    lastItem: function() {
        if (this._historyIndex != this._history.length) {
            this._historyIndex = this._history.length;
            this._indexChanged();
        }

        return this._historyIndex ? this._history[this._historyIndex -1] : null;
    },

    addItem: function(input) {
        if (this._history.length == 0 ||
            this._history[this._history.length - 1] != input) {

            this._history = this._history.filter(entry => entry != input);
            this._history.push(input);
            this._save();
        }
        this._historyIndex = this._history.length;
    },

    _indexChanged: function() {
        let current = this._history[this._historyIndex] || '';
        
        this._entry.set_text(current);
    },

    _save: function() {
        if (this._history.length > HISTORY_LIMIT)
            this._history.splice(0, this._history.length - HISTORY_LIMIT);
        
        if (this._remember)
            this.settings.set_strv('location-history', this._history);
    }
});


// Boxpointer.PopupAnimation
const PopupAnimation = {
    NONE:  0,
    SLIDE: 1 << 0,
    FADE:  1 << 1,
    FULL:  ~0
};

const SearchCompletionMenu = new Mang.Class({
    Name: UUID + '-SearchCompletionMenu',
    Extends: PopupMenu.PopupMenu,
    ParentConstrParams: [[0], 0, St.Side.TOP],
    
    _init: function(entry) {
        this.callParent('_init', entry, 0, St.Side.TOP);
        
        this._entry = entry;
        
        Main.layoutManager.overviewGroup.add_child(this.actor);
        this.actor.add_style_class_name('files-view-search-entry-completion-menu');
        this.actor.hide();
        
        this._mappedHandler = this._entry.connect('notify::mapped', () => {
            if (!this._entry.mapped)
                this.close();
        });
        
        this._entry.connect('destroy', () => this.destroy());
        
        // keep focus style on entry when completion menu is open
        this._entryPseudoClassHandler = this._entry.connect('notify::pseudo-class', () => {
            if (this.isOpen)
                this._entry.add_style_pseudo_class('focus');
        });
        
        let monitorHeight = Main.layoutManager.primaryMonitor.height;
        this.actor.set_style('max-height:' + Math.max(monitorHeight * 7/10, 650) + 'px;');
        
        // submenu provide scrollable menu and submenu item is hidden
        this._subMenuItem = new PopupMenu.PopupSubMenuMenuItem("", false);
        this._subMenuItem.menu.actor.remove_style_class_name('popup-sub-menu');
        this.addMenuItem(this._subMenuItem);
    },
    
    getFocus: function() {
        let direction = DirectionType.TAB_FORWARD;
        if (!this.actor.navigate_focus(null, direction, false))
            this.actor.grab_key_focus();
    },
    
    addCompletionAction: function(text, callback, needsIcon) {
        let item = this._subMenuItem.menu.addAction(text, callback);
        let itemActor = GS_VERSION < '3.33.0' ? item.actor : item;
        
        if (needsIcon) {
            let iconEnd = new St.Icon({ style_class: 'popup-menu-arrow', icon_name: 'pan-end-symbolic', x_align: Clutter.ActorAlign.END, x_expand: true, visible: false });
            itemActor.add_child(iconEnd);
            itemActor.add_style_class_name('has-pan-end');
            
            itemActor.connect('notify::pseudo-class', (actor) => {
                let children = actor.get_children();
                let lastChild = children.length && children[children.length - 1];
                if (lastChild instanceof St.Icon)
                    lastChild.visible = itemActor.pseudo_class && itemActor.pseudo_class.indexOf('focus') != -1;
            });
        }
        
        itemActor.connect('popup-menu', () => {
            this.close();
            this._entry.focusSearch();
        });
        
        itemActor.connect('key-press-event', (actor, event) => {
            if (event.type() != Clutter.EventType.KEY_PRESS)
                return Clutter.EVENT_PROPAGATE;
            
            if (event.get_key_symbol() == Clutter.KEY_Escape) {
                this.close();
                this._entry.focusSearch();
                return Clutter.EVENT_STOP;
            }
            
            else if (event.get_key_symbol() == Clutter.KEY_Left) {
                this._noAnimation = true;
                this.close();
                this._entry.reverseTab();
                this._noAnimation = false;
                if (this.isOpen)
                    this.getFocus();
                return Clutter.EVENT_STOP;
            }
            
            // 65421 is KP_ENTER key
            else if (event.get_key_symbol() == 65421) {
                item.activate(event);
                return Clutter.EVENT_STOP;
            }
            
            else if (event.get_key_symbol() == Clutter.KEY_Right) {
                this._noAnimation = true;
                item.activate(event);
                this._entry.doTab();
                this._noAnimation = false;
                if (this.isOpen)
                    this.getFocus();
                return Clutter.EVENT_STOP;
            }
            
            return Clutter.EVENT_PROPAGATE;
        });
        
        item._updateAdjustment = FileIcon.MenuItem.prototype._updateAdjustment;
        item.itemActor = itemActor;
        itemActor.connect('key-focus-in', () => item._updateAdjustment());
    },
    
    removeAllCompletionActions: function() {
        this._subMenuItem.menu.removeAll();
    },
    
    add3DotsItem: function() {
        let item = new PopupMenu.PopupMenuItem("…", { hover: false, activate: false });
        this._subMenuItem.menu.addMenuItem(item);
        let itemActor = GS_VERSION < '3.33.0' ? item.actor : item;
        
        itemActor.connect('popup-menu', () => {
            this.close();
            this._entry.focusSearch();
        });
        
        itemActor.connect('key-press-event', (actor, event) => {
            if (event.type() == Clutter.EventType.KEY_PRESS && event.get_key_symbol() == Clutter.KEY_Escape) {
                this.close();
                this._entry.focusSearch();
                return Clutter.EVENT_STOP;
            }
            return Clutter.EVENT_PROPAGATE;
        });
        
        item._updateAdjustment = FileIcon.MenuItem.prototype._updateAdjustment;
        item.itemActor = itemActor;
        itemActor.connect('key-focus-in', () => item._updateAdjustment());
    },
    
    open: function() {
        this._subMenuItem.menu.open();
        let subMenuItemActor = GS_VERSION < '3.33.0' ? this._subMenuItem.actor : this._subMenuItem;
        subMenuItemActor.hide();
        
        // save this._entry.clutter_text.x because sometimes it is 0 (when clutter_text.x is queried before clutter_text finish displaying)
        if (this._entry.clutter_text.x !== 0)
            this._entrySizes = [this._entry.clutter_text.x, this._entry.width];
        let [clutterTextX, entryWidth] = this._entrySizes ? this._entrySizes : [this._entry.clutter_text.x, this._entry.width];
        let [success, cursorX, cursorY_, lineHeight_] = this._entry.clutter_text.position_to_coords(-1);
        if (success)
            this._entry._completionMenu.setSourceAlignment(Math.min((cursorX + clutterTextX) / entryWidth, 0.9));
        this.callParent('open', this._noAnimation ? PopupAnimation.NONE : PopupAnimation.FULL);
    },
    
    close: function() {
        this.callParent('close', this._noAnimation ? PopupAnimation.NONE : PopupAnimation.FULL);
        this._subMenuItem.menu.close();
    },
    
    destroy: function() {
        this._entry.disconnect(this._mappedHandler);
        this._entry.disconnect(this._entryPseudoClassHandler);
        this.callParent('destroy');
    }
});



